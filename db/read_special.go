package db

//	получить все зонкосты из доставки по цене
func (db *DataBase) SelectDeliverySpecial(delivery *Delivery, countryID uint) (error, Zone, bool) {

	err := db.HandleDB.
		Preload("Zones").
		Preload("Zones.Countries").
		Preload("Zones.ZoneCosts").
		First(delivery).
		Error

	var index = -1
	//var countryID = uint(21)
	//	перебираем все зоны
	for zoneIndex, zone := range delivery.Zones {
		for _, country := range zone.Countries {
			if country.ID == countryID {
				index = zoneIndex
			}
		}
	}

	if index != -1 {
		//zoneExt.ID = uint(index)
		//err = db.HandleDB.
		//	Preload("ZoneCosts").
		//	First(zoneExt).
		//	Error
	}

	//log.Warnf("\nzoneCosts: %v\n", err)
	return err, delivery.Zones[index], delivery.WeightType
}
