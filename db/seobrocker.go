package db

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/ikeikeikeike/go-sitemap-generator/stm"
	log "github.com/sirupsen/logrus"
)

//	SERVICES

// 	функция для обновления sitemap(Product/Category)
func (db *DataBase) SeoProductUpdater() {
	// 	создаем переменные
	var products []Product

	// 	дергаем функцию выборки для продуктов

	if err := db.SelectAllItems(&products); err != nil { //	проверяем, что не вернули ошибку
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		return
	}

	//далее создаём сайтмап.
	sm := stm.NewSitemap(0)

	sm.SetDefaultHost("https://tarkov.grey-line.com")

	//устанавливаем путь для сохранения сайтмапов
	sm.SetSitemapsPath("/usr/local/share/www")
	sm.SetPublicPath("")
	sm.SetSitemapsHost("http://s3.amazonaws.com/sitemap-generator/")
	//компоновать или нет
	sm.SetFilename("sitemap-product")
	sm.SetCompress(true)
	sm.Create()
	//заносим в сео сайтмап категории
	for _, product := range products {
		if product.SeoLink != "" {
			sm.Add(stm.URL{{"loc", "/" + product.SeoLink}, {"lastmod", product.UpdatedAt}, {"priority", 0.5}})
		}
	}

	//сбрасываем паблик патч до нуля

	sm.Finalize()
	log.Info("Seo Sitemap update success!")
	seoSiteMapUpdater(0)
}

func (db *DataBase) SeoCategoryUpdater() {
	// 	создаем переменные
	var categories []Category

	// 	дергаем функцию выборки для категорий
	if err := db.SelectAllItems(&categories); err != nil { //	проверяем, что не вернули ошибку
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		return
	}

	//далее создаём сайтмап.
	sm := stm.NewSitemap(0)

	sm.SetDefaultHost("https://tarkov.grey-line.com")
	//компоновать или нет
	sm.SetFilename("sitemap-category")
	sm.SetCompress(true)
	//сбрасываем паблик патч до нуля
	sm.SetPublicPath("")
	//устанавливаем путь для сохранения сайтмапов
	sm.SetSitemapsPath("/usr/local/share/www")

	sm.SetSitemapsHost("http://s3.amazonaws.com/sitemap-generator/")

	sm.Create()
	//заносим в сео сайтмап категории
	for _, category := range categories {
		if category.SeoLink != "" {
			sm.Add(stm.URL{{"loc", "/" + category.SeoLink}, {"lastmod", category.UpdatedAt}, {"priority", 0.5}})
		}
	}

	sm.Finalize()
	log.Info("Seo Sitemap update success!")
	seoSiteMapUpdater(1)
}

type Sitemapindex struct {
	XMLName xml.Name `xml:"sitemapindex"`
	Text    string   `xml:",chardata"`
	Xmlns   string   `xml:"xmlns,attr"`
	Sitemap []struct {
		Text    string `xml:",chardata"`
		Loc     string `xml:"loc"`
		Lastmod string `xml:"lastmod"`
	} `xml:"sitemap"`
}

func seoSiteMapUpdater(flag int) {

	data, _ := ioutil.ReadFile("/usr/local/share/www/sitemap.xml")
	var categoryTime string
	var productTime string
	note := &Sitemapindex{}
	_ = xml.Unmarshal([]byte(data), &note)
	if len(note.Sitemap) > 0 {
		categoryTime = note.Sitemap[0].Lastmod
		productTime = note.Sitemap[1].Lastmod
	} else {
		productTime = time.Now().String()
		categoryTime = time.Now().String()
	}

	log.Errorf("-->>>>>>>> %v", note.Sitemap)
	// 	дергаем функцию выборки для продуктов
	f, err := os.Create("/usr/local/share/www/sitemap.xml")
	if err != nil {
		fmt.Println(err)
		return
	}
	site := "https://tarkov.grey-line.com"

	//если флаг == 1, обновляем время категории.
	//если флаг == 0, обновляем время продукта.
	if flag == 1 {
		categoryTime = time.Now().String()
	} else {
		productTime = time.Now().String()
	}

	l, err := f.WriteString(`<?xml version="1.0" encoding="UTF-8"?>
	<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
	   <loc>` + site + `/sitemap-category.xml.gz</loc>
	   <lastmod>` + categoryTime + `</lastmod>
	</sitemap>
	<sitemap>
	   <loc>` + site + `/sitemap-product.xml.gz</loc>
	   <lastmod>` + productTime + `</lastmod>
	</sitemap>
	</sitemapindex>`)
	if err != nil {
		fmt.Println(err, l)
		f.Close()
		return
	}
	//устанавливаем путь для сохранения сайтмапов

}
