package db

import (
	"errors"
	"reflect"

	log "github.com/sirupsen/logrus"
)

// 	DeleteItem - удаление записи
func (db *DataBase) DeleteItem(item interface{}) error {
	// 	разное поведение для разных структур
	switch item.(type) {
	//	*******************************************************************

	// 	Address
	case *Address:
		// тут структура для данных
		address := item.(*Address)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", address.ID).Delete(Address{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *address)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *address ID=%v", (*address).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Assembly
	case *Assembly:
		// тут структура для данных
		assembly := item.(*Assembly)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", assembly.ID).Delete(Assembly{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *assembly)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *assembly ID=%v", (*assembly).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Brand
	case *Brand:
		//	тут структура для данных
		brand := item.(*Brand)
		image := &brand.Image
		locales := brand.LocalizedDescriptions
		// 	пробуем удалить
		//	создаем транзакцию
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *brand)
			return err
		} else {
			// пробую удалить локаль для бренда
			if err = tx.Model(&brand).Association("LocalizedDescriptions").Delete(locales).Error; err != nil {
				log.Errorf("Не удалось удалить локаль %+v", locales)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
				}
				return err
			} else {
				log.Errorf("Локаль удалена успешно %+v", locales)
				// пробуем удалить картинку для бренда
				if err := tx.Model(&brand).Association("Image").Delete(image).Error; err != nil {
					log.Errorf("Не удалось удалить картинку %v", image)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
					} else {
						log.Errorf("Произошел откат при попытке удаленния локали при удалении объекта %+v", locales)
					}
					return err
				} else {
					log.Errorf("Картинка для бренда удалена успешно %+v", image)
					//удаляем брэнд, игнорируя мануфекчур
					if err := tx.Model(&brand).Omit("Manufacturer").Where("id = ?", brand.ID).Delete(Brand{}).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при удалении объекта: %v", *brand)
						//	откат
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
						}
						return err
					} else {
						//	отправляем транзакцию что бы создать брэнд
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при удалении объекта: %v", *brand)
							return err
						} else {
							log.Warnf("Попытка удаления объекта: *brand ID=%v", (*brand).ID)
							db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

	// 	Cart
	case *Cart:
		//	тут структура для данных
		cart := item.(*Cart)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", cart.ID).Delete(Cart{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *cart)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *cart ID=%v", (*cart).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Category
	case *Category:
		// тут структура для данных
		category := item.(*Category)
		locales := category.LocalizedDescriptions
		// 	пробуем удалить
		//	создаем транзакцию
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *category)
			return err
		} else {
			// пробую удалить локаль
			if err = tx.Model(&category).Association("LocalizedDescriptions").Delete(locales).Error; err != nil {
				log.Errorf("Не удалось удалить локаль %+v", locales)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
				}
				return err
			} else {
				log.Errorf("Локаль удалена успешно %+v", locales)
				if err := tx.Where("id = ?", category.ID).Delete(Category{}).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при удалении объекта: %v", *category)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
					}
					return err
				} else {
					//	отправляем транзакцию
					if err = tx.Commit().Error; err != nil {
						log.Errorf("Ошибка в транзакции при удалении объекта: %v", *category)
						return err
					} else {
						log.Warnf("Попытка удаления объекта *category ID=%v", (*category).ID)
						db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
						return nil
					}
				}
			}
		}

	// 	Country
	case *Country:
		// тут структура для данных
		country := item.(*Country)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", country.ID).Delete(Country{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *country)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *country ID=%v", (*country).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Currency
	case *Currency:
		// тут структура для данных
		currency := item.(*Currency)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", currency.ID).Delete(Currency{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *currency)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *currency ID=%v", (*currency).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Customer
	case *Customer:
		// тут структура для данных
		customer := item.(*Customer)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", customer.ID).Delete(Customer{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *customer)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *customer ID=%v", (*customer).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Customer rank
	case *CustomerRank:
		// тут структура для данных
		customerRank := item.(*CustomerRank)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", customerRank.ID).Delete(CustomerRank{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *customerRank)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *customerRank ID=%v", (*customerRank).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Delivery
	case *Delivery:
		// тут структура для данных
		delivery := item.(*Delivery)
		image := &delivery.Image
		locales := delivery.LocalizedDescriptions
		// 	пробуем удалить
		//	создаем транзакцию
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *delivery)
			return err
		} else {
			// пробую удалить локаль
			if err = tx.Model(&delivery).Association("LocalizedDescriptions").Delete(locales).Error; err != nil {
				log.Errorf("Не удалось удалить локаль %+v", locales)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
				}
				return err
			} else {
				log.Errorf("Локаль удалена успешно %+v", locales)
				// пробуем удалить картинку
				if err := tx.Model(&delivery).Association("Image").Delete(image).Error; err != nil {
					log.Errorf("Не удалось удалить картинку %v", image)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
					} else {
						log.Errorf("Произошел откат при попытке удаленния локали при удалении объекта %+v", locales)
					}
					return err
				} else {
					log.Errorf("Картинка удалена успешно %+v", image)
					if err := tx.Where("id = ?", delivery.ID).Delete(Delivery{}).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при удалении объекта: %v", *delivery)
						//	откат
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
						}
						return err
					} else {
						//	отправляем транзакцию
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при удалении объекта: %v", *delivery)
							return err
						} else {
							log.Warnf("Попытка удаления объекта *delivery ID=%v", (*delivery).ID)
							db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

	// 	Incoming
	case *Incoming:
		// тут структура для данных
		incoming := item.(*Incoming)
		// 	пробуем удалить
		image := &incoming.Scan
		incomingItems := incoming.IncomingItems
		// 	пробуем удалить
		//	создаем транзакцию
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *incoming)
			return err
		} else {
			if tx.Model(&incoming).Association("IncomingItems").Count() > 0 {
				// пробую удалить IncomingItems
				if err = tx.Model(&incoming).Association("IncomingItems").Delete(incomingItems).Error; err != nil {
					log.Errorf("Не удалось удалить incomingItems %+v", incomingItems)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении incomingItems %+v", incomingItems)
					}
					return err
				}
			}
			log.Errorf("IncomingItems удалена успешно %+v", incomingItems)
			// пробуем удалить incoming
			if err := tx.Model(&incoming).Association("Scan").Delete(image).Error; err != nil {
				log.Errorf("Не удалось удалить картинку %v", image)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить IncomingItems при удалении incomingItems %+v", incomingItems)
				} else {
					log.Errorf("Произошел откат при попытке удаленния incomingItems при удалении объекта %+v", incomingItems)
				}
				return err
			} else {
				log.Errorf("Картинка удалена успешно %+v", image)
				if err := tx.Where("id = ?", incoming.ID).Delete(Incoming{}).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при удалении объекта: %v", *incoming)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении incomingItems %+v", incomingItems)
					}
					return err
				} else {
					//	отправляем транзакцию
					if err = tx.Commit().Error; err != nil {
						log.Errorf("Ошибка в транзакции при удалении объекта: %v", *incoming)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить incoming при удалении incoming %+v", incoming)
							return err
						}
						return err
					} else {
						log.Warnf("Попытка удаления объекта *incoming ID=%v", (*incoming).ID)
						db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
						return nil
					}
				}
			}
			return nil
		}

	// 	Item
	case *Item:
		// тут структура для данных
		deletedItem := item.(*Item)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", deletedItem.ID).Delete(Item{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *deletedItem)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *deletedItem ID=%v", (*deletedItem).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Item shadow
	case *ItemShadow:
		// тут структура для данных
		itemShadow := item.(*ItemShadow)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", itemShadow.ID).Delete(ItemShadow{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *itemShadow)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *itemShadow ID=%v", (*itemShadow).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Language
	case *Language:
		// тут структура для данных
		language := item.(*Language)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", language.ID).Delete(Language{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *language)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *language ID=%v", (*language).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// Localization
	case *Localization:
		// тут структура для данных
		localization := item.(*Localization)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", localization.ID).Delete(Localization{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *localization)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *localization ID=%v", (*localization).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Log
	case *Log:
		// тут структура для данных
		crudLog := item.(*Log)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", crudLog.ID).Delete(Log{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *crudLog)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *crudLog ID=%v", (*crudLog).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Manufacturer
	case *Manufacturer:
		// тут структура для данных
		manufacturer := item.(*Manufacturer)
		// 	пробуем удалить
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *manufacturer)
			return err
		} else {
			// удаляем manufacturer, принудительно запрещая трогать Brands
			if err := tx.Model(&manufacturer).Association("Brands").Clear().Error; err != nil {
				//	логи
				log.Errorf("Ошибка при удалении ассоциации с Brands: %v", *manufacturer)
				return err
			} else {
				if err := tx.Model(&manufacturer).Omit("Brands").Where("id = ?", manufacturer.ID).Delete(Manufacturer{}).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при удалении объекта: %v", *manufacturer)
					return err
				} else {
					// пытаемся внести изменения в базу
					if err = tx.Commit().Error; err != nil {
						log.Errorf("Ошибка в транзакции при удалении объекта: %v", *manufacturer)
						return err
					} else {
						log.Warnf("Попытка удаления объекта *manufacturer ID=%v", (*manufacturer).ID)
						db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
						return nil
					}
				}
			}
		}
	// 	Option
	case *Option:
		// тут структура для данных
		option := item.(*Option)
		optionvals := option.OptionValues
		locales := option.LocalizedDescriptions
		// 	пробуем удалить
		//	создаем транзакцию
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *option)
			return err
		} else {
			// пробую удалить локаль
			if err = tx.Model(&option).Association("LocalizedDescriptions").Delete(locales).Error; err != nil {
				log.Errorf("Не удалось удалить локаль %+v", locales)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
				}
				return err
			} else {
				log.Errorf("Локаль удалена успешно %+v", locales)
				// пробуем удалить картинку
				if err = tx.Model(&option).Association("OptionValues").Delete(optionvals).Error; err != nil {
					log.Errorf("Не удалось удалить optionvals %+v", optionvals)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить optionvals при удалении optionvals %+v", optionvals)
					}
					return err
				} else {
					log.Errorf("optionvals удалена успешно %+v", optionvals)
					if err := tx.Where("id = ?", option.ID).Delete(Option{}).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при удалении объекта: %v", *option)
						//	откат
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
						}
						return err
					} else {
						//	отправляем транзакцию
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при удалении объекта: %v", *option)
							return err
						} else {
							log.Warnf("Попытка удаления объекта *option ID=%v", (*option).ID)
							db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

	// 	OptionValue
	case *OptionValue:
		// тут структура для данных
		optionValue := item.(*OptionValue)
		image := optionValue.Image
		locales := optionValue.LocalizedDescriptions
		// 	пробуем удалить
		//	создаем транзакцию
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *optionValue)
			return err
		} else {
			// пробую удалить локаль
			if err = tx.Model(&optionValue).Association("LocalizedDescriptions").Delete(locales).Error; err != nil {
				log.Errorf("Не удалось удалить локаль %+v", locales)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
				}
				return err
			} else {
				log.Errorf("Локаль удалена успешно %+v", locales)
				// пробуем удалить картинку
				if err := tx.Model(&optionValue).Association("Image").Delete(&image).Error; err != nil {
					log.Errorf("Не удалось удалить картинку %v", image)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
					} else {
						log.Errorf("Произошел откат при попытке удаленния локали при удалении объекта %+v", locales)
					}
					return err
				} else {
					log.Errorf("Картинка удалена успешно %+v", image)
					// удаляем optionValue
					if err := tx.Where("id = ?", optionValue.ID).Delete(OptionValue{}).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при удалении объекта: %v", *optionValue)
						//	откат
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
						}
						return err
					} else {
						//	отправляем транзакцию в базу
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при удалении объекта: %v", *optionValue)
							return err
						} else {
							log.Warnf("Попытка удаления объекта *optionValue ID=%v", (*optionValue).ID)
							db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

	// 	Options set
	case *OptionsSet:
		// тут структура для данных
		optionsSet := item.(*OptionsSet)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", optionsSet.ID).Delete(OptionsSet{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *optionsSet)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *optionsSet ID=%v", (*optionsSet).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Order
	case *Order:
		// тут структура для данных
		order := item.(*Order)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", order.ID).Delete(Order{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *order)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *order ID=%v", (*order).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Payment
	case *Payment:
		// тут структура для данных
		payment := item.(*Payment)
		image := &payment.Image
		locales := payment.LocalizedDescriptions
		// 	пробуем удалить
		//	создаем транзакцию
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *payment)
			return err
		} else {
			// пробую удалить локаль
			if err = tx.Model(&payment).Association("LocalizedDescriptions").Delete(locales).Error; err != nil {
				log.Errorf("Не удалось удалить локаль %+v", locales)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
				}
				return err
			} else {
				log.Errorf("Локаль удалена успешно %+v", locales)
				// пробуем удалить картинку
				if err := tx.Model(&payment).Association("Image").Delete(image).Error; err != nil {
					log.Errorf("Не удалось удалить картинку %v", image)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
					} else {
						log.Errorf("Произошел откат при попытке удаленния локали при удалении объекта %+v", locales)
					}
					return err
				} else {
					log.Errorf("Картинка удалена успешно %+v", image)
					if err := tx.Where("id = ?", payment.ID).Delete(Payment{}).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при удалении объекта: %v", *payment)
						//	откат
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
						}
						return err
					} else {
						//	отправляем транзакцию
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при удалении объекта: %v", *payment)
							return err
						} else {
							log.Warnf("Попытка удаления объекта *payment ID=%v", (*payment).ID)
							db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

	// 	Product
	case *Product:
		// тут структура для данных
		product := item.(*Product)
		imageFront := &product.ImageFront
		imageBack := &product.ImageBack
		locales := product.LocalizedDescriptions
		materials := product.LocalizedMaterial
		features := product.LocalizedFeatures
		// 	пробуем удалить
		//	создаем транзакцию
		log.Info("Внутри функции удаления продукта.")
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *product)
			return err
		} else {
			// пробую удалить локаль
			if tx.Model(&product).Association("LocalizedDescriptions").Count() > 0 {
				if err = tx.Model(&product).Association("LocalizedDescriptions").Delete(&locales).Error; err != nil {
					log.Errorf("Не удалось удалить локаль %+v", locales)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
					}
					return err
				}
			}
			log.Errorf("Локаль удалена успешно %+v", locales)
			// пробую удалить материалы
			if tx.Model(&product).Association("LocalizedMaterial").Count() > 0 {
				if err = tx.Model(&product).Association("LocalizedMaterial").Delete(materials).Error; err != nil {
					log.Errorf("Не удалось удалить LocalizedMaterial %+v", materials)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить LocalizedMaterial при удалении локали %+v", materials)
					}
					return err
				}
			}
			log.Errorf("LocalizedMaterial удалена успешно %+v", materials)
			// пробую удалить Фичи
			if tx.Model(&product).Association("LocalizedFeatures").Count() > 0 {
				if err = tx.Model(&product).Association("LocalizedFeatures").Delete(features).Error; err != nil {
					log.Errorf("Не удалось удалить LocalizedFeatures %+v", features)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить LocalizedFeatures при удалении локали %+v", features)
					}
					return err
				}
			}
			log.Errorf("LocalizedFeatures удалена успешно %+v", features)
			// пробуем удалить картинку если она есть
			if product.ImageFront.ID != 0 {
				if err := tx.Model(&product).Association("ImageFront").Delete(imageFront).Error; err != nil {
					log.Errorf("Не удалось удалить картинку %v", imageFront)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить imageFront при удалении imageFront %+v", imageFront)
					} else {
						log.Errorf("Произошел откат при попытке удаленния imageFront при удалении объекта %+v", imageFront)
					}
					return err
				}
			}
			log.Errorf("Картинка imageFront удалена успешно %+v", imageFront)
			if product.ImageBack.ID != 0 {
				if err := tx.Model(&product).Association("ImageBack").Delete(imageBack).Error; err != nil {
					log.Errorf("Не удалось удалить картинку %v", imageBack)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить imageBack при удалении imageBack %+v", imageBack)
					} else {
						log.Errorf("Произошел откат при попытке удаленния imageBack при удалении объекта %+v", imageBack)
					}
					return err
				}
				log.Errorf("Картинка imageBack удалена успешно %+v", imageBack)
			}
			// сносим зависимость product.ProductOptions.OptionValues. Что бы при удалении продукта не пострадали OptionValues
			for _, productImage := range product.ProductImages {
				if err := tx.Model(&productImage).Association("Image").Clear().Error; err != nil {
					//	логи
					log.Errorf("Ошибка при удалении объекта product.ProductImages: %v", product.ProductImages)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", product.ProductImages)
					}
					return err
				}
				if err := tx.Model(&productImage).Association("OptionValues").Clear().Error; err != nil {
					//	логи
					log.Errorf("Ошибка при удалении объекта product.OptionValues: %v", product.ProductImages)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", product.ProductImages)
					}
					return err
				}
			}
			// сносим зависимость product.ProductOptions. Что бы при удалении продукта не пострадалb опции
			for _, productImage := range product.ProductOptions {
				log.Info("Бесконечный цикл? 2")
				if err := tx.Model(&productImage).Association("Option").Clear().Error; err != nil {
					//	логи
					log.Errorf("Ошибка при удалении объекта product.ProductOptions: %v", product.ProductImages)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", product.ProductImages)
					}
					return err
				}
				if err := tx.Model(&productImage).Association("OptionValues").Clear().Error; err != nil {
					//	логи
					log.Errorf("Ошибка при удалении объекта product.OptionValues: %v", product.ProductImages)
					//	откат
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при удалении локали %+v", product.ProductImages)
					}
					return err
				}
			}

			// Удаляем продукт по айди, предварительно запрещая gorm вносить изменения в продукт, категорию, брэнды.
			if err := tx.Model(Product{}).Omit("Categories", "Brand", "Manufacturer").Where("id = ?", product.ID).Delete(Product{}).Error; err != nil {
				//	логи
				// log.Errorf("Ошибка при удалении объекта: %v", *product)
				log.Errorf("Ошибка при удалении объекта: Product ")
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить локаль при удалении локали %+v", locales)
				}
				return err
			} else {
				// TODO на время тестов отключил вывод. консоль долго мотать. верну назад потом.
				// log.Warnf("Попытка удаления объекта: %v", *product)
				log.Warnf("Попытка удаления объекта: Product")

				//	отправляем транзакцию
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при удалении объекта: %v", *product)
					return err
				} else {
					log.Warnf("Попытка удаления объекта *product ID=%v", (*product).ID)
					db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
				}
			}
			return nil
		}

	// 	Product shadow
	case *ProductShadow:
		// тут структура для данных
		productShadow := item.(*ProductShadow)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", productShadow.ID).Delete(ProductShadow{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *productShadow)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *productShadow ID=%v", (*productShadow).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Purchase
	case *Purchase:
		// тут структура для данных
		purchase := item.(*Purchase)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", purchase.ID).Delete(Purchase{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *purchase)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *purchase ID=%v", (*purchase).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

		// 	Region
	case *Region:
		// тут структура для данных
		region := item.(*Region)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", region.ID).Delete(Region{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *region)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *region ID=%v", (*region).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

		// 	Role
	case *Role:
		// тут структура для данных
		role := item.(*Role)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", role.ID).Delete(Role{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *role)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *role ID=%v", (*role).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

		// 	User
	case *User:
		// тут структура для данных
		user := item.(*User)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", user.ID).Delete(User{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *user)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *user ID=%v", (*user).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

		// 	Zone
	case *Zone:
		// тут структура для данных
		zone := item.(*Zone)
		// 	пробуем удалить
		if err := db.HandleDB.Where("id = ?", zone.ID).Delete(Zone{}).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при удалении объекта: %v", *zone)
			return err
		} else {
			log.Warnf("Попытка удаления объекта *zone ID=%v", (*zone).ID)
			db.CreateLog(item, nil, "delete", "Guest") // запись успешного удаления в лог
			return nil
		}

	default:
		log.Errorf("Попытка удаления объекта неизвестного типа: %v", reflect.TypeOf(item))
		err := errors.New("Попытка удаления объекта неизвестного типа")
		return err

		// ****************************************************************
	}
}
