package db

import (
	"time"

	"github.com/jinzhu/gorm"
)

var schemaName string = "public"

type DataBase struct {
	HandleDB *gorm.DB
}

type UserGroup string
type Tags string

//// 	новость
//type News struct {
//	gorm.Model
//	// ID        uint       `json:"_id" db:"id" gorm:"primary_key auto_increment"`
//	// CreatedAt *time.Time `json:"date_create" db:"date_create"`
//	// DateRead  *time.Time `json:"date_read" db:"date_read" gorm:"default:null"`
//	Title       string `json:"Title" db:"title"`
//	Description string `json:"Description" db:"description"`
//	Text        string `json:"Text" db:"text"`
//	Author      string `json:"Author" db:"author"`
//	URL         string `json:"URL" db:"url"`
//	Image       string `json:"Image" db:"image"`
//	Tag         Tags   `json:"TAGS" db:"tags"`
//}

//	assembly
type Assembly struct {
	gorm.Model
	Order         Order  `json:"order" db:"order"`                  //	заказ
	DeliveryColor string `json:"deliveryColor" db:"delivery_color"` //	цвет доставки
	Weight        uint   `json:"weight" db:"weight"`                //	вес
}

//	address
type Address struct {
	gorm.Model
	City        string  `json:"city" db:"city"`               //	город
	Comments    string  `json:"comments" db:"comments"`       //	комментарий
	Description string  `json:"description" db:"description"` //	строка адреса
	Email       string  `json:"email" db:"email"`             //	почта
	FirstName   string  `json:"firstName" db:"first_name"`    //	имя
	LastName    string  `json:"lastName" db:"last_name"`      //	фамилия
	Country     Country `json:"country" db:"country"`         //	страна
	CountryID   uint    `json:"countryID" db:"country_id"`    //	ид страны
	Phone       string  `json:"phone" db:"phone"`             //	телефон
	Region      Region  `json:"region" db:"region"`           //	регион
	RegionID    uint    `json:"regionID" db:"region_id"`      //	ид регион
	Zip         string  `json:"zip" db:"zip"`                 //	zip
}

// 	brand
type Brand struct {
	gorm.Model
	// ID        uint       `json:"_id" db:"id" gorm:"primary_key auto_increment"`irritative
	// CreatedAt *time.Time `json:"date_create" db:"date_create"`
	// DateRead  *time.Time `json:"date_read" db:"date_read" gorm:"default:null"`
	LocalizedDescriptions []LocalizedDescription `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"` //	описание
	Image                 Image                  `json:"image" db:"image"`                                                           //	картинка
	ImageID               uint                   `json:"imageID" db:"image_id"`                                                      //	ид картинки
	Priority              uint                   `json:"priority" db:"priority" gorm:"default:5"`                                    //	приоритет
	Manufacturer          Manufacturer           `json:"manufacturer" db:"manufacturer" gorm:"foreignkey:OwnerID"`                   //	ид владельца
	//ManufacturerID        uint                   `json:"manufacturerID" db:"manufacturer_id"`
	SeoLink   string `json:"seoLink" db:"seo_link"`     //	сео ссылка
	OwnerID   uint   `json:"ownerID" db:"owner_id"`     //	ид владельца
	OwnerType string `json:"ownerType" db:"owner_type"` // тип владельца
	// ManufacturerID        uint                   `json:"manufacturer_id" db:"manufacturer_id"` //	ID родительской категории
}

//	корзина
type Cart struct {
	gorm.Model
	CartItems  []CartItem `json:"cartItems" db:"cart_items" gorm:"many2many:cart__cart_items"` //	список продуктов в корзине
	Order      Order      `json:"order" db:"order"`                                            //	линк на заказ, если есть
	OrderID    uint       `json:"orderID" db:"order_id"`                                       //	ИД заказа
	Delivery   Delivery   `json:"delivery" db:"delivery"`                                      //	линк на доставку, если есть
	DeliveryID uint       `json:"deliveryID" db:"delivery_id"`                                 //	ИД заказа
	Payment    Payment    `json:"payment" db:"payment"`                                        //	линк на оплату, если есть
	PaymentID  uint       `json:"paymentID" db:"payment_id"`                                   //	ИД заказа
	Address    Address    `json:"address" db:"address"`                                        //	линк на адрес, если есть
	AddressID  uint       `json:"addressID" db:"address_id"`                                   //	ИД заказа
	Customer   Customer   `json:"customer" db:"customer"`                                      //	линк на заказчика, если есть
	CustomerID uint       `json:"customerID" db:"customer_id"`                                 //	ИД заказчика
	Comment    string     `json:"comment" db:"comment"`                                        //	текстовое поле на всякий случай
}

//	корзина
type CartItem struct {
	gorm.Model
	Product        Product       `json:"products" db:"products"`                                                    //	продукт
	ProductID      uint          `json:"productID" db:"product_id"`                                                 //	продукт
	OrderCount     uint          `json:"orderCount" db:"order_count"`                                               //	количество
	AvailableCount uint          `json:"availableCount" db:"available_count"`                                       //	доступное количество
	OptionValues   []OptionValue `json:"optionValues" db:"option_values" gorm:"many2many:cart_item__option_values"` //	значения опций заказа

	//CartID         uint    `json:"cartID" db:"cart_id"`                 //	текстовое поле на всякий случай
}

// 	категория
type Category struct {
	gorm.Model
	LocalizedDescriptions []LocalizedDescription `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"` //	описание
	LocalizedSEOs         []LocalizedSEO         `json:"localizedSEOs" db:"localized_seos" gorm:"polymorphic:Owner"`                 //  SEO
	ImageBackground       Image                  `json:"imageBackground" db:"image_background"`                                      //  фон
	ImageBackgroundID     uint                   `json:"imageBackgroundID" db:"image_background_id"`                                 //  фон
	ImageIcon             Image                  `json:"imageIcon" db:"image_icon"`                                                  //	иконка
	ImageIconID           uint                   `json:"imageIconID" db:"image_icon_id"`                                             //  иконка
	Images                []Image                `json:"images" db:"images" gorm:"many2many:category_images"`                        //  картинки
	Categories            []Category             `json:"categories" db:"categories" gorm:"foreignkey:CategoryID"`                    //	потомки
	CategoryID            uint                   `json:"categoryID" db:"category_id"`                                                //	ID родительской категории
	Priority              uint                   `json:"priority" db:"priority"`                                                     //  priority
	IsEnabled             bool                   `json:"isEnabled" db:"is_enabled"`                                                  //  is enabled
	URL                   string                 `json:"url" db:"url"`                                                               //  url
	Products              []Product              `json:"products" db:"products" gorm:"many2many:categories__products"`               //	значения опций
	SeoLink               string                 `json:"seoLink" db:"seo_link"`
	Type                  string                 `json:"type" db:"type"` //  тип
}

//	country
type Country struct {
	gorm.Model
	Name    string   `json:"name" db:"name"`        //	имя
	NameRu  string   `json:"nameRU" db:"name_ru"`   //	имя на русском
	Code2   string   `json:"code2" db:"code2"`      //	двухзначный код
	Code3   string   `json:"code3" db:"code3"`      //	трехзначный код
	Regions []Region `json:"regions" db:"regions"`  //	список регионов
	Image   Image    `json:"image" db:"image"`      //	картинка
	ImageID uint     `json:"imageID" db:"image_id"` //	ид картинки
}

//	сountry для короткого возврата инфы
type CountryShort struct {
	ID     uint   `json:"ID"`     //	ид
	Name   string `json:"name"`   //	имя
	NameRu string `json:"nameRU"` //	имя на русском
}

// 	валюта
type Currency struct {
	gorm.Model
	Code       string  `json:"code" db:"code"`             //	код валюты
	Title      string  `json:"title" db:"title"`           //	заголовок валюты
	Multiplier float32 `json:"multiplier" db:"multiplier"` //	курс
	Symbol     string  `json:"symbol" db:"symbol"`         //	символ валюты
	Before     bool    `json:"before" db:"before"`         //	с какой стороны валюты
}

//	customer
type Customer struct {
	gorm.Model
	Name string `json:"name" db:"name"` // имя покупателя
}

//	customer rank
type CustomerRank struct {
	gorm.Model
	CustomerID uint `json:"customerID" db:"customer_id"` //	ид покупателя
}

//	delivery
type Delivery struct {
	gorm.Model
	LocalizedDescriptions []LocalizedDescription `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"` //	описание
	Image                 Image                  `json:"image" db:"image"`                                                           //	картинка
	ImageID               uint                   `json:"imageID" db:"image_id"`                                                      //	ID - картинка
	Zones                 []Zone                 `json:"zones" db:"zones"`                                                           //	Zone
	WeightType            bool                   `json:"weightType" db:"weight_type"`                                                // Regular or Volume
	Color                 string                 `json:"color" db:"color"`                                                           // color
	Enabled               bool                   `json:"enabled" db:"enabled"`                                                       // видимость
	Priority              uint                   `json:"priority" db:"priority"`                                                     // приоритеты
}

// 	image
type Image struct {
	gorm.Model
	Name      string       `json:"name" db:"name"`
	URL       string       `json:"url" db:"url"`
	Type      ImageType    `json:"type" db:"type"`
	TypeID    uint         `json:"typeID" db:"type_id"`
	Options   ImageOptions `json:"options" db:"options"`
	OptionsID uint         `json:"optionsID" db:"options_id"`
	Priority  uint         `json:"priority" db:"priority"`
}

// 	image
type ImageShort struct {
	ID        uint           `json:"ID"`
	Name      string         `json:"name" db:"name"`
	URL       string         `json:"url" db:"url"`
	Type      ImageTypeShort `json:"type" db:"type"`
	TypeID    uint           `json:"typeID" db:"type_id"`
	Options   ImageOptions   `json:"options" db:"options"`
	OptionsID uint           `json:"optionsID" db:"options_id"`
	Priority  uint           `json:"priority" db:"priority"`
}

// 	image for brand
type ImageOptions struct {
	ID   uint   `json:"ID"`
	Name string `json:"name" db:"name"` //	имя
}

// 	image for brand
type ImageType struct {
	gorm.Model
	Name string `json:"name" db:"name"` //	имя
	Path string `json:"path" db:"path"` //	путь
}

// 	image for brand
type ImageTypeShort struct {
	ID   uint   `json:"ID"`
	Name string `json:"name" db:"name"` //	имя
	Path string `json:"path" db:"path"` //	путь
}

//	incoming
type Incoming struct {
	gorm.Model
	Invoice       string         `json:"invoice" db:"invoice"`              //	номер накладной
	Comment       string         `json:"comment" db:"comment"`              //	коммент
	Scan          Image          `json:"scan" db:"scan"`                    //	скан накладной
	ScanID        uint           `json:"scanID" db:"scan_id"`               //	ид скана накладной
	IncomingItems []IncomingItem `json:"incomingItems" db:"incoming_items"` //	позиции накладной
	Checked       bool           `json:"checked" db:"checked"`              //	бит проверки
}

//	incoming
type IncomingItem struct {
	gorm.Model
	IncomingID   uint          `json:"incomingID" db:"incoming_id"`                                                   //	ид накладной
	OptionValues []OptionValue `json:"optionValues" db:"option_values" gorm:"many2many:incoming_item__option_values"` //	значения опций заказа
	Product      Product       `json:"product" db:"product"`                                                          //	продукт
	ProductID    uint          `json:"productId" db:"product_id"`                                                     //	ид продукта
	Amount       uint          `json:"amount" db:"amount"`                                                            //	количество
}

//	item
type Item struct {
	gorm.Model
	ItemID                string        // идентификатор для куаркода и склада
	Product               Product       `json:"product" db:"product"`                                                 //	продукт
	ProductID             uint          `json:"productId" db:"product_id"`                                            //	ид продукта
	Order                 Order         `json:"order" db:"order"`                                                     //	заказ
	OrderID               uint          `json:"orderID" db:"order_ID"`                                                //	заказ
	OptionValues          []OptionValue `json:"optionValues" db:"option_values" gorm:"many2many:item__option_values"` //	значения опций заказа
	Status                ItemStatus    `json:"status" db:"status"`                                                   //	статус заказа
	StatusID              uint          `json:"statusID" db:"status_id"`                                              //  количество пакетов в заказе
	DeliveryPackageNumber uint          //  в каком из пакетов заказа этот айтем
	//optionsSets: OptionsSet[]; //  optionsSet
	Placement          string
	WarehouseBoxNumber uint   //  номер коробки на складе
	Comments           string //  комментс
	Price              uint   //  цена продажи
	//price Price  //  цена продажи
	//currency: CurrencyShadow; //  валюта на момент покупки
	//assembled?: boolean;
	//checked?: boolean;
	Sale       Sale     //  скидка
	SaleID     uint     // id скидка
	Currency   Currency //  скидка
	CurrencyID uint     // id скидка
}

//	item status
type ItemStatus struct {
	gorm.Model
	Name string `json:"name" db:"name"` //	название
	//-needed (Заказ отправлен системой на сборку по складу)
	//-stock (заказ находится в коробке и ждет проверки)
	//-shipped (заказ отправлен)
	//-reserved (Заказ отменен менеджером/клиентом)
}

//	item shadow
type ItemShadow struct {
	gorm.Model
	Order     Order  `json:"order" db:"order" gorm:"foreignkey:OwnerID"` //	заказ
	OwnerID   uint   `json:"ownerID" db:"owner_id"`                      //	ид владльца
	OwnerType string `json:"ownerType" db:"owner_type"`                  //	тип владельца
}

// 	язык
type Language struct {
	gorm.Model
	Language string `json:"language" db:"language"` //	язык
	Title    string `json:"title" db:"title"`       //	заголовок
}

// тип данных для хранения перевода текста на разные языки
type Localization struct {
	gorm.Model
	Keyword     *string `json:"keyword" db:"keyword" gorm:"unique;not null"` // индификатор
	RU          string  `json:"ru" db:"ru"`                                  //	язык
	EN          string  `json:"en" db:"en"`                                  //	язык
	Description string  `json:"description" db:"description"`                //	описание
}

// тип данных для хранения перевода текста на разные языки
type Log struct {
	gorm.Model
	Type        string `json:"type" db:"type"`          // изменияемый тип
	Action      string `json:"action" db:"action"`      // вид действия
	ObjectID    uint   `json:"objectId" db:"object_id"` // айди изменяемого обьекта
	FuncPath    string `json:"funcPath" db:"func_path"`
	Json        string `json:"json" db:"json"`                // json обьекта до изменения
	JsonRequest string `json:"jsonRequest" db:"json_request"` // присланный json
}

// 	локализованное описание
type LocalizedDescription struct {
	gorm.Model
	Language         string `json:"language" db:"language"`                  //	язык
	Name             string `json:"name" db:"name"`                          //	имя
	Title            string `json:"title" db:"title"`                        //	заголовок
	Description      string `json:"description" db:"description"`            //	описание
	DescriptionTwo   string `json:"descriptionTwo" db:"description_two"`     //	описание
	DescriptionThree string `json:"descriptionThree" db:"description_three"` //	описание
	Keywords         string `json:"keywords" db:"keywords"`                  //	ключевые поля
	OwnerID          uint   `json:"ownerID" db:"owner_id"`                   //	ид владельца
	OwnerType        string `json:"ownerType" db:"owner_type"`               //	тип владельца
}

// 	локализованное описание
type LocalizedDescriptionShort struct {
	ID               uint   `json:"ID"`
	Language         string `json:"language"`         //	язык
	Name             string `json:"name"`             //	имя
	Title            string `json:"title"`            //	заголовок
	Description      string `json:"description"`      //	описание
	DescriptionTwo   string `json:"descriptionTwo"`   //	описание
	DescriptionThree string `json:"descriptionThree"` //	описание
	Keywords         string `json:"keywords"`         //	ключевые поля
}

// 	локализованные фичи
type LocalizedFeature struct {
	gorm.Model
	Language  string `json:"language" db:"language"`    //	язык
	Text      string `json:"text" db:"text"`            //	текст
	OwnerID   uint   `json:"ownerID" db:"owner_id"`     //	ид владельца
	OwnerType string `json:"ownerType" db:"owner_type"` //	тип владельца
}

// 	локализованные фичи
type LocalizedFeatureShort struct {
	ID       uint
	Language string `json:"language" db:"language"` //	язык
	Text     string `json:"text" db:"text"`         //	текст
}

// 	локализованные материалы
type LocalizedMaterial struct {
	gorm.Model
	Language  string `json:"language" db:"language"`    //	язык
	Text      string `json:"text" db:"text"`            //	текст
	OwnerID   uint   `json:"ownerID" db:"owner_id"`     //	ид владельца
	OwnerType string `json:"ownerType" db:"owner_type"` //	тип владельца
}

// 	локализованные материалы
type LocalizedMaterialShort struct {
	ID       uint
	Language string `json:"language" db:"language"` //	язык
	Text     string `json:"text" db:"text"`         //	текст
}

// 	локализованный текст - одно поле
type LocalizedText struct {
	gorm.Model
	Language  string `json:"language" db:"language"`    //	язык
	Text      string `json:"text" db:"text"`            //	текст
	OwnerID   uint   `json:"ownerID" db:"owner_id"`     //	ид владельца
	OwnerType string `json:"ownerType" db:"owner_type"` //	тип владельца
}

// 	локализованный текст - одно поле
type LocalizedTextShort struct {
	ID       uint
	Language string `json:"language" db:"language"` //	язык
	Text     string `json:"text" db:"text"`         //	текст
}

// 	локализованное Seo
type LocalizedSEO struct {
	gorm.Model
	Language    string `json:"language" db:"language"`       //	язык
	Name        string `json:"name" db:"name"`               //	имя
	Title       string `json:"title" db:"title"`             //	заголовок
	Keywords    string `json:"keywords" db:"keywords"`       //	ключевые поля
	Description string `json:"description" db:"description"` //	описание
	OwnerID     uint   `json:"ownerID" db:"owner_id"`        //	ид владельца
	OwnerType   string `json:"ownerType" db:"owner_type"`    //	тип владельца
}

// 	локализованное Seo
type LocalizedSEOShort struct {
	ID          uint   `json:"ID"`
	Language    string `json:"language" db:"language"`       //	язык
	Name        string `json:"name" db:"name"`               //	имя
	Title       string `json:"title" db:"title"`             //	заголовок
	Keywords    string `json:"keywords" db:"keywords"`       //	ключевые поля
	Description string `json:"description" db:"description"` //	описание
}

//	производитель
type Manufacturer struct {
	gorm.Model
	Name     string `json:"name" db:"name"`         //	имя
	Cent     uint32 `json:"cent" db:"cent"`         //	процент ?
	Color    string `json:"color" db:"color"`       //	цвет
	Contacts string `json:"contacts" db:"contacts"` //	контакты
	Site     string `json:"site" db:"site"`         //	сайт
	Email    string `json:"email" db:"email"`       //	почта
	Vk       string `json:"vk" db:"vk"`             //	страница вк
	Phone    string `json:"phone" db:"phone"`       //	телефон
	Bank     string `json:"bank" db:"bank"`         //	реквизиты банка
	Contract string `json:"contract" db:"contract"` //	информация по контракту
	Discount string `json:"discount" db:"discount"` //	информация о скидке
	Address  string `json:"address" db:"address"`   //	адрес
	Delivery string `json:"delivery" db:"delivery"` //	особенности доствка
	Comment  string `json:"comment" db:"comment"`   //	коммент
	//Priority    uint       `json:"priority" db:"priority"`
	Brands []Brand `json:"brands" db:"brands" gorm:"polymorphic:Owner"` //  список брендов
}

// 	меню	админки
type Menu struct {
	gorm.Model
	Icon        string     `json:"icon" db:"icon"`                //	иконка
	Name        string     `json:"name" db:"name"`                //	название
	Title       string     `json:"title" db:"title"`              //	заголовок
	Url         string     `json:"url" db:"url"`                  //	линк
	Priority    uint       `json:"priority" db:"priority"`        //	приоритет
	IsFavourite bool       `json:"isFavourite" db:"is_favourite"` //	избранные
	Items       []Currency `json:"items" db:"items"`              //	значения меню
}

// 	значения опций
type Option struct {
	gorm.Model
	Priority              uint                   `json:"priority" db:"priority" gorm:"default:5"`                                                                                                                                                         //	приоритет
	OptionType            OptionType             `json:"type" db:"type"`                                                                                                                                                                                  //	тип опции
	OptionTypeID          uint                   `json:"optionTypeID" db:"option_type_id"`                                                                                                                                                                //	идентификатор типа опции
	Enabled               bool                   `json:"enabled" db:"enabled"`                                                                                                                                                                            //	опция включена
	LocalizedDescriptions []LocalizedDescription `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"`                                                                                                                      //  описание
	OptionValues          []OptionValue          `json:"optionValues" db:"option_values" gorm:"many2many:option__option_values;foreignkey:ID;association_foreignkey:ID;association_jointable_foreignkey:option_value_id;jointable_foreignkey:option_id;"` //	значения опций
}

// 	значения типов опций
type OptionType struct {
	gorm.Model
	Name string `json:"name" db:"name"`
}

// 	значения опций
type OptionValue struct {
	gorm.Model
	Priority              uint                   `json:"priority" db:"priority" gorm:"default:5"`                                                                                                                                            //	приоритет
	Image                 Image                  `json:"image" db:"image"`                                                                                                                                                                   //	картинка
	ImageID               uint                   `json:"imageID" db:"image_id"`                                                                                                                                                              //	ид картинки
	Us                    string                 `json:"us" db:"us"`                                                                                                                                                                         //	американский размер
	Int                   string                 `json:"int" db:"int"`                                                                                                                                                                       //	европейский
	Ru                    string                 `json:"ru" db:"ru"`                                                                                                                                                                         //	российский
	Inch                  string                 `json:"inch" db:"inch"`                                                                                                                                                                     //	в дюймах
	Cent                  string                 `json:"cent" db:"cent"`                                                                                                                                                                     //	в сантиметрах
	LocalizedDescriptions []LocalizedDescription `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"`                                                                                                         //	описание
	Enabled               bool                   `json:"enabled" db:"enabled"`                                                                                                                                                               //	включена или нет
	Option                Option                 `json:"option" db:"option" gorm:"many2many:option__option_values;foreignkey:ID;association_foreignkey:ID;association_jointable_foreignkey:option_id;jointable_foreignkey:option_value_id;"` //	значения опций                                                         // опция
}

//	options set
type OptionsSet struct {
	gorm.Model
	OptionValues      []OptionValue `json:"optionValues" db:"option_values" gorm:"many2many:option_set__option_values;foreignkey:ID;association_foreignkey:ID;association_jointable_foreignkey:option_id;jointable_foreignkey:option_value_id;"` //	значения опции
	Price             ProductPrice  `json:"price" db:"price"`                                                                                                                                                                                    //  цены
	PriceID           uint          `json:"priceID" db:"price_id"`                                                                                                                                                                               //  ID цены
	Weight            uint          `json:"weight" db:"weight"`                                                                                                                                                                                  //  вес
	VolumeWeightX     uint          `json:"volumeWeightX" db:"volume_weight_x"`                                                                                                                                                                  //  объемный вес X
	VolumeWeightY     uint          `json:"volumeWeightY" db:"volume_weight_y"`                                                                                                                                                                  //  объемный вес Y
	VolumeWeightZ     uint          `json:"volumeWeightZ" db:"volume_weight_z"`                                                                                                                                                                  //  объемный вес Z
	WarehousePosition string        `json:"whPosition" db:"wh_position"`                                                                                                                                                                         //  позиция на складе
	Article           string        `json:"article" db:"article"`                                                                                                                                                                                //  артикул производителя
	Enabled           bool          `json:"enabled" db:"enabled"`                                                                                                                                                                                //  включение / выключение комбинации сет
}

//	order
type Order struct {
	gorm.Model
	UpdatedStatusAt          time.Time     `json:"updatedStatusAt" db:"updated_status_at"`                       //  дата последней правки человеком
	OrderID                  string        `json:"orderID" db:"order_id"`                                        //  Идентификатор для людей
	CartItems                []CartItem    `json:"cartItems" db:"cart_items" gorm:"many2many:order__cart_items"` //	список продуктов в корзине
	Priority                 OrderPriority `json:"priority" db:"priority"`                                       //	приоритет
	PriorityID               uint          `json:"priorityID" db:"priority_id"`                                  //	ид приоритета
	Status                   OrderStatus   `json:"status" db:"status"`                                           //	статус заказа
	StatusID                 uint          `json:"statusID" db:"status_id"`                                      //  количество пакетов в заказе
	Items                    []Item        `json:"items" db:"items" gorm:"foreignkey:OrderID"`                   //  список айтемов
	Customer                 Customer      `json:"customer" db:"customer"`                                       //  покупатель
	CustomerID               uint          `json:"customerID" db:"customer_id"`                                  //  ID покупателя
	CreatedShippingAddress   Address       `json:"createdShippingAddress" db:"created_shipping_address"`         //  изначальный адрес доставки (взять оттуда поле комментария пользователя)
	CreatedShippingAddressID uint          `json:"createdShippingAddressID" db:"created_shipping_address_id"`    //  ID - изначальный адрес доставки (взять оттуда поле комментария пользователя)
	CurrentShippingAddress   Address       `json:"currentShippingAddress" db:"current_shipping_address"`         //  измененный адрес доставки
	CurrentShippingAddressID uint          `json:"currentShippingAddressID" db:"current_shipping_address_id"`    //  ID - измененный адрес доставки
	Comments                 string        `json:"comments" db:"comments"`                                       //  комментарий
	Delivery                 Delivery      `json:"delivery" db:"delivery"`                                       //  доставка
	DeliveryID               uint          `json:"deliveryID" db:"delivery_id"`                                  // ID статус заказа
	DeliveryPackageNumber    uint          `json:"deliveryPackageNumber" db:"delivery_package_number"`           //  номер пакетов в заказе
	DeliveryPackageCount     uint          `json:"deliveryPackageCount" db:"delivery_package_count"`             //  ID доставка
	Weight                   float32       `json:"weight" db:"weight"`                                           //  вес
	WarehouseBoxNumber       uint          `json:"warehouseBoxNumber" db:"warehouse_box_number"`                 //	номер коробки на складе                            //  номер коробки на складе
	Payment                  Payment       `json:"payment" db:"payment"`                                         //  оплата
	PaymentID                uint          `json:"paymentID" db:"payment_id"`                                    //  ID оплата
	PaymentURL               string        `json:"paymentURL" db:"payment_url"`                                  //  урл страницы оплаты
	//  history
	ItemsShadow             []ItemShadow `json:"itemsShadow" db:"items_shadow"  gorm:"polymorphic:Owner"` //  список покупок при создании
	ShippingAddressShadow   Address      `json:"shippingAddressShadow" db:"shipping_address_shadow"`      //  измененный адрес доставки
	ShippingAddressShadowID uint         `json:"shippingAddressShadowID" db:"shipping_address_shadow_id"` // ID - измененный адрес доставки
	TrackingNumber          string       `json:"trackingNumber" db:"tracking_number"`                     //  номер отслеживания
	Sale                    Sale         `json:"sale" db:"sale"`                                          //  скидка
	SaleID                  uint         `json:"saleID" db:"sale_id"`                                     //  скидка
	ProductsCost            uint         `json:"productsCost" db:"products_cost"`                         //	итоговая цена
	DeliveryCost            uint         `json:"deliveryCost" db:"delivery_cost"`                         //	итоговая цена
	TotalCost               uint         `json:"totalCost" db:"total_cost"`                               //	итоговая цена
	Currency                Currency     `json:"currency" db:"currency"`                                  //	валюта
	CurrencyID              uint         `json:"currencyID" db:"currency_id"`                             //	ид валюты
}

//	order priority
type OrderPriority struct {
	gorm.Model
	Name string `json:"name" db:"name"` //	название
	//-приостановленный
	//-обычный
	//-ускоренный
}

//	order status
type OrderStatus struct {
	gorm.Model
	Name string `json:"name" db:"name"` //	название
	//-Failed (автооплата не прошла)
	//Payment check (ожидает проверки оплаты)
	//-Comment Check (ожидает проверки комментария)
	//-Waiting (недостает вещей в наличии и заказ ждет)
	//-Assembly (Заказ отправлен системой на сборку по складу)
	//-In Box (заказ находится в коробке и ждет проверки)
	//-Shipped (заказ отправлен)
	//-canceled (Заказ отменен менеджером/клиентом)
}

//	payment
type Payment struct {
	gorm.Model
	LocalizedDescriptions []LocalizedDescription `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"` //	описание
	Image                 Image                  `json:"image" db:"image"`                                                           //	оплата
	ImageID               uint                   `json:"imageID" db:"image_id"`                                                      //	ид картинки оплаты
	Discounts             []PaymentDiscount      `json:"discounts" db:"discounts"`                                                   //	скидки в магазинах
	Priority              uint                   `json:"priority" db:"priority"`                                                     // приоритеты
}

//	payment
type PaymentDiscount struct {
	gorm.Model
	Shop      Shop `json:"shop" db:"shop"`            //	магазин
	ShopID    uint `json:"shopID" db:"shop_id"`       //	ID магазина
	Discount  uint `json:"discount" db:"discount"`    //	скидка
	Enabled   bool `json:"image" db:"enabled"`        //	активна/неактивна
	PaymentID uint `json:"paymentID" db:"payment_id"` //	к какой оплате относится
}

//	product
type Product struct {
	gorm.Model
	ImageFront               Image                  `json:"imageFront" db:"image_front"`                                                //	фронтальная картинка
	ImageFrontID             uint                   `json:"imageFrontID" db:"image_front__id"`                                          //	ид фронтальной картинки
	ImageBack                Image                  `json:"imageBack" db:"image_back"`                                                  //	задняя картинка
	ImageBackID              uint                   `json:"imageBackID" db:"image_back_id"`                                             //	ид задней картинки
	Items                    []Item                 `json:"items" db:"items"`                                                           //  список айтемов
	ProductImages            []ProductImage         `json:"productImages" db:"product_images" gorm:"foreignkey:ProductID"`              //	возможные картинки продукта
	LocalizedDescriptions    []LocalizedDescription `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"` //  описание
	LocalizedDescriptionAKey uint                   //	?
	LocalizedFeatures        []LocalizedFeature     `json:"localizedFeatures" db:"localized_features" gorm:"polymorphic:Owner"`                                   //  локализованые детали
	LocalizedMaterial        []LocalizedMaterial    `json:"localizedMaterial" db:"localized_materials" gorm:"polymorphic:Owner"`                                  //  локализованые материалы
	LocalizedSEOs            []LocalizedSEO         `json:"localizedSEOs" db:"localized_seo" gorm:"polymorphic:Owner"`                                            // сео расширенный
	SeoLink                  string                 `json:"seoLink" db:"seo_link"`                                                                                //  сео линк
	Brand                    Brand                  `json:"brand" db:"brand"`                                                                                     //  торговая марка
	BrandID                  uint                   `json:"brandID" db:"brand_id"`                                                                                //  ID - торговая марка
	Manufacturer             Manufacturer           `json:"manufacturer" db:"manufacturer"`                                                                       //  производитель
	ManufacturerID           uint                   `json:"manufacturerID" db:"manufacturer_id"`                                                                  //  ID - производитель
	Price                    ProductPrice           `json:"price" db:"price"`                                                                                     //  цены
	PriceID                  uint                   `json:"priceID" db:"price_id"`                                                                                //  ID цены
	Categories               []Category             `json:"categories" db:"categories" gorm:"many2many:categories__products" gorm:"association_autoupdate:false"` //  категории
	//  parameters
	// restrictedDeliveries: Delivery[]; //  запрещенные доставки
	Weight            float32 `json:"weight" db:"weight"`                 //  вес
	VolumeWeightX     uint    `json:"volumeWeightX" db:"volume_weight_x"` //  объемный вес X
	VolumeWeightY     uint    `json:"volumeWeightY" db:"volume_weight_y"` //  объемный вес Y
	VolumeWeightZ     uint    `json:"volumeWeightZ" db:"volume_weight_z"` //  объемный вес Z
	Fstek             string  `json:"fstek" db:"fstek"`                   //  FSTEK федеральная служба чего то там.
	WarehousePosition string  `json:"whPosition" db:"wh_position"`        //  позиция на складе
	// localizedProductTypes: LocalizedDescription[]; //  тип товара для таможни и не только
	Article               string              `json:"article" db:"article"`                                            //  	артикул производителя
	Priority              uint                `json:"priority" db:"priority"`                                          //	приоритет при отображении
	ProductOptions        []ProductOption     `json:"productOptions" db:"product_options" gorm:"foreignkey:ProductID"` //	возможные опции продукта
	ProductAvailability   ProductAvailability `json:"productAvailability" db:"product_availability"`                   //  	доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
	ProductAvailabilityID uint                `json:"productAvailabilityID" db:"product_availability_id"`              //	ид доступности
	ProductVisibility     ProductVisibility   `json:"productVisibility" db:"product_visibility"`                       //  	отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
	ProductVisibilityID   uint                `json:"productVisibilityID" db:"product_visibility_id"`                  //	ид видимости
	// options: Option[]; // список опций
	// optionsSets: OptionsSet[]; //  список массивов опций
	// sale: Sale; //  скидка
	Type string `json:"type" db:"type"` //  тип
}

//	product
type ProductShort struct {
	ID                       uint                        //
	ImageFront               ImageShort                  `json:"imageFront" db:"image_front"`                                                //	фронтальная картинка
	ImageFrontID             uint                        `json:"imageFrontID" db:"image_front__id"`                                          //	ид фронтальной картинки
	ImageBack                ImageShort                  `json:"imageBack" db:"image_back"`                                                  //	задняя картинка
	ImageBackID              uint                        `json:"imageBackID" db:"image_back_id"`                                             //	ид задней картинки
	Items                    []Item                      `json:"items" db:"items"`                                                           //  список айтемов
	ProductImages            []ProductImage              `json:"productImages" db:"product_images" gorm:"foreignkey:ProductID"`              //	возможные картинки продукта
	LocalizedDescriptions    []LocalizedDescriptionShort `json:"localizedDescriptions" db:"localized_descriptions" gorm:"polymorphic:Owner"` //  описание
	LocalizedDescriptionAKey uint                        //	?
	LocalizedFeatures        []LocalizedFeatureShort     `json:"localizedFeatures" db:"localized_features" gorm:"polymorphic:Owner"`                                   //  локализованые детали
	LocalizedMaterial        []LocalizedMaterialShort    `json:"localizedMaterial" db:"localized_materials" gorm:"polymorphic:Owner"`                                  //  локализованые материалы
	LocalizedSEOs            []LocalizedSEOShort         `json:"localizedSEOs" db:"localized_seo" gorm:"polymorphic:Owner"`                                            // сео расширенный
	SeoLink                  string                      `json:"seoLink" db:"seo_link"`                                                                                //  сео линк
	Brand                    Brand                       `json:"brand" db:"brand"`                                                                                     //  торговая марка
	BrandID                  uint                        `json:"brandID" db:"brand_id"`                                                                                //  ID - торговая марка
	Manufacturer             Manufacturer                `json:"manufacturer" db:"manufacturer"`                                                                       //  производитель
	ManufacturerID           uint                        `json:"manufacturerID" db:"manufacturer_id"`                                                                  //  ID - производитель
	Price                    ProductPriceShort           `json:"price" db:"price"`                                                                                     //  цены
	PriceID                  uint                        `json:"priceID" db:"price_id"`                                                                                //  ID цены
	Categories               []Category                  `json:"categories" db:"categories" gorm:"many2many:categories__products" gorm:"association_autoupdate:false"` //  категории
	//  parameters
	// restrictedDeliveries: Delivery[]; //  запрещенные доставки
	Weight            float32 `json:"weight" db:"weight"`                 //  вес
	VolumeWeightX     uint    `json:"volumeWeightX" db:"volume_weight_x"` //  объемный вес X
	VolumeWeightY     uint    `json:"volumeWeightY" db:"volume_weight_y"` //  объемный вес Y
	VolumeWeightZ     uint    `json:"volumeWeightZ" db:"volume_weight_z"` //  объемный вес Z
	Fstek             string  `json:"fstek" db:"fstek"`                   //  FSTEK федеральная служба чего то там.
	WarehousePosition string  `json:"whPosition" db:"wh_position"`        //  позиция на складе
	// localizedProductTypes: LocalizedDescription[]; //  тип товара для таможни и не только
	Article               string                   `json:"article" db:"article"`                                            //  	артикул производителя
	Priority              uint                     `json:"priority" db:"priority"`                                          //	приоритет при отображении
	ProductOptions        []ProductOption          `json:"productOptions" db:"product_options" gorm:"foreignkey:ProductID"` //	возможные опции продукта
	ProductAvailability   ProductAvailabilityShort `json:"productAvailability" db:"product_availability"`                   //  	доступоность для вариантов продаж (пердзаказ, заказ по требованию и тд.)
	ProductAvailabilityID uint                     `json:"productAvailabilityID" db:"product_availability_id"`              //	ид доступности
	ProductVisibility     ProductVisibilityShort   `json:"productVisibility" db:"product_visibility"`                       //  	отображать или нет в магазине (показывать, не показывать, показывать при наличии на складе)
	ProductVisibilityID   uint                     `json:"productVisibilityID" db:"product_visibility_id"`                  //	ид видимости
	// options: Option[]; // список опций
	// optionsSets: OptionsSet[]; //  список массивов опций
	// sale: Sale; //  скидка
	Type string `json:"type" db:"type"` //  тип
}

//	product availability status
type ProductAvailability struct {
	gorm.Model
	Name string `json:"name" db:"name"` //	название
	//	preorder (возожны предзаказы), dropship (возможен дропшип), stock only (только покупка того что в наличие)
}

//	product availability status
type ProductAvailabilityShort struct {
	ID   uint   //
	Name string `json:"name" db:"name"` //	название
	//	preorder (возожны предзаказы), dropship (возможен дропшип), stock only (только покупка того что в наличие)
}

//	product
type ProductImage struct {
	gorm.Model
	ProductID    uint          `json:"productID" db:"product_id"`                                                     //	ид продукта
	Image        Image         `json:"image" db:"image"`                                                              //	картинка
	ImageID      uint          `json:"imageID" db:"image_id"`                                                         //	ид картинки
	OptionValues []OptionValue `json:"optionValues" db:"option_values" gorm:"many2many:product_image__option_values"` //	список возможных значений
}

type ProductOption struct {
	gorm.Model                 //	картинка
	ProductID    uint          `json:"productID" db:"product_id"`                                                      //	ид продукта
	Option       Option        `json:"option" db:"option"`                                                             //	имя опции
	OptionID     uint          `json:"optionID" db:"option_id"`                                                        // ид опции
	OptionValues []OptionValue `json:"optionValues" db:"option_values" gorm:"many2many:product_option__option_values"` //	список возможных значений
}

//	product price
type ProductPrice struct {
	gorm.Model
	Buy  uint32 `json:"buy" db:"buy"`   //	цена покупки
	Sell uint32 `json:"sell" db:"sell"` //	цена продажи
}

//	product price
type ProductPriceShort struct {
	ID   uint   //
	Buy  uint32 `json:"buy" db:"buy"`   //	цена покупки
	Sell uint32 `json:"sell" db:"sell"` //	цена продажи
}

//	product shadow
type ProductShadow struct {
	gorm.Model
}

//	product visibility status
type ProductVisibility struct {
	gorm.Model
	Name string `json:"name" db:"name"` //	имя
	//	enabled (товар всегда отображается), Stock enabled (товар отображается только если в наличие есть item), disabled (товар не отображается)
}

//	product visibility status
type ProductVisibilityShort struct {
	ID   uint   //
	Name string `json:"name" db:"name"` //	имя
	//	enabled (товар всегда отображается), Stock enabled (товар отображается только если в наличие есть item), disabled (товар не отображается)
}

//	purchase
type Purchase struct {
	gorm.Model
}

//	region
type Region struct {
	gorm.Model
	Country   Country `json:"country" db:"country"`      //	страна
	CountryID uint    `json:"countryID" db:"country_id"` //	ид страны
	Name      string  `json:"name" db:"name"`            //	название
	Code      string  `json:"code" db:"code"`            //	код
}

//	region short
type RegionShort struct {
	ID   uint   `json:"ID"`
	Name string `json:"name"` //	название
	Code string `json:"code"` //	код
}

// 	роль
type Role struct {
	gorm.Model
	Name        string `json:"name" db:"name"`               //	название
	Description string `json:"description" db:"description"` //	описание
}

//	sale
type Sale struct {
	gorm.Model
	Name  string `json:"name" db:"name"`   //	название скидки
	Value uint   `json:"value" db:"value"` //	размер
}

//	shop
type Shop struct {
	gorm.Model
	Name string `json:"name" db:"name"` //	название магазина
	// Value uint   `json:"value" db:"value"`
}

// 	пользователь
type User struct {
	gorm.Model
	UserName    string `json:"username" db:"username"`       //	имя
	Login       string `json:"login" db:"login"`             //	логин
	Password    string `json:"password" db:"password"`       //	пароль
	Email       string `json:"email" db:"email"`             //	почта
	Description string `json:"description" db:"description"` //	описание
	Image       string `json:"image" db:"image"`             //	картинка
	Roles       []Role `json:"roles" db:"roles"`             //	роли
}

// 	зона доставки
type Zone struct {
	gorm.Model
	Countries  []Country  `json:"countries" db:"countries" gorm:"many2many:zone_countries"` //	страны зоны
	ZoneCosts  []ZoneCost `json:"zoneCosts" db:"zone_costs"`                                //	цены
	DeliveryID uint       `json:"deliveryID" db:"delivery_id"`                              //	ид доставки
}

// 	цены в зоне доставки
type ZoneCost struct {
	gorm.Model
	Weight float32 `json:"weight" db:"weight"`  //	вес доставляемого товара
	Cost   uint    `json:"cost" db:"cost"`      //	цена
	ZoneID uint    `json:"zoneID" db:"zone_id"` //	идентификатор зоны
}
