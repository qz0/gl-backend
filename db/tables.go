package db

// 	/////////////////////////////////
// 	таблицы

//	addresses
func (Address) TableName() string {
	return schemaName + ".addresses"
}

//	assemblies
func (Assembly) TableName() string {
	return schemaName + ".assemblies"
}

//	brands
func (Brand) TableName() string {
	return schemaName + ".brands"
}

//	carts
func (Cart) TableName() string {
	return schemaName + ".carts"
}

//	cart items
func (CartItem) TableName() string {
	return schemaName + ".cart_items"
}

//	categories
func (Category) TableName() string {
	return schemaName + ".categories"
}

//	countries
func (Country) TableName() string {
	return schemaName + ".countries"
}

//	customers
func (Customer) TableName() string {
	return schemaName + ".customers"
}

//	customer ranks
func (CustomerRank) TableName() string {
	return schemaName + ".customer_ranks"
}

//	images
func (Image) TableName() string {
	return schemaName + ".images"
}

//	image options
func (ImageOptions) TableName() string {
	return schemaName + ".image_options"
}

//	image types
func (ImageType) TableName() string {
	return schemaName + ".image_types"
}

//	incomings
func (Incoming) TableName() string {
	return schemaName + ".incomings"
}

//	incoming items
func (IncomingItem) TableName() string {
	return schemaName + ".incoming_items"
}

//	items
func (Item) TableName() string {
	return schemaName + ".items"
}

//	item statuses
func (ItemStatus) TableName() string {
	return schemaName + ".item_statuses"
}

//	item shadows
func (ItemShadow) TableName() string {
	return schemaName + ".item_shadows"
}

//	languages
func (Language) TableName() string {
	return schemaName + ".languages"
}

//	localalized descriptions
func (Localization) TableName() string {
	return schemaName + ".localizations"
}

//	LogCrud
func (Log) TableName() string {
	return schemaName + ".logs"
}

//	localalized descriptions
func (LocalizedDescription) TableName() string {
	return schemaName + ".localized_descriptions"
}

//	localalized Features
func (LocalizedFeature) TableName() string {
	return schemaName + ".localized_features"
}

//	localalized Materials
func (LocalizedMaterial) TableName() string {
	return schemaName + ".localized_materials"
}

//	localalized SEOs
func (LocalizedSEO) TableName() string {
	return schemaName + ".localized_seos"
}

//	manufacturers
func (Manufacturer) TableName() string {
	return schemaName + ".manufacturers"
}

//	options
func (Option) TableName() string {
	return schemaName + ".options"
}

//	option types
func (OptionType) TableName() string {
	return schemaName + ".option_types"
}

//	option values
func (OptionValue) TableName() string {
	return schemaName + ".option_values"
}

//	options sets
func (OptionsSet) TableName() string {
	return schemaName + ".options_sets"
}

//	orders
func (Order) TableName() string {
	return schemaName + ".orders"
}

//	order priorities
func (OrderPriority) TableName() string {
	return schemaName + ".order_priorities"
}

//	order statuses
func (OrderStatus) TableName() string {
	return schemaName + ".order_statuses"
}

//	payments
func (Payment) TableName() string {
	return schemaName + ".payments"
}

//	payment discounts
func (PaymentDiscount) TableName() string {
	return schemaName + ".payment_discounts"
}

//	products
func (Product) TableName() string {
	return schemaName + ".products"
}

//	product availabilities
func (ProductAvailability) TableName() string {
	return schemaName + ".product_availabilities"
}

//	products images
func (ProductImage) TableName() string {
	return schemaName + ".product_images"
}

//	products options
func (ProductOption) TableName() string {
	return schemaName + ".product_options"
}

//	product prices
func (ProductPrice) TableName() string {
	return schemaName + ".product_prices"
}

//	product shadows
func (ProductShadow) TableName() string {
	return schemaName + ".product_shadows"
}

//	product visibilities
func (ProductVisibility) TableName() string {
	return schemaName + ".product_visibilities"
}

//	purchases
func (Purchase) TableName() string {
	return schemaName + ".purchases"
}

//	regions
func (Region) TableName() string {
	return schemaName + ".regions"
}

//	roles
func (Role) TableName() string {
	return schemaName + ".roles"
}

//	shops
func (Shop) TableName() string {
	return schemaName + ".shops"
}

//	users
func (User) TableName() string {
	return schemaName + ".users"
}

//	zones
func (Zone) TableName() string {
	return schemaName + ".zones"
}

//	zone costs
func (ZoneCost) TableName() string {
	return schemaName + ".zone_costs"
}
