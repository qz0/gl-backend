package db

import (
	"errors"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	"reflect"
)

// 	SelectAllItems - вывод всех записей
func (db *DataBase) SelectAllItems(items interface{}) error {

	// 	разное поведение для разных структур
	switch items.(type) {
	// ****************************************************************

	// 	Address
	case *[]Address:
		// тут структура для данных
		addresses := items.(*[]Address)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(addresses).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %+v", *addresses)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *addresses (%d)", len(*addresses))
			return nil
		}

	// 	Assembly
	case *[]Assembly:
		// тут структура для данных
		assemblies := items.(*[]Assembly)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(assemblies).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *assemblies)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *assemblies (%d)", len(*assemblies))
			return nil
		}

	// 	Brand
	case *[]Brand:
		// тут структура для данных
		brands := items.(*[]Brand)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Manufacturer").
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Image.Options").
			Preload("Image.Type").
			Preload("Image").
			Where("").Find(brands).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *brands)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *brands (%d)", len(*brands))
			return nil
		}

	// 	Cart
	case *[]Cart:
		// тут структура для данных
		carts := items.(*[]Cart)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Customer").
			Preload("Order").
			Preload("Delivery").
			Preload("Payment").
			Preload("Address").
			Preload("Address.Country").
			Preload("Address.Region").
			Preload("CartItems").
			Preload("CartItems.Product").
			Preload("CartItems.Product.ImageFront").
			Preload("CartItems.Product.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Where("").Find(carts).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *carts)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *carts (%d)", len(*carts))
			return nil
		}

	// 	Category
	case *[]Category:
		// тут структура для данных
		categories := items.(*[]Category)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ImageBackground").
			Preload("ImageBackground.Options").
			Preload("ImageBackground.Type").
			Preload("ImageIcon").
			Preload("ImageIcon.Options").
			Preload("ImageIcon.Type").
			Preload("Categories").
			Preload("Categories.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Categories.LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Products").
			Find(categories).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *categories)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *categories (%d)", len(*categories))
			return nil
		}

	// 	Country
	case *[]Country:
		// тут структура для данных
		countries := items.(*[]Country)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Regions").
			Preload("Image").
			Find(countries).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *countries)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *countries (%d)", len(*countries))
			return nil
		}

	// 	Currency
	case *[]Currency:
		// тут структура для данных
		currencies := items.(*[]Currency)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(currencies).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *currencies)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *currencies (%d)", len(*currencies))
			return nil
		}

	// 	Customer
	case *[]Customer:
		// тут структура для данных
		customers := items.(*[]Customer)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(customers).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *customers)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *customers (%d)", len(*customers))
			return nil
		}

	// 	Customer rank
	case *[]CustomerRank:
		// тут структура для данных
		customerRanks := items.(*[]CustomerRank)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(customerRanks).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *customerRanks)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *customerRanks (%d)", len(*customerRanks))
			return nil
		}

	// 	Delivery
	case *[]Delivery:
		// тут структура для данных
		deliveries := items.(*[]Delivery)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Image").
			Preload("Image.Options").
			Preload("Image.Type").
			Preload("Zones").
			Preload("Zones.Countries").
			Order("priority").
			Find(deliveries).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *deliveries)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *deliveries (%d)", len(*deliveries))
			return nil
		}

	// 	Images
	case *[]Image:
		// тут структура для данных
		images := items.(*[]Image)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Type").
			Preload("Options").
			//Order("priority").
			Find(images).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *images)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *images (%d)", len(*images))
			return nil
		}

	// 	Image options-s
	case *[]ImageOptions:
		// тут структура для картинок
		imageOptionsS := items.(*[]ImageOptions)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(imageOptionsS).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *imageOptionsS)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *imageOptionsS (%d)", len(*imageOptionsS))
			return nil
		}

	// 	Image types
	case *[]ImageType:
		// тут структура для картинок
		imageTypes := items.(*[]ImageType)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(imageTypes).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *imageTypes)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *imageTypes (%d)", len(*imageTypes))
			return nil
		}

	// 	Incoming
	case *[]Incoming:
		// тут структура для картинок
		incomings := items.(*[]Incoming)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Order("id ASC").
			Preload("Scan").
			Preload("Scan.Type").
			Preload("Scan.Options").
			Preload("IncomingItems").
			Preload("IncomingItems.OptionValues").
			Preload("IncomingItems.OptionValues.Option").
			Preload("IncomingItems.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("IncomingItems.Product").
			Preload("IncomingItems.Product.Brand").
			Preload("IncomingItems.Product.ImageFront").
			Preload("IncomingItems.Product.Manufacturer").
			Preload("IncomingItems.Product.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("IncomingItems.Product.LocalizedFeatures").
			Preload("IncomingItems.Product.LocalizedMaterial").
			Preload("IncomingItems.Product.LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Find(incomings).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *incomings)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *incomings (%d)", len(*incomings))
			return nil
		}

	// 	Item
	case *[]Item:
		// тут структура для картинок
		selectedItems := items.(*[]Item)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Order").
			Preload("Order.Status").
			Preload("OptionValues").
			Preload("Product").
			Find(selectedItems).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *selectedItems)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *Items (%d)", len(*selectedItems))
			return nil
		}

	// 	Item shadow
	case *[]ItemShadow:
		// тут структура для картинок
		itemShadows := items.(*[]ItemShadow)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(itemShadows).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *itemShadows)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *itemShadows (%d)", len(*itemShadows))
			return nil
		}

	// 	Language
	case *[]Language:
		// тут структура для языков
		languages := items.(*[]Language)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(languages).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *languages)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *languages (%d)", len(*languages))
			return nil
		}

	case *[]Localization:
		// тут структура для языков
		localizations := items.(*[]Localization)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(localizations).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *localizations)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *localizations (%d)", len(*localizations))
			return nil
		}

	case *[]Log:
		// тут структура для языков
		crudLogs := items.(*[]Log)
		// 	пробуем выбрать
		if err := db.HandleDB.
			// Select("id, created_at, type, action, user, object_id").
			Order("id DESC").
			Limit(100).
			Find(crudLogs).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе обьекта: %v", *crudLogs)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *crudLogs (%d)", len(*crudLogs))
			return nil
		}
	// 	Manufacturer
	case *[]Manufacturer:
		// тут структура для данных
		manufacturers := items.(*[]Manufacturer)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Brands").
			Preload("Brands.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Brands.Image").
			Find(manufacturers).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *manufacturers)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *manufacturers (%d)", len(*manufacturers))
			return nil
		}

	// 	Option
	case *[]Option:
		// тут структура для данных
		options := items.(*[]Option)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("OptionType").
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("OptionValues").
			Preload("OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("OptionValues.Image").
			Preload("OptionValues.Image.Type").
			Preload("OptionValues.Image.Options").
			Preload("OptionValues.Option").
			Preload("OptionValues.Option.OptionType").
			Order("priority").
			Find(options).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *options)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *options (%d)", len(*options))
			return nil
		}

	// 	OptionType
	case *[]OptionType:
		// тут структура для данных
		optionTypes := items.(*[]OptionType)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(optionTypes).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *optionTypes)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *optionTypes (%d)", len(*optionTypes))
			return nil
		}

	// 	OptionValue
	case *[]OptionValue:
		// тут структура для данных
		optionValues := items.(*[]OptionValue)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Image").
			Preload("Image.Type").
			Preload("Image.Options").
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Option").
			Preload("Option.OptionType").
			Preload("Option.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Find(optionValues).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *optionValues)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *optionValues (%d)", len(*optionValues))
			return nil
		}

	// 	Options set
	case *[]OptionsSet:
		// тут структура для данных
		optionsSets := items.(*[]OptionsSet)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(optionsSets).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *optionsSets)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *optionsSets (%d)", len(*optionsSets))
			return nil
		}

	// 	Order
	case *[]Order:
		// тут структура для данных
		orders := items.(*[]Order)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Priority").
			Preload("Status").
			Preload("Payment").
			Preload("Delivery").
			Preload("Items").
			Preload("ItemsShadow").
			Preload("CreatedShippingAddress").
			Preload("CurrentShippingAddress").
			Preload("ShippingAddressShadow").
			Preload("Customer").
			Preload("Sale").
			Find(orders).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *orders)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *orders (%d)", len(*orders))
			return nil
		}

	// 	OrderPriority
	case *[]OrderPriority:
		// тут структура для данных
		orderPriorities := items.(*[]OrderPriority)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(orderPriorities).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *orderPriorities)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *orderPriorities (%d)", len(*orderPriorities))
			return nil
		}

	// 	OrderStatus
	case *[]OrderStatus:
		// тут структура для данных
		orderStatuses := items.(*[]OrderStatus)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(orderStatuses).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *orderStatuses)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *orderStatuses (%d)", len(*orderStatuses))
			return nil
		}

	// 	Payment
	case *[]Payment:
		// тут структура для данных
		payments := items.(*[]Payment)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Discounts").
			Preload("Image").
			Preload("Image.Type").
			Order("priority").
			Find(payments).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *payments)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *payments (%d)", len(*payments))
			return nil
		}

	// 	Product
	case *[]Product:
		// тут структура для данных
		products := items.(*[]Product)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("ImageFront").         //+
			Preload("ImageFront.Type").    //+
			Preload("ImageFront.Options"). //+
			Preload("ImageBack").          //+
			Preload("ImageBack.Type").     //+
			Preload("ImageBack.Options").  //+
			Preload("ProductImages").
			Preload("ProductImages.Image").         //+
			Preload("ProductImages.Image.Type").    //+
			Preload("ProductImages.Image.Options"). //+
			Preload("ProductImages.OptionValues").  //+
			Preload("ProductImages.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ProductImages.OptionValues.Image").         //+
			Preload("ProductImages.OptionValues.Image.Type").    //+
			Preload("ProductImages.OptionValues.Image.Options"). //+
			Preload("Items").
			Preload("Items.Status").
			Preload("Items.Sale").
			Preload("Items.Currency").
			Preload("Items.Order").
			Preload("Items.Order.Items").
			Preload("Items.Order.Status").
			Preload("Items.OptionValues").
			Preload("Items.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedMaterial", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedFeatures", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Brand"). //+
			Preload("Brand.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Brand.Image").               //+
			Preload("Brand.Image.Type").          //+
			Preload("Brand.Image.Options").       //+
			Preload("Brand.Manufacturer").        //+
			Preload("Brand.Manufacturer.Brands"). //+
			Preload("Categories").
			Preload("Categories.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Manufacturer").        //+
			Preload("Manufacturer.Brands"). //+
			Preload("Price").               //+
			Preload("ProductOptions").
			Preload("ProductOptions.OptionValues"). //+
			Preload("ProductOptions.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                                   //+
			Preload("ProductOptions.OptionValues.Image").         //+
			Preload("ProductOptions.OptionValues.Image.Type").    //+
			Preload("ProductOptions.OptionValues.Image.Options"). //+
			Preload("ProductOptions.Option").                     //+
			Preload("ProductOptions.Option.OptionType").          //+
			Preload("ProductOptions.Option.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ProductOptions.Option.OptionValues"). //+
			Preload("ProductOptions.Option.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ProductOptions.Option.OptionValues.Image").         //+
			Preload("ProductOptions.Option.OptionValues.Image.Type").    //+
			Preload("ProductOptions.Option.OptionValues.Image.Options"). //+
			Preload("ProductAvailability").                              //+
			Preload("ProductVisibility").
			Order("priority DESC").
			Find(products).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *products)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *products (%d)", len(*products))
			return nil
		}

	// 	Product Availability
	case *[]ProductAvailability:
		// тут структура для данных
		productAvailabilityStatuses := items.(*[]ProductAvailability)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(productAvailabilityStatuses).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *productAvailabilityStatuses)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *productAvailabilityStatuses (%d)", len(*productAvailabilityStatuses))
			return nil
		}

	// 	Product shadow
	case *[]ProductShadow:
		// тут структура для данных
		productShadows := items.(*[]ProductShadow)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(productShadows).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *productShadows)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *productShadows (%d)", len(*productShadows))
			return nil
		}

	// 	Product Visibility
	case *[]ProductVisibility:
		// тут структура для данных
		productVisibilityStatuses := items.(*[]ProductVisibility)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(productVisibilityStatuses).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *productVisibilityStatuses)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *productVisibilityStatuses (%d)", len(*productVisibilityStatuses))
			return nil
		}

	// 	Purchase
	case *[]Purchase:
		// тут структура для данных
		purchases := items.(*[]Purchase)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(purchases).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *purchases)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *purchases (%d)", len(*purchases))
			return nil
		}

	// 	Region
	case *[]Region:
		// тут структура для данных
		regions := items.(*[]Region)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Country").
			//Limit(1).
			Find(regions).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *regions)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *regions (%d)", len(*regions))
			return nil
		}

	// 	Role
	case *[]Role:
		// тут структура для данных
		roles := items.(*[]Option)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(roles).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *roles)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *roles (%d)", len(*roles))
			return nil
		}

	// 	User
	case *[]User:
		// тут структура для данных
		users := items.(*[]User)
		// 	пробуем выбрать
		if err := db.HandleDB.Find(users).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *users)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *users (%d)", len(*users))
			return nil
		}

	case *[]Zone:
		// тут структура для данных
		zones := items.(*[]Zone)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Countries").
			Preload("Countries.Regions").
			Preload("Countries.Image").
			Preload("Countries.Image.Type").
			Preload("Countries.Image.Options").
			Preload("ZoneCosts").
			Find(zones).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объектов: %v", *zones)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *zones (%d)", len(*zones))
			return nil
		}

	default:
		log.Errorf("Попытка выбора объектов неизвестного типа: %v", reflect.TypeOf(items))
		err := errors.New("Попытка выбора объектов неизвестного типа")
		return err

		// ****************************************************************
	}
}
