package db

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// 	ChangePriorityOfItem - перемещение записи
func (db *DataBase) ChangePriorityOfItem(item interface{}) error {

	// 	разное поведение для разных структур
	switch item.(type) {
	//	*******************************************************************

	// 	Delivery
	case *Delivery:
		// переданная структура для данных
		receivedDelivery := item.(*Delivery)
		// времянка для исходных данных
		var delivery *Delivery
		// меняем ид
		delivery.ID = receivedDelivery.ID

		// пробуем получить экземпляр доставки
		if err := db.HandleDB.First(&delivery).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при смене приоритета объекта - ошибка при получении исходного объекта: %v", *delivery)
			return err
		} else {
			delivery.Priority = receivedDelivery.Priority
			if err = db.HandleDB.Save(&delivery).Error; err != nil {
				log.Errorf("Ошибка при смене приоритета объекта - ошибка сохранения в БД: %v", *delivery)
				return err
			} else {
				log.Warnf("Попытка смены приоритета объекта *delivery ID=%v", (*delivery).ID)
				return nil
			}
		}

	// 	Payment
	case *Payment:
		// переданная структура для данных
		receivedPayment := item.(*Payment)
		// времянка для исходных данных
		var payment *Payment
		// меняем ид
		payment.ID = receivedPayment.ID

		// пробуем получить экземпляр доставки
		if err := db.HandleDB.First(&payment).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при смене приоритета объекта - ошибка при получении исходного объекта: %v", *payment)
			return err
		} else {
			payment.Priority = receivedPayment.Priority
			if err = db.HandleDB.Save(&payment).Error; err != nil {
				log.Errorf("Ошибка при смене приоритета объекта - ошибка сохранения в БД: %v", *payment)
				return err
			} else {
				log.Warnf("Попытка смены приоритета объекта *payment ID=%v", (*payment).ID)
				return nil
			}
		}

	default:
		log.Errorf("Попытка смены приоритета объекта неизвестного типа: %v", reflect.TypeOf(item))
		err := errors.New("Попытка смены приоритета объекта неизвестного типа")
		return err

		//	*******************************************************************
	}
}

// 	UpdateItem - изменение записи
func (db *DataBase) UpdateItem(item interface{}) error {

	// 	разное поведение для разных структур
	switch item.(type) {

	// ****************************************************************

	// 	Address
	case *Address:
		// тут структура для мемо
		address := item.(*Address)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *address)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(address).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *address)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении address %+v", *address)
				}
				return err
			} else {
				// пробуем внести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *address)
					return err
				} else {
					log.Warnf("Изменен *address ID=%v", (*address).ID)
					return nil
				}
			}
		}
	// 	Assembly
	case *Assembly:
		// тут структура для мемо
		assembly := item.(*Assembly)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *assembly)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(assembly).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *assembly)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении assembly %+v", assembly)
				}
				return err
			} else {
				// пробуем внести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *assembly)
					return err
				} else {
					log.Warnf("Изменен *assembly ID=%v", (*assembly).ID)
					return nil
				}
			}
		}

	// 	Brand
	case *Brand:
		// тут структура для мемо
		brand := item.(*Brand)
		manufacture := brand.Manufacturer
		// 	пробуем обновить брэнд
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *brand)
			return err
		} else {
			// сохранение основной части брэнда, за исключением manufacture
			if err := tx.Model(&brand).Omit("Manufacturer").Set("gorm:association_autoupdate", false).Save(brand).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *brand)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении brand %+v", brand)
				}
				return err
			} else {
				// замена ассоциации manufacture при обновлении брэнда
				if err := tx.Model(&brand).Association("Manufacturer").Replace(&manufacture).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при изменении зависимости мануфекчур: %v", *brand)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении brand %+v", brand)
					}
					return err
				} else {
					// пробуем внести изменения в базу
					if err = tx.Commit().Error; err != nil {
						log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *brand)
						return err
					} else {
						log.Warnf("Изменен *brand ID=%v", (*brand).ID)
						return nil
					}
				}
			}
		}

	// 	Cart
	case *Cart:
		// тут структура для мемо
		cart := item.(*Cart)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *cart)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(cart).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *cart)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении cart %+v", cart)
				}
				return err
			} else {
				// пробуем внести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *cart)
					return err
				} else {
					log.Warnf("Изменен *cart ID=%v", (*cart).ID)
					return nil
				}
			}
		}

	// 	Category
	case *Category:
		// тут структура для мемо
		category := item.(*Category)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *category)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(category).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *category)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении category %+v", category)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *category)
					return err
				} else {
					log.Warnf("Изменен * ID=%v", (*category).ID)
					return nil
				}
			}
		}

	// 	Country
	case *Country:
		// тут структура для мемо
		country := item.(*Country)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *country)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(country).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *country)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении country %+v", country)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *country)
					return err
				} else {
					log.Warnf("Изменен * ID=%v", (*country).ID)
					return nil
				}
			}
		}

	// 	Currency
	case *Currency:
		// тут структура для мемо
		currency := item.(*Currency)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *currency)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(currency).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *currency)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении currency %+v", currency)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *currency)
					return err
				} else {
					log.Warnf("Изменен * ID=%v", (*currency).ID)
					return nil
				}
			}
		}

	// 	Customer
	case *Customer:
		// тут структура для мемо
		customer := item.(*Customer)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *customer)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(customer).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *customer)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении customer %+v", customer)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *customer)
					return err
				} else {
					log.Warnf("Изменен * ID=%v", (*customer).ID)
					return nil
				}
			}
		}

	// 	Customer rank
	case *CustomerRank:
		// тут структура для мемо
		customerRank := item.(*CustomerRank)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *customerRank)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(customerRank).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *customerRank)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении customerRank %+v", customerRank)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *customerRank)
					return err
				} else {
					log.Warnf("Изменен *customerRank ID=%v", (*customerRank).ID)
					return nil
				}
			}
		}

		// 	Delivery
	case *Delivery:
		// тут структура для мемо
		delivery := item.(*Delivery)
		// var zones []Zone
		zones := delivery.Zones

		var tdelivery Delivery
		tdelivery.ID = delivery.ID

		// берём старые значения для сравнения
		if db.SelectItem(&tdelivery); err != nil {
			return err
		}

		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *delivery)
			return err
		} else {
			// сравниваем старые зоны с новыми по айди. Те что не вошли в обновлённую версию деливери, стираем
			for _, lastIds := range tdelivery.Zones {
				var flag int = 0
				for _, newIds := range delivery.Zones {
					if newIds.ID == lastIds.ID {
						flag = 1
						break
					}
					continue
				}
				if flag == 0 {
					// Стираем те что не вошли.
					if err := tx.Model(&lastIds).Delete(&lastIds).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при изменении объекта: %v", lastIds)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить при обновлении zone %+v", lastIds)
						}
						return err
					}
				}
			}

			if err := tx.Set("gorm:association_autoupdate", false).Save(delivery).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *delivery)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении delivery %+v", delivery)
				}
				return err
			} else {
				// проходимся по списку зон, и индивидуальным подходом обновляем каждую.
				for _, zone := range zones {
					if err := tx.Model(&zone).Set("gorm:association_autoupdate", false).Updates(&zone).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при изменении объекта: %v", zone)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить при обновлении zone %+v", zone)
						}
						return err
					} else {
						// обновляем кантри
						if err := tx.Model(&zone).Association("Countries").Replace(zone.Countries).Error; err != nil {
							log.Errorf("Ошибка при изменении объекта: %v", zone)
							if err = tx.Rollback().Error; err != nil {
								log.Errorf("Не удалось откатить при обновлении zone %+v", zone)
							}
							return err
						} else {
							if tx.Model(&zone).Association("ZoneCosts").Count() != 0 {
								var zoneCosts []ZoneCost
								zid := strconv.Itoa(int(zone.ID))
								if err := tx.Unscoped().Where("zone_id = ?", fmt.Sprint(zid)).Find(&zoneCosts).Delete(zoneCosts).Error; err != nil {
									log.Errorf("Ошибка при изменении объекта: %v", zone)
									if err = tx.Rollback().Error; err != nil {
										log.Errorf("Не удалось откатить при обновлении zone %+v", zone)
									}
									return err
								}
							}
							// обновляем зонКост
							if err := tx.Model(&zone).Association("ZoneCosts").Replace(zone.ZoneCosts).Error; err != nil {
								log.Errorf("Ошибка при изменении объекта: %v", zone)
								if err = tx.Rollback().Error; err != nil {
									log.Errorf("Не удалось откатить при обновлении zone %+v", zone)
								}
								return err
							}
						}
					}
				}
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *delivery)
					return err
				} else {
					log.Warnf("Изменен *delivery ID=%v", (*delivery).ID)
					return nil
				}
			}
		}

	// 	Incoming
	case *Incoming:
		// тут структура для мемо
		incoming := item.(*Incoming)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *incoming)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(incoming).Error; err != nil {
				//	логиImageFront
				log.Errorf("Ошибка при изменении объекта: %v", *incoming)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении incoming %+v", incoming)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *incoming)
					return err
				} else {
					log.Warnf("Изменен *incoming ID=%v", (*incoming).ID)
					return nil
				}
			}
		}

	// 	Item
	case *Item:
		// тут структура для мемо
		updatedItem := item.(*Item)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *updatedItem)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(updatedItem).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *updatedItem)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении updatedItem %+v", updatedItem)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *updatedItem)
					return err
				} else {
					log.Warnf("Изменен *Item ID=%v", (*updatedItem).ID)
					return nil
				}
			}
		}

	// 	Item shadow
	case *ItemShadow:
		// тут структура для мемо
		itemShadow := item.(*ItemShadow)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *itemShadow)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(itemShadow).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *itemShadow)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении itemShadow %+v", itemShadow)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *itemShadow)
					return err
				} else {
					log.Warnf("Изменен *itemShadow ID=%v", (*itemShadow).ID)
					return nil
				}
			}
		}

		// 	Language
	case *Language:
		// тут структура для мемо
		language := item.(*Language)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *language)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(language).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *language)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении language %+v", language)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *language)
					return err
				} else {
					log.Warnf("Изменен *language ID=%v", (*language).ID)
					return nil
				}
			}
		}

	case *Localization:
		// тут структура для мемо
		localization := item.(*Localization)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *localization)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(localization).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *localization)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении localization %+v", localization)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *localization)
					return err
				} else {
					log.Warnf("Изменен *localization ID=%v", (*localization).ID)
					return nil
				}
			}
		}

	// 	Manufacturer
	case *Manufacturer:
		// тут структура для мемо
		manufacturer := item.(*Manufacturer)
		brands := manufacturer.Brands
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *manufacturer)
			return err
		} else {
			//обновляем manufacturer
			if err := tx.Model(&manufacturer).Omit("Brands").Set("gorm:association_autoupdate", false).Updates(&manufacturer).Error; err != nil {
				log.Errorf("Ошибка при изменении объекта: %v", *manufacturer)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении manufacturer %+v", manufacturer)
				}
				return err
			} else {
				// обновляем брэнд зависимость
				if err := tx.Model(&manufacturer).Association("Brands").Replace(brands).Error; err != nil {
					log.Errorf("Ошибка при изменении объекта: %v", brands)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении brands %+v", brands)
					}
					return err
				} else {
					// пробуем занести изменения manufacturer в базу данных
					if err = tx.Commit().Error; err != nil {
						log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *manufacturer)
						return err
					} else {
						log.Warnf("Изменен *manufacturer ID=%v", (*manufacturer).ID)
						return nil
					}
				}
			}
		}

	// 	Option
	case *Option:
		// тут структура для мемо
		option := item.(*Option)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *option)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(option).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *option)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении option %+v", option)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *option)
					return err
				} else {
					log.Warnf("Изменен *option ID=%v", (*option).ID)
					return nil
				}
			}
		}

	// 	OptionValue
	case *OptionValue:
		// тут структура для мемо
		optionValue := item.(*OptionValue)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *optionValue)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(optionValue).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *optionValue)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении optionValue %+v", optionValue)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *optionValue)
					return err
				} else {
					log.Warnf("Изменен *optionValue ID=%v", (*optionValue).ID)
					return nil
				}
			}
		}

	// 	Options set
	case *OptionsSet:
		// тут структура для мемо
		optionsSet := item.(*OptionsSet)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *optionsSet)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(optionsSet).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *optionsSet)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении optionsSet %+v", optionsSet)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *optionsSet)
					return err
				} else {
					log.Warnf("Изменен *optionsSet ID=%v", (*optionsSet).ID)
					return nil
				}
			}
		}

	// 	Order
	case *Order:
		// тут структура для мемо
		order := item.(*Order)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *order)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(&order).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *order)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении order %+v", order)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *order)
					return err
				} else {
					log.Warnf("Изменен *order ID=%v", (*order).ID)
					return nil
				}
			}
		}

	// 	Payment
	case *Payment:
		// тут структура для мемо
		payment := item.(*Payment)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *payment)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(payment).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *payment)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении payment %+v", payment)
				}
				return err
			} else {

				////////// LOCALIZED DESCRIPTIONS
				// проходимся по списку и обновляем принудительно.
				for _, local := range payment.LocalizedDescriptions {
					if err := tx.Model(local).Update(local).Error; err != nil {
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить при обновлении LocalizeDescriptions %+v", local)
						}
						return err
					}
				}

				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *payment)
					return err
				} else {
					log.Warnf("Изменен *payment ID=%v", (*payment).ID)
				}
				return nil
			}
		}

	// 	Product
	case *Product:
		// тут структура для мемо
		product := item.(*Product)
		//массив опций продукта, для точечного обновления
		productOptions := product.ProductOptions
		//массив картинок продукта, для точечного обновления
		productImages := product.ProductImages
		// 	пробуем создать
		var tproduct Product
		tproduct.ID = product.ID

		// берём старые значения для сравнения
		if db.SelectItem(&tproduct); err != nil {
			log.Errorf("Не удалось найти продукт в базе %+v", *product)
			return err
		}
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *product)
			return err
		} else {
			// обновляем продукт, за исключением того что в омите
			if err := tx.Model(&product).Omit("Categories", "Brand", "Manufacturer").Set("gorm:association_autoupdate", false).
				Save(product).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *product)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении product %+v", product)
				}
				return err
			}
			// проходимся по списку опций продукта, и индивидуальным подходом обновляем каждую.
			for _, productOption := range productOptions {
				// принудительно обновляем продукт опшинс что бы обновились.
				if err := tx.Model(&productOption).Omit("OptionValues").Set("gorm:association_autoupdate", false).
					Updates(&productOption).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при изменении объекта: %v", productOption)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении productOption %+v", productOption)
					}
					return err
				}
				// принудительно заменяем ассоциации опшин вэлью, что бы нормально обновлялись привязанные к продукту списки этой сущности
				if err := tx.Model(&productOption).Association("OptionValues").Replace(productOption.OptionValues).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при изменении объекта: %v", productOption)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении productOption %+v", productOption)
					}
					return err
				}
			}

			////////// PRICE
			// проходимся по списку и обновляем принудительно.
			price := product.Price
			if err := tx.Model(price).Update(price).Error; err != nil {
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении LocalizeDescriptions %+v", price)
				}
				return err
			}

			////////// LOCALIZED DESCRIPTIONS
			// проходимся по списку и обновляем принудительно.
			for _, local := range product.LocalizedDescriptions {
				if err := tx.Model(local).Update(local).Error; err != nil {
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении LocalizeDescriptions %+v", local)
					}
					return err
				}
			}

			/////////FEATURES AND MATERIALS UPDATE
			// сравниваем старые FEATURES с новыми по айди. Те что не вошли в обновлённую версию деливери, стираем
			for _, lastIds := range tproduct.LocalizedFeatures {
				var flag int = 0
				for _, newIds := range product.LocalizedFeatures {
					if newIds.ID == lastIds.ID {
						flag = 1
						break
					}
					continue
				}
				if flag == 0 {
					// Стираем те что не вошли.
					if err := tx.Model(&lastIds).Delete(&lastIds).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при изменении объекта: %v", lastIds)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить при обновлении zone %+v", lastIds)
						}
						return err
					}
				}
			}
			//TODO скорее всего косяк
			// сравниваем старые MATERIALS с новыми по айди. Те что не вошли в обновлённую версию деливери, стираем
			for _, lastIds := range tproduct.LocalizedMaterial {
				var flag int = 0
				for _, newIds := range product.LocalizedMaterial {
					if newIds.ID == lastIds.ID {
						flag = 1
						break
					}
					continue
				}
				if flag == 0 {
					// Стираем те что не вошли.
					if err := tx.Model(&lastIds).Delete(&lastIds).Error; err != nil {
						//	логи
						log.Errorf("Ошибка при изменении объекта: %v", lastIds)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить при обновлении zone %+v", lastIds)
						}
						return err
					}
				}
			}
			// проходимся по списку картинок продукта, и индивидуальным подходом обновляем каждую.
			for _, productImage := range productImages {
				//обновляем картинку принудительно
				if err := tx.Model(&productImage).Set("gorm:association_autoupdate", false).Updates(&productImage).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при изменении объекта: %v", productImage)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении productImage %+v", productImage)
					}
					return err
				}
				// принудительно заменяем ассоциации опшин вэлью, что бы нормально обновлялись привязанные к продукту списки этой сущности
				if err := tx.Model(&productImage).Association("OptionValues").Replace(productImage.OptionValues).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при изменении объекта: %v", productImage)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении productOption %+v", productImage)
					}
					return err
				}
			}
			// заменяем ассоциацию ProductOptions в продукте
			if err := tx.Model(&product).Association("ProductOptions").Replace(productOptions).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", productOptions)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении productOption %+v", productOptions)
				}
				return err
			} else {
				// заменяем ассоциацию ProductImages в продукте
				if err := tx.Model(&product).Association("ProductImages").Replace(productImages).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при изменении объекта: %v", productImages)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении ProductImages %+v", productImages)
					}
					return err
				} else {
					// заменяем привязанные категории у этого продукта на другие
					if err := tx.Model(&product).Association("Categories").Replace(product.Categories).Error; err != nil {
						log.Errorf("Ошибка при изменении объекта: %v", *product)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить при обновлении product %+v", product)
						}
						return err
					} else {
						// пробуем занести изменения в базу
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *product)
							return err
						} else {
							log.Warnf("Изменен *product ID=%v", (*product).ID)
							return nil
						}
					}
				}
			}
		}

	// 	Product shadow
	case *ProductShadow:
		// тут структура для мемо
		productShadow := item.(*ProductShadow)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *productShadow)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(productShadow).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *productShadow)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении productShadow %+v", productShadow)
				}
				return err
			} else {
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *productShadow)
					return err
				} else {
					log.Warnf("Изменен *productShadow ID=%v", (*productShadow).ID)
					return nil
				}
			}
		}

	// 	Purchase
	case *Purchase:
		// тут структура для мемо
		purchase := item.(*Purchase)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *purchase)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(purchase).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *purchase)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении purchase %+v", purchase)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *purchase)
					return err
				} else {
					log.Warnf("Изменен *purchase ID=%v", (*purchase).ID)
					return nil
				}
			}
		}

	// 	Region
	case *Region:
		// тут структура для мемо
		region := item.(*Region)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *region)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(region).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *region)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении region %+v", region)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *region)
					return err
				} else {
					log.Warnf("Изменен *region ID=%v", (*region).ID)
					return nil
				}
			}
		}

	// 	Role
	case *Role:
		// тут структура для мемо
		role := item.(*Role)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *role)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(role).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *role)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении role %+v", role)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *role)
					return err
				} else {
					log.Warnf("Изменен *role ID=%v", (*role).ID)
					return nil
				}
			}
		}
	// 	User
	case *User:
		// тут структура для мемо
		user := item.(*User)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *user)
			return err
		} else {
			if err := tx.Set("gorm:association_autoupdate", false).Save(user).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *user)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении user %+v", user)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *user)
					return err
				} else {
					log.Warnf("Изменен *user ID=%v", (*user).ID)
					return nil
				}
			}
		}

		// 	Zone
	case *Zone:
		// тут структура для мемо
		zone := item.(*Zone)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *zone)
			return err
		} else {
			if err := tx.Model(&zone).Set("gorm:association_autoupdate", false).Updates(&zone).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *zone)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении zone %+v", zone)
				}
				return err
			} else {
				if err := tx.Model(&zone).Association("Countries").Replace(zone.Countries).Error; err != nil {
					log.Errorf("Ошибка при изменении объекта: %v", *zone)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить при обновлении zone %+v", zone)
					}
					return err
				} else {
					if err := tx.Model(&zone).Association("ZoneCosts").Replace(zone.ZoneCosts).Error; err != nil {
						log.Errorf("Ошибка при изменении объекта: %v", *zone)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить при обновлении zone %+v", zone)
						}
						return err
					} else {
						// пробуем занести изменения в базу
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *zone)
							return err
						} else {
							log.Warnf("Изменен *zone ID=%v", (*zone).ID)
							return nil
						}
					}
				}
			}
		}

	default:
		log.Errorf("Попытка изменения объекта неизвестного типа: %v", reflect.TypeOf(item))
		err := errors.New("Попытка изменения объекта неизвестного типа")
		return err

		// ****************************************************************
	}
}

// 	UpdateItemOmit - изменение записи
func (db *DataBase) UpdateItemOmit(item interface{}, omit string) error {

	switch item.(type) {

	// 	Order
	case *Order:
		// тут структура для мемо
		order := item.(*Order)

		// 	пробуем создать
		tx := db.HandleDB.Begin()
		if tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *order)
			return err
		} else {

			if err := tx.Model(&order).Omit(omit).Updates(&order).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *order)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении order %+v", order)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *order)
					return err
				} else {
					log.Warnf("Изменен *order ID=%v", (*order).ID)
					return nil
				}
			}
		}

	default:
		log.Errorf("Попытка изменения объекта неизвестного типа: %v", reflect.TypeOf(item))
		err := errors.New("Попытка изменения объекта неизвестного типа")
		return err

		// ****************************************************************
	}
}

// 	UpdateItemOmit - изменение записи
func (db *DataBase) UpdateItemSelect(item interface{}, selQuery string, selArgs ...interface{}) error {

	switch item.(type) {

	// 	Incoming
	case *Incoming:
		// тут структура для мемо
		incoming := item.(*Incoming)

		// 	пробуем создать
		tx := db.HandleDB.Begin()
		if tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *incoming)
			return err
		} else {

			if err := tx.Model(&incoming).Select(selQuery).Save(&incoming).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *incoming)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении order %+v", incoming)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *incoming)
					return err
				} else {
					log.Warnf("Изменен *incoming ID=%v", (*incoming).ID)
					return nil
				}
			}
		}

	// 	Order
	case *Item:
		// тут структура для мемо
		selectedItem := item.(*Item)

		// 	пробуем создать
		tx := db.HandleDB.Begin()
		if tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *selectedItem)
			return err
		} else {

			if err := tx.Model(&selectedItem).Select(selQuery, selArgs...).Save(&selectedItem).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *selectedItem)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении item %+v", selectedItem)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *selectedItem)
					return err
				} else {
					log.Warnf("Изменен *item ID=%v", (*selectedItem).ID)
					return nil
				}
			}
		}

	// 	Order
	case *Order:
		// тут структура для мемо
		order := item.(*Order)

		// 	пробуем создать
		tx := db.HandleDB.Begin()
		if tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для обновления %+v", *order)
			return err
		} else {

			if err := tx.Model(&order).Select(selQuery).Save(&order).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при изменении объекта: %v", *order)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить при обновлении order %+v", order)
				}
				return err
			} else {
				// пробуем занести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при обновлении объекта: %v", *order)
					return err
				} else {
					log.Warnf("Изменен *order ID=%v", (*order).ID)
					return nil
				}
			}
		}

	default:
		log.Errorf("Попытка изменения объекта неизвестного типа: %v", reflect.TypeOf(item))
		err := errors.New("Попытка изменения объекта неизвестного типа")
		return err

		// ****************************************************************
	}
}
