package db

import (
	"errors"
	"fmt"

	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"

	// согласно официального руководства
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var err error

//InitDB - инициализация БД
func (db *DataBase) InitDB(hostName string, port int, userName string, dbName string, password string, sslMode string, schema string) error {
	// 	присваиваем хост подключения
	if len(hostName) == 0 {
		log.Error("Пустой хост")
		return errors.New("При формировании структуры настроек данных передали пустой хост")
	}

	// 	присваиваем порт поключения
	if port < 1 && port > 65535 {
		log.Error("Пустой порт")
		return errors.New("При формировании структуры настроек данных передали пустой порт")
	}

	// 	присваиваем имя пользователя
	if len(userName) == 0 {
		log.Error("Пустое имя пользователя")
		return errors.New("При формировании структуры настроек данных передали пустое имя пользователя")
	}

	// 	присваиваем название базы
	if len(dbName) == 0 {
		log.Error("Пустое название базы")
		return errors.New("При формировании структуры настроек данных передали пустое название базы")
	}

	// 	присваиваем название базы
	if len(sslMode) == 0 {
		log.Error("Пустое название sslMode")
		return errors.New("При формировании структуры настроек данных передали пустое название sslMode")
	}

	// 	присваиваем название базы
	if len(schema) == 0 {
		log.Error("Пустое название схемы")
		return errors.New("При формировании структуры настроек данных передали пустое название схемы")
	}

	// 	Открываем БД
	db.HandleDB, err = gorm.Open("postgres",
		fmt.Sprintf("host=%s port=%d user=%s dbname=%s password=%s sslmode=%s", hostName, port, userName, dbName, password, sslMode))
	if err != nil {
		log.Error("Не удалось подключиться к базе: ", err)
		return err
	}

	// 	Создаем схему
	initSchema(db.HandleDB, schema)

	initTable(db.HandleDB)

	log.Info("Подключение к БД успешно")

	return err
}

// 	инициализация схемы
func initSchema(db *gorm.DB, schema string) error {
	var count int
	//	проверяю есть ли схема
	db.Table("pg_catalog.pg_namespace").Where("nspname = ?", schema).Count(&count)
	if count == 0 {
		// 	схемы нет - создаем
		db.Exec("CREATE SCHEMA " + schema)
		// db.Exec("SET search_path TO " + schema)
		log.Info("создали схему")
		// 	зациклили
		initSchema(db, schema)
	}

	// gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
	// 	return schema + "." + defaultTableName
	// }

	return err
}

// 	инициализация схемы
func initTable(db *gorm.DB) error {

	//	****	ADDRESS	****
	//	если нету создаем
	if !db.HasTable(&Address{}) {
		if err := db.CreateTable(&Address{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Address: %v", err)
		} else {
			log.Info("Создана таблица для Address")
		}
	}

	//	****	ASSEMBLY	****
	//	если нету создаем
	if !db.HasTable(&Assembly{}) {
		if err := db.CreateTable(&Assembly{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Assembly: %v", err)
		} else {
			log.Info("Создана таблица для Assembly")
		}
	}

	//	****	BRANDS	****
	//	если нету создаем
	if !db.HasTable(&Brand{}) {
		if err := db.CreateTable(&Brand{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Brand: %v", err)
		} else {
			log.Info("Создана таблица для Brand")
		}
	}

	//	****	CARTS	****
	//	если нету создаем
	if !db.HasTable(&Cart{}) {
		if err := db.CreateTable(&Cart{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Cart: %v", err)
		} else {
			log.Info("Создана таблица для Cart")
		}
	}

	//	****	CART ITEMS	****
	//	если нету создаем
	if !db.HasTable(&CartItem{}) {
		if err := db.CreateTable(&CartItem{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для CartItem: %v", err)
		} else {
			log.Info("Создана таблица для CartItem")
		}
	}

	//	****	CATEGORY	****
	//	если нету создаем
	if !db.HasTable(&Category{}) {
		if err := db.CreateTable(&Category{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Category: %v", err)
		} else {
			log.Info("Создана таблица для Category")
		}
	}

	//	****	COUNTRY	****
	//	если нету создаем
	if !db.HasTable(&Country{}) {
		if err := db.CreateTable(&Country{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Country: %v", err)
		} else {
			log.Info("Создана таблица для Country")
		}
	}

	//	****	CURRENCY	****
	//	если нету создаем
	if !db.HasTable(&Currency{}) {
		if err := db.CreateTable(&Currency{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Currency: %v", err)
		} else {
			log.Info("Создана таблица для Currency")
		}
	}

	//	****	CUSTOMER	****
	//	если нету создаем
	if !db.HasTable(&Customer{}) {
		if err := db.CreateTable(&Customer{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Customer: %v", err)
		} else {
			log.Info("Создана таблица для Customer")
		}
	}

	//	****	CUSTOMER RANK	****
	//	если нету создаем
	if !db.HasTable(&CustomerRank{}) {
		if err := db.CreateTable(&CustomerRank{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Customer rank: %v", err)
		} else {
			log.Info("Создана таблица для Customer rank")
		}
	}

	//	****	DELIVERY	****
	//	если нету создаем
	if !db.HasTable(&Delivery{}) {
		if err := db.CreateTable(&Delivery{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Delivery: %v", err)
		} else {
			log.Info("Создана таблица для Delivery")
		}
	}

	//	****	INCOMING	****
	//	если нету создаем
	if !db.HasTable(&Incoming{}) {
		if err := db.CreateTable(&Incoming{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Incoming: %v", err)
		} else {
			log.Info("Создана таблица для Incoming")
		}
	}

	//	****	INCOMING ITEM	****
	//	если нету создаем
	if !db.HasTable(&IncomingItem{}) {
		if err := db.CreateTable(&IncomingItem{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Incoming item: %v", err)
		} else {
			log.Info("Создана таблица для Incoming item")
		}
	}

	//	****	IMAGES 	****
	//	если нету создаем
	if !db.HasTable(&Image{}) {
		if err := db.CreateTable(&Image{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Image: %v", err)
		} else {
			log.Info("Создана таблица для Image")
		}
	}

	//	****	IMAGE OPTIONS	****
	//	если нету создаем
	if !db.HasTable(&ImageOptions{}) {
		if err := db.CreateTable(&ImageOptions{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Image options: %v", err)
		} else {
			log.Info("Создана таблица для Image options")
		}
	}

	//	****	IMAGE TYPE	****
	//	если нету создаем
	if !db.HasTable(&ImageType{}) {
		if err := db.CreateTable(&ImageType{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Image type: %v", err)
		} else {
			log.Info("Создана таблица для Image type")
		}
	}

	//	****	ITEM	****
	//	если нету создаем
	if !db.HasTable(&Item{}) {
		if err := db.CreateTable(&Item{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Item: %v", err)
		} else {
			log.Info("Создана таблица для Item")
		}
	}

	//	****	ITEM STATUS	****
	//	если нету создаем
	if !db.HasTable(&ItemStatus{}) {
		if err := db.CreateTable(&ItemStatus{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Item status: %v", err)
		} else {
			log.Info("Создана таблица для Item status")
		}
	}

	//	****	ITEM SHADOW	****
	//	если нету создаем
	if !db.HasTable(&ItemShadow{}) {
		if err := db.CreateTable(&ItemShadow{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Item shadow: %v", err)
		} else {
			log.Info("Создана таблица для Item shadow")
		}
	}

	//	****	LANGUAGE	****
	//	если нету создаем
	if !db.HasTable(&Language{}) {
		db.CreateTable(&Language{})
		log.Info("Создана таблица языков")
	}

	//	****	LOCALIZATION	****
	//	если нету создаем
	if !db.HasTable(&Localization{}) {
		db.CreateTable(&Localization{})
		log.Info("Создана таблица переводов")
	}

	//	****	LogCrud 	****
	//	если нету создаем
	if !db.HasTable(&Log{}) {
		db.CreateTable(&Log{})
		log.Info("Создана таблица логов CRUD")
	}

	//	****	LOCALIZED DESCRIPTION	****
	//	если нету создаем
	if !db.HasTable(&LocalizedDescription{}) {
		if err := db.CreateTable(&LocalizedDescription{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для LocalizedDescription: %v", err)
		} else {
			log.Info("Создана таблица для LocalizedDescription")
		}
	}

	//	****	LOCALIZED FEATURES	****
	//	если нету создаем
	if !db.HasTable(&LocalizedFeature{}) {
		if err := db.CreateTable(&LocalizedFeature{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для LocalizedFeature: %v", err)
		} else {
			log.Info("Создана таблица для LocalizedFeature")
		}
	}

	//	****	LOCALIZED MATERIALS	****
	//	если нету создаем
	if !db.HasTable(&LocalizedMaterial{}) {
		if err := db.CreateTable(&LocalizedMaterial{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для LocalizedMaterial: %v", err)
		} else {
			log.Info("Создана таблица для LocalizedMaterial")
		}
	}
	//	получаем текущее значение в переменную
	//var localizedFeatures []LocalizedFeature
	//db.Find(&localizedFeatures)
	//log.Warnf("LocalizedTexts: %+v", localizedSEOs)

	//	****	LOCALIZED SEOS	****
	//	если нету создаем
	if !db.HasTable(&LocalizedSEO{}) {
		if err := db.CreateTable(&LocalizedSEO{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для LocalizedSEO: %v", err)
		} else {
			log.Info("Создана таблица для LocalizedSEO")
		}
	}

	//	****	LOCALIZED TEXTS	****
	//	если нету создаем
	if !db.HasTable(&LocalizedText{}) {
		if err := db.CreateTable(&LocalizedText{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для LocalizedText: %v", err)
		} else {
			log.Info("Создана таблица для LocalizedText")
		}
	}

	//	****	MANUFACTURER	****
	//	если нету создаем
	if !db.HasTable(&Manufacturer{}) {
		if err := db.CreateTable(&Manufacturer{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Manufacturer: %v", err)
		} else {
			log.Info("Создана таблица для Manufacturer")
		}
	}

	//	****	OPTION ****
	//	если нету создаем
	if !db.HasTable(&Option{}) {
		if err := db.CreateTable(&Option{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Option: %v", err)
		} else {
			log.Info("Создана таблица для Option")
		}
	}

	//	****	OPTION TYPE	****
	//	если нету создаем
	if !db.HasTable(&OptionType{}) {
		if err := db.CreateTable(&OptionType{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для OptionType: %v", err)
		} else {
			log.Info("Создана таблица для OptionType")
		}
	}

	//	****	OPTION VALUE	****
	//	если нету создаем
	if !db.HasTable(&OptionValue{}) {
		if err := db.CreateTable(&OptionValue{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для OptionValue: %v", err)
		} else {
			log.Info("Создана таблица для OptionValue")
		}
	}

	//	****	OPTION SET ****
	//	если нету создаем
	if !db.HasTable(&OptionsSet{}) {
		if err := db.CreateTable(&OptionsSet{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для OptionsSet: %v", err)
		} else {
			log.Info("Создана таблица для OptionsSet")
		}
	}

	//	****	ORDER ****
	//	если нету создаем
	if !db.HasTable(&Order{}) {
		if err := db.CreateTable(&Order{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Order: %v", err)
		} else {
			log.Info("Создана таблица для Order")
		}
	}

	//	****	ORDER PRIORITY ****
	//	если нету создаем
	if !db.HasTable(&OrderPriority{}) {
		if err := db.CreateTable(&OrderPriority{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для OrderPriority: %v", err)
		} else {
			log.Info("Создана таблица для OrderPriority")
		}
	}

	//	****	ORDER STATUS ****
	//	если нету создаем
	if !db.HasTable(&OrderStatus{}) {
		if err := db.CreateTable(&OrderStatus{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для OrderStatus: %v", err)
		} else {
			log.Info("Создана таблица для OrderStatus")
		}
	}

	//	****	PAYMENT	****
	//	если нету создаем
	if !db.HasTable(&Payment{}) {
		if err := db.CreateTable(&Payment{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Payment: %v", err)
		} else {
			log.Info("Создана таблица для Payment")
		}
	}

	//	****	PAYMENT DISCOUNT	****
	//	если нету создаем
	if !db.HasTable(&PaymentDiscount{}) {
		if err := db.CreateTable(&PaymentDiscount{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Payment: %v", err)
		} else {
			log.Info("Создана таблица для Payment")
		}
	}

	//	****	PRODUCT	****
	//	если нету создаем
	if !db.HasTable(&Product{}) {
		if err := db.CreateTable(&Product{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Product: %v", err)
		} else {
			log.Info("Создана таблица для Product")
		}
	}

	//	****	PRODUCT	AVAILABILITY STATUS***
	//	если нету создаем
	if !db.HasTable(&ProductAvailability{}) {
		if err := db.CreateTable(&ProductAvailability{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для ProductAvailability: %v", err)
		} else {
			log.Info("Создана таблица для ProductAvailability")
		}
	}

	//	****	PRODUCT IMAGES	****
	//	если нету создаем
	if !db.HasTable(&ProductImage{}) {
		if err := db.CreateTable(&ProductImage{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для ProductImage: %v", err)
		} else {
			log.Info("Создана таблица для ProductImage")
		}
	}

	//	****	PRODUCT OPTIONS	****
	//	если нету создаем
	if !db.HasTable(&ProductOption{}) {
		if err := db.CreateTable(&ProductOption{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для ProductOption: %v", err)
		} else {
			log.Info("Создана таблица для ProductOption")
		}
	}

	//	****	PRODUCT PRICE	****
	//	если нету создаем
	if !db.HasTable(&ProductPrice{}) {
		if err := db.CreateTable(&ProductPrice{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Product price: %v", err)
		} else {
			log.Info("Создана таблица для Product price")
		}
	}

	//	****	PRODUCT SHADOW	****
	//	если нету создаем
	if !db.HasTable(&ProductShadow{}) {
		if err := db.CreateTable(&ProductShadow{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Product shadow: %v", err)
		} else {
			log.Info("Создана таблица для Product shadow")
		}
	}

	//	****	PRODUCT	VISIBILITY STATUS****
	//	если нету создаем
	if !db.HasTable(&ProductVisibility{}) {
		if err := db.CreateTable(&ProductVisibility{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для ProductVisibility: %v", err)
		} else {
			log.Info("Создана таблица для ProductVisibility")
		}
	}

	//	****	PURCHASE	****
	//	если нету создаем
	if !db.HasTable(&Purchase{}) {
		if err := db.CreateTable(&Purchase{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Purchase: %v", err)
		} else {
			log.Info("Создана таблица для Purchase")
		}
	}

	//	****	REGION	****
	//	если нету создаем
	if !db.HasTable(&Region{}) {
		if err := db.CreateTable(&Region{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Region: %v", err)
		} else {
			log.Info("Создана таблица для Region")
		}
	}

	//	****	ROLE	****
	//	если нету создаем
	if !db.HasTable(&Role{}) {
		if err := db.CreateTable(&Role{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Role: %v", err)
		} else {
			log.Info("Создана таблица для Role")
		}
	}

	//	****	SHOP	****
	//	если нету создаем
	if !db.HasTable(&Shop{}) {
		if err := db.CreateTable(&Shop{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Shop: %v", err)
		} else {
			log.Info("Создана таблица для Shop")
		}
	}

	//	****	USER	****
	//	если нету создаем
	if !db.HasTable(&User{}) {
		if err := db.CreateTable(&User{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для User: %v", err)
		} else {
			log.Info("Создана таблица для User")
		}
	}

	//	****	ZONE	****
	//	если нету создаем
	if !db.HasTable(&Zone{}) {
		if err := db.CreateTable(&Zone{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для Zone: %v", err)
		} else {
			log.Info("Создана таблица для Zone")
		}
	}

	//	****	ZONE COST	****
	//	если нету создаем
	if !db.HasTable(&ZoneCost{}) {
		if err := db.CreateTable(&ZoneCost{}).Error; err != nil {
			log.Errorf("Ошибка при создании таблицы для ZoneCost: %v", err)
		} else {
			log.Info("Создана таблица для ZoneCost")
		}
	}

	// AutoMigrate
	// TODO: убить в проде
	db.AutoMigrate(
		&Brand{},
		&Cart{},
		&CartItem{},
		&Image{},
		&Incoming{},
		&IncomingItem{},
		&Item{},
		&ItemStatus{},
		&PaymentDiscount{},
		&Order{},
		&OrderPriority{},
		&OptionValue{},
		&OrderStatus{},
		&Product{},
		&ProductAvailability{},
		&ProductVisibility{},
		&Log{},
		&Localization{})

	//	****
	//	Возврат
	return err
}

// 	закрываем базу
func (db *DataBase) CloseDB() error {
	//	вызов закрытия
	err = db.HandleDB.Close()
	// 	если ошибка - пишем в лог
	if err != nil {
		log.Error(err)
	}

	return err
}
