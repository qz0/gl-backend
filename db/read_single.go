package db

import (
	"errors"
	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
	"reflect"
)

// 	SelectItem - вывод одной записи
func (db *DataBase) SelectItem(item interface{}) error {
	// 	разное поведение для разных структур
	switch item.(type) {

	// ****************************************************************

	// 	Address
	case *Address:
		// тут структура для данных
		address := item.(*Address)
		// 	попытка выбора
		if err := db.HandleDB.First(address).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта address: %v", *address)
			return err
		} else {
			log.Infof("Попытка выбора объекта *address ID=%v", (*address).ID)
			return nil
		}

	// 	Assembly
	case *Assembly:
		// тут структура для данных
		assembly := item.(*Assembly)
		// 	попытка выбора
		if err := db.HandleDB.First(assembly).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта assembly: %v", *assembly)
			return err
		} else {
			log.Infof("Попытка выбора объекта *assembly ID=%v", (*assembly).ID)
			return nil
		}

	// 	Brand
	case *Brand:
		// тут структура для данных
		brand := item.(*Brand)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Manufacturer").
			Preload("Image.Options").
			Preload("Image.Type").
			Preload("Image").
			First(brand).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта brand: %v", *brand)
			return err
		} else {
			log.Infof("Попытка выбора объекта *brand ID=%v", (*brand).ID)
			return nil
		}

	// 	Cart
	case *Cart:
		// тут структура для данных
		cart := item.(*Cart)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("Customer").
			Preload("Order").
			Preload("Delivery").
			Preload("Payment").
			Preload("Address").
			Preload("Address.Country").
			Preload("Address.Region").
			Preload("CartItems").
			Preload("CartItems.Product").            //Обратить внимание, поменял тут а не в types.go
			Preload("CartItems.Product.ImageFront"). //Обратить внимание, поменял тут а не в types.go
			Preload("CartItems.Product.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}). //Обратить внимание, поменял тут а не в types.go
			First(cart).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта cart: %v", *cart)
			return err
		} else {
			log.Infof("Попытка выбора объекта *cart ID=%v", (*cart).ID)
			return nil
		}

	// 	Category
	case *Category:
		// тут структура для данных
		category := item.(*Category)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Products", func(db *gorm.DB) *gorm.DB {
				return db.Order("priority DESC")
			}).
			Preload("Products.ImageFront").
			Preload("Products.ProductOptions").
			Preload("Products.ProductOptions.OptionValues").
			Preload("Products.ProductOptions.OptionValues.Image").
			Preload("Products.ProductOptions.OptionValues.Image.Type").
			Preload("Products.ProductOptions.OptionValues.Image.Options").
			//Preload("Image.Options").
			//Preload("Image.Type").
			Preload("Products.ImageBack").
			Preload("ImageBackground").
			Preload("ImageBackground.Options").
			Preload("ImageBackground.Type").
			Preload("ImageIcon").
			Preload("ImageIcon.Options").
			Preload("ImageIcon.Type").
			// Preload("Products.ProductImages").
			// Preload("Products.localizedFeatures").
			Preload("Products.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Products.LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Products.Brand").
			Preload("Products.Brand.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Products.Manufacturer").
			Preload("Products.Price").
			First(category).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта category: %v", *category)
			return err
		} else {
			log.Infof("Попытка выбора объекта *category ID=%v", (*category).ID)
			return nil
		}

	// 	Country
	case *Country:
		// тут структура для данных
		country := item.(*Country)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("Regions").
			Preload("Image").
			First(country).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта country: %v", *country)
			return err
		} else {
			log.Infof("Попытка выбора объекта *country ID=%v", (*country).ID)
			return nil
		}

	// 	Currency
	case *Currency:
		// тут структура для данных
		currency := item.(*Currency)
		// 	попытка выбора
		if err := db.HandleDB.First(currency).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта currency: %v", *currency)
			return err
		} else {
			log.Infof("Попытка выбора объекта *currency ID=%v", (*currency).ID)
			return nil
		}

	// 	Customer
	case *Customer:
		// тут структура для данных
		customer := item.(*Customer)
		// 	попытка выбора
		if err := db.HandleDB.First(customer).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта customer: %v", *customer)
			return err
		} else {
			log.Infof("Попытка выбора объекта *customer ID=%v", (*customer).ID)
			return nil
		}

	// 	Customer rank
	case *CustomerRank:
		// тут структура для данных
		customerRank := item.(*CustomerRank)
		// 	попытка выбора
		if err := db.HandleDB.First(customerRank).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта customerRank: %v", *customerRank)
			return err
		} else {

			log.Infof("Попытка выбора объекта *customerRank ID=%v", (*customerRank).ID)
			return nil
		}

	// 	Delivery
	case *Delivery:
		// тут структура для данных
		delivery := item.(*Delivery)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Image").
			Preload("Image.Options").
			Preload("Image.Type").
			Preload("Zones").
			Preload("Zones.Countries").
			Preload("Zones.ZoneCosts", func(db *gorm.DB) *gorm.DB {
				return db.Order("weight ASC")
			}).
			First(delivery).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта delivery: %v", *delivery)
			return err
		} else {
			log.Infof("Попытка выбора объекта *delivery ID=%v", (*delivery).ID)
			return nil
		}

	// 	Image
	case *Image:
		// тут структура для данных
		image := item.(*Image)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("Options").
			Preload("Type").
			First(image).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта image: %v", *image)
			return err
		} else {
			log.Infof("Попытка выбора объекта *image ID=%v", (*image).ID)
			return nil
		}

	// 	Image options
	case *ImageOptions:
		// тут структура для данных
		imageOptions := item.(*ImageOptions)
		// 	попытка выбора
		if err := db.HandleDB.First(imageOptions).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта imageOptions: %v", *imageOptions)
			return err
		} else {
			log.Infof("Попытка выбора объекта *imageOptions ID=%v", (*imageOptions).ID)
			return nil
		}

	// 	Image type
	case *ImageType:
		// тут структура для данных
		imageType := item.(*ImageType)
		// 	попытка выбора
		if err := db.HandleDB.First(imageType).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта imageType: %v", *imageType)
			return err
		} else {
			log.Infof("Попытка выбора объекта *imageType ID=%v", (*imageType).ID)
			return nil
		}

	// 	Incoming
	case *Incoming:
		// тут структура для данных
		incoming := item.(*Incoming)
		// 	попытка выбора
		if err := db.HandleDB.
			Order("id ASC").
			Preload("Scan").
			Preload("Scan.Type").
			Preload("Scan.Options").
			Preload("IncomingItems").
			Preload("IncomingItems.OptionValues").
			Preload("IncomingItems.Product").
			Preload("IncomingItems.Product.Brand").
			Preload("IncomingItems.Product.ImageFront").
			Preload("IncomingItems.Product.Manufacturer").
			Preload("IncomingItems.Product.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("IncomingItems.Product.LocalizedFeatures", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("IncomingItems.Product.LocalizedMaterial", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("IncomingItems.Product.LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			First(incoming).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта incoming: %v", *incoming)
			return err
		} else {
			log.Infof("Попытка выбора объекта *incoming ID=%v", (*incoming).ID)
			return nil
		}

		// 	IncomingItem
	case *IncomingItem:
		// тут структура для данных
		incomingItem := item.(*IncomingItem)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("Product").
			Preload("Product.Manufacturer").
			First(incomingItem).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта incomingItem: %v", *incomingItem)
			return err
		} else {
			log.Infof("Попытка выбора объекта *incomingItem ID=%v", (*incomingItem).ID)
			return nil
		}

	// 	Item
	case *Item:
		// тут структура для данных
		selectedItem := item.(*Item)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("OptionValues"). //+
			Preload("OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                    //+
			Preload("OptionValues.Image").         //+
			Preload("OptionValues.Image.Type").    //+
			Preload("OptionValues.Image.Options"). //+
			Preload("Product").                    //+
			Preload("Order").                      //+
			First(selectedItem).Error; err != nil {
			log.Errorf("Ошибка при выборе объекта item: %v", *selectedItem)
			return err
		} else {
			log.Infof("Попытка выбора объекта *Item ID=%v", (*selectedItem).ID)
			return nil
		}

	// 	Item shadow
	case *ItemShadow:
		// тут структура для данных
		itemShadow := item.(*ItemShadow)
		// 	попытка выбора
		if err := db.HandleDB.First(itemShadow).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта itemShadow: %v", *itemShadow)
			return err
		} else {
			log.Infof("Попытка выбора объекта *itemShadow ID=%v", (*itemShadow).ID)
			return nil
		}

	case *Language:
		// тут структура для данных
		language := item.(*Language)
		// 	попытка выбора
		if err := db.HandleDB.First(language).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта language: %v", *language)
			return err
		} else {
			log.Infof("Попытка выбора объекта *language ID=%v", (*language).ID)
			return nil
		}

	case *Localization:
		// тут структура для языков
		localization := item.(*Localization)
		// 	пробуем выбрать
		if err := db.HandleDB.First(localization).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе обьекта localization: %v", *localization)
			return err
		} else {
			log.Infof("Попытка выбора объекта *localization ID=%v", (*localization).ID)
			return nil
		}

	case *Log:
		// тут структура для языков
		crudLog := item.(*Log)
		// 	пробуем выбрать
		if err := db.HandleDB.First(crudLog).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе обьекта crudLog: %v", *crudLog)
			return err
		} else {
			log.Infof("Попытка выбора объекта *crudLog ID=%v", (*crudLog).ID)
			return nil
		}

	// 	Manufacturer
	case *Manufacturer:
		// тут структура для данных
		manufacturer := item.(*Manufacturer)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("Brands").
			Preload("Brands.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			First(manufacturer).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта manufacturer: %v", *manufacturer)
			return err
		} else {
			log.Infof("Попытка выбора объекта *manufacturer ID=%v", (*manufacturer).ID)
			return nil
		}

	// 	Option
	case *Option:
		// тут структура для данных
		option := item.(*Option)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("OptionType").
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("OptionValues").
			Preload("OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("OptionValues.Image").
			Preload("OptionValues.Image.Type").
			Preload("OptionValues.Image.Options").
			Preload("OptionValues.Option").
			Preload("OptionValues.Option.OptionType").
			First(option).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта option: %v", *option)
			return err
		} else {
			log.Infof("Попытка выбора объекта *option ID=%v", (*option).ID)
			return nil
		}

	// 	Option value
	case *OptionValue:
		// тут структура для данных
		optionValue := item.(*OptionValue)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Option").
			Preload("Option.OptionType").
			Preload("Option.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Image.Options").
			Preload("Image.Type").
			Preload("Image").
			First(optionValue).Error; err != nil {

			//	логи
			log.Errorf("Ошибка при выборе объекта optionValue: %v", *optionValue)
			return err
		} else {
			log.Infof("Попытка выбора объекта *optionValue ID=%v", (*optionValue).ID)
			return nil
		}

	// 	Options set
	case *OptionsSet:
		// тут структура для данных
		optionsSet := item.(*OptionsSet)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			First(optionsSet).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта optionsSet: %v", *optionsSet)
			return err
		} else {
			log.Infof("Попытка выбора объекта *optionsSet ID=%v", (*optionsSet).ID)
			return nil
		}

	// 	Order
	case *Order:
		// тут структура для данных
		order := item.(*Order)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("Priority").
			Preload("Status").
			Preload("Delivery").
			Preload("Delivery.LocalizedDescriptions").
			Preload("Delivery.Image").
			Preload("Delivery.Image.Options").
			Preload("Delivery.Image.Type").
			Preload("Payment").
			Preload("Payment.LocalizedDescriptions").
			Preload("CartItems").
			Preload("CartItems.Product").
			Preload("CartItems.Product.Price").
			Preload("CartItems.Product.LocalizedDescriptions").
			Preload("CartItems.Product.ImageFront").
			Preload("CartItems.Product.ImageFront.Options").
			Preload("CartItems.Product.ImageFront.Type").
			Preload("CartItems.OptionValues").
			Preload("CartItems.OptionValues.LocalizedDescriptions").
			Preload("Items").
			Preload("Items.Product").
			Preload("Items.Product.Price").
			Preload("Items.Product.LocalizedDescriptions").
			Preload("Items.Product.ImageFront").
			Preload("Items.Product.ImageFront.Options").
			Preload("Items.Product.ImageFront.Type").
			Preload("Items.OptionValues").
			Preload("Items.OptionValues.LocalizedDescriptions").
			Preload("ItemsShadow").
			Preload("CreatedShippingAddress").
			Preload("CreatedShippingAddress.Country").
			Preload("CreatedShippingAddress.Region").
			Preload("CurrentShippingAddress").
			Preload("CurrentShippingAddress.Country").
			Preload("CurrentShippingAddress.Region").
			Preload("ShippingAddressShadow").
			Preload("Customer").
			Preload("Currency").
			Preload("Sale").
			First(order).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта order: %v", *order)
			return err
		} else {
			log.Infof("Попытка выбора объекта *order ID=%v", (*order).ID)
			return nil
		}

	// 	Payment
	case *Payment:
		// тут структура для данных
		payment := item.(*Payment)
		//log.Infof("Попытка выбора: %+v", (*payment).ID)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Discounts").
			Preload("Image").
			First(payment).
			//Where("id = ?", (*payment).ID).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта payment: %v", *payment)
			return err
		} else {
			log.Infof("Попытка выбора объекта *payment ID=%v", (*payment).ID)
			return nil
		}

	// 	Product
	case *Product:
		// тут структура для данных
		product := item.(*Product)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("ImageFront").         //+
			Preload("ImageFront.Type").    //+
			Preload("ImageFront.Options"). //+
			Preload("ImageBack").          //+
			Preload("ImageBack.Type").     //+
			Preload("ImageBack.Options").  //+
			Preload("ProductImages").
			Preload("ProductImages.Image").         //+
			Preload("ProductImages.Image.Type").    //+
			Preload("ProductImages.Image.Options"). //+
			Preload("ProductImages.OptionValues", func(db *gorm.DB) *gorm.DB {
				return db.Order("priority ASC")
			}). //+
			Preload("ProductImages.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ProductImages.OptionValues.Image").         //+
			Preload("ProductImages.OptionValues.Image.Type").    //+
			Preload("ProductImages.OptionValues.Image.Options"). //+
			Preload("Items").
			Preload("Items.Order").
			Preload("Items.Order.Items").
			Preload("Items.Order.Status").
			Preload("Items.OptionValues").
			Preload("Items.Status").
			Preload("Items.Sale").
			Preload("Items.Currency").
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedMaterial", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedFeatures", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Brand.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Brand.Image").               //+
			Preload("Brand.Image.Type").          //+
			Preload("Brand.Image.Options").       //+
			Preload("Brand.Manufacturer").        //+
			Preload("Brand.Manufacturer.Brands"). //+
			Preload("Categories").
			Preload("Categories.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Categories.LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Categories.ImageBackground").
			Preload("Manufacturer").        //+
			Preload("Manufacturer.Brands"). //+
			Preload("Price").               //+
			Preload("ProductOptions").
			Preload("ProductOptions.Option").            //+
			Preload("ProductOptions.Option.OptionType"). //+
			Preload("ProductOptions.Option.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                            //+
			Preload("ProductOptions.Option.OptionValues"). //+
			Preload("ProductOptions.Option.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ProductOptions.Option.OptionValues.Image").         //+
			Preload("ProductOptions.Option.OptionValues.Image.Type").    //+
			Preload("ProductOptions.Option.OptionValues.Image.Options"). //+
			Preload("ProductOptions.OptionValues", func(db *gorm.DB) *gorm.DB {
				return db.Order("priority DESC")
			}). //+
			Preload("ProductOptions.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ProductOptions.OptionValues.Image").         //+
			Preload("ProductOptions.OptionValues.Image.Type").    //+
			Preload("ProductOptions.OptionValues.Image.Options"). //+
			Preload("ProductImages").                             //+
			Preload("ProductAvailability").                       //+
			Preload("ProductVisibility").
			First(product).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта product: %v", *product)
			return err
		} else {
			log.Infof("Попытка выбора объекта *product ID=%v", (*product).ID)
			return nil
		}

	// 	Product shadow
	case *ProductShadow:
		// тут структура для данных
		productShadow := item.(*ProductShadow)
		// 	попытка выбора
		if err := db.HandleDB.First(productShadow).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта productShadow: %v", *productShadow)
			return err
		} else {
			log.Infof("Попытка выбора объекта *productShadow ID=%v", (*productShadow).ID)
			return nil
		}

	// 	Purchase
	case *Purchase:
		// тут структура для данных
		purchase := item.(*Purchase)
		// 	попытка выбора
		if err := db.HandleDB.First(purchase).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта purchase: %v", *purchase)
			return err
		} else {
			log.Infof("Попытка выбора объекта *purchase ID=%v", (*purchase).ID)
			return nil
		}

	// 	Region
	case *Region:
		// тут структура для данныхPreload
		region := item.(*Region)
		// 	попытка выбора
		if err := db.HandleDB.
			Preload("Country").
			First(region).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта region: %v", *region)
			return err
		} else {
			log.Infof("Попытка выбора объекта *region ID=%v", (*region).ID)
			return nil
		}

	// 	Role
	case *Role:
		// тут структура для данных
		role := item.(*Role)
		// 	попытка выбора
		if err := db.HandleDB.First(role).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта role: %v", *role)
			return err
		} else {
			log.Infof("Попытка выбора объекта *role ID=%v", (*role).ID)
			return nil
		}

	// 	User
	case *User:
		// тут структура для данных
		user := item.(*User)
		// 	выборка
		if err := db.HandleDB.Find(user).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта user: %v", *user)
			return err
		} else {
			log.Infof("Попытка выбора объекта *user ID=%v", (*user).ID)
			return nil
		}

		// 	Zone
	case *Zone:
		// тут структура для данных
		zone := item.(*Zone)
		// 	выборка
		if err := db.HandleDB.
			Preload("Countries").
			Preload("Countries.Regions").
			Preload("Countries.Image").
			Preload("Countries.Image.Type").
			Preload("Countries.Image.Options").
			Preload("ZoneCosts").
			Find(zone).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе объекта zone: %v", *zone)
			return err
		} else {
			log.Infof("Попытка выбора объекта *zone %v", (*zone).ID)
			return nil
		}

	default:
		log.Errorf("Попытка выбора объекта неизвестного типа: %v", reflect.TypeOf(item))
		err := errors.New("Попытка выбора объекта неизвестного типа")
		return err

		// ****************************************************************
	}
}
