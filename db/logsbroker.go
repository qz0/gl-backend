package db

import (
	"encoding/json"
	"fmt"
	"runtime"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

//	SERVICES

// 	функция для обновления sitemap(Product/Category)
func (db *DataBase) CreateLog(item interface{}, request interface{}, action string, user string) error {
	// 	создаем переменную для хранения лога
	var crudLog Log

	// обработка цепочки вызова функции, для сохранения в логи всего пути выполнения изменения в бд, начиная с роутера.
	// запускаем цикл, за шаг перем не ноль, дабы исключить из цепочки логов эту функцию.
	// stepCount - максимальное количество шагов в цепочке, которое мы будем анализировать.
	for stepCount := 1; stepCount <= 10; stepCount++ {
		// получаем через рантайм указатель на положение stepCount в цепочке шаг наверх, а так же строку вызова внутри функции.
		pc, _, line, _ := runtime.Caller(stepCount)
		// получаем через указатель полный путь к функции записанный на этом шагу
		fullFuncPath := runtime.FuncForPC(pc).Name()
		// дробим строку через точку, что бы получить массив строк, в котором последним элементом останется имя функции.
		splitedFuncPath := strings.Split(fullFuncPath, ".")
		// выбираем имя функции из массива в новую переменную, для лёгкого чтения дальнейших строк.
		funcName := splitedFuncPath[len(splitedFuncPath)-1]
		// если за stepCount шагов мы дошли до роута, то заканчиваем сбор имён в цепочке функций.
		if funcName == "ServeHTTP" {
			break
		} else {
			// исключаем пустые имена функций, что периодически могут появляться если шаги ушли достаточно далеко.
			if funcName != "" {
				// сохраняем/дописываем в crudLog.FuncPath имя и строку вызова функции.
				crudLog.FuncPath += funcName + "(" + strconv.Itoa(line) + ") | "
			}
		}
	}
	// для удобства отладки, выводим полученную цепочку функций в консоль.
	log.Info(crudLog.FuncPath)
	//	тип действия
	var switcher interface{}
	if action == "delete" {
		switcher = item
	} else {
		switcher = request
	}

	switch switcher.(type) {
	// 	Assembly
	case *Assembly:
		logRequest := switcher.(*Assembly)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Assembly"
	// 	Address
	case *Address:
		logRequest := switcher.(*Address)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Address"
	//	Brand
	case *Brand:
		logRequest := switcher.(*Brand)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Brand"
	// 	Cart
	case *Cart:
		logRequest := switcher.(*Cart)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Cart"
	// 	Category
	case *Category:
		logRequest := switcher.(*Category)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Category"
	// 	Country
	case *Country:
		logRequest := switcher.(*Country)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Country"
	// 	Currency
	case *Currency:
		logRequest := switcher.(*Currency)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Currency"
	// 	Customer
	case *Customer:
		logRequest := switcher.(*Customer)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Customer"
	// 	Customer rank
	case *CustomerRank:
		logRequest := switcher.(*CustomerRank)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "CustomerRank"
	// 	Delivery
	case *Delivery:
		logRequest := switcher.(*Delivery)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Delivery"
	// 	Image
	case *Image:
		logRequest := switcher.(*Image)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Image"
	// 	Incoming
	case *Incoming:
		logRequest := switcher.(*Incoming)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Incoming"
	// 	Item
	case *Item:
		logRequest := switcher.(*Item)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Item"
	// 	Image
	case *ItemShadow:
		logRequest := switcher.(*ItemShadow)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "ItemShadow"
	// 	Language
	case *Language:
		logRequest := switcher.(*Language)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Language"
	// 	Language
	case *Localization:
		logRequest := switcher.(*Localization)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Localization"
	//	Manufacturer
	case *Manufacturer:
		logRequest := switcher.(*Manufacturer)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Manufacturer"
	// 	Option
	case *Option:
		logRequest := switcher.(*Option)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Option"
	//	OptionValue
	case *OptionValue:
		logRequest := switcher.(*OptionValue)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "OptionValue"
	// 	Options set
	case *OptionsSet:
		logRequest := switcher.(*OptionsSet)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "OptionsSet"
	// 	Order
	case *Order:
		logRequest := switcher.(*Order)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Order"
	// 	Payment
	case *Payment:
		logRequest := switcher.(*Payment)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Payment"
		// 	Product
	case *Product:
		logRequest := switcher.(*Product)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Product"
	// 	Product shadow
	case *ProductShadow:
		logRequest := switcher.(*ProductShadow)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "ProductShadow"
	// 	Purchase
	case *Purchase:
		logRequest := switcher.(*Purchase)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Purchase"
	// 	Region
	case *Region:
		logRequest := switcher.(*Region)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Region"
	// 	Role
	case *Role:
		logRequest := switcher.(*Role)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Role"
	// 	User
	case *User:
		logRequest := switcher.(*User)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "User"
	// 	Zone
	case *Zone:
		logRequest := switcher.(*Zone)
		crudLog.ObjectID = logRequest.ID
		crudLog.Type = "Zone"
	default:
		log.Warn("defauld in log modele")
		return nil
	}

	crudLog.Action = action
	crudLog.JsonRequest = jsonString(request)
	crudLog.Json = jsonString(item)
	if err = db.CreateItem(&crudLog); err != nil {
		log.Error("Ошибка при удалении объекта - не удалось создать лог : %+v", err)
		return err
	}
	log.Info("Лог cоздан для " + crudLog.Type + " " + action)
	return nil
}

func jsonString(targetJson interface{}) string {
	jsonString, err := json.Marshal(targetJson)
	if err != nil {
		fmt.Println(err)
		return err.Error()
	}
	return string(jsonString)
}
