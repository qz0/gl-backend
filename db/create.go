package db

import (
	"errors"
	"reflect"

	log "github.com/sirupsen/logrus"
)

// 	CreateItem - создание записи
func (db *DataBase) CreateItem(item interface{}) error {

	// 	разное поведение для разных структур
	switch item.(type) {
	//	*******************************************************************

	// 	Assembly
	case *Assembly:
		// тут структура для данных
		assembly := item.(*Assembly)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&assembly).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *assembly)
			return err
		} else {
			log.Warnf("Создан *assembly ID=%v", (*assembly).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Address
	case *Address:
		// тут структура для данных
		address := item.(*Address)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&address).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *address)
			return err
		} else {
			log.Warnf("Создан *address ID=%v", (*address).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Brand
	case *Brand:
		// тут структура для данных
		brand := item.(*Brand)
		image := &brand.Image
		locales := brand.LocalizedDescriptions
		manufacturer := brand.Manufacturer
		// 	пробуем создать брэндLocalizedDescriptions
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *brand)
			return err
		} else {
			// создаём брэнд, игнорируя мануфекчур, что бы дальше по коду сделать это индивидуально и не поменять данные в производителе
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&brand).Omit("Manufacturer").Create(&brand).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при создании объекта: %v", *brand)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить brand при создании brand %+v", brand)
				}
				return err
			}
			// пробуем создать ассоциацию с manufacturer если пришла не пустая
			if manufacturer.ID != 0 {
				if err := tx.Model(&brand).Association("Manufacturer").Append(manufacturer).Error; err != nil {
					log.Errorf("Не удалось добавить manufacturer %+v", manufacturer)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить manufacturer при удалении manufacturer %+v", manufacturer)
					}
					return err
				}
			}
			// пробуем создать картинку
			if err := tx.Model(&brand).Association("Image").Append(image).Error; err != nil {
				log.Errorf("Не удалось добавить картинку %+v", image)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить картинку при удалении картинки %+v", image)
				}
				return err
			} else {

				// пробую создать локаль для брэнда
				if err := tx.Model(&brand).Association("LocalizedDescriptions").Append(locales).Error; err != nil {
					log.Errorf("Не удалось добавить локаль %+v", locales)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при создании локали %+v", locales)
					}
					return err
				} else {

					// пробуем создать картинку
					if err := tx.Model(&brand).Association("Image").Append(image).Error; err != nil {
						log.Errorf("Не удалось добавить картинку %+v", image)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить картинку при удалении картинки %+v", image)
						}
						return err
					} else {
						log.Errorf("Картинка добавлена успешно %+v", image)
						// вносим изменения в базу
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при удалении объекта: %v", *brand)
							return err
						} else {
							log.Warnf("Создан *brand ID=%v", (*brand).ID)
							db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

	// 	Cart
	case *Cart:
		// тут структура для данных
		cart := item.(*Cart)
		// 	пробуем создать брэнд
		// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Model(&cart).Create(&cart).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *cart)
			return err
		} else {
			log.Warnf("Создан *cart ID=%v", (*cart).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Category
	case *Category:
		// тут структура для данных
		category := item.(*Category)
		image := &category.ImageBackground
		imageIcon := &category.ImageIcon
		locales := category.LocalizedDescriptions
		// 	пробуем создать брэнд
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *category)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&category).Create(&category).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при создании объекта: %v", *category)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить category при создании category %+v", category)
				}
				return err
			} else {
				// пробую создать локаль
				if err := tx.Model(&category).Association("LocalizedDescriptions").Append(locales).Error; err != nil {
					log.Errorf("Не удалось добавить локаль %+v", locales)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при создании локали %+v", locales)
					}
					return err
				} else {
					// пробуем создать картинку
					if err := tx.Model(&category).Association("ImageBackground").Append(image).Error; err != nil {
						log.Errorf("Не удалось добавить картинку %+v", image)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить картинку при удалении картинки %+v", image)
						}
						return err
					} else {
						// пробуем создать картинку
						if err := tx.Model(&category).Association("ImageIcon").Append(imageIcon).Error; err != nil {
							log.Errorf("Не удалось добавить картинку %+v", imageIcon)
							if err = tx.Rollback().Error; err != nil {
								log.Errorf("Не удалось откатить картинку при удалении картинки %+v", imageIcon)
							}
							return err
						} else {
							// вносим изменения в базу
							if err = tx.Commit().Error; err != nil {
								log.Errorf("Ошибка в транзакции при удалении объекта: %v", *category)
								return err
							} else {
								log.Warnf("Создан *category ID=%v", (*category).ID)
								db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
								return nil
							}
						}
					}
				}
			}
		}

	// 	Country
	case *Country:
		// тут структура для данных
		country := item.(*Country)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&country).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *country)
			return err
		} else {
			log.Warnf("Создан *country ID=%v", (*country).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Currency
	case *Currency:
		// тут структура для данных
		currency := item.(*Currency)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&currency).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *currency)
			return err
		} else {
			log.Warnf("Создан *currency ID=%v", (*currency).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Customer
	case *Customer:
		// тут структура для данных
		customer := item.(*Customer)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&customer).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *customer)
			return err
		} else {
			log.Warnf("Создан *customer ID=%v", (*customer).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Customer rank
	case *CustomerRank:
		// тут структура для данных
		customerRank := item.(*CustomerRank)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&customerRank).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *customerRank)
			return err
		} else {
			log.Warnf("Создан *customerRank ID=%v", (*customerRank).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Delivery
	case *Delivery:
		// тут структура для данных
		delivery := item.(*Delivery)
		image := &delivery.Image
		locales := delivery.LocalizedDescriptions
		// 	пробуем создать брэнд
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *delivery)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&delivery).Create(&delivery).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при создании объекта: %v", *delivery)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить delivery при создании delivery %+v", delivery)
				}
				return err
			} else {
				// пробую создать локаль
				if err := tx.Model(&delivery).Association("LocalizedDescriptions").Append(locales).Error; err != nil {
					log.Errorf("Не удалось добавить локаль %+v", locales)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при создании локали %+v", locales)
					}
					return err
				} else {

					// пробуем создать картинку
					if err := tx.Model(&delivery).Association("Image").Append(image).Error; err != nil {
						log.Errorf("Не удалось добавить картинку %+v", image)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить картинку при удалении картинки %+v", image)
						}
						return err
					} else {
						// вносим изменения в базу
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при удалении объекта: %v", *delivery)
							return err
						} else {
							log.Warnf("Создан *image ID=%v", (*image).ID)
							db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
						}
					}
				}
				return nil
			}
		}

	// 	Image
	case *Image:
		// тут структура для данных
		image := item.(*Image)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&image).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *image)
			return err
		} else {
			log.Warnf("Создан *image ID=%v", (*image).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Incoming
	case *Incoming:
		// тут структура для данных
		incoming := item.(*Incoming)
		image := &incoming.Scan

		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *incoming)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&incoming).Create(&incoming).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при создании объекта: %v", *incoming)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить incoming при создании incoming %+v", incoming)
				}
				return err
			} else {

				// пробуем создать картинку
				if err := tx.Model(&incoming).Association("Scan").Append(image).Error; err != nil {
					log.Errorf("Не удалось добавить картинку %+v", image)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить картинку при удалении картинки %+v", image)
					}
					return err
				} else {
					// пробуем коммит в бд
					if err = tx.Commit().Error; err != nil {
						log.Errorf("Ошибка в транзакции при создании объекта: %v", *incoming)
						return err
					} else {
						log.Warnf("Создан *incoming ID=%v", (*incoming).ID)
						db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
						return nil
					}
				}
			}

		}

	// 	Item
	case *Item:
		// тут структура для данных
		newItem := item.(*Item)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&newItem).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *newItem)
			return err
		} else {
			log.Warnf("Создан *item ID=%v", (*newItem).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Image
	case *ItemShadow:
		// тут структура для данных
		itemShadow := item.(*ItemShadow)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&itemShadow).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *itemShadow)
			return err
		} else {
			log.Warnf("Создан *itemShadow ID=%v", (*itemShadow).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Language
	case *Language:
		// тут структура для данных
		language := item.(*Language)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&language).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *language)
			return err
		} else {
			log.Warnf("Создан *language ID=%v", (*language).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

		// 	Language
	case *Localization:
		// тут структура для данных
		localization := item.(*Localization)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для создания %+v", *localization)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&localization).Create(&localization).Error; err != nil {
				log.Errorf("Ошибка при создании объекта: %v", *localization)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить localization при создании %+v", localization)
				}
				return err
			} else {
				log.Errorf("Попытка создания объекта: %v", *localization)
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при создании объекта: %v", *localization)
					return err
				} else {
					log.Warnf("Создан *localization ID=%v", (*localization).ID)
					db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
					return nil
				}
			}
		}

	case *Log:
		crudLog := item.(*Log)
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для создания %+v", *crudLog)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&crudLog).Create(&crudLog).Error; err != nil {
				log.Errorf("Ошибка при создании объекта: %v", *crudLog)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить localization при создании %+v", crudLog)
				}
				return err
			} else {
				// пытаемся внести изменения в базу
				if err = tx.Commit().Error; err != nil {
					log.Errorf("Ошибка в транзакции при создании объекта: %v", *crudLog)
					return err
				} else {
					log.Warnf("Создан *crudLog ID=%v", (*crudLog).ID)
					db.CreateLog(nil, item, "create", "Guest") // запись успешного создания в лог
					return nil
				}
			}
		}

	// 	Manufacturer
	case *Manufacturer:
		// тут структура для данных
		manufacturer := item.(*Manufacturer)
		brands := manufacturer.Brands
		// 	Создаём транзакцию для работы с базой данных.
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для создания %+v", *manufacturer)
			return err
		} else {
			// создаём обьект manufacturer, принудительно запрещая добавлять Brands самому
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&manufacturer).Omit("Brands").Create(&manufacturer).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при создании объекта: %v", *manufacturer)
				return err
			} else {
				// принудительно создаём ассоциацию с брендом для manufacturer
				if err := tx.Model(&manufacturer).Association("Brands").Append(brands).Error; err != nil {
					//	логи
					log.Errorf("Ошибка при обновлении ассоциации для объекта: %v", brands)
					return err
				} else {
					// пытаемся занести в базу новый manufacturer
					if err = tx.Commit().Error; err != nil {
						log.Errorf("Ошибка в транзакции при создании объекта: %v", *manufacturer)
						return err
					} else {
						log.Warnf("Создан *manufacturer ID=%v", (*manufacturer).ID)
						db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
						return nil
					}
				}
			}
		}

	// 	Option
	case *Option:
		// тут структура для данных
		option := item.(*Option)
		locales := option.LocalizedDescriptions
		optionValues := option.OptionValues
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для создания %+v", *option)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&option).Create(&option).Error; err != nil {
				log.Errorf("Не удалось создать option %+v", option)
				//	откат
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить option при создании %+v", locales)
				}
				return err
			} else {
				// пробую создать локаль
				if err := tx.Model(&option).Association("LocalizedDescriptions").Append(locales).Error; err != nil {
					log.Errorf("Не удалось добавить локаль %+v", locales)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить option при создании %+v", locales)
					}
					return err
				} else {
					// пробую создать локаль
					if err := tx.Model(&option).Association("OptionValues").Append(optionValues).Error; err != nil {
						log.Errorf("Не удалось добавить значения опций %+v", optionValues)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить option при создании %+v", locales)
						}
						return err
					} else {
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при создании объекта: %v", *option)
							return err
						} else {
							log.Warnf("Создан *option ID=%v", (*option).ID)
							db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

		// 	OptionValue
	case *OptionValue:
		// тут структура для данных
		optionValue := item.(*OptionValue)
		image := &optionValue.Image
		locales := optionValue.LocalizedDescriptions
		// 	пробуем создать брэнд
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *optionValue)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Create(&optionValue).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при создании объекта: %v", *optionValue)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить optionValue при создании optionValue %+v", optionValue)
				}
				return err
			} else {
				// пробую создать локаль
				if err := tx.Model(&optionValue).Association("LocalizedDescriptions").Append(locales).Error; err != nil {
					log.Errorf("Не удалось добавить локаль %+v", locales)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при создании локали %+v", locales)
					}
					return err
				} else {
					// пробуем создать картинку
					if err := tx.Model(&optionValue).Association("Image").Append(image).Error; err != nil {
						log.Errorf("Не удалось добавить картинку %+v", image)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить картинку при удалении картинки %+v", image)
						}
						return err
					} else {
						// пробуем записать изменения в базу
						if err = tx.Commit().Error; err != nil {
							log.Errorf("Ошибка в транзакции при создании объекта: %v", *optionValue)
							return err
						} else {
							log.Warnf("Создан *image ID=%v", (*image).ID)
							db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
							return nil
						}
					}
				}
			}
		}

	// 	Options set
	case *OptionsSet:
		// тут структура для данных
		optionsSet := item.(*OptionsSet)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&optionsSet).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *optionsSet)
			return err
		} else {
			log.Warnf("Создан *optionsSet ID=%v", (*optionsSet).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Order
	case *Order:
		// тут структура для данных
		order := item.(*Order)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&order).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *order)
			return err
		} else {
			log.Warnf("Попытка создания объекта *order ID=%v", (*order).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Payment
	case *Payment:
		// тут структура для данных
		payment := item.(*Payment)
		image := &payment.Image
		locales := payment.LocalizedDescriptions
		discounts := payment.Discounts
		// 	пробуем создать
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для создания %+v", *payment)
			return err
		} else {
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&payment).Create(&payment).Error; err != nil {
				//	логи
				log.Errorf("Ошибка при создании объекта: %v", *payment)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить payment при создании payment %+v", payment)
				}
				return err
			} else {
				// пробую создать локаль
				if err := tx.Model(&payment).Association("LocalizedDescriptions").Append(locales).Error; err != nil {
					log.Errorf("Не удалось добавить локаль %+v", locales)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить locales при создании locales %+v", locales)
					}
					return err
				} else {
					// пробуем создать скидки
					if err := tx.Model(&payment).Association("Discounts").Append(discounts).Error; err != nil {
						log.Errorf("Не удалось добавить discounts %+v", discounts)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить добавление картинки discounts %+v", discounts)
						}
						return err
					} else {
						// пробуем создать картинку
						if err := tx.Model(&payment).Association("Image").Append(image).Error; err != nil {
							log.Errorf("Не удалось добавить картинку %+v", image)
							if err = tx.Rollback().Error; err != nil {
								log.Errorf("Не удалось откатить картинку при добавлении картинки %+v", image)
							}
							return err
						} else {
							if err = tx.Commit().Error; err != nil {
								log.Errorf("Ошибка в транзакции при создании объекта: %v", *payment)
								return err
							} else {
								log.Warnf("Создан *image ID=%v", (*image).ID)
								db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
								return nil
							}
						}
					}
				}
			}
		}

	// 	Product
	case *Product:
		// тут структура для данных для создания продукта
		product := item.(*Product)
		imageFront := &product.ImageFront
		imageBack := &product.ImageBack
		locales := product.LocalizedDescriptions
		features := product.LocalizedFeatures
		materials := product.LocalizedMaterial
		categories := product.Categories
		productOptions := product.ProductOptions
		productImages := product.ProductImages
		//TODO CТЕРЕТЬ СЛЕДУЮЩИЕ 2 СТРОЧКИ! ЭТО ДЛЯ УДОБСТВА ТЕСТОВ
		{
			product.BrandID = 111
			product.ManufacturerID = 151
		}
		if tx := db.HandleDB.Begin(); tx.Error != nil {
			log.Errorf("Не удалось создать транзакцию для удаления %+v", *product)
			return err
		} else {
			// создаём продукт
			// cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
			if err := tx.Set("gorm:association_autoupdate", false).Model(&product).Omit("Categories", "Brand", "Manufacturer").Create(&product).Error; err != nil {
				log.Errorf("Ошибка при создании объекта: %v", *product)
				if err = tx.Rollback().Error; err != nil {
					log.Errorf("Не удалось откатить product при создании product %+v", product)
				}
				return err
			} else {
				// пробую создать локаль
				if err := tx.Model(&product).Association("LocalizedDescriptions").Append(locales).Error; err != nil {
					log.Errorf("Не удалось добавить локаль %+v", locales)
					if err = tx.Rollback().Error; err != nil {
						log.Errorf("Не удалось откатить локаль при создании локали %+v", locales)
					}
					return err
				} else {
					// пробую создать features
					if err := tx.Model(&product).Association("LocalizedFeatures").Replace(features).Error; err != nil {
						log.Errorf("Не удалось добавить LocalizedFeatures %+v", features)
						if err = tx.Rollback().Error; err != nil {
							log.Errorf("Не удалось откатить LocalizedFeatures при создании LocalizedFeatures %+v", features)
						}
						return err
					} else {
						// пробую создать materials
						if err := tx.Model(&product).Association("LocalizedMaterial").Replace(materials).Error; err != nil {
							log.Errorf("Не удалось добавить LocalizedMaterial %+v", materials)
							if err = tx.Rollback().Error; err != nil {
								log.Errorf("Не удалось откатить LocalizedMaterial при создании LocalizedMaterial %+v", materials)
							}
							return err
						} else {
							// пробуем создать ассоциацию с categories
							if err := tx.Model(&product).Association("Categories").Append(categories).Error; err != nil {
								log.Errorf("Не удалось добавить categories %+v", categories)
								if err = tx.Rollback().Error; err != nil {
									log.Errorf("Не удалось откатить categories при удалении categories %+v", categories)
								}
								return err
							} else {
								// пробуем создать ассоциацию с productOptions
								if err := tx.Model(&product).Association("ProductOptions").Append(productOptions).Error; err != nil {
									log.Errorf("Не удалось добавить ProductOptions %+v", productOptions)
									if err = tx.Rollback().Error; err != nil {
										log.Errorf("Не удалось откатить ProductOptions при удалении ProductOptions %+v", productOptions)
									}
									return err
								} else {
									if err := tx.Model(&product).Association("ProductImages").Append(productImages).Error; err != nil {
										log.Errorf("Не удалось добавить productImages %+v", productImages)
										if err = tx.Rollback().Error; err != nil {
											log.Errorf("Не удалось откатить productImages при удалении ProductOptions %+v", productImages)
										}
										return err
									} else {
										// пробуем создать ImageFront
										if err := tx.Model(&product).Association("ImageFront").Append(imageFront).Error; err != nil {
											log.Errorf("Не удалось добавить ImageFront %+v", imageFront)
											if err = tx.Rollback().Error; err != nil {
												log.Errorf("Не удалось откатить ImageFront при удалении ImageFront %+v", imageFront)
											}
											return err
										} else {
											// пробуем создать ImageBack
											if err := tx.Model(&product).Association("ImageBack").Append(imageBack).Error; err != nil {
												log.Errorf("Не удалось добавить ImageBack %+v", imageBack)
												if err = tx.Rollback().Error; err != nil {
													log.Errorf("Не удалось откатить ImageBack при удалении ImageBack %+v", imageBack)
												}
												return err
											} else {
												// комитим в базу данных изменения, если всё прошло без ошибок
												if err = tx.Commit().Error; err != nil {
													log.Errorf("Ошибка в транзакции при сооздании объекта: %v", *product)
													return err
												} else {
													log.Warnf("Создан *imageBack ID=%v", (*imageBack).ID)
													db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
													return nil
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	// 	Product shadow
	case *ProductShadow:
		// тут структура для данных
		productShadow := item.(*ProductShadow)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).
			Create(&productShadow).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *productShadow)
			return err
		} else {
			log.Warnf("Создан *productShadow ID=%v", (*productShadow).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Purchase
	case *Purchase:
		// тут структура для данных
		purchase := item.(*Purchase)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&purchase).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *purchase)
			return err
		} else {
			log.Warnf("Создан *purchase ID=%v", (*purchase).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Region
	case *Region:
		// тут структура для данных
		region := item.(*Region)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).
			Create(&region).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *region)
			return err
		} else {
			log.Warnf("Создан *region ID=%v", (*region).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Role
	case *Role:
		// тут структура для данных
		role := item.(*Role)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&role).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *role)
			return err
		} else {
			log.Warnf("Создан *role ID=%v", (*role).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	User
	case *User:
		// тут структура для данных
		user := item.(*User)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&user).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *user)
			return err
		} else {
			log.Warnf("Создан *user ID=%v", (*user).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	// 	Zone
	case *Zone:
		// тут структура для данных
		zone := item.(*Zone)
		//Пробуем создать. cоздаём с gorm:association_autoupdate для защиты существующих обьектов ассоциации от автоматических изменений.
		if err := db.HandleDB.Set("gorm:association_autoupdate", false).Create(&zone).Error; err != nil {
			//	логи
			log.Errorf("Ошибка при создании объекта: %v", *zone)
			return err
		} else {
			log.Warnf("Создан *zone ID=%v", (*zone).ID)
			db.CreateLog(nil, item, "create", "Guest") // запись успешного удаления в лог
			return nil
		}

	default:
		log.Errorf("Попытка создания объекта неизвестного типа: %v", reflect.TypeOf(item))
		err := errors.New("Попытка создания объекта неизвестного типа")
		return err

		//	*******************************************************************
	}
}
