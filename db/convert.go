package db

//	конвертеры

//	COUNTRY из длинного формата в короткий
func ConvertCountryLongToShort(country Country) CountryShort {
	//	присваеваем значения
	var countryShort = CountryShort{
		ID:     country.ID,
		Name:   country.Name,
		NameRu: country.NameRu,
	}
	return countryShort
}

//	IMAGE из длинного формата в короткий
func ConvertImageLongToShort(image Image) ImageShort {
	//	присваеваем значения
	var imageShort = ImageShort{
		ID:        image.ID,
		Name:      image.Name,
		URL:       image.URL,
		Type:      ConvertImageTypeLongToShort(image.Type),
		TypeID:    image.TypeID,
		Options:   image.Options,
		OptionsID: image.OptionsID,
		Priority:  image.Priority,
	}
	return imageShort
}

//	IMAGE TYPE из длинного формата в короткий
func ConvertImageTypeLongToShort(imageType ImageType) ImageTypeShort {
	//	присваеваем значения
	var imageTypeShort = ImageTypeShort{
		ID:   imageType.ID,
		Name: imageType.Name,
		Path: imageType.Path,
	}
	return imageTypeShort
}

//	locale description из длинного формата в короткий
func ConvertLocalDescLongToShort(ld LocalizedDescription) LocalizedDescriptionShort {
	//	присваеваем значения
	var ldShort = LocalizedDescriptionShort{
		ID:               ld.ID,
		Language:         ld.Language,
		Name:             ld.Name,
		Title:            ld.Title,
		Description:      ld.Description,
		DescriptionTwo:   ld.DescriptionTwo,
		DescriptionThree: ld.DescriptionThree,
		Keywords:         ld.Keywords,
	}
	return ldShort
}

//	locale feature из длинного формата в короткий
func ConvertLocalFeatureLongToShort(lf LocalizedFeature) LocalizedFeatureShort {
	//	присваеваем значения
	var lfShort = LocalizedFeatureShort{
		ID:       lf.ID,
		Language: lf.Language,
		Text:     lf.Text,
	}
	return lfShort
}

//	locale material из длинного формата в короткий
func ConvertLocalMaterialLongToShort(lm LocalizedMaterial) LocalizedMaterialShort {
	//	присваеваем значения
	var lmShort = LocalizedMaterialShort{
		ID:       lm.ID,
		Language: lm.Language,
		Text:     lm.Text,
	}
	return lmShort
}

//	locale text из длинного формата в короткий
func ConvertLocalTextLongToShort(lt LocalizedText) LocalizedTextShort {
	//	присваеваем значения
	var ltShort = LocalizedTextShort{
		ID:       lt.ID,
		Language: lt.Language,
		Text:     lt.Text,
	}
	return ltShort
}

//	страна из длинного формата в короткий
func ConvertLocalSEOLongToShort(lSEO LocalizedSEO) LocalizedSEOShort {
	//	присваеваем значения
	var lSeoShort = LocalizedSEOShort{
		ID:          lSEO.ID,
		Language:    lSEO.Language,
		Name:        lSEO.Name,
		Title:       lSEO.Title,
		Description: lSEO.Description,
		Keywords:    lSEO.Keywords,
	}
	return lSeoShort
}

//	PRODUCT из длинного формата в короткий
func ConvertProductLongToShort(product Product) ProductShort {
	//	localizedFeatures
	var localizedFeatures []LocalizedFeatureShort
	var localizedMaterials []LocalizedMaterialShort
	var localizedSEOs []LocalizedSEOShort
	var localizedDescriptions []LocalizedDescriptionShort
	//	переборы массивов
	for _, localizedFeature := range product.LocalizedFeatures {
		localizedFeatures = append(localizedFeatures, ConvertLocalFeatureLongToShort(localizedFeature))
	}
	for _, localizedMaterial := range product.LocalizedMaterial {
		localizedMaterials = append(localizedMaterials, ConvertLocalMaterialLongToShort(localizedMaterial))
	}
	for _, localizedDescription := range product.LocalizedDescriptions {
		localizedDescriptions = append(localizedDescriptions, ConvertLocalDescLongToShort(localizedDescription))
	}
	for _, localizedSEO := range product.LocalizedSEOs {
		localizedSEOs = append(localizedSEOs, ConvertLocalSEOLongToShort(localizedSEO))
	}

	//	присваеваем значения
	var countryShort = ProductShort{
		ID:           product.ID,
		ImageFront:   ConvertImageLongToShort(product.ImageFront),
		ImageFrontID: product.ImageFrontID,

		ImageBack:   ConvertImageLongToShort(product.ImageBack),
		ImageBackID: product.ImageBackID,

		Items: product.Items,

		ProductImages: product.ProductImages,

		LocalizedDescriptions:    localizedDescriptions,
		LocalizedDescriptionAKey: product.LocalizedDescriptionAKey,
		LocalizedFeatures:        localizedFeatures,
		LocalizedMaterial:        localizedMaterials,
		LocalizedSEOs:            localizedSEOs,

		SeoLink: product.SeoLink,

		Brand:   product.Brand,
		BrandID: product.BrandID,

		Manufacturer:   product.Manufacturer,
		ManufacturerID: product.ManufacturerID,

		Price:   ConvertProductPriceLongToShort(product.Price),
		PriceID: product.PriceID,

		Categories: product.Categories,

		Weight:            product.Weight,
		VolumeWeightX:     product.VolumeWeightX,
		VolumeWeightY:     product.VolumeWeightY,
		VolumeWeightZ:     product.VolumeWeightZ,
		Fstek:             product.Fstek,
		WarehousePosition: product.WarehousePosition,
		Article:           product.Article,

		Priority:       product.Priority,
		ProductOptions: product.ProductOptions,

		ProductAvailability:   ConvertProductAvailabilityLongToShort(product.ProductAvailability),
		ProductAvailabilityID: product.ProductAvailabilityID,

		ProductVisibility:   ConvertProductVisibilityLongToShort(product.ProductVisibility),
		ProductVisibilityID: product.ProductVisibilityID,

		Type: product.Type,
	}

	return countryShort
}

//	PRODUCT AVAILABILITY из длинного формата в короткий
func ConvertProductAvailabilityLongToShort(productAvailability ProductAvailability) ProductAvailabilityShort {
	//	присваеваем значения
	var productAvailabilityShort = ProductAvailabilityShort{
		ID:   productAvailability.ID,
		Name: productAvailability.Name,
	}
	return productAvailabilityShort
}

//	PRODUCT PRICE из длинного формата в короткий
func ConvertProductPriceLongToShort(productPrice ProductPrice) ProductPriceShort {
	//	присваеваем значения
	var imageTypeShort = ProductPriceShort{
		ID:   productPrice.ID,
		Buy:  productPrice.Buy,
		Sell: productPrice.Sell,
	}
	return imageTypeShort
}

//	PRODUCT VISIBILITY из длинного формата в короткий
func ConvertProductVisibilityLongToShort(productVisibility ProductVisibility) ProductVisibilityShort {
	//	присваеваем значения
	var productVisibilityShort = ProductVisibilityShort{
		ID:   productVisibility.ID,
		Name: productVisibility.Name,
	}
	return productVisibilityShort
}

//	регион из длинного формата в короткий
func ConvertRegionLongToShort(region Region) RegionShort {
	//	присваеваем значения
	var regionShort = RegionShort{
		ID:   region.ID,
		Name: region.Name,
		Code: region.Code,
	}
	return regionShort
}
