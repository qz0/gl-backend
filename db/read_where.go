package db

import (
	"errors"
	"reflect"

	"github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
)

//
// 	SelectAllItems - вывод всех записей
func (db *DataBase) SelectItemsWhere(items interface{}, offset int, limit uint, queryString string, queryArgs ...interface{}) error {

	// 	разное поведение для разных структур
	switch items.(type) {
	// ****************************************************************

	//Category
	case *[]Category:
		// Моя копипаста, но всё же вроде верно понял суть.
		categories := items.(*[]Category)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("ImageBackground").
			Preload("ImageBackground.Type").
			Preload("ImageBackground.Options").
			Preload("ImageIcon").
			Preload("ImageIcon.Type").
			Preload("ImageIcon.Options").
			Preload("Categories").
			Preload("Categories.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Categories.LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Products"). //+
			Preload("Products.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(categories).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *categories)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *categories (%d)", len(*categories))
			return nil
		}

	// 	Country
	case *[]Country:
		// тут структура для данных
		countries := items.(*[]Country)
		//log.Errorf("DELIVERY: %+v %+v", queryString, queryArgs)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Regions"). //+
			Where(queryString, queryArgs...).
			Find(countries).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *countries)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *countries (%d)", len(*countries))
			return nil
		}

	// 	Delivery
	case *[]Delivery:
		// тут структура для данных
		deliveries := items.(*[]Delivery)
		//log.Errorf("DELIVERY: %+v %+v", queryString, queryArgs)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Image").         //+
			Preload("Image.Type").    //+
			Preload("Image.Options"). //+
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Zones").                         //+
			Preload("Zones.Countries").               //+
			Preload("Zones.Countries.Image").         //+
			Preload("Zones.Countries.Image.Type").    //+
			Preload("Zones.Countries.Image.Options"). //+
			Preload("Zones.ZoneCosts").               //+
			Where(queryString, queryArgs...).
			Order("priority").
			//Where(queryString, queryArgs).
			//Offset(offset).
			//Limit(limit).
			Find(deliveries).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *deliveries)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *deliveries (%d)", len(*deliveries))
			return nil
		}

	// 	Item
	case *[]Item:
		// тут структура для данных
		itemStruct := items.(*[]Item)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("OptionValues").               //+
			Preload("OptionValues.Image").         //+
			Preload("OptionValues.Image.Type").    //+
			Preload("OptionValues.Image.Options"). //+
			Preload("OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Order"). //+
			// Preload("Order.UpdatedStatusAt").                              //+
			Preload("Order.Priority").                                     //+
			Preload("Order.Status").                                       //+
			Preload("Order.Customer").                                     //+
			Preload("Order.CreatedShippingAddress").                       //+
			Preload("Order.CreatedShippingAddress.Country").               //+
			Preload("Order.CreatedShippingAddress.Country.Image").         //+
			Preload("Order.CreatedShippingAddress.Country.Image.Type").    //+
			Preload("Order.CreatedShippingAddress.Country.Image.Options"). //+
			Preload("Order.CurrentShippingAddress").                       //+
			Preload("Order.CurrentShippingAddress.Country").               //+
			Preload("Order.CurrentShippingAddress.Country.Image").         //+
			Preload("Order.CurrentShippingAddress.Country.Image.Type").    //+
			Preload("Order.CurrentShippingAddress.Country.Image.Options"). //+
			Preload("Order.ShippingAddressShadow").                        //+
			Preload("Order.ShippingAddressShadow.Country").                //+
			Preload("Order.ShippingAddressShadow.Country.Image").          //+
			Preload("Order.ShippingAddressShadow.Country.Image.Type").     //+
			Preload("Order.ShippingAddressShadow.Country.Image.Options").  //+
			Preload("Order.Delivery.Image").                               //+
			Preload("Order.Delivery.Image.Type").                          //+
			Preload("Order.Delivery.Image.Options").                       //+
			Preload("Order.Delivery.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Order.Delivery.Zones").                         //+
			Preload("Order.Delivery.Zones.Countries").               //+
			Preload("Order.Delivery.Zones.Countries.Image").         //+
			Preload("Order.Delivery.Zones.Countries.Image.Type").    //+
			Preload("Order.Delivery.Zones.Countries.Image.Options"). //+
			Preload("Order.Delivery.Zones.ZoneCosts").               //+
			Preload("Order.Sale").                                   //+
			Preload("Order.ItemsShadow").                            //+
			Preload("Order.ItemsShadow.Order").                      //+
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(itemStruct).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *itemStruct)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов Item (%d)", len(*itemStruct))
			return nil
		}

	case *[]Log:
		// тут структура для языков
		crudLogs := items.(*[]Log)
		// 	пробуем выбрать
		if err := db.HandleDB.
			// Select("id, created_at, type, action, user, object_id").
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Order("id DESC").
			Find(crudLogs).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *crudLogs)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *crudLogs (%d) : ", len(*crudLogs))
			return nil
		}

	// 	Option
	case *[]Option:
		// тут структура для данных
		options := items.(*[]Option)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("OptionType"). //+
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                    //+
			Preload("OptionValues").               //+
			Preload("OptionValues.Image").         //+
			Preload("OptionValues.Image.Type").    //+
			Preload("OptionValues.Image.Options"). //+
			Preload("OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("OptionValues.Option").
			Preload("OptionValues.Option.OptionType").
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(options).
			Order("priority").
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *options (%d)", len(*options))
			return nil
		}

		// 	OptionValue +++
	case *[]OptionValue:
		// тут структура для данных
		optionsValue := items.(*[]OptionValue)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Image").         //+
			Preload("Image.Type").    //+
			Preload("Image.Options"). //+
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Option").
			Preload("Option.OptionType").
			Preload("Option.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(optionsValue).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *optionsValue)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *optionsValue (%d)", len(*optionsValue))
			return nil
		}

		//OptionsSet
	case *[]OptionsSet:
		// тут структура для данных
		optionsSet := items.(*[]OptionsSet)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(optionsSet).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *optionsSet)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *optionsSet (%d)", len(*optionsSet))
			return nil
		}

	// 	Order
	case *[]Order:
		// тут структура для данных
		orders := items.(*[]Order)
		// 	пробуем выбрать
		if err := db.HandleDB.
			//Preload("UpdatedStatusAt").                              //+
			Preload("Priority").                                     //+
			Preload("Status").                                       //+
			Preload("Customer").                                     //+
			Preload("CreatedShippingAddress").                       //+
			Preload("CreatedShippingAddress.Country").               //+
			Preload("CreatedShippingAddress.Country.Image").         //+
			Preload("CreatedShippingAddress.Country.Image.Type").    //+
			Preload("CreatedShippingAddress.Country.Image.Options"). //+
			Preload("CurrentShippingAddress").                       //+
			Preload("CurrentShippingAddress.Country").               //+
			Preload("CurrentShippingAddress.Country.Image").         //+
			Preload("CurrentShippingAddress.Country.Image.Type").    //+
			Preload("CurrentShippingAddress.Country.Image.Options"). //+
			Preload("ShippingAddressShadow").                        //+
			Preload("ShippingAddressShadow.Country").                //+
			Preload("ShippingAddressShadow.Country.Image").          //+
			Preload("ShippingAddressShadow.Country.Image.Type").     //+
			Preload("ShippingAddressShadow.Country.Image.Options").  //+
			Preload("Payment").                                      //+
			Preload("Payment.Discounts").                            //+
			Preload("Payment.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Delivery").               //+
			Preload("Delivery.Image").         //+
			Preload("Delivery.Image.Type").    //+
			Preload("Delivery.Image.Options"). //+
			Preload("Delivery.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                                //+
			Preload("Delivery.Zones").                         //+
			Preload("Delivery.Zones.Countries").               //+
			Preload("Delivery.Zones.Countries.Image").         //+
			Preload("Delivery.Zones.Countries.Image.Type").    //+
			Preload("Delivery.Zones.Countries.Image.Options"). //+
			Preload("Delivery.Zones.ZoneCosts").               //+
			Preload("Sale").                                   //+
			Preload("ItemsShadow").                            //+
			Preload("ItemsShadow.Order").                      //+
			Preload("Items").                                  //+
			Preload("CartItems").                              //+
			//Where("status_id = ?", 5).
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(orders).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *orders)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *orders (%d)", len(*orders))
			return nil
		}

	// 	Payment
	case *[]Payment:
		// тут структура для данных
		payments := items.(*[]Payment)
		//log.Errorf("Payment: %+v %+v", queryString, queryArgs)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("Image").         //+
			Preload("Image.Type").    //+
			Preload("Image.Options"). //+
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}). //+
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(payments).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *payments)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *payments (%d) ", len(*payments))
			return nil
		}

		//Product
	case *[]Product:
		// Моя копипаста, но всё же вроде верно понял суть.
		products := items.(*[]Product)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Preload("ImageFront").         //+
			Preload("ImageFront.Type").    //+
			Preload("ImageFront.Options"). //+
			Preload("ImageBack").          //+
			Preload("ImageBack.Type").     //+
			Preload("ImageBack.Options").  //+
			Preload("ProductImages").
			Preload("ProductImages.Image").         //+
			Preload("ProductImages.Image.Type").    //+
			Preload("ProductImages.Image.Options"). //+
			Preload("ProductImages.OptionValues").  //+
			Preload("ProductImages.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                                  //+
			Preload("ProductImages.OptionValues.Image").         //+
			Preload("ProductImages.OptionValues.Image.Type").    //+
			Preload("ProductImages.OptionValues.Image.Options"). //+
			Preload("Items").
			Preload("Items.Order").
			Preload("Items.Order.Items").
			Preload("Items.Order.Status").
			Preload("Items.OptionValues").
			Preload("Items.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Items.Status").
			Preload("Items.Sale").
			Preload("Items.Currency").
			Preload("LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}). //+
			Preload("LocalizedSEOs", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedMaterial", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("LocalizedFeatures", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Brand"). //+
			Preload("Brand.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                   //+
			Preload("Brand.Image").               //+
			Preload("Brand.Image.Type").          //+
			Preload("Brand.Image.Options").       //+
			Preload("Brand.Manufacturer").        //+
			Preload("Brand.Manufacturer.Brands"). //+
			Preload("Categories").
			Preload("Categories.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).
			Preload("Manufacturer").        //+
			Preload("Manufacturer.Brands"). //+
			Preload("Price").               //+
			Preload("ProductOptions").
			Preload("ProductOptions.Option").            //+
			Preload("ProductOptions.Option.OptionType"). //+
			Preload("ProductOptions.Option.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                            //+
			Preload("ProductOptions.Option.OptionValues"). //+
			Preload("ProductOptions.Option.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                                          //+
			Preload("ProductOptions.Option.OptionValues.Image").         //+
			Preload("ProductOptions.Option.OptionValues.Image.Type").    //+
			Preload("ProductOptions.Option.OptionValues.Image.Options"). //+
			Preload("ProductOptions.OptionValues").                      //+
			Preload("ProductOptions.OptionValues.LocalizedDescriptions", func(db *gorm.DB) *gorm.DB {
				return db.Order("language ASC")
			}).                                                   //+
			Preload("ProductOptions.OptionValues.Image").         //+
			Preload("ProductOptions.OptionValues.Image.Type").    //+
			Preload("ProductOptions.OptionValues.Image.Options"). //+
			Preload("ProductImages").                             //+
			Preload("ProductAvailability").                       //+
			Preload("ProductVisibility").
			Order("priority DESC").
			Where(queryString, queryArgs...).
			Offset(offset).
			Limit(limit).
			Find(products).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *products)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *products (%d)", len(*products))
			return nil
		}

	// 	Region
	case *[]Region:
		// тут структура для данных
		regions := items.(*[]Region)
		//log.Errorf("DELIVERY: %+v %+v", queryString, queryArgs)
		// 	пробуем выбрать
		if err := db.HandleDB.
			Where(queryString, queryArgs...).
			Find(regions).
			Error; err != nil {
			//	логи
			log.Errorf("Ошибка при выборе с условием объектов: %+v %+v", err, *regions)
			return err
		} else {
			log.Infof("Попытка выбора с условием объектов *regions (%d)", len(*regions))
			return nil
		}

	default:
		log.Errorf("Попытка выбора c условием объектов неизвестного типа: %v", reflect.TypeOf(items))
		err := errors.New("Попытка выбора c условием объектов неизвестного типа")
		return err

		// ****************************************************************
	}
}
