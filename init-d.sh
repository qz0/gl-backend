#!/bin/sh
### BEGIN INIT INFO
# Provides:          backend
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       Backend for grey-line.com
### END INIT INFO

APP_NAME="greyline backend"
MY_PATH=/opt/greyline
DAEMON=backend
DAEMON_OPTS=""
#USER=greyline
USER=root

PID_FILE=/var/run/backend.pid
LOG_FILE=backend.log

# START
start() {
  touch /opt/greyline/backend.pid
  cd $MY_PATH
#  sudo -u $USER -b ./$DAEMON
  ./$DAEMON &
  echo "Try to start $APP_NAME "
  sleep 4
  echo "$APP_NAME has started"
  exit
}


# STOP
stop() {
  echo "Try to stop $APP_NAME "
  if pgrep -u $USER -x $DAEMON && kill -0 $(pgrep -u $USER -x $DAEMON); then
    kill $(pgrep -u $USER -x $DAEMON)
    echo "$APP_NAME has killed"
  else
    echo "Cant find $DAEMON process"
  fi
}

# STATUS
status() {
  if pgrep -u $USER -x $DAEMON && kill -0 $(pgrep -u $USER -x $DAEMON); then
    echo "$APP_NAME is running"
  else
    echo "$APP_NAME isn't running"
  fi
}


# UNINSTALL
uninstall() {
  echo -n "Are you really sure you want to uninstall this service? That cannot be undone. [yes|No] "
  local SURE
  read SURE
  if [ "$SURE" = "yes" ]; then
    stop
    rm -f "$PIDFILE"
    echo "Notice: log file is not be removed: '$LOGFILE'" >&2
    update-rc.d -f backend remove
    rm -fv "$0"
  fi
}

# SWITCH
case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  uninstall)
    uninstall
    ;;
  restart)
    stop
    start
    exit 1
    ;;
  status)
    status
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|uninstall|status}"
    exit 1
    ;;
esac