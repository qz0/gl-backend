#!/bin/bash
ssh -i ~/.ssh/backend_id_rsa greyline@ssh.grey-line.com -p 9022 "sudo service backend stop"

scp -i ~/.ssh/backend_id_rsa -P 9022 backend greyline@ssh.grey-line.com:/opt/greyline

#scp -i ~/.ssh/backend_id_rsa -P 9022 init-d.sh greyline@ssh.grey-line.com:/opt/greyline

ssh -i ~/.ssh/backend_id_rsa greyline@ssh.grey-line.com -p 9022 "sudo service backend start &"
