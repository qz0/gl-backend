package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var LogRoutes = []Route{
	//	сами функции
	Route{
		"createLog",
		"Создаем объект",
		"PUT",
		"/logs/create",
		createLog,
		//validateMiddleware(createItem),
	},
	Route{
		"deleteLog",
		"Удаляем объект",
		"DELETE",
		"/logs/delete",
		deleteLog,
		//validateMiddleware(deleteItem),
	},
	Route{
		"getAllLogs",
		"Получаем список всех объектов",
		"GET",
		"/logs/read_all",
		getAllLogs,
		//validateMiddleware(getAllItems),
	},
	Route{
		"getCurrentLog",
		"Получаем конкретный объект",
		"GET",
		"/logs/read_current",
		getCurrentLog,
		//validateMiddleware(getCurrentItem),
	},
	Route{
		"getLogWhere",
		"Получаем конкретный объект",
		"GET",
		"/logs/get_log_by_date",
		getLogWhere,
		//validateMiddleware(getCurrentItem),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/logs/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/logs/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/logs/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/logs/read_current",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func getLogWhere(w http.ResponseWriter, r *http.Request) {
	// 	создаем переменные
	var logs []db.Log

	//	подключаем хранилище
	tempStorage := storage

	startDate := r.URL.Query().Get("start_date")
	finalDate := r.URL.Query().Get("final_date")
	logType := r.URL.Query().Get("type")
	id := r.URL.Query().Get("id")
	itemID, _ := strconv.Atoi(id)

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if itemID != 0 {
		log.Info("itemID:", itemID)
		if logType != "" {
			if finalDate == "" {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ? and type = ? and object_id = ?", fmt.Sprint(startDate), fmt.Sprint(logType), fmt.Sprint(itemID)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			} else {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ? and created_at <= ? and type = ? and object_id = ?", fmt.Sprint(startDate), fmt.Sprint(finalDate), fmt.Sprint(logType), fmt.Sprint(itemID)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			}
		} else {
			if finalDate == "" {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ? and object_id = ?", fmt.Sprint(startDate), fmt.Sprint(itemID)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			} else {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ? and created_at <= ? and object_id = ?", fmt.Sprint(startDate), fmt.Sprint(finalDate), fmt.Sprint(itemID)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			}
		}
	} else {
		if logType != "" {
			if finalDate == "" {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ? and type = ?", fmt.Sprint(startDate), fmt.Sprint(logType)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			} else {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ? and created_at <= ? and type = ?", fmt.Sprint(startDate), fmt.Sprint(finalDate), fmt.Sprint(logType)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			}
		} else {
			if finalDate == "" {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ?", fmt.Sprint(startDate)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			} else {
				if err := tempStorage.SelectItemsWhere(&logs, 0, 10, "created_at >= ? and created_at <= ?", fmt.Sprint(startDate), fmt.Sprint(finalDate)); err != nil {
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(logs)
				}
			}
		}
	}
}

func createLog(w http.ResponseWriter, r *http.Request) {
	//	создаем переменные
	var crudLog db.Log
	var logs []db.Log

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&crudLog); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", crudLog)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&crudLog); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&logs); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(logs)
	}
}

// 	Удаляем объект
func deleteLog(w http.ResponseWriter, r *http.Request) {
	// 	создаем переменные
	var crudLog db.Log
	var logs []db.Log

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	logID := r.URL.Query().Get("id")

	// 	имя получателя
	if logID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(logID)
		crudLog.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&crudLog); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&crudLog); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	logID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&logs); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(logs)
	}
}

// 	Получаем список всех объектов
func getAllLogs(w http.ResponseWriter, r *http.Request) {
	// 	создаем переменные
	var logs []db.Log

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки и проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&logs); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(logs)
	}
}

// 	Получаем объект по ИД
func getCurrentLog(w http.ResponseWriter, r *http.Request) {
	// 	создаем переменные
	var crudLog db.Log

	//	подключаем хранилище
	tempStorage := storage

	logID := r.URL.Query().Get("id")
	log.Errorf(logID)
	// 	имя получателя
	if logID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(logID)
		crudLog.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&crudLog); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(crudLog)
		}
	}
}

// 	Изменяем объект
func updateLog(w http.ResponseWriter, r *http.Request) {
	// 	создаем переменные
	var crudLog db.Log
	var tempLog db.Log
	var logs []db.Log

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&crudLog); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", crudLog)
		//	сохраняем ИД для поиска
		tempLog.ID = crudLog.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempLog); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&crudLog); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&logs); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(logs)
	}
}
