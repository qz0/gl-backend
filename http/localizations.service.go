package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/archi-chester/backend/db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var LocalizationRoutes = []Route{
	//	сами функции
	Route{
		"createLocalization",
		"Создаем объект",
		"PUT",
		"/localizations/create",
		createLocalization,
		//validateMiddleware(createLocalization),
	},
	Route{
		"deleteLocalization",
		"Удаляем объект",
		"DELETE",
		"/localizations/delete",
		deleteLocalization,
		//validateMiddleware(deleteLocalization),
	},
	Route{
		"getAllLocalization",
		"Получаем список всех объектов",
		"GET",
		"/localizations/read_all",
		getAllLocalizations,
		//validateMiddleware(getAllLocalization),
	},
	Route{
		"getCurrentLocalization",
		"Получаем конкретный объект",
		"GET",
		"/localizations/read_current",
		getCurrentLocalization,
		//validateMiddleware(getCurrentLocalization),
	},
	Route{
		"updateLocalization",
		"Изменяем объект",
		"PATCH",
		"/localizations/update",
		updateLocalization,
		//validateMiddleware(updateLocalization),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/localizations/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/localizations/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/localizations/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/localizations/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/localizations/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createLocalization(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createlocalization")

	//	создаем переменные
	var localization db.Localization
	var localizations []db.Localization

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&localization); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", localization)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&localization); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&localizations); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(localizations)
	}
}

// 	Удаляем объект
func deleteLocalization(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deletelocalization")

	// 	создаем переменные
	var localization db.Localization
	//var templocalization db.localization
	var localizations []db.Localization

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	localizationID := r.URL.Query().Get("id")

	// 	имя получателя
	if localizationID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(localizationID)
		localization.ID = uint(id)
		//templocalization.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&localization); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&localization); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	localizationID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&localizations); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(localizations)
	}
}

// 	Получаем список всех объектов
func getAllLocalizations(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAlllocalizations")

	// 	создаем переменные
	var localizations []db.Localization

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&localizations); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(localizations)
	}
}

// 	Получаем объект по ИД
func getCurrentLocalization(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentlocalization")

	// 	создаем переменные
	var localization db.Localization

	//	подключаем хранилище
	tempStorage := storage

	localizationID := r.URL.Query().Get("id")

	// 	имя получателя
	if localizationID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(localizationID)
		localization.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&localization); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(localization)
		}
	}
}

// 	Изменяем объект
func updateLocalization(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updatelocalization")

	// 	создаем переменные
	var localization db.Localization
	var tempLocalization db.Localization
	var localizations []db.Localization

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&localization); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", localization)
		//	сохраняем ИД для поиска
		tempLocalization.ID = localization.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempLocalization); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&localization); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempLocalization, &localization, "update", "Guest") // запись успешного удаления в лог
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&localizations); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(localizations)
	}
}
