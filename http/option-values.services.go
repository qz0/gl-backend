package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var OptionValueRoutes = []Route{
	//	сами функции
	Route{
		"changePriorityOfOptionValue",
		"Изменяем приоритет объекта",
		"PATCH",
		"/option_values/change_priority",
		changePriorityOfOptionValue,
		//validateMiddleware(changePriorityOfOption),
	},
	Route{
		"createOptionValue",
		"Создаем объект",
		"PUT",
		"/option_values/create",
		createOptionValue,
		//validateMiddleware(createOptionValue),
	},
	Route{
		"deleteOptionValue",
		"Удаляем объект",
		"DELETE",
		"/option_values/delete",
		deleteOptionValue,
		//validateMiddleware(deleteOptionValue),
	},
	Route{
		"getAllOptionValues",
		"Получаем список всех объектов",
		"GET",
		"/option_values/read_all",
		getAllOptionValues,
		//validateMiddleware(getAllOptionValues),
	},
	Route{
		"getCurrentOptionValue",
		"Получаем конкретный объект",
		"GET",
		"/option_values/read_current",
		getCurrentOptionValue,
		//validateMiddleware(getCurrentOptionValue),
	},
	Route{
		"updateOptionValue",
		"Изменяем объект",
		"PATCH",
		"/option_values/update",
		updateOptionValue,
		//validateMiddleware(updateOptionValue),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/option_values/change_priority",
		statusOK,
	},
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/option_values/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/option_values/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/option_values/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/option_values/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/option_values/update",
		statusOK,
	},
}

//	SERVICES

func changePriorityOfOptionValue(w http.ResponseWriter, r *http.Request) {
	// моя копипаста, но вроде верно понял суть.
	// 	создаем переменные
	var optionValue db.OptionValue
	var tempOptionValue db.OptionValue
	var optionValues []db.OptionValue

	//	подключаем хранилище
	tempStorage := storage
	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&optionValue); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", optionValue)
		//	сохраняем ИД для поиска
		tempOptionValue.ID = optionValue.ID
	}
	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOptionValue); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	} else {
		log.Warn("Запись есть")

	}

	tempID := tempOptionValue.Option.ID
	//	выясняем в какую сторону перемещение
	if optionValue.Priority < tempOptionValue.Priority {
		//	число приоритета уменьшили
		//	получаем список доставок где приоритет выше

		if err := tempStorage.SelectItemsWhere(&optionValues, 0, 10, "priority >= ? and priority < ? and option_id = ?", fmt.Sprint(optionValue.Priority), fmt.Sprint(tempOptionValue.Priority), fmt.Sprint(tempID)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			log.Warn(optionValue.Option.ID)
			//	изменяем там приоритеты
			for index, currentOptionValue := range optionValues {
				// 	прибавляем к приоритету единицу
				currentOptionValue.Priority = currentOptionValue.Priority + 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentOptionValue); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err),
						http.StatusBadRequest)
				} else {
					log.Warnf("Updated OptionValue: %v", currentOptionValue)
				}
			}
		}
	} else {
		//	число приоритета такое же или увеличено
		//	получаем список доставок где приоритет ниже
		if err := tempStorage.SelectItemsWhere(&optionValues, 0, 10, "priority <= ? and priority > ? and option_id = ?",
			fmt.Sprint(optionValue.Priority), fmt.Sprint(tempOptionValue.Priority), fmt.Sprint(tempID)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentOptionValue := range optionValues {
				// 	прибавляем к приоритету единицу
				currentOptionValue.Priority = currentOptionValue.Priority - 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentOptionValue); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
				} else {
					log.Infof("Updated OPTIONVALUE: %v", currentOptionValue)
				}
			}
		}
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	log.Warn(tempOptionValue.Option.ID)
	if err := tempStorage.UpdateItem(&optionValue); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден_: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку

	if err := tempStorage.SelectItemsWhere(&optionValues, 0, 10, "option_id = ?",
		fmt.Sprint(tempID)); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionValues)
	}
}

// 	добавляем объект
func createOptionValue(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createOptionValue")

	//	создаем переменные
	var optionValue db.OptionValue
	var optionValues []db.OptionValue

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&optionValue); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", optionValue)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&optionValue); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionValues); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionValues)
	}
}

// 	Удаляем объект
func deleteOptionValue(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteOptionValue")

	// 	создаем переменные
	var optionValue db.OptionValue
	//var tempOptionValue db.OptionValue
	var optionValues []db.OptionValue

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	optionValueID := r.URL.Query().Get("id")

	// 	имя получателя
	if optionValueID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(optionValueID)
		optionValue.ID = uint(id)
		//tempOptionValue.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&optionValue); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&optionValue); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	optionValueID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionValues); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionValues)
	}
}

// 	Получаем список всех объектов
func getAllOptionValues(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOptionValues")

	// 	создаем переменные
	var optionValues []db.OptionValue

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionValues); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionValues)
	}
}

// 	Получаем объект по ИД
func getCurrentOptionValue(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentOptionValue")

	// 	создаем переменные
	var optionValue db.OptionValue

	//	подключаем хранилище
	tempStorage := storage

	optionValueID := r.URL.Query().Get("id")

	// 	имя получателя
	if optionValueID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(optionValueID)
		optionValue.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&optionValue); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(optionValue)
		}
	}
}

// 	Изменяем объект
func updateOptionValue(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateOptionValue")

	// 	создаем переменные
	var optionValue db.OptionValue
	var tempOptionValue db.OptionValue
	var optionValues []db.OptionValue

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&optionValue); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", optionValue)
		//	сохраняем ИД для поиска
		tempOptionValue.ID = optionValue.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOptionValue); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&optionValue); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempOptionValue, &optionValue, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionValues); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionValues)
	}
}
