package http

//
//import (
//	"encoding/json"
//	"fmt"
//	"io"
//	"net/http"
//	"os"
//	"strconv"
//
//	"github.com/archi-chester/backend/db"
//
//	log "github.com/sirupsen/logrus"
//)
//
//// 	массив роутинга
//var ImageDeliveryRoutes = []Route{
//	//	сами функции
//	Route{
//		"createImageDelivery",
//		"Создаем объект",
//		"PUT",
//		"/images/deliveries/create",
//		createImageDelivery,
//		//validateMiddleware(createImageDelivery),
//	},
//	Route{
//		"deleteImageDelivery",
//		"Удаляем объект",
//		"DELETE",
//		"/images/deliveries/delete",
//		deleteImageDelivery,
//		//validateMiddleware(deleteImageDelivery),
//	},
//	Route{
//		"getAllImageDeliveries",
//		"Получаем список всех объектов",
//		"GET",
//		"/images/deliveries/read_all",
//		getAllImageDeliveries,
//		//validateMiddleware(getAllImageDeliveries),
//	},
//	Route{
//		"getCurrentImageDelivery",
//		"Получаем конкретный объект",
//		"GET",
//		"/images/deliveries/read_current",
//		getCurrentImageDelivery,
//		//validateMiddleware(getCurrentImageDelivery),
//	},
//	Route{
//		"updateImageDelivery",
//		"Изменяем объект",
//		"PATCH",
//		"/images/deliveries/update",
//		updateImageDelivery,
//		//validateMiddleware(updateImageDelivery),
//	},
//	Route{
//		"uploadImageDelivery",
//		"Добавляем файл в репозиторий",
//		"POST",
//		"/images/deliveries/upload",
//		uploadImageDelivery,
//		//validateMiddleware(getAllCurrencies),
//	},
//	//	STATUS OK
//	Route{
//		"statusOK",
//		"Создаем объект",
//		"OPTIONS",
//		"/images/deliveries/create",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Удаляем объект",
//		"OPTIONS",
//		"/images/deliveries/delete",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем все объекты",
//		"OPTIONS",
//		"/images/deliveries/read_all",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем конкретный объект",
//		"OPTIONS",
//		"/images/deliveries/read_current",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Изменяем объект",
//		"OPTIONS",
//		"/images/deliveries/update",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Добавляем файл",
//		"OPTIONS",
//		"/images/deliveries/upload",
//		statusOK,
//	},
//}
//
////	SERVICES
//
//// 	добавляем объект
//func createImageDelivery(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("createImageDelivery")
//
//	//	создаем переменные
//	var imageDelivery db.ImageDelivery
//	var imageDeliveries []db.ImageDelivery
//
//	// 	переменные
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&imageDelivery); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", imageDelivery)
//	}
//
//	// 	дергаем функцию создания
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.CreateItem(&imageDelivery); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageDeliveries); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageDeliveries)
//	}
//}
//
//// 	Удаляем объект
//func deleteImageDelivery(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("deleteImageDelivery")
//
//	// 	создаем переменные
//	var imageDelivery db.ImageDelivery
//	//var tempImageDelivery db.ImageDelivery
//	var imageDeliveries []db.ImageDelivery
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	получаем параметр
//	imageDeliveryID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageDeliveryID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageDeliveryID)
//		imageDelivery.ID = uint(id)
//		//tempImageDelivery.ID = uint(id)
//
//		//	проверяем, что такая запись есть
//		if err := tempStorage.SelectItem(&imageDelivery); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		} else {
//			// 	дергаем функцию удаления
//			if err = tempStorage.DeleteItem(&imageDelivery); err != nil {
//				//	вернули ошибку - возвращаем 400
//				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
//				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
//			} else {
//				//  все нормально - возвращаем 200
//				w.WriteHeader(200)
//			}
//		}
//	} else {
//		//	imageDeliveryID is empty
//		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageDeliveries); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageDeliveries)
//	}
//}
//
//// 	Получаем список всех объектов
//func getAllImageDeliveries(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getAllImageDeliveries")
//
//	// 	создаем переменные
//	var imageDeliveries []db.ImageDelivery
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageDeliveries); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageDeliveries)
//	}
//}
//
//// 	Получаем объект по ИД
//func getCurrentImageDelivery(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getCurrentImageDelivery")
//
//	// 	создаем переменные
//	var imageDelivery db.ImageDelivery
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	imageDeliveryID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageDeliveryID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageDeliveryID)
//		imageDelivery.ID = uint(id)
//		// 	возвращаем нотисы
//		if err := tempStorage.SelectItem(&imageDelivery); err != nil {
//			//	вернули ошибку - возвращаем 404
//			log.Errorf("Ошибка при выборе объекта: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
//			return
//		} else {
//			//  все нормально - возвращаем 200
//			w.WriteHeader(200)
//			//	возвращаем полученные объекты
//			_ = json.NewEncoder(w).Encode(imageDelivery)
//		}
//	}
//}
//
//// 	Изменяем объект
//func updateImageDelivery(w http.ResponseWriter, r *http.Request) {
//	//	log
//	log.Warn("updateImageDelivery")
//
//	// 	создаем переменные
//	var imageDelivery db.ImageDelivery
//	var tempImageDelivery db.ImageDelivery
//	var imageDeliveries []db.ImageDelivery
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&imageDelivery); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", imageDelivery)
//		//	сохраняем ИД для поиска
//		tempImageDelivery.ID = imageDelivery.ID
//	}
//
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempImageDelivery); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&imageDelivery); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageDeliveries); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageDeliveries)
//	}
//}
//
//// 	uploadImageDelivery - изменяем категорию
//func uploadImageDelivery(w http.ResponseWriter, r *http.Request) {
//
//	log.Warn("uploadImageDelivery")
//	// 	создаем переменные
//
//	log.Infof("%v", r)
//	fmt.Println("Метод ПОСТ")
//	err := r.ParseMultipartForm(200000)
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//	log.Infof("%v", r)
//
//	// они все тут
//	formdata := r.MultipartForm // ok, no problem so far, read the Form data
//
//	//get the *fileheaders
//	files := formdata.File["myFile"] // grab the filenames
//	//var fileHeader multipart.FileHeader
//
//	log.Warnf("files: %v", files)
//	for key, _ := range files {
//		log.Warnf("file: %v", key)
//		//file, _ := f[key].Open()
//		log.Warn("Success...")
//		//defer file.Close()
//
//	}
//	fmt.Println(len(files))
//
//	file, handler, err := r.FormFile("myFile")
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer file.Close()
//	log.Warnf("%v", handler.Header)
//
//	filename := getNewUUID()
//	//f, err := os.OpenFile("./"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
//	f, err := os.OpenFile("/usr/local/share/imageDeliveries/"+filename+".jpg", os.O_WRONLY|os.O_CREATE, 0666)
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer f.Close()
//	io.Copy(f, file)
//
//	//file, err := files[0].Open()
//
//	log.Warn("1")
//	log.Errorf("%v", err)
//	//defer file.Close()
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//
//	//var dbCategories []db.Category
//	//var dbCategory db.Category
//	//
//	////	подключаем хранилище
//	//tempStorage := storage
//	//
//	//// 	читаем тело запроса в буфер
//	//bs, err := ioutil.ReadAll(r.Body)
//	//if err != nil {
//	//	log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//	//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
//	//	return
//	//}
//	//
//	////	анмаршалим буфер в структурку
//	//err = json.Unmarshal(bs, &dbCategory)
//	//if err != nil {
//	//	log.Errorf("Неверный формат запроса: %+v", err)
//	//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
//	//	return
//	//} else {
//	//
//	//	log.Warnf("Имеем структурку: %+v", dbCategory)
//	//}
//	//
//	//// 	дергаем функцию изменения
//	//tempStorage.UpdateItem(&dbCategory)
//	//
//	//// 	возвращаем нотисы
//	//tempStorage.SelectAllItems(&dbCategories)
//	//
//
//	w.WriteHeader(201)
//	_ = json.NewEncoder(w).Encode(filename)
//	// 	возвращаем валюту
//	//return
//}
