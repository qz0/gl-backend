package http

import (
	"encoding/json"
	"errors"
	"fmt"
	"image"
	"io"
	"net/http"
	"os"
	"strconv"
	"sync"

	"github.com/disintegration/imaging"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var ImageRoutes = []Route{
	//	сами функции
	//Route{
	//	"createImageBrand",
	//	"Создаем объект",
	//	"PUT",
	//	"/images/brands/create",
	//	createImageBrand,
	//	//validateMiddleware(createImageBrand),
	//},
	//Route{
	//	"deleteImageBrand",
	//	"Удаляем объект",
	//	"DELETE",
	//	"/images/brands/delete",
	//	deleteImageBrand,
	//	//validateMiddleware(deleteImageBrand),
	//},
	Route{
		"getAllImageTypes",
		"Получаем список всех объектов",
		"GET",
		"/images/types/read_all",
		getAllImageTypes,
		//validateMiddleware(getAllImageBrands),
	},
	Route{
		"getCurrentImageType",
		"Получаем конкретный объект",
		"GET",
		"/images/types/read_current",
		getCurrentImageType,
		//validateMiddleware(getCurrentImageBrand),
	},
	//Route{
	//	"updateImageBrand",
	//	"Изменяем объект",
	//	"PATCH",
	//	"/images/brands/update",
	//	updateImageBrand,
	//	//validateMiddleware(updateImageBrand),
	//},
	Route{
		"uploadImage",
		"Добавляем файл в репозиторий",
		"POST",
		"/images/upload",
		uploadImage,
		//validateMiddleware(getAllCurrencies),
	},
	//	STATUS OK
	//Route{
	//	"statusOK",
	//	"Создаем объект",
	//	"OPTIONS",
	//	"/images/brands/create",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Удаляем объект",
	//	"OPTIONS",
	//	"/images/brands/delete",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/images/types/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/images/types/read_current",
		statusOK,
	},
	//Route{
	//	"statusOK",
	//	"Изменяем объект",
	//	"OPTIONS",
	//	"/images/brands/update",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Добавляем файл",
		"OPTIONS",
		"/images/upload",
		statusOK,
	},
}

//	SERVICES

//// 	добавляем объект
//func createImage(image *db.Image) {
//	//	log
//	log.Warn("createImage")
//	//var imageBrands []db.ImageBrand
//
//}

// 	добавляем объект
func createImage(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createImageBrand")

	//	создаем переменные
	var image db.Image
	var images []db.Image

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&image); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", image)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&image); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&images); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(images)
	}
}

// 	Удаляем объект
func deleteImage(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteImageBrand")

	// 	создаем переменные
	var image db.Image
	//var tempImageBrand db.ImageBrand
	var images []db.Image

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	imageBrandID := r.URL.Query().Get("id")

	// 	имя получателя
	if imageBrandID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(imageBrandID)
		image.ID = uint(id)
		//tempImageBrand.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&image); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&image); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	imageBrandID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&images); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(images)
	}
}

// 	Получаем список всех объектов
func getAllImages(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllImageBrands")

	// 	создаем переменные
	var images []db.Image

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&images); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(images)
	}
}

// 	Получаем список всех объектов
func getAllImageTypes(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllImageBrands")

	// 	создаем переменные
	var imageTypes []db.ImageType

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&imageTypes); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(imageTypes)
	}
}

// 	Получаем объект по ИД
func getCurrentImage(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentImageBrand")

	// 	создаем переменные
	var image db.Image

	//	подключаем хранилище
	tempStorage := storage

	imageID := r.URL.Query().Get("id")

	// 	имя получателя
	if imageID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(imageID)
		image.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&image); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(image)
		}
	}
}

// 	Получаем объект по ИД
func getCurrentImageType(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("getCurrentImageType")

	// 	создаем переменные
	var imageType db.ImageType

	//	подключаем хранилище
	tempStorage := storage

	imageTypeID := r.URL.Query().Get("id")

	// 	имя получателя
	if imageTypeID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(imageTypeID)
		imageType.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&imageType); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(imageType)
		}
	}
}

// 	Изменяем объект
func updateImage(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateImageBrand")

	// 	создаем переменные
	var image db.Image
	var tempImage db.Image
	var images []db.Image

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&image); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", image)
		//	сохраняем ИД для поиска
		tempImage.ID = image.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempImage); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&image); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempImage, &image, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&images); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(images)
	}
}

// 	uploadImage - загружаем картинку
func uploadImage(w http.ResponseWriter, r *http.Request) {

	log.Error("uploadImage")
	// 	создаем переменные
	var newImage db.Image

	//	хранилище
	tempStorage := storage

	log.Infof("From Client: %v", r)
	//fmt.Println("Метод ПОСТ")
	err := r.ParseMultipartForm(200000)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}
	log.Infof("After changes: %v", r)

	// они все тут
	formData := r.MultipartForm // ok, no problem so far, read the Form data
	log.Infof("formData: %v", formData)

	fileType := formData.Value["type"]
	// 	конвертируем в UINT
	id, _ := strconv.Atoi(fileType[0])
	imageTypeID := uint(id)

	files := formData.File["myFile"] // grab the filenames
	//var fileHeader multipart.FileHeader

	log.Warnf("files: %v", files)
	for key, _ := range files {
		log.Warnf("file: %v", key)
		//file, _ := f[key].Open()
		log.Warn("Success...")
		//defer file.Close()

	}
	fmt.Println(len(files))

	file, handler, err := r.FormFile("myFile")
	if err != nil {
		log.Errorf("%v", err)
		return
	}
	defer file.Close()
	log.Warnf("link на файл: %v", handler.Header)

	fileName := getNewUUID()
	//	get folder by type
	var fileFolder string
	if fileFolder, err = getImageFolder(imageTypeID); err != nil {
		fileFolder = "default/"
	}
	//f, err := os.OpenFile("./"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	f, err := os.OpenFile("/usr/local/share/images/"+fileFolder+"full/"+fileName+".jpg", os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Errorf("Ошибка сохранения: %v", err)
		return
	}
	defer f.Close()
	io.Copy(f, file)

	//file, err := files[0].Open()

	//log.Warn("1")
	//log.Errorf("%v", err)
	//defer file.Close()
	//if err != nil {
	//	fmt.Fprintln(w, err)
	//	return
	//}

	//var dbCategories []db.Category

	//	заполняем известные данные
	newImage.URL = fileName
	newImage.TypeID = imageTypeID

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&newImage); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - не удалось создать объект в базе данных: : %+v\"}", err), http.StatusBadRequest)
	} else {
		// 	переполучаем объект
		if err := tempStorage.SelectItem(&newImage); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(201)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(newImage)
		}
	}

	// image resize block
	// Open a target image.
	src, err := imaging.Open("/usr/local/share/images/" + fileFolder + "full/" + fileName + ".jpg")
	if err != nil {
		//Если произошла ошибка при попытке открыть картинку, выводим сообщение об ошибке и завершаем функцию.
		log.Fatalf("failed to open image: %v", err)
		return
	}

	var wg sync.WaitGroup

	//	создаем картинку x150
	wg.Add(1)
	go resizeAndSaveImage(src, 150, fileFolder, fileName, "icon", &wg)

	//	создаем картинку x350
	wg.Add(1)
	go resizeAndSaveImage(src, 350, fileFolder, fileName, "small", &wg)

	//	создаем картинку 2Кх2К
	wg.Add(1)
	go resizeAndSaveImage(src, 2000, fileFolder, fileName, "standart", &wg)

	wg.Wait()

}

//	получаем имя папки
func getImageFolder(imageTypeID uint) (string, error) {

	// 	создаем переменные
	var imageType db.ImageType

	//	подключаем хранилище
	tempStorage := storage

	// 	имя получателя
	if imageTypeID != 0 {
		// 	конвертируем в UINT
		imageType.ID = imageTypeID
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&imageType); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе ImageType: %+v", err)
			return "", err
		} else {
			//  все нормально
			log.Warnf("Выбрано: %+v", imageType.Path)
			return imageType.Path + "/", nil
		}
	} else {
		log.Errorf("Ошибка при выборе ImageType: %+v", errors.New("Empty imageTypeID"))
		return "", errors.New("Empty imageTypeID")
	}
}

func resizeAndSaveImage(src image.Image, width int, fileFolder string, fileName string, sizeSubfolder string, wg *sync.WaitGroup) {

	//	2kX2k
	full := imaging.Resize(src, width, 0, imaging.Lanczos)
	// сохраняем созданную картинку с разрешением x2000 full
	err := imaging.Save(full, "/usr/local/share/images/"+fileFolder+sizeSubfolder+"/"+fileName+".jpg")
	if err != nil {
		//если произошла ошибка при попытке сохранить картинку, выводим сообщение об ошибке и завершаем функцию.
		log.Fatalf("failed to save image: %v", err)
		wg.Done()
		return
	}
	wg.Done()
	return
}
