package http

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/gorilla/context"
	"github.com/mitchellh/mapstructure"
	"github.com/twinj/uuid"

	"../db"
	jwt "github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)

// 	Экземпляр БД
var storage db.DataBase

//	****	INIT DB	*********************************
func InitDB(db db.DataBase) {
	// 	делаем экземпляр стораджа БД для пакета
	storage = db
}

// 	****	ОБРАБОТКА ВЫЗОВОВ	***********************

// 	****	PROFILE	******************************
//	получаем профиль текущего пользователя
func getProfile(w http.ResponseWriter, r *http.Request) {

	log.Info("getProfile")
	// 	получаем токен в заголовке
	reqToken := r.Header.Get("Authorization")
	if len(reqToken) == 0 {
		log.Error("Отсутствует токен безопасности")
		json.NewEncoder(w).Encode(Exception{Message: "Отсутствует токен безопасности"})
		return
	}
	// 	грабим
	splitToken := strings.Split(reqToken, "Bearer ")
	reqToken = splitToken[1]
	// 	парсинг токена
	token, _ := jwt.Parse(reqToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("There was an error")
		}
		return []byte(secretKey), nil
	})

	log.Infof("Token: %v", token)
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		log.Infof("Claim: %v", claims)
		var tokenClaim TokenClaim
		mapstructure.Decode(claims, &tokenClaim)
		log.Infof("Авторизовано токеном: %#v", tokenClaim)

		tempStorage := storage
		// 	создаем пользователя
		var dbUser db.User
		dbUser.ID = uint(tokenClaim.ID)
		//	ParentID != 0
		// 	дергаем пользователя
		tempStorage.SelectItem(&dbUser)
		// tempStorage.HandleDB.Where("id = ?", ).First()
		log.Warnf("Пользователь: %v", dbUser)
		// 	возвращаем нотисы
		// storage.Find(&users)
		json.NewEncoder(w).Encode(dbUser)
	} else {
		log.Error("Неверный токен безопасности")
		json.NewEncoder(w).Encode(Exception{Message: "Неверный токен безопасности"})
	}
}

//	*******************************************************
//	*****************	SECURITY	***********************
//	*******************************************************

// 	ТОКЕН
//	создать токен
func createToken(w http.ResponseWriter, r *http.Request) {
	// log.Info("createToken")
	var login Logon
	_ = json.NewDecoder(r.Body).Decode(&login)
	// log.Infof("Login: %v", login)

	var dbUser db.User

	// tempStorage := storage

	// bytepass := []byte(login.Password)
	bytepass := fmt.Sprintf("%x", md5.Sum([]byte(login.Password)))
	// log.Warnf("%s", bytepass)
	storage.HandleDB.Where(&db.User{UserName: login.Username, Password: bytepass}).First(&dbUser)
	// log.Warnf("Пользователь: %v", dbUser)

	if dbUser.ID > 0 {
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"ID":       dbUser.ID,
			"UserName": dbUser.UserName,
			// "password": user.Password,
		})
		tokenString, error := token.SignedString([]byte(secretKey))
		if error != nil {
			log.Errorf("Ошибка авторизации: %v", error)
			// fmt.Println(error)
		}
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
	} else {
		log.Error("Ошибка авторизации")

		w.WriteHeader(400)
		json.NewEncoder(w).Encode("{'status':'Error'}")
		//return
		//json.NewEncoder(w).Encode("{'status':'Error'}")
	}
}

// GetNewUUID - функция отдает новый UUID
func getNewUUID() string {
	return uuid.NewV4().String()
}

// 	statusOK - статус ОК
func statusOK(w http.ResponseWriter, r *http.Request) {
	// Here we adding headers
	// w.Header().Set("Access-Control-Allow-Headers", "*")
	// w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
	// // http.SetCookie(w, &http.Cookie{Name: "api_key", Value: app.GetAPIKey()})
	// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	// inner.ServeHTTP(w, r)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode("{'status':'OK'}")
	return
}

//	validateMiddleware - ВАЛИДАЦИЯ
func validateMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Info("validateMiddleware")
		authorizationHeader := r.Header.Get("Authorization")

		log.Info(authorizationHeader)
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, error := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("There was an error")
					}
					return []byte(secretKey), nil
				})
				if error != nil {
					w.WriteHeader(400)
					// json.NewEncoder(w).Encode(Exception{Message: error.Error()})
					return
				}
				if token.Valid {
					log.Info("Valid")
					context.Set(r, "decoded", token.Claims)
					next(w, r)
				} else {
					json.NewEncoder(w).Encode(Exception{Message: "Invalid authorization token"})
				}
			}
		} else {
			json.NewEncoder(w).Encode(Exception{Message: "An authorization header is required"})
		}
	})
}

//	КОНВЕРТЕР МЕНЮ
// func convertCategory(categories []db.Category, category db.Category) []db.Category {
// 	//	создаем временную переменную
// 	var newCategories []db.Category

// 	//	перебираем элементы
// 	for _, currentCategory := range categories {
// 		//	ParentID != 0
// 		if category.ParentID == currentCategory.ID {
// 			currentCategory.Categories = append(currentCategory.Categories, category)
// 		}
// 		//	есть потомки
// 		if len(currentCategory.Categories) > 0 {
// 			currentCategory.Categories = convertCategory(currentCategory.Categories, category)
// 		}
// 		//	добавляем элемент в новый массив
// 		newCategories = append(newCategories, currentCategory)
// 	}

// 	return newCategories
// }

// //	все сообщения
// func getMessage(w http.ResponseWriter, r *http.Request) {
// 	// 	получаем токен в заголовке
// 	reqToken := r.Header.Get("Authorization")
// 	if len(reqToken) == 0 {
// 		log.Error("Отсутствует токен безопасности")
// 		json.NewEncoder(w).Encode(Exception{Message: "Отсутствует токен безопасности"})
// 		return
// 	}
// 	// 	грабим
// 	splitToken := strings.Split(reqToken, "Bearer ")
// 	reqToken = splitToken[1]
// 	// 	парсинг токена
// 	token, _ := jwt.Parse(reqToken, func(token *jwt.Token) (interface{}, error) {
// 		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
// 			return nil, fmt.Errorf("There was an error")
// 		}
// 		return []byte(secretKey), nil
// 	})

// 	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
// 		var tokenClaim TokenClaim
// 		mapstructure.Decode(claims, &tokenClaim)
// 		log.Infof("Авторизовано: %v", tokenClaim)
// 		json.NewEncoder(w).Encode(tokenClaim)
// 	} else {
// 		log.Error("Неверный токен безопасности")
// 		json.NewEncoder(w).Encode(Exception{Message: "Неверный токен безопасности"})
// 	}
// }

// //	все сообщения
// func testMessage(w http.ResponseWriter, r *http.Request) {
// 	decoded := context.Get(r, "decoded")
// 	var tokenClaim TokenClaim
// 	mapstructure.Decode(decoded.(jwt.MapClaims), &tokenClaim)
// 	log.Infof("user: %v", tokenClaim)
// 	json.NewEncoder(w).Encode(tokenClaim)
// }

//	************************************************************

// 	/////////////

////	открыть токен
//func openToken(w http.ResponseWriter, r *http.Request) {
//	log.Info("openToken")
//	var tokenClaim TokenClaim
//	_ = json.NewDecoder(r.Body).Decode(&user)
//	log.Info(user)
//	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
//		"": user.userID,
//		// "password": user.Password,
//	})
//	tokenString, error := token.SignedString([]byte(secretKey))
//	if error != nil {
//		fmt.Println(error)
//	}
//	json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
//}

// 	////////////////////////
//	РАБОТА С МЕМО
// 	добавляем МЕМО
//func createMemo(w http.ResponseWriter, r *http.Request) {
//
//	// 	создаем переменные
//	var dbMemos []db.Memo
//	var dbMemo db.Memo
//	tempStorage := storage
//
//	// 	читаем тело запроса в буфер
//	bs, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	}
//
//	//	анмаршалим буфер в структурку
//	err = json.Unmarshal(bs, &dbMemo)
//	if err != nil {
//		log.Errorf("Неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	}
//
//	// 	возвращаем нотисы
//	tempStorage.CreateItem(&dbMemo)
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbMemos)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbMemos)
//}

// 	Удаляем МЕМО
//func deleteMemo(w http.ResponseWriter, r *http.Request) {
//
//	// log.Warn("Вход в deleteMemo")
//	// 	создаем переменные
//	var dbMemos []db.Memo
//	var dbMemo db.Memo
//
//	tempStorage := storage
//
//	// 	получаем параметры
//	memoID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if memoID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(memoID)
//		dbMemo.ID = uint(id)
//		// 	дергаем функцию удаления
//		tempStorage.DeleteItem(&dbMemo)
//	}
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbMemos)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbMemos)
//}

// 	Изменяем МЕМО
//func updateMemo(w http.ResponseWriter, r *http.Request) {
//
//	// 	создаем переменные
//	var dbMemos []db.Memo
//	var dbMemo db.Memo
//	tempStorage := storage
//
//	// 	читаем тело запроса в буфер
//	bs, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	}
//
//	//	анмаршалим буфер в структурку
//	err = json.Unmarshal(bs, &dbMemo)
//	if err != nil {
//		log.Errorf("Неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//
//		log.Warnf("Имеем структурку: %+v", dbMemo)
//	}
//
//	// 	сохраняем измененную строку
//	tempStorage.UpdateItem(&dbMemo)
//	// tempStorage.HandleDB.Where("id = ?", dbMemo.ID).Updates(&dbMemo)
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbMemos)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbMemos)
//
//}

// 	Получаем список всех заметок
//func getAllMemo(w http.ResponseWriter, r *http.Request) {
//	// log.Info("getAllMemo")
//	// 	создаем переменные
//	var dbMemos []db.Memo
//	// var dbMemo db.Memo
//	tempStorage := storage
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbMemos)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbMemos)
//}

// 	////////////////////////
//	РАБОТА С NEWS
// 	добавляем NEWS
//func createNews(w http.ResponseWriter, r *http.Request) {
//
//	// 	создаем переменные
//	var dbAllNews []db.News
//	var dbOneNews db.News
//	tempStorage := storage
//
//	// 	читаем тело запроса в буфер
//	bs, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	}
//
//	//	анмаршалим буфер в структурку
//	err = json.Unmarshal(bs, &dbOneNews)
//	if err != nil {
//		log.Errorf("Неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	}
//
//	// 	возвращаем нотисы
//	tempStorage.CreateItem(&dbOneNews)
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbAllNews)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbAllNews)
//}

// 	Удаляем NEWS
//func deleteNews(w http.ResponseWriter, r *http.Request) {
//
//	// log.Warn("Вход в deleteMemo")
//	// 	создаем переменные
//	var dbAllNews []db.News
//	var dbOneNews db.News
//
//	tempStorage := storage
//
//	// 	получаем параметры
//	memoID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if memoID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(memoID)
//		dbOneNews.ID = uint(id)
//		// 	дергаем функцию удаления
//		tempStorage.DeleteItem(&dbOneNews)
//	}
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbAllNews)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbAllNews)
//}

// 	Изменяем NEWS
//func updateNews(w http.ResponseWriter, r *http.Request) {
//
//	// 	создаем переменные
//	var dbAllNews []db.News
//	var dbOneNews db.News
//	tempStorage := storage
//
//	// 	читаем тело запроса в буфер
//	bs, err := ioutil.ReadAll(r.Body)
//	if err != nil {
//		log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	}
//
//	//	анмаршалим буфер в структурку
//	err = json.Unmarshal(bs, &dbOneNews)
//	if err != nil {
//		log.Errorf("Неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//
//		log.Warnf("Имеем структурку: %+v", dbOneNews)
//	}
//
//	// 	сохраняем измененную строку
//	tempStorage.UpdateItem(&dbOneNews)
//	// tempStorage.HandleDB.Where("id = ?", dbMemo.ID).Updates(&dbMemo)
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbAllNews)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbAllNews)
//
//}

//// 	Получаем список всех новостей
//func getAllNews(w http.ResponseWriter, r *http.Request) {
//	// log.Info("getAllMemo")
//	// 	создаем переменные
//	var dbAllNews []db.News
//	// var dbMemo db.Memo
//	tempStorage := storage
//
//	// 	возвращаем нотисы
//	tempStorage.SelectAllItems(&dbAllNews)
//
//	// 	возвращаем нотисы
//	json.NewEncoder(w).Encode(dbAllNews)
//}

//	************* A ******************************

//	createA - добавить A
func createA(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Info("createA")
	//	переменные
	var as []db.Manufacturer
	var newA db.Manufacturer

	//	подключение хранилища
	tempStorage := storage

	//	получение данных из бади
	_ = json.NewDecoder(r.Body).Decode(&newA)

	// 	добавляем запись
	tempStorage.CreateItem(&newA)

	// 	выбираем все записи
	tempStorage.SelectAllItems(&as)

	// 	возвращаем все записи
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(as)
}

//	deleteA - удалить A
func deleteA(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Info("deleteA")

	//	переменные
	var a db.Manufacturer
	var as []db.Manufacturer

	//	цепляем базу
	tempStorage := storage

	// 	получаем параметр
	aID := r.URL.Query().Get("id")

	// 	имя получателя
	if aID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(aID)
		a.ID = uint(id)
		// 	дергаем функцию удаления
		tempStorage.DeleteItem(&a)
	}

	// 	выбираем все записи
	tempStorage.SelectAllItems(&as)

	// 	возвращаем все записи
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(as)
}

//	getAllAs - получение списка всех A
func getAllAs(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Info("getAllAs")

	//	создаем пременные
	var as []db.Manufacturer

	//	цепляем базу
	tempStorage := storage

	// 	выбираем все записи нотисы
	tempStorage.SelectAllItems(&as)

	// 	возвращаем список из всех записей
	json.NewEncoder(w).Encode(as)
}

//	getA - получение конкретного A
func getA(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Info("getA")

	//	создаем пременные
	var a db.Manufacturer

	//	цепляем базу
	tempStorage := storage

	aID := r.URL.Query().Get("id")

	// 	имя получателя
	if aID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(aID)
		a.ID = uint(id)
		// 	выбираем нотисы
		tempStorage.SelectItem(&a)
	}
	// 	возвращаем a
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(a)
}

//	updateA - изменить A
func updateA(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Info("updateA")

	//	переменные
	var a db.Manufacturer
	var as []db.Manufacturer

	//	цепляем базу
	tempStorage := storage

	// 	читаем тело запроса в буфер
	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf("Невозможно прочитать тело запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
		return
	}

	//	анмаршалим буфер в структурку
	err = json.Unmarshal(bs, &a)
	if err != nil {
		log.Errorf("Неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Warnf("Имеем структурку: %+v", a)
	}

	// 	дергаем функцию изменения
	tempStorage.UpdateItem(&a)

	// 	получаем as
	tempStorage.SelectAllItems(&as)

	// 	возвращаем as
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(as)
}

//	функция очистки от постороннего мусора
func filterExternalText(dirtyText string) string {
	reg := regexp.MustCompile("[^A-Za-z0-9_/-]+")
	clearText := reg.ReplaceAllString(dirtyText, "")
	return clearText
}
