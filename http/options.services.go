package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var OptionRoutes = []Route{
	//	сами функции
	Route{
		"changePriorityOfOption",
		"Изменяем приоритет объекта",
		"PATCH",
		"/options/change_priority",
		changePriorityOfOption,
		//validateMiddleware(changePriorityOfOption),
	},
	Route{
		"createOption",
		"Создаем объект",
		"PUT",
		"/options/create",
		createOption,
		//validateMiddleware(createOption),
	},
	Route{
		"deleteOption",
		"Удаляем объект",
		"DELETE",
		"/options/delete",
		deleteOption,
		//validateMiddleware(deleteOption),
	},
	Route{
		"getAllOptions",
		"Получаем список всех объектов",
		"GET",
		"/options/read_all",
		getAllOptions,
		//validateMiddleware(getAllOptions),
	},
	Route{
		"getCurrentOption",
		"Получаем конкретный объект",
		"GET",
		"/options/read_current",
		getCurrentOption,
		//validateMiddleware(getCurrentOption),
	},
	Route{
		"updateOption",
		"Изменяем объект",
		"PATCH",
		"/options/update",
		updateOption,
		//validateMiddleware(updateOption),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/options/change_priority",
		statusOK,
	},
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/options/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/options/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/options/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/options/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/options/update",
		statusOK,
	},
}

//	SERVICES

func changePriorityOfOption(w http.ResponseWriter, r *http.Request) {
	// моя копипаста, но вроде верно понял суть.
	// 	создаем переменные
	var option db.Option
	var tempOption db.Option
	var options []db.Option

	//	подключаем хранилище
	tempStorage := storage
	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", option)
		//	сохраняем ИД для поиска
		tempOption.ID = option.ID
	}
	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOption); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	} else {
		log.Warn("Запись есть")
	}
	//	выясняем в какую сторону перемещение
	if option.Priority < tempOption.Priority {
		//	число приоритета уменьшили
		//	получаем список доставок где приоритет выше
		if err := tempStorage.SelectItemsWhere(&options, 0, 10, "priority >= ? and priority < ?", fmt.Sprint(option.Priority), fmt.Sprint(tempOption.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentOption := range options {
				// 	прибавляем к приоритету единицу
				currentOption.Priority = currentOption.Priority + 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentOption); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err),
						http.StatusBadRequest)
				} else {
					log.Warnf("Updated Option: %v", currentOption)
				}
			}
		}
	} else {
		//	число приоритета такое же или увеличено
		//	получаем список доставок где приоритет ниже
		if err := tempStorage.SelectItemsWhere(&options, 0, 10, "priority <= ? and priority > ?",
			fmt.Sprint(option.Priority), fmt.Sprint(tempOption.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentOption := range options {
				// 	прибавляем к приоритету единицу
				currentOption.Priority = currentOption.Priority - 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentOption); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
				} else {

					log.Infof("Updated OPTION: %v", currentOption)
				}
			}
		}
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&option); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден_: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&options); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(options)
	}
}

// 	добавляем объект
func createOption(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createOption")

	//	создаем переменные
	var option db.Option
	var options []db.Option

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", option)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&option); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&options); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(options)
	}
}

// 	Удаляем объект
func deleteOption(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteOption")

	// 	создаем переменные
	var option db.Option
	//var tempOption db.Option
	var options []db.Option

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	optionID := r.URL.Query().Get("id")

	// 	имя получателя
	if optionID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(optionID)
		option.ID = uint(id)
		//tempOption.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&option); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&option); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	optionID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&options); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(options)
	}
}

// 	Получаем список всех объектов
func getAllOptions(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOptions")

	// 	создаем переменные
	var options []db.Option

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&options); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(options)
	}
}

// 	Получаем объект по ИД
func getCurrentOption(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentOption")

	// 	создаем переменные
	var option db.Option

	//	подключаем хранилище
	tempStorage := storage

	optionID := r.URL.Query().Get("id")

	// 	имя получателя
	if optionID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(optionID)
		option.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&option); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(option)
		}
	}
}

// 	Изменяем объект
func updateOption(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateOption")

	// 	создаем переменные
	var option db.Option
	var tempOption db.Option
	var options []db.Option

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&option); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", option)
		//	сохраняем ИД для поиска
		tempOption.ID = option.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOption); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&option); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempOption, &option, "update", "Guest") // запись успешного удаления в лог
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&options); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(options)
	}
}
