package http

import (
	//"crypto/md5"
	"encoding/json"
	"fmt"
	"../db"
	log "github.com/sirupsen/logrus"
	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
)

// 	массив роутинга
var ProductVisibilityRoutes = []Route{
	//	сами функции
	//Route{
	//	"createProductVisibilityStatus",
	//	"Создаем объект",
	//	"PUT",
	//	"/product_visibility_statuses/create",
	//	createProductVisibilityStatus,
	//	//validateMiddleware(createProductVisibilityStatus),
	//},
	//Route{
	//	"deleteProductVisibilityStatus",
	//	"Удаляем объект",
	//	"DELETE",
	//	"/product_visibility_statuses/delete",
	//	deleteProductVisibilityStatus,
	//	//validateMiddleware(deleteProductVisibilityStatus),
	//},
	Route{
		"getAllProductVisibilities",
		"Получаем список всех объектов",
		"GET",
		"/product_visibilities/read_all",
		getAllProductVisibilities,
		//validateMiddleware(getAllProductVisibilityStatuses),
	},
	//Route{
	//	"getCurrentProductVisibilityStatus",
	//	"Получаем конкретный объект",
	//	"GET",
	//	"/product_visibility_statuses/read_current",
	//	getCurrentProductVisibilityStatus,
	//	//validateMiddleware(getCurrentProductVisibilityStatus),
	//},
	//Route{
	//	"updateProductVisibilityStatus",
	//	"Изменяем объект",
	//	"PATCH",
	//	"/product_visibility_statuses/update",
	//	updateProductVisibilityStatus,
	//	//validateMiddleware(updateProductVisibilityStatus),
	//},
	//	STATUS OK
	//Route{
	//	"statusOK",
	//	"Создаем объект",
	//	"OPTIONS",
	//	"/product_visibility_statuses/create",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Удаляем объект",
	//	"OPTIONS",
	//	"/product_visibility_statuses/delete",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/product_visibilities/read_all",
		statusOK,
	},
	//Route{
	//	"statusOK",
	//	"Получаем конкретный объект",
	//	"OPTIONS",
	//	"/product_visibility_statuses/read_current",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Изменяем объект",
	//	"OPTIONS",
	//	"/productVisibilityStatus_visibility_statuses/update",
	//	statusOK,
	//},
}

//	SERVICES

// 	добавляем объект
//func createProductVisibilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("createProductVisibilityStatus")
//
//	//	создаем переменные
//	var productVisibilityStatus db.ProductVisibilityStatus
//	var productVisibilityStatuses []db.ProductVisibilityStatus
//
//	// 	переменные
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&productVisibilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", productVisibilityStatus)
//	}
//
//	// 	дергаем функцию создания
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.CreateItem(&productVisibilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&productVisibilityStatuses); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(productVisibilityStatuses)
//	}
//}

// 	Удаляем объект
//func deleteProductVisibilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("deleteProductVisibilityStatus")
//
//	// 	создаем переменные
//	var productVisibilityStatus db.ProductVisibilityStatus
//	//var tempProductVisibilityStatus db.ProductVisibilityStatus
//	var productVisibilityStatuses []db.ProductVisibilityStatus
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	получаем параметр
//	productVisibilityStatusID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if productVisibilityStatusID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(productVisibilityStatusID)
//		productVisibilityStatus.ID = uint(id)
//		//tempProductVisibilityStatus.ID = uint(id)
//
//		//	проверяем, что такая запись есть
//		if err := tempStorage.SelectItem(&productVisibilityStatus); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		} else {
//			// 	дергаем функцию удаления
//			if err = tempStorage.DeleteItem(&productVisibilityStatus); err != nil {
//				//	вернули ошибку - возвращаем 400
//				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
//				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
//			} else {
//				//  все нормально - возвращаем 200
//				w.WriteHeader(200)
//			}
//		}
//	} else {
//		//	productVisibilityStatusID is empty
//		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&productVisibilityStatuses); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(productVisibilityStatuses)
//	}
//}

// 	Получаем список всех объектов
func getAllProductVisibilities(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllProductVisibilityStatuses")

	// 	создаем переменные
	var productVisibilities []db.ProductVisibility

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&productVisibilities); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(productVisibilities)
	}
}

// 	Получаем объект по ИД
//func getCurrentProductVisibilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getCurrentProductVisibilityStatus")
//
//	// 	создаем переменные
//	var productVisibilityStatus db.ProductVisibilityStatus
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	productVisibilityStatusID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if productVisibilityStatusID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(productVisibilityStatusID)
//		productVisibilityStatus.ID = uint(id)
//		// 	возвращаем нотисы
//		if err := tempStorage.SelectItem(&productVisibilityStatus); err != nil {
//			//	вернули ошибку - возвращаем 404
//			log.Errorf("Ошибка при выборе объекта: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
//			return
//		} else {
//			//  все нормально - возвращаем 200
//			w.WriteHeader(200)
//			//	возвращаем полученные объекты
//			_ = json.NewEncoder(w).Encode(productVisibilityStatus)
//		}
//	}
//}

// 	Изменяем объект
//func updateProductVisibilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	log.Warn("updateProductVisibilityStatus")
//
//	// 	создаем переменные
//	var productVisibilityStatus db.ProductVisibilityStatus
//	var tempProductVisibilityStatus db.ProductVisibilityStatus
//	var productVisibilityStatuses []db.ProductVisibilityStatus
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&productVisibilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", productVisibilityStatus)
//		//	сохраняем ИД для поиска
//		tempProductVisibilityStatus.ID = productVisibilityStatus.ID
//	}
//
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempProductVisibilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&productVisibilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&productVisibilityStatuses); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(productVisibilityStatuses)
//	}
//}
