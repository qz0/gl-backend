package http

import (
	"fmt"
	"net/http"

	"../db"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

var secretKey string

// AddHeaders adds all needed headers
func AddHeaders(inner http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Here we adding headers
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
		// http.SetCookie(w, &http.Cookie{Name: "api_key", Value: app.GetAPIKey()})
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		inner.ServeHTTP(w, r)
		//log.Println(w.Header().Get("api_key"))
	})
}

// создание роутера
func CreateRouter(r []Route) *mux.Router {
	// создаем экземпляр роутера
	router := mux.NewRouter().StrictSlash(true)

	log.Info("createRouter")
	var handler http.Handler
	//	создаем роутер

	for _, route := range r {
		// 	ссылка на функцию
		handler = route.HandlerFunc

		handler = AddHeaders(handler)
		router.
			Methods([]string{route.Method}...).
			Path(route.Path).
			Name(route.Name).
			Handler(handler)
	}
	// log.Warnf("Router: %+v", router)
	return router
}

// Serve запуск веб-сервера
func CreateWebServer(listeningPort int, db db.DataBase, httpMode bool) {
	var cert_file2 = "/etc/letsencrypt/live/grey-line.com/cert.pem"
	var key_file2 = "/etc/letsencrypt/live/grey-line.com/privkey.pem"
	var cert_file = "/opt/greyline/cert.pem"
	var key_file = "/opt/greyline/privkey.pem"

	// 	Передаем экземпляр БД
	InitDB(db)
	// 	создаем ключик
	secretKey = getNewUUID()

	//	заполняем роуты
	Routes := append(GeneralRoutes, AddressRoutes...)
	Routes = append(Routes, AssemblyRoutes...)
	Routes = append(Routes, BrandRoutes...)
	Routes = append(Routes, BrockerRoutes...)
	Routes = append(Routes, CartRoutes...)
	Routes = append(Routes, CategoryRoutes...)
	Routes = append(Routes, CountryRoutes...)
	Routes = append(Routes, CurrencyRoutes...)
	Routes = append(Routes, CustomerRankRoutes...)
	Routes = append(Routes, CustomerRoutes...)
	Routes = append(Routes, DeliveryRoutes...)
	//Routes = append(Routes, ImageBrandRoutes...)
	Routes = append(Routes, ImageRoutes...)
	//Routes = append(Routes, ImageCategoryRoutes...)
	//Routes = append(Routes, ImageDeliveryRoutes...)
	//Routes = append(Routes, ImageProductRoutes...)
	Routes = append(Routes, IncomingRoutes...)
	Routes = append(Routes, ItemShadowRoutes...)
	Routes = append(Routes, ItemRoutes...)
	Routes = append(Routes, LanguageRoutes...)
	Routes = append(Routes, LocalizationRoutes...)
	Routes = append(Routes, LogRoutes...)
	Routes = append(Routes, ManufacturerRoutes...)
	Routes = append(Routes, OptionValueRoutes...)
	Routes = append(Routes, OptionTypeRoutes...)
	Routes = append(Routes, OptionRoutes...)
	Routes = append(Routes, OptionsSetRoutes...)
	Routes = append(Routes, OrderRoutes...)
	Routes = append(Routes, OrderPriorityRoutes...)
	Routes = append(Routes, OrderStatusRoutes...)
	Routes = append(Routes, PaymentRoutes...)
	Routes = append(Routes, PaypalRoutes...)
	Routes = append(Routes, ProductRoutes...)
	Routes = append(Routes, ProductAvailabilityRoutes...)
	Routes = append(Routes, ProductVisibilityRoutes...)
	Routes = append(Routes, ProductShadowRoutes...)
	Routes = append(Routes, PurchaseRoutes...)
	Routes = append(Routes, SberbankRoutes...)
	Routes = append(Routes, SeoRoutes...)
	Routes = append(Routes, RegionRoutes...)
	Routes = append(Routes, RoleRoutes...)
	Routes = append(Routes, UserRoutes...)
	Routes = append(Routes, ZoneRoutes...)

	// 	создаем экземпляр роутера
	r := CreateRouter(Routes)

	// headersOk := handlers.AllowedHeaders([]string{"Content-Type", "X-Forwarded-For"})
	// originsOk := handlers.AllowedOrigins([]string{"*"})
	// methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"})
	log.Infof("Запуск сервера %s:%d", "", listeningPort)
	// err := http.ListenAndServe(fmt.Sprintf("%s:%d", "", listeningPort), handlers.CORS(headersOk, originsOk, methodsOk)(router))

	// On the default page we will simply serve our static index page.
	r.Handle("/", http.FileServer(http.Dir("./views/")))
	// We will setup our server so we can serve static assest like images, css from the /static/{file} route
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	// 	стартуем сервер
	//	если передали режим http - принудительно включаем в http
	if httpMode {
		http.ListenAndServe(fmt.Sprintf(":%d", listeningPort), r)
	} else {
		//	обычный старт
		if err := http.ListenAndServeTLS(fmt.Sprintf(":%d", listeningPort), cert_file, key_file, r); err != nil {
			log.Errorf("Ошибка запуска сервера в https %d %+v", listeningPort, err)

			if err := http.ListenAndServeTLS(fmt.Sprintf(":%d", listeningPort), cert_file2, key_file2, r); err != nil {
				log.Errorf("Ошибка запуска сервера в https %d %+v", listeningPort, err)
				http.ListenAndServe(fmt.Sprintf(":%d", listeningPort), r)
			}
		}
	}

}
