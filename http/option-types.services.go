package http

import (
	"encoding/json"
	"fmt"
	"net/http"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var OptionTypeRoutes = []Route{
	//	сами функции
	Route{
		"createOptionType",
		"Создаем объект",
		"PUT",
		"/optionTypes/create",
		createOptionType,
		//validateMiddleware(createOptionType),
	},
	//Route{
	//	"deleteOptionType",
	//	"Удаляем объект",
	//	"DELETE",
	//	"/option_values/delete",
	//	deleteOptionType,
	//	//validateMiddleware(deleteOptionType),
	//},
	Route{
		"getAllOptionTypes",
		"Получаем список всех объектов",
		"GET",
		"/option_types/read_all",
		getAllOptionTypes,
		//validateMiddleware(getAllOptionTypes),
	},
	//Route{
	//	"getCurrentOptionType",
	//	"Получаем конкретный объект",
	//	"GET",
	//	"/option_values/read_current",
	//	getCurrentOptionType,
	//	//validateMiddleware(getCurrentOptionType),
	//},
	//Route{
	//	"updateOptionType",
	//	"Изменяем объект",
	//	"PATCH",
	//	"/option_values/update",
	//	updateOptionType,
	//	//validateMiddleware(updateOptionType),
	//},
	//	STATUS OK
	//Route{
	//	"statusOK",
	//	"Создаем объект",
	//	"OPTIONS",
	//	"/option_values/create",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Удаляем объект",
	//	"OPTIONS",
	//	"/option_values/delete",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/option_types/read_all",
		statusOK,
	},
	//Route{
	//	"statusOK",
	//	"Получаем конкретный объект",
	//	"OPTIONS",
	//	"/option_values/read_current",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Изменяем объект",
	//	"OPTIONS",
	//	"/option_values/update",
	//	statusOK,
	//},
}

//	SERVICES

// 	добавляем объект
func createOptionType(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createOptionType")

	//	создаем переменные
	var optionType db.OptionType
	var optionTypes []db.OptionType

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&optionType); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", optionType)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&optionType); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionTypes); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionTypes)
	}
}

// 	Удаляем объект
//func deleteOptionType(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("deleteOptionType")
//
//	// 	создаем переменные
//	var optionType db.OptionType
//	//var tempOptionType db.OptionType
//	var optionTypes []db.OptionType
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	получаем параметр
//	optionTypeID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if optionTypeID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(optionTypeID)
//		optionType.ID = uint(id)
//		//tempOptionType.ID = uint(id)
//
//		//	проверяем, что такая запись есть
//		if err := tempStorage.SelectItem(&optionType); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		} else {
//			// 	дергаем функцию удаления
//			if err = tempStorage.DeleteItem(&optionType); err != nil {
//				//	вернули ошибку - возвращаем 400
//				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
//				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
//			} else {
//				//  все нормально - возвращаем 200
//				w.WriteHeader(200)
//			}
//		}
//	} else {
//		//	optionTypeID is empty
//		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&optionTypes); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(optionTypes)
//	}
//}

// 	Получаем список всех объектов
func getAllOptionTypes(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOptionTypes")

	// 	создаем переменные
	var optionTypes []db.OptionType

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionTypes); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionTypes)
	}
}

// 	Получаем объект по ИД
//func getCurrentOptionType(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getCurrentOptionType")
//
//	// 	создаем переменные
//	var optionType db.OptionType
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	optionTypeID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if optionTypeID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(optionTypeID)
//		optionType.ID = uint(id)
//		// 	возвращаем нотисы
//		if err := tempStorage.SelectItem(&optionType); err != nil {
//			//	вернули ошибку - возвращаем 404
//			log.Errorf("Ошибка при выборе объекта: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
//			return
//		} else {
//			//  все нормально - возвращаем 200
//			w.WriteHeader(200)
//			//	возвращаем полученные объекты
//			_ = json.NewEncoder(w).Encode(optionType)
//		}
//	}
//}

// 	Изменяем объект
//func updateOptionType(w http.ResponseWriter, r *http.Request) {
//	//	log
//	log.Warn("updateOptionType")
//
//	// 	создаем переменные
//	var optionType db.OptionType
//	var tempOptionType db.OptionType
//	var optionTypes []db.OptionType
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&optionType); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", optionType)
//		//	сохраняем ИД для поиска
//		tempOptionType.ID = optionType.ID
//	}
//
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempOptionType); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&optionType); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&optionTypes); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(optionTypes)
//	}
//}
