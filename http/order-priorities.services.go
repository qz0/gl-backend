package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var OrderPriorityRoutes = []Route{
	//	сами функции
	//Route{
	//	"createOrderPriority",
	//	"Создаем объект",
	//	"PUT",
	//	"/order_priorities/create",
	//	createOrderPriority,
	//	//validateMiddleware(createOrderPriority),
	//},
	//Route{
	//	"deleteOrderPriority",
	//	"Удаляем объект",
	//	"DELETE",
	//	"/order_priorities/delete",
	//	deleteOrderPriority,
	//	//validateMiddleware(deleteOrderPriority),
	//},
	Route{
		"getAllOrderPriorities",
		"Получаем список всех объектов",
		"GET",
		"/order_priorities/read_all",
		getAllOrderPriorities,
		//validateMiddleware(getAllOrderPriorities),
	},
	//Route{
	//	"getCurrentOrderPriority",
	//	"Получаем конкретный объект",
	//	"GET",
	//	"/order_priorities/read_current",
	//	getCurrentOrderPriority,
	//	//validateMiddleware(getCurrentOrderPriority),
	//},
	//Route{
	//	"updateOrderPriority",
	//	"Изменяем объект",
	//	"PATCH",
	//	"/order_priorities/update",
	//	updateOrderPriority,
	//	//validateMiddleware(updateOrderPriority),
	//},
	//	STATUS OK
	//Route{
	//	"statusOK",
	//	"Создаем объект",
	//	"OPTIONS",
	//	"/order_priorities/create",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Удаляем объект",
	//	"OPTIONS",
	//	"/order_priorities/delete",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/order_priorities/read_all",
		statusOK,
	},
	//Route{
	//	"statusOK",
	//	"Получаем конкретный объект",
	//	"OPTIONS",
	//	"/order_priorities/read_current",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Изменяем объект",
	//	"OPTIONS",
	//	"/order_priorities/update",
	//	statusOK,
	//},
}

//	SERVICES

// 	добавляем объект
func createOrderPriority(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createOrderPriority")

	//	создаем переменные
	var order db.OrderPriority
	var orderPriorities []db.OrderPriority

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", order)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&order); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderPriorities); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderPriorities)
	}
}

// 	Удаляем объект
func deleteOrderPriority(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteOrderPriority")

	// 	создаем переменные
	var order db.OrderPriority
	//var tempOrderPriority db.OrderPriority
	var orderPriorities []db.OrderPriority

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	orderID := r.URL.Query().Get("id")

	// 	имя получателя
	if orderID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(orderID)
		order.ID = uint(id)
		//tempOrderPriority.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&order); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&order); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	orderID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderPriorities); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderPriorities)
	}
}

// 	Получаем список всех объектов
func getAllOrderPriorities(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOrderPriorities")

	// 	создаем переменные
	var orderPriorities []db.OrderPriority

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderPriorities); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderPriorities)
	}
}

// 	Получаем объект по ИД
func getCurrentOrderPriority(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentOrderPriority")

	// 	создаем переменные
	var orderPriority db.OrderPriority

	//	подключаем хранилище
	tempStorage := storage

	orderPriorityID := r.URL.Query().Get("id")

	// 	имя получателя
	if orderPriorityID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(orderPriorityID)
		orderPriority.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&orderPriority); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(orderPriority)
		}
	}
}

// 	Изменяем объект
func updateOrderPriority(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateOrderPriority")

	// 	создаем переменные
	var orderPriority db.OrderPriority
	var tempOrderPriority db.OrderPriority
	var orderPriorities []db.OrderPriority

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&orderPriority); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", orderPriority)
		//	сохраняем ИД для поиска
		tempOrderPriority.ID = orderPriority.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOrderPriority); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&orderPriority); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempOrderPriority, &orderPriority, "update", "Guest") // запись успешного удаления в лог

		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderPriorities); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderPriorities)
	}
}
