package http

//
//import (
//	"encoding/json"
//	"fmt"
//	"io"
//	"net/http"
//	"os"
//	"strconv"
//
//	"github.com/archi-chester/backend/db"
//
//	log "github.com/sirupsen/logrus"
//)
//
//// 	массив роутинга
//var ImageCategoryRoutes = []Route{
//	//	сами функции
//	Route{
//		"createImageCategory",
//		"Создаем объект",
//		"PUT",
//		"/images/categories/create",
//		createImageCategory,
//		//validateMiddleware(createImageCategory),
//	},
//	Route{
//		"deleteImageCategory",
//		"Удаляем объект",
//		"DELETE",
//		"/images/categories/delete",
//		deleteImageCategory,
//		//validateMiddleware(deleteImageCategory),
//	},
//	Route{
//		"getAllImageCategor",
//		"Получаем список всех объектов",
//		"GET",
//		"/images/categories/read_all",
//		getAllImageCategories,
//		//validateMiddleware(getAllImageCategories),
//	},
//	Route{
//		"getCurrentImageCategory",
//		"Получаем конкретный объект",
//		"GET",
//		"/images/categories/read_current",
//		getCurrentImageCategory,
//		//validateMiddleware(getCurrentImageCategory),
//	},
//	Route{
//		"updateImageCategory",
//		"Изменяем объект",
//		"PATCH",
//		"/images/categories/update",
//		updateImageCategory,
//		//validateMiddleware(updateImageCategory),
//	},
//	Route{
//		"uploadImageCategory",
//		"Добавляем файл в репозиторий",
//		"POST",
//		"/images/categories/upload",
//		uploadImageCategory,
//		//validateMiddleware(getAllCurrencies),
//	},
//	//	STATUS OK
//	Route{
//		"statusOK",
//		"Создаем объект",
//		"OPTIONS",
//		"/images/categories/create",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Удаляем объект",
//		"OPTIONS",
//		"/images/categories/delete",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем все объекты",
//		"OPTIONS",
//		"/images/categories/read_all",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем конкретный объект",
//		"OPTIONS",
//		"/images/categories/read_current",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Изменяем объект",
//		"OPTIONS",
//		"/images/categories/update",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Добавляем файл",
//		"OPTIONS",
//		"/images/categories/upload",
//		statusOK,
//	},
//}
//
////	SERVICES
//
//// 	добавляем объект
//func createImageCategory(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("createImageCategory")
//
//	//	создаем переменные
//	var image db.Image
//	var images []db.Image
//
//	// 	переменные
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&image); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", image)
//	}
//
//	// 	дергаем функцию создания
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.CreateItem(&image); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&images); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(images)
//	}
//}
//
//// 	Удаляем объект
//func deleteImageCategory(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("deleteImageCategory")
//
//	// 	создаем переменные
//	var image db.Image
//	//var tempImageCategory db.ImageCategory
//	var images []db.Image
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	получаем параметр
//	imageCategoryID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageCategoryID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageCategoryID)
//		image.ID = uint(id)
//		//tempImageCategory.ID = uint(id)
//
//		//	проверяем, что такая запись есть
//		if err := tempStorage.SelectItem(&image); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		} else {
//			// 	дергаем функцию удаления
//			if err = tempStorage.DeleteItem(&image); err != nil {
//				//	вернули ошибку - возвращаем 400
//				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
//				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
//			} else {
//				//  все нормально - возвращаем 200
//				w.WriteHeader(200)
//			}
//		}
//	} else {
//		//	imageCategoryID is empty
//		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&images); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(images)
//	}
//}
//
//// 	Получаем список всех объектов
//func getAllImageCategories(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getAllImageCategories")
//
//	// 	создаем переменные
//	var images []db.Image
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&images); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(images)
//	}
//}
//
//// 	Получаем объект по ИД
//func getCurrentImageCategory(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getCurrentImageCategory")
//
//	// 	создаем переменные
//	var image db.Image
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	imageCategoryID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageCategoryID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageCategoryID)
//		image.ID = uint(id)
//		// 	возвращаем нотисы
//		if err := tempStorage.SelectItem(&image); err != nil {
//			//	вернули ошибку - возвращаем 404
//			log.Errorf("Ошибка при выборе объекта: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
//			return
//		} else {
//			//  все нормально - возвращаем 200
//			w.WriteHeader(200)
//			//	возвращаем полученные объекты
//			_ = json.NewEncoder(w).Encode(image)
//		}
//	}
//}
//
//// 	Изменяем объект
//func updateImageCategory(w http.ResponseWriter, r *http.Request) {
//	//	log
//	log.Warn("updateImageCategory")
//
//	// 	создаем переменные
//	var image db.Image
//	var tempImage db.Image
//	var images []db.Image
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&image); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", image)
//		//	сохраняем ИД для поиска
//		tempImage.ID = image.ID
//	}
//
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempImage); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&image); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&images); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(images)
//	}
//}
//
//// 	uploadImageCategory - изменяем категорию
//func uploadImageCategory(w http.ResponseWriter, r *http.Request) {
//
//	log.Warn("uploadImageCategory")
//	// 	создаем переменные
//
//	log.Infof("%v", r)
//	fmt.Println("Метод ПОСТ")
//	err := r.ParseMultipartForm(200000)
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//	log.Infof("%v", r)
//
//	// они все тут
//	formdata := r.MultipartForm // ok, no problem so far, read the Form data
//
//	//get the *fileheaders
//	files := formdata.File["myFile"] // grab the filenames
//	//var fileHeader multipart.FileHeader
//
//	log.Warnf("files: %v", files)
//	for key, _ := range files {
//		log.Warnf("file: %v", key)
//		//file, _ := f[key].Open()
//		log.Warn("Success...")
//		//defer file.Close()
//
//	}
//	fmt.Println(len(files))
//
//	file, handler, err := r.FormFile("myFile")
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer file.Close()
//	log.Warnf("%v", handler.Header)
//
//	filename := getNewUUID()
//	//f, err := os.OpenFile("./"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
//	f, err := os.OpenFile("/usr/local/share/imageCategories/"+filename+".jpg", os.O_WRONLY|os.O_CREATE, 0666)
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer f.Close()
//	io.Copy(f, file)
//
//	//file, err := files[0].Open()
//
//	log.Warn("1")
//	log.Errorf("%v", err)
//	//defer file.Close()
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//
//	//var dbCategories []db.Category
//	//var dbCategory db.Category
//	//
//	////	подключаем хранилище
//	//tempStorage := storage
//	//
//	//// 	читаем тело запроса в буфер
//	//bs, err := ioutil.ReadAll(r.Body)
//	//if err != nil {
//	//	log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//	//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
//	//	return
//	//}
//	//
//	////	анмаршалим буфер в структурку
//	//err = json.Unmarshal(bs, &dbCategory)
//	//if err != nil {
//	//	log.Errorf("Неверный формат запроса: %+v", err)
//	//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
//	//	return
//	//} else {
//	//
//	//	log.Warnf("Имеем структурку: %+v", dbCategory)
//	//}
//	//
//	//// 	дергаем функцию изменения
//	//tempStorage.UpdateItem(&dbCategory)
//	//
//	//// 	возвращаем нотисы
//	//tempStorage.SelectAllItems(&dbCategories)
//	//
//
//	w.WriteHeader(201)
//	_ = json.NewEncoder(w).Encode(filename)
//	// 	возвращаем валюту
//	//return
//}
