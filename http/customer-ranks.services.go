package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var CustomerRankRoutes = []Route{
	//	сами функции
	Route{
		"createCustomerRank",
		"Создаем объект",
		"PUT",
		"/customer_ranks/create",
		createCustomerRank,
		//validateMiddleware(createCustomerRank),
	},
	Route{
		"deleteCustomerRank",
		"Удаляем объект",
		"DELETE",
		"/customer_ranks/delete",
		deleteCustomerRank,
		//validateMiddleware(deleteCustomerRank),
	},
	Route{
		"getAllCustomerRanks",
		"Получаем список всех объектов",
		"GET",
		"/customer_ranks/read_all",
		getAllCustomerRanks,
		//validateMiddleware(getAllCustomerRanks),
	},
	Route{
		"getCurrentCustomerRank",
		"Получаем конкретный объект",
		"GET",
		"/customer_ranks/read_current",
		getCurrentCustomerRank,
		//validateMiddleware(getCurrentCustomerRank),
	},
	Route{
		"updateCustomerRank",
		"Изменяем объект",
		"PATCH",
		"/customer_ranks/update",
		updateCustomerRank,
		//validateMiddleware(updateCustomerRank),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/customer_ranks/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/customer_ranks/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/customer_ranks/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/customer_ranks/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/customer_ranks/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createCustomerRank(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createCustomerRank")

	//	создаем переменные
	var customerRank db.CustomerRank
	var customerRanks []db.CustomerRank

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&customerRank); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", customerRank)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&customerRank); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customerRanks); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customerRanks)
	}
}

// 	Удаляем объект
func deleteCustomerRank(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteCustomerRank")

	// 	создаем переменные
	var customerRank db.CustomerRank
	//var tempCustomerRank db.CustomerRank
	var customerRanks []db.CustomerRank

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	customerRankID := r.URL.Query().Get("id")

	// 	имя получателя
	if customerRankID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(customerRankID)
		customerRank.ID = uint(id)
		//tempCustomerRank.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&customerRank); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&customerRank); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	customerRankID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customerRanks); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customerRanks)
	}
}

// 	Получаем список всех объектов
func getAllCustomerRanks(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllCustomerRanks")

	// 	создаем переменные
	var customerRanks []db.CustomerRank

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customerRanks); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customerRanks)
	}
}

// 	Получаем объект по ИД
func getCurrentCustomerRank(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentCustomerRank")

	// 	создаем переменные
	var customerRank db.CustomerRank

	//	подключаем хранилище
	tempStorage := storage

	customerRankID := r.URL.Query().Get("id")

	// 	имя получателя
	if customerRankID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(customerRankID)
		customerRank.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&customerRank); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(customerRank)
		}
	}
}

// 	Изменяем объект
func updateCustomerRank(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCustomerRank")

	// 	создаем переменные
	var customerRank db.CustomerRank
	var tempCustomerRank db.CustomerRank
	var customerRanks []db.CustomerRank

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&customerRank); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", customerRank)
		//	сохраняем ИД для поиска
		tempCustomerRank.ID = customerRank.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCustomerRank); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&customerRank); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempCustomerRank, &customerRank, "update", "Guest") // запись успешного удаления в лог
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customerRanks); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customerRanks)
	}
}
