package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var BrandRoutes = []Route{
	//	сами функции
	Route{
		"createBrand",
		"Создаем объект",
		"PUT",
		"/brands/create",
		createBrand,
		//validateMiddleware(createBrand),
	},
	Route{
		"deleteBrand",
		"Удаляем объект",
		"DELETE",
		"/brands/delete",
		deleteBrand,
		//validateMiddleware(deleteBrand),
	},
	Route{
		"getAllBrands",
		"Получаем список всех объектов",
		"GET",
		"/brands/read_all",
		getAllBrands,
		//validateMiddleware(getAllBrands),
	},
	Route{
		"getCurrentBrand",
		"Получаем конкретный объект",
		"GET",
		"/brands/read_current",
		getCurrentBrand,
		//validateMiddleware(getCurrentBrand),
	},
	Route{
		"updateBrand",
		"Изменяем объект",
		"PATCH",
		"/brands/update",
		updateBrand,
		//validateMiddleware(updateBrand),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/brands/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/brands/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/brands/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/brands/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/brands/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createBrand(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createBrand")

	//	создаем переменные
	var brand db.Brand
	var brands []db.Brand

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&brand); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", brand)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&brand); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		//w.WriteHeader(201)
	}

	// // добавляем картинку к брэнду
	// //	проверяем, что не вернули ошибку
	// if err := tempStorage.UpdateItem(&brand); err != nil {
	// 	//	вернули ошибку - возвращаем 400
	// 	log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
	// 	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	// } else {
	// 	//  все нормально - возвращаем 200
	// 	w.WriteHeader(201)
	// }

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&brands); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(201)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(brands)
	}
}

// 	Удаляем объект
func deleteBrand(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteBrand")

	// 	создаем переменные
	var brand db.Brand
	//var tempBrand db.Brand
	var brands []db.Brand

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	brandID := r.URL.Query().Get("id")

	// 	имя получателя
	if brandID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(brandID)
		brand.ID = uint(id)
		//tempBrand.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&brand); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&brand); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			}
		}
	} else {
		//	brandID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&brands); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(brands)
	}
}

// 	Получаем список всех объектов
func getAllBrands(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllBrands")

	// 	создаем переменные
	var brands []db.Brand

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&brands); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(brands)
	}
}

// 	Получаем объект по ИД
func getCurrentBrand(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentBrand")

	// 	создаем переменные
	var brand db.Brand

	//	подключаем хранилище
	tempStorage := storage

	brandID := r.URL.Query().Get("id")

	// 	имя получателя
	if brandID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(brandID)
		brand.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&brand); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(brand)
		}
	}
}

// 	Изменяем объект
func updateBrand(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateBrand")

	// 	создаем переменные
	var brand db.Brand
	var tempBrand db.Brand
	var brands []db.Brand

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&brand); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", brand)
		//	сохраняем ИД для поиска
		tempBrand.ID = brand.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempBrand); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&brand); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempBrand, &brand, "update", "Guest") // запись успешного удаления в лог
		//  все нормально - возвращаем 200
		//w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&brands); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(brands)
	}
}
