package http

//
//import (
//	"encoding/json"
//	"fmt"
//	"io"
//	"net/http"
//	"os"
//	"strconv"
//
//	"github.com/archi-chester/backend/db"
//
//	log "github.com/sirupsen/logrus"
//)
//
//// 	массив роутинга
//var ImageProductRoutes = []Route{
//	//	сами функции
//	Route{
//		"createImageProduct",
//		"Создаем объект",
//		"PUT",
//		"/images/products/create",
//		createImageProduct,
//		//validateMiddleware(createImageProduct),
//	},
//	Route{
//		"deleteImageProduct",
//		"Удаляем объект",
//		"DELETE",
//		"/images/products/delete",
//		deleteImageProduct,
//		//validateMiddleware(deleteImageProduct),
//	},
//	Route{
//		"getAllImageProducts",
//		"Получаем список всех объектов",
//		"GET",
//		"/images/products/read_all",
//		getAllImageProducts,
//		//validateMiddleware(getAllImageProducts),
//	},
//	Route{
//		"getCurrentImageProduct",
//		"Получаем конкретный объект",
//		"GET",
//		"/images/products/read_current",
//		getCurrentImageProduct,
//		//validateMiddleware(getCurrentImageProduct),
//	},
//	Route{
//		"updateImageProduct",
//		"Изменяем объект",
//		"PATCH",
//		"/images/products/update",
//		updateImageProduct,
//		//validateMiddleware(updateImageProduct),
//	},
//	Route{
//		"uploadImageProduct",
//		"Добавляем файл в репозиторий",
//		"POST",
//		"/images/products/upload",
//		uploadImageProduct,
//		//validateMiddleware(getAllCurrencies),
//	},
//	//	STATUS OK
//	Route{
//		"statusOK",
//		"Создаем объект",
//		"OPTIONS",
//		"/images/products/create",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Удаляем объект",
//		"OPTIONS",
//		"/images/products/delete",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем все объекты",
//		"OPTIONS",
//		"/images/products/read_all",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем конкретный объект",
//		"OPTIONS",
//		"/images/products/read_current",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Изменяем объект",
//		"OPTIONS",
//		"/images/products/update",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Добавляем файл",
//		"OPTIONS",
//		"/images/products/upload",
//		statusOK,
//	},
//}
//
////	SERVICES
//
//// 	добавляем объект
//func createImageProduct(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("createImageProduct")
//
//	//	создаем переменные
//	var imageProduct db.ImageProduct
//	var imageProducts []db.ImageProduct
//
//	// 	переменные
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&imageProduct); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", imageProduct)
//	}
//
//	// 	дергаем функцию создания
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.CreateItem(&imageProduct); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageProducts); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageProducts)
//	}
//}
//
//// 	Удаляем объект
//func deleteImageProduct(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("deleteImageProduct")
//
//	// 	создаем переменные
//	var imageProduct db.ImageProduct
//	//var tempImageProduct db.ImageProduct
//	var imageProducts []db.ImageProduct
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	получаем параметр
//	imageProductID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageProductID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageProductID)
//		imageProduct.ID = uint(id)
//		//tempImageProduct.ID = uint(id)
//
//		//	проверяем, что такая запись есть
//		if err := tempStorage.SelectItem(&imageProduct); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		} else {
//			// 	дергаем функцию удаления
//			if err = tempStorage.DeleteItem(&imageProduct); err != nil {
//				//	вернули ошибку - возвращаем 400
//				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
//				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
//			} else {
//				//  все нормально - возвращаем 200
//				w.WriteHeader(200)
//			}
//		}
//	} else {
//		//	imageProductID is empty
//		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageProducts); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageProducts)
//	}
//}
//
//// 	Получаем список всех объектов
//func getAllImageProducts(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getAllImageProducts")
//
//	// 	создаем переменные
//	var imageProducts []db.ImageProduct
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageProducts); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageProducts)
//	}
//}
//
//// 	Получаем объект по ИД
//func getCurrentImageProduct(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getCurrentImageProduct")
//
//	// 	создаем переменные
//	var imageProduct db.ImageProduct
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	imageProductID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageProductID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageProductID)
//		imageProduct.ID = uint(id)
//		// 	возвращаем нотисы
//		if err := tempStorage.SelectItem(&imageProduct); err != nil {
//			//	вернули ошибку - возвращаем 404
//			log.Errorf("Ошибка при выборе объекта: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
//			return
//		} else {
//			//  все нормально - возвращаем 200
//			w.WriteHeader(200)
//			//	возвращаем полученные объекты
//			_ = json.NewEncoder(w).Encode(imageProduct)
//		}
//	}
//}
//
//// 	Изменяем объект
//func updateImageProduct(w http.ResponseWriter, r *http.Request) {
//	//	log
//	log.Warn("updateImageProduct")
//
//	// 	создаем переменные
//	var imageProduct db.ImageProduct
//	var tempImageProduct db.ImageProduct
//	var imageProducts []db.ImageProduct
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&imageProduct); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", imageProduct)
//		//	сохраняем ИД для поиска
//		tempImageProduct.ID = imageProduct.ID
//	}
//
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempImageProduct); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&imageProduct); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageProducts); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageProducts)
//	}
//}
//
//// 	uploadImageProduct - изменяем категорию
//func uploadImageProduct(w http.ResponseWriter, r *http.Request) {
//
//	log.Warn("uploadImageProduct")
//	// 	создаем переменные
//
//	log.Infof("%v", r)
//	fmt.Println("Метод ПОСТ")
//	err := r.ParseMultipartForm(200000)
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//	log.Infof("%v", r)
//
//	// они все тут
//	formdata := r.MultipartForm // ok, no problem so far, read the Form data
//
//	//get the *fileheaders
//	files := formdata.File["myFile"] // grab the filenames
//	//var fileHeader multipart.FileHeader
//
//	log.Warnf("files: %v", files)
//	for key, _ := range files {
//		log.Warnf("file: %v", key)
//		//file, _ := f[key].Open()
//		log.Warn("Success...")
//		//defer file.Close()
//
//	}
//	fmt.Println(len(files))
//
//	file, handler, err := r.FormFile("myFile")
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer file.Close()
//	log.Warnf("%v", handler.Header)
//
//	filename := getNewUUID()
//	//f, err := os.OpenFile("./"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
//	f, err := os.OpenFile("/usr/local/share/imageProducts/"+filename+".jpg", os.O_WRONLY|os.O_CREATE, 0666)
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer f.Close()
//	io.Copy(f, file)
//
//	//file, err := files[0].Open()
//
//	log.Warn("1")
//	log.Errorf("%v", err)
//	//defer file.Close()
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//
//	//var dbCategories []db.Category
//	//var dbCategory db.Category
//	//
//	////	подключаем хранилище
//	//tempStorage := storage
//	//
//	//// 	читаем тело запроса в буфер
//	//bs, err := ioutil.ReadAll(r.Body)
//	//if err != nil {
//	//	log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//	//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"невозможно прочитать тело запроса: %s\"}", err), http.StatusUnprocessableEntity)
//	//	return
//	//}
//	//
//	////	анмаршалим буфер в структурку
//	//err = json.Unmarshal(bs, &dbCategory)
//	//if err != nil {
//	//	log.Errorf("Неверный формат запроса: %+v", err)
//	//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"неверный формат запроса: %s\"}", err), http.StatusUnprocessableEntity)
//	//	return
//	//} else {
//	//
//	//	log.Warnf("Имеем структурку: %+v", dbCategory)
//	//}
//	//
//	//// 	дергаем функцию изменения
//	//tempStorage.UpdateItem(&dbCategory)
//	//
//	//// 	возвращаем нотисы
//	//tempStorage.SelectAllItems(&dbCategories)
//	//
//
//	w.WriteHeader(201)
//	_ = json.NewEncoder(w).Encode(filename)
//	// 	возвращаем валюту
//	//return
//}
