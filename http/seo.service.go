package http

import (
	//"crypto/md5"
	"encoding/json"

	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
)

// 	массив роутинга
var SeoRoutes = []Route{

	Route{
		"getContent",
		"Получаем конкретный объект",
		"GET",
		"/seo_brocker/update_sitemap",
		updateSitemap,
		//validateMiddleware(getCurrentAddress),
	},

	Route{
		"statusOK",
		"Получаем конкретный объект",
		"GET",
		"/seo_brocker/update_sitemap",
		statusOK,
	},
}

// 	Получаем пользователя по ИД
func updateSitemap(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentAddress")

	// 	создаем переменные
	storage.SeoCategoryUpdater()
	storage.SeoProductUpdater()
	//  все нормально - возвращаем 200
	w.WriteHeader(200)
	//	возвращаем полученные объекты
	_ = json.NewEncoder(w).Encode("finished")
}
