package http

import (
	//"crypto/md5"
	"encoding/json"
	"fmt"

	"../db"

	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var CartRoutes = []Route{
	//	сами функции
	Route{
		"createCart",
		"Создаем объект",
		"PUT",
		"/carts/create",
		createCart,
		//validateMiddleware(createCart),
	},
	Route{
		"deleteCart",
		"Удаляем объект",
		"DELETE",
		"/carts/delete",
		deleteCart,
		//validateMiddleware(deleteCart),
	},
	Route{
		"getAllCarts",
		"Получаем список всех объектов",
		"GET",
		"/carts/read_all",
		getAllCarts,
		//validateMiddleware(getAllCarts),
	},
	Route{
		"getCurrentCart",
		"Получаем конкретный объект",
		"GET",
		"/carts/read_current",
		getCurrentCart,
		//validateMiddleware(getCurrentCart),
	},
	Route{
		"updateCart",
		"Изменяем объект",
		"PATCH",
		"/carts/update",
		updateCart,
		//validateMiddleware(updateCart),
	},
	Route{
		"addProductToCart",
		"добавляем объект",
		"PATCH",
		"/carts/add_product",
		addProductToCart,
		//validateMiddleware(updateCart),
	},
	Route{
		"deleteProductFromCart",
		"удаляем объект",
		"PATCH",
		"/carts/delete_product",
		deleteProductFromCart,
		//validateMiddleware(updateCart),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/carts/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/carts/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/carts/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/carts/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/carts/update",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/carts/add_product",
		statusOK,
	},
	Route{
		"statusOK",
		"Удалить объект",
		"OPTIONS",
		"/carts/delete_product",
		statusOK,
	},
}

//	SERVICES

// 	добавляем корзину
func createCart(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createCart")

	//	создаем переменные
	var cart db.Cart
	var carts []db.Cart

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", cart)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&cart); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&carts); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(carts)
	}
}

// 	Удаляем корзину
func deleteCart(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteCart")

	// 	создаем переменные
	var cart db.Cart
	//var tempCart db.Cart
	var carts []db.Cart

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	cartID := r.URL.Query().Get("id")

	// 	имя получателя
	if cartID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(cartID)
		cart.ID = uint(id)
		//tempCart.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&cart); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&cart); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	cartID is empty
		log.Errorf("Ошибка при удалении объекта - передан пjson.NewEncoder(w).Encode(устой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&carts); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(carts)
	}
}

// 	Получаем список всех корзин
func getAllCarts(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllCarts")

	// 	создаем переменные
	var carts []db.Cart

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&carts); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(carts)
	}
}

// 	Получаем корзину по ИД
func getCurrentCart(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentCart")

	// 	создаем переменные
	var cart db.Cart

	//	подключаем хранилище
	tempStorage := storage

	cartID := r.URL.Query().Get("id")

	// 	имя получателя
	if cartID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(cartID)
		cart.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&cart); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(cart)
		}
	}
}

// 	Изменяем корзину
func updateCart(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCart")

	// 	создаем переменные
	var cart db.Cart
	var tempCart db.Cart
	var carts []db.Cart

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", cart)
		//	сохраняем ИД для поиска
		tempCart.ID = cart.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCart); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&cart); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempCart, &cart, "update", "Guest") // запись успешного удаления в лог
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&carts); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(carts)
	}
}

// 	Добавляем товар в корзину
func addProductToCart(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCart")

	// 	создаем переменные
	var cart db.Cart
	var tempCart db.Cart
	var carts []db.Cart

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", cart)
		//	сохраняем ИД для поиска
		tempCart.ID = cart.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCart); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&cart); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&carts); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(carts)
	}
}

// 	Удаляем товар из корзины
func deleteProductFromCart(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCart")

	// 	создаем переменные
	var cart db.Cart
	var tempCart db.Cart
	var carts []db.Cart

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", cart)
		//	сохраняем ИД для поиска
		tempCart.ID = cart.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCart); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&cart); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&carts); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(carts)
	}
}
