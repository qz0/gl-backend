package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var IncomingRoutes = []Route{
	//	сами функции
	Route{
		"checkedIncoming",
		"Ставим бит првоерено на объект",
		"GET",
		"/incomings/checked",
		checkedIncoming,
		//validateMiddleware(checkedIncoming),
	},
	Route{
		"createIncoming",
		"Создаем объект",
		"PUT",
		"/incomings/create",
		createIncoming,
		//validateMiddleware(createIncoming),
	},
	Route{
		"deleteIncoming",
		"Удаляем объект",
		"DELETE",
		"/incomings/delete",
		deleteIncoming,
		//validateMiddleware(deleteIncoming),
	},
	Route{
		"getAllIncomings",
		"Получаем список всех объектов",
		"GET",
		"/incomings/read_all",
		getAllIncomings,
		//validateMiddleware(getAllIncomings),
	},
	Route{
		"getCurrentIncoming",
		"Получаем конкретный объект",
		"GET",
		"/incomings/read_current",
		getCurrentIncoming,
		//validateMiddleware(getCurrentIncoming),
	},
	Route{
		"updateIncoming",
		"Изменяем объект",
		"PATCH",
		"/incomings/update",
		updateIncoming,
		//validateMiddleware(updateIncoming),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/incomings/checked",
		statusOK,
	},
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/incomings/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/incomings/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/incomings/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/incomings/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/incomings/update",
		statusOK,
	},
}

//	SERVICES

// 	Получаем объект по ИД
func checkedIncoming(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentIncoming")

	// 	создаем переменные
	var incoming db.Incoming
	var incomings []db.Incoming

	//	подключаем хранилище
	tempStorage := storage

	incomingID := r.URL.Query().Get("id")
	log.Warnf("incomingID: %v", incomingID)

	// 	имя получателя
	if incomingID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(incomingID)
		incoming.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&incoming); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			log.Warn("Incoming выбран успешно")
			//	пробуем создать items из incoming items
			for _, incomingItem := range incoming.IncomingItems {
				//	по количеству записей в amount
				for i := 0; uint(i) < incomingItem.Amount; i++ {
					//	создали новый айтем
					var item db.Item
					var selectedItem db.Item
					var err error
					//	дублируем продукт
					item.ProductID = incomingItem.ProductID
					//	дублируем список опций
					item.OptionValues = incomingItem.OptionValues
					//	проставляем статус на складе
					item.StatusID = 2 //	stock

					//	пробуем найти данные элементы  в существующих заказах
					if err, selectedItem = findItem(incomingItem.ProductID, 1, incomingItem.OptionValues); err != nil {
						//	найти не удалось создаем с нуля
						if err := tempStorage.CreateItem(&item); err != nil {
							//	вернули ошибку - возвращаем 404
							log.Errorf("Ошибка при создании объекта: %+v", err)
							http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при обновлении создании: %+v\"}", err), http.StatusNotFound)
							return
						}
					} else {
						//	удалось обнаружить меняем статус
						var tempSelectedItem db.Item //	техническая переменная для логов

						log.Warnf("Выбрали существующий айтем selectedItem: %+v", selectedItem.ID)
						//	еще один вызов для лога
						if err := tempStorage.SelectItem(&tempSelectedItem); err != nil {
							//	вернули ошибку - возвращаем 404
							log.Errorf("Ошибка при выборе объектов: %+v", err)
						}
						log.Warnf("Выбрали существующий айтем айтем: %+v", selectedItem.ID)
						//	меняем статус проставляем ордер
						selectedItem.StatusID = 4

						if err := tempStorage.UpdateItemSelect(&selectedItem, "status_id", nil); err != nil {
							//	вернули ошибку - возвращаем 400
							log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
							http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
							return
						} else {
							//  все нормально - возвращаем 200
							tempStorage.CreateLog(&tempSelectedItem, &selectedItem, "update", "Guest") // запись успешного удаления в лог

						}
					}

				}
			}

			// меняем статус checked на true
			incoming.Checked = true
			if err := tempStorage.UpdateItemSelect(&incoming, "checked", nil); err != nil {
				//	вернули ошибку - возвращаем 404
				log.Errorf("Ошибка при обновлении объекта: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при обновлении объекта: %+v\"}", err), http.StatusNotFound)
				return
			} else {
				log.Warnf("Обновление прошло успешно: %+v", err)
				// 	дергаем функцию выборки
				//	проверяем, что не вернули ошибку
				if err := tempStorage.SelectAllItems(&incomings); err != nil {
					//	вернули ошибку - возвращаем 404
					log.Errorf("Ошибка при выборе объектов: %+v", err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
					return
				} else {
					//  все нормально - возвращаем 200
					w.WriteHeader(200)
					//	возвращаем полученные объекты
					_ = json.NewEncoder(w).Encode(incomings)
				}
			}
		}
	}
}

// 	добавляем объект
func createIncoming(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createIncoming")

	//	создаем переменные
	var incoming db.Incoming
	var incomings []db.Incoming

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&incoming); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", "incoming")
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&incoming); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&incomings); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(incomings)
	}
}

// 	Удаляем объект
func deleteIncoming(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteIncoming")

	// 	создаем переменные
	var incoming db.Incoming
	//var tempIncoming db.Incoming
	var incomings []db.Incoming

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	incomingID := r.URL.Query().Get("id")

	// 	имя получателя
	if incomingID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(incomingID)
		incoming.ID = uint(id)
		//tempIncoming.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&incoming); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&incoming); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	incomingID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&incomings); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(incomings)
	}
}

// 	Получаем список всех объектов
func getAllIncomings(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllIncomings")

	// 	создаем переменные
	var incomings []db.Incoming

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&incomings); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(incomings)
	}
}

// 	Получаем объект по ИД
func getCurrentIncoming(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentIncoming")

	// 	создаем переменные
	var incoming db.Incoming

	//	подключаем хранилище
	tempStorage := storage

	incomingID := r.URL.Query().Get("id")

	// 	имя получателя
	if incomingID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(incomingID)
		incoming.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&incoming); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(incoming)
		}
	}
}

// 	Изменяем объект
func updateIncoming(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateIncoming")

	// 	создаем переменные
	var incoming db.Incoming
	var tempIncoming db.Incoming
	var incomings []db.Incoming

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&incoming); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", incoming)
		//	сохраняем ИД для поиска
		tempIncoming.ID = incoming.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempIncoming); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&incoming); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempIncoming, &incoming, "update", "Guest") // запись успешного удаления в лог
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&incomings); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(incomings)
	}
}
