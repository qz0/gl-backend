package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var DeliveryRoutes = []Route{
	//	сами функции
	Route{
		"changePriorityOfDelivery",
		"Изменяем приоритет объекта",
		"PATCH",
		"/deliveries/change_priority",
		changePriorityOfDelivery,
		//validateMiddleware(changePriorityOfDelivery),
	},
	Route{
		"createDelivery",
		"Создаем объект",
		"PUT",
		"/deliveries/create",
		createDelivery,
		//validateMiddleware(createDelivery),
	},
	Route{
		"deleteDelivery",
		"Удаляем объект",
		"DELETE",
		"/deliveries/delete",
		deleteDelivery,
		//validateMiddleware(deleteDelivery),
	},
	Route{
		"getAllDeliveries",
		"Получаем список всех объектов",
		"GET",
		"/deliveries/read_all",
		getAllDeliveries,
		//validateMiddleware(getAllDeliveries),
	},
	Route{
		"getDeliveriesWhere",
		"Получаем список всех объектов параметрам",
		"GET",
		"/deliveries/read_where",
		getDeliveriesWhere,
		//validateMiddleware(getAllDeliveries),
	},
	Route{
		"getAllDeliveriesByShop",
		"Получаем список всех объектов",
		"GET",
		"/deliveries/read_all_by_shop",
		getAllDeliveriesByShop,
		//validateMiddleware(getAllDeliveriesByShop),
	},
	Route{
		"getCurrentDelivery",
		"Получаем конкретный объект",
		"GET",
		"/deliveries/read_current",
		getCurrentDelivery,
		//validateMiddleware(getCurrentDelivery),
	},
	Route{
		"getDeliveryCost",
		"Получаем конкретный объект",
		"POST",
		"/deliveries/get_cost",
		getDeliveryCost,
		//validateMiddleware(getCurrentDelivery),
	},
	Route{
		"updateDelivery",
		"Изменяем объект",
		"PATCH",
		"/deliveries/update",
		updateDelivery,
		//validateMiddleware(updateDelivery),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/deliveries/change_priority",
		statusOK,
	},
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/deliveries/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/deliveries/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты по параметру",
		"OPTIONS",
		"/deliveries/read_where",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/deliveries/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/deliveries/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем цену доставки",
		"OPTIONS",
		"/deliveries/get_cost",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/deliveries/update",
		statusOK,
	},
}

//	SERVICES

// 	меняем приоритет объекта
func changePriorityOfDelivery(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("changePriorityOfDelivery")

	// 	создаем переменные
	var delivery db.Delivery
	var tempDelivery db.Delivery
	var deliveries []db.Delivery

	//	подключаем хранилище
	tempStorage := storage
	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&delivery); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", delivery)
		//	сохраняем ИД для поиска
		tempDelivery.ID = delivery.ID
	}
	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempDelivery); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	} else {
		log.Warnf("Запись есть: %+v", tempDelivery)
	}
	//	выясняем в какую сторону перемещение
	if delivery.Priority < tempDelivery.Priority {
		//	число приоритета уменьшили
		//	получаем список доставок где приоритет выше
		if err := tempStorage.SelectItemsWhere(&deliveries, 0, 10, "priority >= ? and priority < ?", fmt.Sprint(delivery.Priority), fmt.Sprint(tempDelivery.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentDelivery := range deliveries {
				// 	прибавляем к приоритету единицу
				currentDelivery.Priority = currentDelivery.Priority + 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentDelivery); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err),
						http.StatusBadRequest)
				} else {
					log.Warnf("Updated DELIVERY: %v", currentDelivery)
				}
			}
		}
	} else {
		//	число приоритета такое же или увеличено
		//	получаем список доставок где приоритет ниже
		if err := tempStorage.SelectItemsWhere(&deliveries, 0, 10, "priority <= ? and priority > ?",
			fmt.Sprint(delivery.Priority), fmt.Sprint(tempDelivery.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentDelivery := range deliveries {
				// 	отнимаем от приоритета единицу
				currentDelivery.Priority = currentDelivery.Priority - 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentDelivery); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
				} else {
					log.Infof("Updated DELIVERY Down priority: %v", currentDelivery)
				}
			}
		}
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&delivery); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден_: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&deliveries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveries)
	}
}

// 	добавляем объект
func createDelivery(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createDelivery")

	//	создаем переменные
	var delivery db.Delivery
	var deliveries []db.Delivery

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&delivery); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", delivery)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&delivery); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&deliveries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveries)
	}
}

// 	Удаляем объект
func deleteDelivery(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteDelivery")

	// 	создаем переменные
	var delivery db.Delivery
	//var tempDelivery db.Delivery
	var deliveries []db.Delivery

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	deliveryID := r.URL.Query().Get("id")

	// 	имя получателя
	if deliveryID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(deliveryID)
		delivery.ID = uint(id)
		//tempDelivery.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&delivery); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&delivery); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	deliveryID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&deliveries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveries)
	}
}

//	получаем цену доставки по весу
func getCostByWeight(weight float32, costZones []db.ZoneCost) (error, uint) {

	//	возможно в начале отсортировать
	var cost uint

	//	вышибаем по большему
	for _, costZone := range costZones {
		if weight < costZone.Weight {
			return nil, costZone.Cost
		}
		cost = costZone.Cost
	}
	return nil, cost
}

// 	Получаем список всех объектов для магазина
func getAllDeliveriesByShop(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllDeliveriesByShop")

	// 	создаем переменные
	var deliveries []db.Delivery

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&deliveries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveries)
	}
}

// 	Получаем список всех объектов
func getAllDeliveries(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllDeliveries")

	// 	создаем переменные
	var deliveries []db.Delivery

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&deliveries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveries)
	}
}

// 	Получаем список всех объектов
func getDeliveriesWhere(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllDeliveries")

	// 	создаем переменные
	var deliveries []db.Delivery

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectItemsWhere(&deliveries, 0, 100, "enabled = ?", fmt.Sprint("true")); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveries)
	}
}

// 	Получаем объект по ИД
func getCurrentDelivery(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentDelivery")

	// 	создаем переменные
	var delivery db.Delivery

	//	подключаем хранилище
	tempStorage := storage

	deliveryID := r.URL.Query().Get("id")

	// 	имя получателя
	if deliveryID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(deliveryID)
		delivery.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&delivery); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(delivery)
		}
	}
}

// 	добавляем объект
func getDeliveryCost(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createDelivery")

	//	создаем переменные
	var cart db.Cart
	var deliveries []db.Delivery
	var deliveryCost uint
	//var totalWeight float32
	//var delivery db.Delivery
	//var zone db.Zone
	var err error
	//var weightType bool

	// 	переменные
	tempStorage := storage

	//log.Warn("проверяем, что не вернули ошибку")
	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	}

	//log.Warnf("cart.Address: %+v", cart.Address)
	if err, deliveryCost = getDeliveryCostLocal(cart.CartItems, cart.Address, cart.DeliveryID); err != nil {
		log.Errorf("Ошибка при подсчете цены доставки: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при подсчете цены доставки: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	}
	//delivery.ID = cart.DeliveryID
	//
	////	дергаем специальный вызов для получения списка цен доставок
	//if err, zone, weightType = tempStorage.SelectDeliverySpecial(&delivery, cart.Address.Country.ID); err != nil {
	//	//	вернули ошибку - возвращаем 404
	//	log.Errorf("Ошибка при выборе объекта: %+v", err)
	//}
	//
	////	получить суммарный вес товаров исходя из доставки
	//if len(cart.CartItems) > 0 {
	//	if err, weight, vWeight := getWeightsByProducts(cart.CartItems); err != nil {
	//		//	вернули ошибку - возвращаем 422
	//		log.Errorf("Ошибка при получении totalWeight: %+v", err)
	//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при получении totalWeight: : %+v\"}", err), http.StatusUnprocessableEntity)
	//		return
	//	} else {
	//		log.Warnf("weightType: %+v", weightType)
	//		if !weightType {
	//			if weight > float32(vWeight) {
	//				log.Warnf("Weight: %+v", weight)
	//				totalWeight = weight
	//			} else {
	//				log.Warnf("vWeight: %+v", vWeight)
	//				totalWeight = float32(vWeight)
	//				//log.Warnf("totalWeight: %+v", totalWeight)
	//			}
	//		} else {
	//			totalWeight = weight
	//		}
	//	}
	//}
	//log.Warnf("Итого вес: %+v", totalWeight)
	//
	////	получить по весу из зонекоста цену доставки
	//if err, cost := getCostByWeight(totalWeight, zone.ZoneCosts); err != nil {
	//	//	вернули ошибку - возвращаем 404
	//	log.Errorf("Ошибка при подборе цены к весу: %+v", err)
	//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при подборе цены к весу: : %+v\"}", err), http.StatusNotFound)
	//	return
	//} else {
	//	deliveryCost = cost
	//}
	//log.Warnf("Итого цена: %+v", deliveryCost)

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&deliveries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveryCost)
	}
}

//	подсчет цены доставки локально
func getDeliveryCostLocal(cartItems []db.CartItem, address db.Address, deliveryID uint) (error, uint) {
	log.Warnf("getDeliveryCostLocal: %+v", len(cartItems))
	//	создаем переменные
	var deliveryCost uint
	var totalWeight float32
	var delivery db.Delivery
	var zone db.Zone
	var err error
	var weightType bool

	// 	переменные
	tempStorage := storage

	delivery.ID = deliveryID

	//	дергаем специальный вызов для получения списка цен доставок
	if err, zone, weightType = tempStorage.SelectDeliverySpecial(&delivery, address.Country.ID); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объекта: %+v", err)
	}

	//	получить суммарный вес товаров исходя из доставки
	if len(cartItems) > 0 {
		if err, weight, vWeight := getWeightsByProducts(cartItems); err != nil {
			//	вернули ошибку - возвращаем 422
			log.Errorf("Ошибка при получении totalWeight: %+v", err)
			return err, deliveryCost
		} else {
			log.Warnf("weightType: %+v", weightType)
			if !weightType {
				if weight > float32(vWeight) {
					log.Warnf("Weight: %+v", weight)
					totalWeight = weight
				} else {
					log.Warnf("vWeight: %+v", vWeight)
					totalWeight = float32(vWeight)
					//log.Warnf("totalWeight: %+v", totalWeight)
				}
			} else {
				totalWeight = weight
			}
		}
	}
	log.Warnf("Итого вес: %+v", totalWeight)

	//	получить по весу из зонекоста цену доставки
	if err, cost := getCostByWeight(totalWeight, zone.ZoneCosts); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при подборе цены к весу: %+v", err)
		return err, deliveryCost
	} else {
		deliveryCost = cost
	}
	log.Warnf("Итого цена: %+v", deliveryCost)
	return nil, deliveryCost
}

// 	Изменяем объект
func updateDelivery(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateDelivery")

	// 	создаем переменные
	var delivery db.Delivery
	var tempDelivery db.Delivery
	var deliveries []db.Delivery

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&delivery); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", delivery)
		//	сохраняем ИД для поиска
		tempDelivery.ID = delivery.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempDelivery); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&delivery); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempDelivery, &delivery, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&deliveries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(deliveries)
	}
}
