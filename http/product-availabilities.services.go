package http

import (
	//"crypto/md5"
	"encoding/json"
	"fmt"
	"../db"
	log "github.com/sirupsen/logrus"
	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
)

// 	массив роутинга
var ProductAvailabilityRoutes = []Route{
	//	сами функции
	//Route{
	//	"createProductAvailabilityStatus",
	//	"Создаем объект",
	//	"PUT",
	//	"/product_availability_statuses/create",
	//	createProductAvailabilityStatus,
	//	//validateMiddleware(createProductAvailabilityStatus),
	//},
	//Route{
	//	"deleteProductAvailabilityStatus",
	//	"Удаляем объект",
	//	"DELETE",
	//	"/product_availability_statuses/delete",
	//	deleteProductAvailabilityStatus,
	//	//validateMiddleware(deleteProductAvailabilityStatus),
	//},
	Route{
		"getAllProductAvailabilities",
		"Получаем список всех объектов",
		"GET",
		"/product_availabilities/read_all",
		getAllProductAvailabilities,
		//validateMiddleware(getAllProductAvailabilityStatuses),
	},
	//Route{
	//	"getCurrentProductAvailabilityStatus",
	//	"Получаем конкретный объект",
	//	"GET",
	//	"/product_availability_statuses/read_current",
	//	getCurrentProductAvailabilityStatus,
	//	//validateMiddleware(getCurrentProductAvailabilityStatus),
	//},
	//Route{
	//	"updateProductAvailabilityStatus",
	//	"Изменяем объект",
	//	"PATCH",
	//	"/product_availability_statuses/update",
	//	updateProductAvailabilityStatus,
	//	//validateMiddleware(updateProductAvailabilityStatus),
	//},
	//	STATUS OK
	//Route{
	//	"statusOK",
	//	"Создаем объект",
	//	"OPTIONS",
	//	"/product_availability_statuses/create",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Удаляем объект",
	//	"OPTIONS",
	//	"/product_availability_statuses/delete",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/product_availabilities/read_all",
		statusOK,
	},
	//Route{
	//	"statusOK",
	//	"Получаем конкретный объект",
	//	"OPTIONS",
	//	"/product_availability_statuses/read_current",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Изменяем объект",
	//	"OPTIONS",
	//	"/product_availability_statuses/update",
	//	statusOK,
	//},
}

//	SERVICES

// 	добавляем объект
//func createProductAvailabilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("createProductAvailabilityStatus")
//
//	//	создаем переменные
//	var productAvailability db.ProductAvailability
//	var productAvailabilities []db.ProductAvailability
//
//	// 	переменные
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&productAvailability); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", productAvailability)
//	}
//
//	// 	дергаем функцию создания
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.CreateItem(&productAvailability); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&productAvailabilities); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(productAvailabilities)
//	}
//}

// 	Удаляем объект
//func deleteProductAvailabilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("deleteProductAvailabilityStatus")
//
//	// 	создаем переменные
//	var productAvailabilityStatus db.ProductAvailabilityStatus
//	//var tempProductAvailabilityStatus db.ProductAvailabilityStatus
//	var productAvailabilityStatuses []db.ProductAvailabilityStatus
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	получаем параметр
//	productAvailabilityStatusID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if productAvailabilityStatusID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(productAvailabilityStatusID)
//		productAvailabilityStatus.ID = uint(id)
//		//tempProductAvailabilityStatus.ID = uint(id)
//
//		//	проверяем, что такая запись есть
//		if err := tempStorage.SelectItem(&productAvailabilityStatus); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		} else {
//			// 	дергаем функцию удаления
//			if err = tempStorage.DeleteItem(&productAvailabilityStatus); err != nil {
//				//	вернули ошибку - возвращаем 400
//				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
//				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
//			} else {
//				//  все нормально - возвращаем 200
//				w.WriteHeader(200)
//			}
//		}
//	} else {
//		//	productAvailabilityStatusID is empty
//		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&productAvailabilityStatuses); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(productAvailabilityStatuses)
//	}
//}

// 	Получаем список всех объектов
func getAllProductAvailabilities(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllProductAvailabilityStatuses")

	// 	создаем переменные
	var productAvailabilities []db.ProductAvailability

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&productAvailabilities); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(productAvailabilities)
	}
}

// 	Получаем объект по ИД
//func getCurrentProductAvailabilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getCurrentProductAvailabilityStatus")
//
//	// 	создаем переменные
//	var productAvailabilityStatus db.ProductAvailabilityStatus
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	productAvailabilityStatusID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if productAvailabilityStatusID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(productAvailabilityStatusID)
//		productAvailabilityStatus.ID = uint(id)
//		// 	возвращаем нотисы
//		if err := tempStorage.SelectItem(&productAvailabilityStatus); err != nil {
//			//	вернули ошибку - возвращаем 404
//			log.Errorf("Ошибка при выборе объекта: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
//			return
//		} else {
//			//  все нормально - возвращаем 200
//			w.WriteHeader(200)
//			//	возвращаем полученные объекты
//			_ = json.NewEncoder(w).Encode(productAvailabilityStatus)
//		}
//	}
//}

// 	Изменяем объект
//func updateProductAvailabilityStatus(w http.ResponseWriter, r *http.Request) {
//	//	log
//	log.Warn("updateProductAvailabilityStatus")
//
//	// 	создаем переменные
//	var productAvailabilityStatus db.ProductAvailabilityStatus
//	var tempProductAvailabilityStatus db.ProductAvailabilityStatus
//	var productAvailabilityStatuses []db.ProductAvailabilityStatus
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&productAvailabilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", productAvailabilityStatus)
//		//	сохраняем ИД для поиска
//		tempProductAvailabilityStatus.ID = productAvailabilityStatus.ID
//	}
//
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempProductAvailabilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&productAvailabilityStatus); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&productAvailabilityStatuses); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(productAvailabilityStatuses)
//	}
//}
