package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var OrderStatusRoutes = []Route{
	//	сами функции
	//Route{
	//	"createOrderStatus",
	//	"Создаем объект",
	//	"PUT",
	//	"/order_statuses/create",
	//	createOrderStatus,
	//	//validateMiddleware(createOrderStatus),
	//},
	//Route{
	//	"deleteOrderStatus",
	//	"Удаляем объект",
	//	"DELETE",
	//	"/order_statuses/delete",
	//	deleteOrderStatus,
	//	//validateMiddleware(deleteOrderStatus),
	//},
	Route{
		"getAllOrderStatuses",
		"Получаем список всех объектов",
		"GET",
		"/order_statuses/read_all",
		getAllOrderStatuses,
		//validateMiddleware(getAllOrderStatuses),
	},
	//Route{
	//	"getCurrentOrderStatus",
	//	"Получаем конкретный объект",
	//	"GET",
	//	"/order_statuses/read_current",
	//	getCurrentOrderStatus,
	//	//validateMiddleware(getCurrentOrderStatus),
	//},
	//Route{
	//	"updateOrderStatus",
	//	"Изменяем объект",
	//	"PATCH",
	//	"/order_statuses/update",
	//	updateOrderStatus,
	//	//validateMiddleware(updateOrderStatus),
	//},
	//	STATUS OK
	//Route{
	//	"statusOK",
	//	"Создаем объект",
	//	"OPTIONS",
	//	"/order_statuses/create",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Удаляем объект",
	//	"OPTIONS",
	//	"/order_statuses/delete",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/order_statuses/read_all",
		statusOK,
	},
	//Route{
	//	"statusOK",
	//	"Получаем конкретный объект",
	//	"OPTIONS",
	//	"/order_statuses/read_current",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Изменяем объект",
	//	"OPTIONS",
	//	"/order_statuses/update",
	//	statusOK,
	//},
}

//	SERVICES

// 	добавляем объект
func createOrderStatus(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createOrderStatus")

	//	создаем переменные
	var orderStatus db.OrderStatus
	var orderStatuses []db.OrderStatus

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&orderStatus); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", orderStatus)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&orderStatus); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderStatuses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderStatuses)
	}
}

// 	Удаляем объект
func deleteOrderStatus(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteOrderStatus")

	// 	создаем переменные
	var orderStatus db.OrderStatus
	//var tempOrderStatus db.OrderStatus
	var orderStatuses []db.OrderStatus

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	orderStatusID := r.URL.Query().Get("id")

	// 	имя получателя
	if orderStatusID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(orderStatusID)
		orderStatus.ID = uint(id)
		//tempOrderStatus.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&orderStatus); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&orderStatus); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	orderStatusID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderStatuses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderStatuses)
	}
}

// 	Получаем список всех объектов
func getAllOrderStatuses(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOrderStatuses")

	// 	создаем переменные
	var orderStatuses []db.OrderStatus

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderStatuses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderStatuses)
	}
}

// 	Получаем объект по ИД
func getCurrentOrderStatus(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentOrderStatus")

	// 	создаем переменные
	var orderStatus db.OrderStatus

	//	подключаем хранилище
	tempStorage := storage

	orderStatusID := r.URL.Query().Get("id")

	// 	имя получателя
	if orderStatusID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(orderStatusID)
		orderStatus.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&orderStatus); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(orderStatus)
		}
	}
}

// 	Изменяем объект
func updateOrderStatus(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateOrderStatus")

	// 	создаем переменные
	var orderStatus db.OrderStatus
	var tempOrderStatus db.OrderStatus
	var orderStatuses []db.OrderStatus

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&orderStatus); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", orderStatus)
		//	сохраняем ИД для поиска
		tempOrderStatus.ID = orderStatus.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOrderStatus); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&orderStatus); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempOrderStatus, &orderStatus, "update", "Guest") // запись успешного удаления в лог

		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orderStatuses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orderStatuses)
	}
}
