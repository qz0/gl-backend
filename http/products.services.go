package http

import (
	//"crypto/md5"
	"encoding/json"
	"errors"
	"fmt"
	"math"

	"../db"

	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var ProductRoutes = []Route{
	//	сами функции
	Route{
		"changePriorityOfProduct",
		"Изменяем приоритет объекта",
		"PATCH",
		"/products/change_priority",
		changePriorityOfProduct,
		//validateMiddleware(changePriorityOfProduct),
	},
	Route{
		"createProduct",
		"Создаем объект",
		"PUT",
		"/products/create",
		createProduct,
		//validateMiddleware(createProduct),
	},
	Route{
		"deleteProduct",
		"Удаляем объект",
		"DELETE",
		"/products/delete",
		deleteProduct,
		//validateMiddleware(deleteProduct),
	},
	Route{
		"getAllProducts",
		"Получаем список всех объектов",
		"GET",
		"/products/read_all",
		getAllProducts,
		//validateMiddleware(getAllProducts),
	},
	Route{
		"getAllProducts",
		"Получаем список всех объектов",
		"GET",
		"/products/read_all_by_shop",
		getAllProductsByShop,
		//validateMiddleware(getAllProductsByShop),
	},
	Route{
		"getAllProductsByCategory",
		"Получаем список всех объектов из определенной категории",
		"POST",
		"/products/read_all_by_category",
		getAllProductsByCategory,
		//validateMiddleware(getAllProductsByCategory),
	},
	Route{
		"getCurrentProduct",
		"Получаем конкретный объект",
		"GET",
		"/products/read_current",
		getCurrentProduct,
		//validateMiddleware(getCurrentProduct),
	},
	Route{
		"updateProduct",
		"Изменяем объект",
		"PATCH",
		"/products/update",
		updateProduct,
		//validateMiddleware(updateProduct),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/products/change_priority",
		statusOK,
	},
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/products/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/products/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/products/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/products/read_all_by_shop",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем список всех объектов из определенной категории",
		"OPTIONS",
		"/products/read_all_by_category",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/products/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/products/update",
		statusOK,
	},
}

//	SERVICES
func changePriorityOfProduct(w http.ResponseWriter, r *http.Request) {
	// моя копипаста, но вроде верно понял суть.
	// 	создаем переменные
	var product db.Product
	var tempProduct db.Product
	var products []db.Product

	//	подключаем хранилище
	tempStorage := storage
	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", product)
		//	сохраняем ИД для поиска
		tempProduct.ID = product.ID
	}
	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempProduct); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	} else {
		log.Warn("Запись есть")
	}
	//	выясняем в какую сторону перемещение
	if product.Priority < tempProduct.Priority {
		//	число приоритета уменьшили
		//	получаем список доставок где приоритет выше
		if err := tempStorage.SelectItemsWhere(&products, 0, 10, "priority >= ? and priority < ?", fmt.Sprint(product.Priority), fmt.Sprint(tempProduct.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentProduct := range products {
				// 	прибавляем к приоритету единицу
				currentProduct.Priority = currentProduct.Priority + 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentProduct); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err),
						http.StatusBadRequest)
				} else {
					log.Warnf("Updated Product: %v", currentProduct)
				}
			}
		}
	} else {
		//	число приоритета такое же или увеличено
		//	получаем список доставок где приоритет ниже
		if err := tempStorage.SelectItemsWhere(&products, 0, 10, "priority <= ? and priority > ?",
			fmt.Sprint(product.Priority), fmt.Sprint(tempProduct.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentProduct := range products {
				// 	прибавляем к приоритету единицу
				currentProduct.Priority = currentProduct.Priority - 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentProduct); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
				} else {
					log.Infof("Updated PRODUCT: %v", currentProduct)
				}
			}
		}
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&product); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден_: : %+v\"}", err), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&products); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(products)
	}
}

// 	добавляем объект
func createProduct(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createProduct")

	//	создаем переменные
	var product db.Product
	var products []db.Product

	// 	переменные
	tempStorage := storage

	// flag проверки изменения сеолинк
	seoflag := 0
	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", product)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&product); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//обновляем Seo Sitemap, если был изменён сеолинк продукта
		seoflag = 1
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&products); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 201
		w.WriteHeader(201)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(products)

		// если изменилось сео, обновляем sitemap
		if seoflag == 1 {
			tempStorage.SeoProductUpdater()
		}

	}
}

// 	Удаляем объект
func deleteProduct(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteProduct")

	// 	создаем переменные
	var product db.Product
	var products []db.Product

	//	подключаем хранилище
	tempStorage := storage
	//flag проверки, обновлялось ли сео
	seoflag := 0
	// 	получаем параметр
	productID := r.URL.Query().Get("id")

	// 	имя получателя
	if productID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(productID)
		product.ID = uint(id)
		//tempProduct.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&product); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&product); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//обновляем Seo Sitemap, если был изменён сеолинк продукта
				seoflag = 1
			}
		}
	} else {
		//	productID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&products); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(products)

		//если изменилось сео, обновляем sitemap
		if seoflag == 1 {
			tempStorage.SeoProductUpdater()
		}
	}
}

// 	Получаем список всех продуктов из категории
func getAllProductsByCategory(w http.ResponseWriter, r *http.Request) {
	//	log
	// 	создаем переменные
	var category db.Category

	//	подключаем хранилище
	tempStorage := storage

	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		if err := tempStorage.SelectItem(&category); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(category.Products)
		}
	}

}

// 	Получаем список всех объектов
func getAllProducts(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllProducts")

	// 	создаем переменные
	var products []db.Product
	var productsShort []db.ProductShort

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&products); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		for _, currentProduct := range products {
			productsShort = append(productsShort, db.ConvertProductLongToShort(currentProduct))
		}
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(productsShort)
	}
}

func getAllProductsByShop(w http.ResponseWriter, r *http.Request) {
	// 	создаем переменные
	var products []db.Product

	//	подключаем хранилище
	tempStorage := storage

	shop := r.URL.Query().Get("shop")
	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectItemsWhere(&products, 0, 10, "product_availability_id = ?", fmt.Sprint(shop)); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
		return

	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(products)
	}
}

// 	Получаем объект по ИД
func getCurrentProduct(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentProduct")

	productID := r.URL.Query().Get("id")

	// 	имя получателя
	if productID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(productID)
		// 	возвращаем нотисы
		if err, product := getProductByID(uint(id)); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(product)
		}
	}
}

//	Получаем продукт по ИД
func getProductByID(productID uint) (error, db.Product) {
	// 	создаем переменные
	var product db.Product

	//	подключаем хранилище
	tempStorage := storage

	if productID > 0 {

		//	присваиваем ид
		product.ID = productID

		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&product); err != nil {

			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			return err, product

		} else {
			//	возвращаем продукт
			return nil, product
		}
	} else {
		log.Errorf("Ошибка при выборе объекта: индекс 0")
		return errors.New("ошибка при выборе объекта: индекс 0"), product
	}
}

// 	Получаем объект по SEO link
func getProductBySeoLink(seoLink string, products *[]db.Product) error {
	//	log
	//log.Warn("getCurrentProduct")

	//	подключаем хранилище
	tempStorage := storage

	//productID := r.URL.Query().Get("id")
	//var products2 []db.Product

	// 	имя получателя
	if seoLink != "" {
		// 	возвращаем нотисы
		if err := tempStorage.SelectItemsWhere(products, 0, 1, "seo_link = ?", seoLink); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			return err
		} else {
			//  все нормально - возвращаем 200
			log.Errorf("Выбран продукт: %+v", *products)
			return nil
		}
	} else {
		log.Error("Пустой линк")
		return errors.New("пустой линк")
	}
}

//	получаем цену по продуктам
func getCostsByProducts(cartItems []db.CartItem, currencyID uint) (error, uint) {

	//	переменные
	var totalCost uint = 0
	//var multiplier float32 = 1

	//	получаем валюту
	var currency db.Currency

	//	подключаем хранилище
	tempStorage := storage

	// 	имя получателя
	if currencyID > 0 {
		// 	конвертируем в UINT
		currency.ID = currencyID
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&currency); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Не удалось получить текущую валюту: %+v", err)
			return errors.New("Не удалось получить текущую валюту"), 0
		} else {
			//  все нормально - возвращаем 200
			//multiplier = currency.Multiplier
		}
	}

	//	перебираем массив
	for _, currentCI := range cartItems {

		if currentCI.ProductID > 0 {

			//	получаем продукт
			if err, product := getProductByID(currentCI.ProductID); err != nil {

				log.Error("При получении продуктов из базы обнаружена ошибка: %+v", err)
				return err, 0

			} else {

				if product.Price.Sell > 0 {
					//	обычный вес
					//totalCost += uint(math.Ceil(float64(product.Price.Sell) / float64(multiplier)))
					totalCost += uint(math.Ceil(float64(product.Price.Sell)))

					log.Warnf("\nЦена: %+v", product)

				} else {

					log.Error("В кортеже продукт с нулевой ценой: %+v", product)
					return errors.New("В кортеже продукт с нулевой ценой: %+v"), 0
				}
			}
		} else {

			log.Error("В кортеже продукт с нулевым индексом")
			return errors.New("В кортеже продукт с нулевым индексом"), 0
		}
	}

	//	добавить доставку

	//	если оба 0 выводим ошибку, иначе вес товара
	if totalCost == 0 {
		log.Error("Итоговая цена нулевая")
		return errors.New("Итоговая цена нулевая"), 0
	} else {
		return nil, totalCost
	}

}

//	получаем веса по продуктам
func getWeightsByProducts(cartItems []db.CartItem) (error, float32, uint) {

	//	переменные
	var weights float32 = 0
	var vWeights uint = 0

	//	перебираем массив
	for _, currentCI := range cartItems {

		if currentCI.ProductID > 0 {

			//	получаем продукт
			if err, product := getProductByID(currentCI.ProductID); err != nil {

				log.Error("При получении продуктов из базы обнаружена ошибка: %+v", err)
				return err, 0, 0

			} else {

				if product.Weight > 0 {
					//	обычный вес
					weights += product.Weight * float32(currentCI.OrderCount)

					log.Warnf("Новый вес: %+v", weights)

				} else {

					log.Error("В кортеже продукт с нулевым весом: %+v", product)
					return errors.New("В кортеже продукт с нулевым весом: %+v"), 0, 0
				}

				//	объемный вес

				if product.VolumeWeightX > 0 && product.VolumeWeightY > 0 && product.VolumeWeightZ > 0 {
					//	обычный вес
					vWeights += getVoumeWeightByXYZ(product.VolumeWeightX, product.VolumeWeightY, product.VolumeWeightZ)

				} else {

					log.Error("В кортеже продукт с нулевым параметром объемного веса: %+v", product)
					return errors.New("В кортеже продукт с нулевым параметром объемного веса: %+v"), 0, 0
				}
			}
		} else {

			log.Error("В кортеже продукт с нулевым индексом")
			return errors.New("В кортеже продукт с нулевым индексом"), 0, 0
		}

	}

	//	если оба 0 выводим ошибку, иначе вес товара
	if weights == 0 && vWeights == 0 {
		log.Error("Оба веса нулевые")
		return errors.New("Оба веса нулевые"), 0, 0
	} else {
		return nil, weights, vWeights
	}

}

//	Получение объемного веса
func getVoumeWeightByXYZ(x uint, y uint, z uint) uint {

	return x * y * z / 5000
}

// 	Изменяем объект
func updateProduct(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateProduct")

	// 	создаем переменные
	var product db.Product
	var tempProduct db.Product
	var products []db.Product
	//var productsShort []db.ProductShort
	seoflag := 0
	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", product)
		//	сохраняем ИД для поиска
		tempProduct.ID = product.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempProduct); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&product); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//делаем темп жсона продукта
		tempStorage.CreateLog(&tempProduct, &product, "update", "Guest") // запись успешного удаления в лог
		seoflag = 1
		// }
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&products); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		//_ = json.NewEncoder(w).Encode(products)
		_ = json.NewEncoder(w).Encode(products)

		//если изменилось сео, обновляем sitemap
		if seoflag == 1 {
			tempStorage.SeoProductUpdater()
		}
	}
}
