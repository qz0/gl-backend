package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var RegionRoutes = []Route{
	//	сами функции
	//Route{
	//	"createRegion",
	//	"Создаем объект",
	//	"PUT",
	//	"/regions/create",
	//	createRegion,
	//	//validateMiddleware(createRegion),
	//},
	//Route{
	//	"deleteRegion",
	//	"Удаляем объект",
	//	"DELETE",
	//	"/regions/delete",
	//	deleteRegion,
	//	//validateMiddleware(deleteRegion),
	//},
	Route{
		"getAllRegions",
		"Получаем список всех объектов",
		"GET",
		"/regions/read_all",
		getAllRegions,
		//validateMiddleware(getAllRegions),
	},
	Route{
		"getAllRegionsShort",
		"Получаем список всех объектов в краткой форме",
		"GET",
		"/regions/read_all_short",
		getAllRegionsShort,
		//validateMiddleware(getAllRegions),
	},
	Route{
		"getAllRegionsByCountry",
		"Получаем список всех объектов",
		"GET",
		"/regions/read_all_by_country_id",
		getRegionsByCountryID,
		//validateMiddleware(getAllRegions),
	},
	Route{
		"getCurrentRegion",
		"Получаем конкретный объект",
		"GET",
		"/regions/read_current",
		getCurrentRegion,
		//validateMiddleware(getCurrentRegion),
	},
	//Route{
	//	"updateRegion",
	//	"Изменяем объект",
	//	"PATCH",
	//	"/regions/update",
	//	updateRegion,
	//	//validateMiddleware(updateRegion),
	//},
	//	STATUS OK
	//Route{
	//	"statusOK",
	//	"Создаем объект",
	//	"OPTIONS",
	//	"/regions/create",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Удаляем объект",
	//	"OPTIONS",
	//	"/regions/delete",
	//	statusOK,
	//},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/regions/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/regions/read_all_by_country_id",
		statusOK,
	},
	//Route{
	//	"statusOK",
	//	"Получаем конкретный объект",
	//	"OPTIONS",
	//	"/regions/read_current",
	//	statusOK,
	//},
	//Route{
	//	"statusOK",
	//	"Изменяем объект",
	//	"OPTIONS",
	//	"/regions/update",
	//	statusOK,
	//},
}

//	SERVICES

// 	добавляем объект
func createRegion(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createRegion")

	//	создаем переменные
	var region db.Region
	var regions []db.Region

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&region); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", region)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&region); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&regions); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(regions)
	}
}

// 	Удаляем объект
func deleteRegion(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteRegion")

	// 	создаем переменные
	var region db.Region
	//var tempRegion db.Region
	var regions []db.Region

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	regionID := r.URL.Query().Get("id")

	// 	имя получателя
	if regionID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(regionID)
		region.ID = uint(id)
		//tempRegion.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&region); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&region); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	regionID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&regions); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(regions)
	}
}

// 	Получаем список всех объектов
func getAllRegions(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("getAllRegions")

	// 	создаем переменные
	var regions []db.Region

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&regions); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(regions)
	}
}

// 	Получаем список всех объектов
func getAllRegionsShort(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("getAllRegionsShort")

	// 	создаем переменные
	var regions []db.Region
	var regionsShort []db.RegionShort

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&regions); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//	перекодируем в сжатую версию
		for _, region := range regions {
			regionsShort = append(regionsShort, db.ConvertRegionLongToShort(region))
		}
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(regionsShort)
	}
}

// 	Получаем список всех объектов по конкретной стране
func getRegionsByCountryID(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllRegions")

	// 	создаем переменные
	var regions []db.Region
	var regionsShort []db.RegionShort

	//	подключаем хранилище
	tempStorage := storage

	countryID := r.URL.Query().Get("countryID")

	// 	имя получателя
	if countryID != "" {
		// 	конвертируем в UINT
		//countryID, _ := strconv.Atoi(ID)

		log.Errorf("поиск с идентификатором: %+v", countryID)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItemsWhere(&regions, 0, 1000, "country_id = ?", fmt.Sprint(countryID)); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объектов: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//	перекодируем в сжатую версию
			for _, region := range regions {
				regionsShort = append(regionsShort, db.ConvertRegionLongToShort(region))
			}
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(regionsShort)
		}
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
}

// 	Получаем объект по ИД
func getCurrentRegion(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentRegion")

	// 	создаем переменные
	var region db.Region

	//	подключаем хранилище
	tempStorage := storage

	regionID := r.URL.Query().Get("id")

	// 	имя получателя
	if regionID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(regionID)
		region.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&region); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(region)
		}
	}
}

// 	Изменяем объект
func updateRegion(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateRegion")

	// 	создаем переменные
	var region db.Region
	var tempRegion db.Region
	var regions []db.Region

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&region); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", region)
		//	сохраняем ИД для поиска
		tempRegion.ID = region.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempRegion); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&region); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {

		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempRegion, &region, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&regions); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(regions)
	}
}
