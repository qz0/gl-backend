package http

import "net/http"

// 	структура для маршрута
type Route struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Method      string `json:"method"`
	// APIVersion  int              `json:"api_version"`
	Path        string           `json:"pattern"`
	HandlerFunc http.HandlerFunc `json:"-"`
}

type Exception struct {
	Message string `json:"message"`
}

type JwtToken struct {
	Token string `json:"token"`
}

type TokenClaim struct {
	ID       int    `json:"id"`
	UserName string `json:"userName"`
}

type Logon struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type PaymentResponse struct {
	ID           string `json:"id"`
	CreateTime   string `json:"create_time"`
	ResourceType string `json:"resource_type"`
	EventType    string `json:"event_type"`
	Summary      string `json:"summary"`
}

//	paypal
//	PP register request
type PaypalRegisterRequest struct {
	Intent             string                   `json:"intent"`
	PurchaseUnits      []PaypalPurchaseUnit     `json:"purchase_units"`
	ApplicationContext PaypalApplicationContext `json:"application_context"`
}

//	PP PurchaseUnit for RegisterRequest
type PaypalPurchaseUnit struct {
	InvoiceID string                     `json:"invoice_id"`
	Amount    PaypalPurchaseUnitAmount   `json:"amount"`
	Payments  PaypalPurchaseUnitPayments `json:"payments"`
}

//	PP PurchaseUnitAmount for PurchaseUnit
type PaypalPurchaseUnitAmount struct {
	CurrencyCode string `json:"currency_code"`
	Value        string `json:"value"`
}

//	PP PaypalPurchaseUnitPayments for PurchaseUnit
type PaypalPurchaseUnitPayments struct {
	Captures []PaypalPurchaseUnitPaymentsCaptures `json:"captures"`
}

//	PP PaypalPurchaseUnitPaymentsCaptures for PaypalPurchaseUnitPayments
type PaypalPurchaseUnitPaymentsCaptures struct {
	InvoiceID string `json:"invoice_id"`
}

//	PP ApplicationContext for RegisterRequest
type PaypalApplicationContext struct {
	ReturnURL string `json:"return_url"`
}

//	PP register response
type PaypalRegisterResponse struct {
	ID     string       `json:"id"`
	Links  []PaypalLink `json:"links"`
	Status string       `json:"status"`
}

//	PP link for RegisterResponse
type PaypalLink struct {
	Href   string `json:"href"`
	Rel    string `json:"rel"`
	Method string `json:"method"`
}

//	PP check status request
type PaypalCheckStatusRequest struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
	OrderID  string `json:"orderId"`
	Language string `json:"language"`
}

//	PP check status response
type PaypalCheckStatusResponse struct {
	ID            string               `json:"id"`
	PurchaseUnits []PaypalPurchaseUnit `json:"purchase_units"`
	Links         []PaypalLink         `json:"links"`
	Status        string               `json:"status"`

	//{
	//	"id": "55A17048CN715352N",
	//	"purchase_units": [
	//{
	//	"reference_id": "default",
	//	"shipping": {
	//	"name": {
	//	"full_name": "John Doe"
	//},
	//	"address": {
	//	"address_line_1": "1 Main St",
	//	"admin_area_2": "San Jose",
	//	"admin_area_1": "CA",
	//	"postal_code": "95131",
	//	"country_code": "US"
	//}
	//},
	//		"payments": {
	//	"captures": [
	//{
	//	"id": "7XB8511609517342V",
	//	"status": "COMPLETED",
	//	"amount": {
	//	"currency_code": "USD",
	//	"value": "35000.00"
	//},
	//	"final_capture": true,
	//	"seller_protection": {
	//	"status": "ELIGIBLE",
	//	"dispute_categories": [
	//	"ITEM_NOT_RECEIVED",
	//	"UNAUTHORIZED_TRANSACTION"
	//]
	//},
	//	"seller_receivable_breakdown": {
	//	"gross_amount": {
	//	"currency_code": "USD",
	//	"value": "35000.00"
	//},
	//	"paypal_fee": {
	//	"currency_code": "USD",
	//	"value": "1540.30"
	//},
	//	"net_amount": {
	//	"currency_code": "USD",
	//	"value": "33459.70"
	//}
	//},
	//	"invoice_id": "285",
	//	"links": [
	//{
	//	"href": "https://api.sandbox.paypal.com/v2/payments/captures/7XB8511609517342V",
	//	"rel": "self",
	//	"method": "GET"
	//},
	//{
	//	"href": "https://api.sandbox.paypal.com/v2/payments/captures/7XB8511609517342V/refund",
	//	"rel": "refund",
	//	"method": "POST"
	//},
	//{
	//	"href": "https://api.sandbox.paypal.com/v2/checkout/orders/55A17048CN715352N",
	//	"rel": "up",
	//	"method": "GET"
	//}
	//],
	//	"create_time": "2020-01-29T15:58:45Z",
	//	"update_time": "2020-01-29T15:58:45Z"
	//}
	//]
	//}
	//}
	//],
	//	"payer": {
	//	"name": {
	//	"given_name": "John",
	//	"surname": "Doe"
	//},
	//	"email_address": "sb-q6a2v578305@personal.example.com",
	//	"payer_id": "ZTQMJX7UKPSHS",
	//	"address": {
	//	"country_code": "US"
	//}
	//},
	//}
}

//	sberbank
//	SB register request
type SberbankRegisterRequest struct {
	UserName    string `json:"userName"`
	Password    string `json:"password"`
	OrderNumber string `json:"orderNumber"`
	Amount      uint   `json:"amount"`
	ReturnUrl   string `json:"returnUrl"`
	//Currency string `json:"currency"`
}

//	SB register response
type SberbankRegisterResponse struct {
	OrderId      string `json:"orderId"`
	FormUrl      string `json:"formUrl"`
	ErrorCode    uint   `json:"errorCode"`
	ErrorMessage string `json:"errorMessage"`
}

//	SB check status request
type SberbankCheckStatusRequest struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
	OrderID  string `json:"orderId"`
	Language string `json:"language"`
}

//	SB check status response
type SberbankCheckStatusResponse struct {
	OrderStatus   uint   `json:"OrderStatus"`
	ErrorCode     string `json:"ErrorCode"`
	ErrorMessage  string `json:"ErrorMessage"`
	OrderNumber   string `json:"OrderNumber"`
	Amount        uint   `json:"Amount"`
	Expiration    string `json:"expiration"`
	DepositAmount uint   `json:"depositAmount"`
	Currency      string `json:"currency"`
	Pan           string `json:"Pan"`
	IP            string `json:"Ip"`

	//"expiration":"202001",
	//"cardholderName":"s",
	//"depositAmount":0,
	//"currency":"643",
	//"authCode":2,"
	//ErrorCode":"2",
	//"ErrorMessage":"Payment is declined",
	//"OrderStatus":6,
	//"OrderNumber":"57",
	//"Pan":"411111**1111",
	//"Amount":70000,
	//"Ip":"188.95.55.14"
}
