package http

// 	массив роутинга
var GeneralRoutes = []Route{
	// 	TOKEN
	Route{
		"createToken",
		"Получаем токин по логину с паролем",
		"POST",
		"/authenticate",
		createToken,
	},
	// 	/////////////////////////////////////////////
	// 	AUTH
	//
	Route{
		"getProfile",
		"Получаем профиль пользователя",
		"GET",
		"/profile",
		validateMiddleware(getProfile),
	},
	// 	***********************************************************
	// 	STATUS OK
	//	---------------------------------
	//	аутентификация
	//	---------------------------------
	Route{
		"statusOK",
		"Получаем инфу",
		"OPTIONS",
		"/authenticate",
		statusOK,
	},
	//	---------------------------------
	//	профиль
	//	---------------------------------
	Route{
		"statusOK",
		"Получаем инфу",
		"OPTIONS",
		"/profile",
		statusOK,
	},
}
