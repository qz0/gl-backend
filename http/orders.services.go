package http

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var OrderRoutes = []Route{
	//	сами функции
	Route{
		"changeOrderStatus",
		"Получаем конкретный объект",
		"GET",
		"/orders/change_status",
		changeOrderStatus,
		//validateMiddleware(getCurrentOrder),
	},
	Route{
		"createOrder",
		"Создаем объект",
		"PUT",
		"/orders/create",
		createOrder,
		//validateMiddleware(createOrder),
	},
	Route{
		"deleteOrder",
		"Удаляем объект",
		"DELETE",
		"/orders/delete",
		deleteOrder,
		//validateMiddleware(deleteOrder),
	},
	Route{
		"getAllOrders",
		"Получаем список всех объектов",
		"GET",
		"/orders/read_all",
		getAllOrders,
		//validateMiddleware(getAllOrders),
	},
	Route{
		"getCurrentOrder",
		"Получаем конкретный объект",
		"GET",
		"/orders/read_current",
		getCurrentOrder,
		//validateMiddleware(getCurrentOrder),
	},
	Route{
		"getOrdersByStatus",
		"Получаем список объектов по параметрам",
		"GET",
		"/orders/read_by_status",
		getOrdersByStatus,
		//validateMiddleware(getOrdersByStatus),
	},
	Route{
		"updateOrder",
		"Изменяем объект",
		"PATCH",
		"/orders/update",
		updateOrder,
		//validateMiddleware(updateOrder),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"меняем статус у заказа",
		"OPTIONS",
		"/orders/change_status",
		statusOK,
	},
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/orders/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/orders/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/orders/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/orders/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем список объектов по параметрам",
		"OPTIONS",
		"/orders/read_by_status",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/orders/update",
		statusOK,
	},
}

//	SERVICES

// 	Получаем объект по ИД
func changeOrderStatus(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentOrder")

	// 	создаем переменные
	var order db.Order
	var tempOrder db.Order
	var orders []db.Order

	//	подключаем хранилище
	tempStorage := storage

	orderID := r.URL.Query().Get("orderID")
	statusID := r.URL.Query().Get("statusID")

	// 	имя получателя
	if orderID != "" && statusID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(orderID)
		order.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&order); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		}

		//	копируем ид для копии старой структуры перед изменением для логов
		tempOrder.ID = order.ID

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&tempOrder); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
			return
		}

		//	меняем статус у ордера
		intStatusID, _ := strconv.Atoi(statusID)
		order.StatusID = uint(intStatusID)

		//	задаем строку изменяемых параметров для апдейта
		selectedParams := "status_id"

		if order.ID == 6 {
			selectedParams += ", "
		}

		// 	дергаем функцию изменения
		//	проверяем, что не вернули ошибку
		if err := tempStorage.UpdateItemSelect(&order, "status_id", nil); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		}

		//	если статус 7 - то надо еще айтемы пройти
		if order.StatusID == 7 {
			//	переменная
			var items []db.Item
			//	получаем массив айтемов с соответствующим ордером
			if err := tempStorage.SelectItemsWhere(&items, 0, 1000, "order_id", order.ID); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
				return
			} else {
				//	для каждого объекта перебором поменяли статус на отправленный
				for _, item := range items {

					//	копируем ид для копии старой структуры перед изменением для логов
					var tempItem db.Item
					tempItem.ID = item.ID

					//	проверяем, что такая запись есть
					if err := tempStorage.SelectItem(&tempItem); err != nil {
						//	вернули ошибку - возвращаем 400
						log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
						http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
						return
					}

					//	правим статус
					item.StatusID = 3 //	3 - shipped

					//	вносим изменения в базу
					if err := tempStorage.UpdateItemSelect(&item, "status_id", nil); err != nil {
						//	вернули ошибку - возвращаем 400
						log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
						http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
					} else {
						tempStorage.CreateLog(&tempItem, &item, "update", "Guest") // запись успешного удаления в лог
					}

				}
			}

		}
	}

	//	получаем старый статус для выборки - он на один меньше
	newStatusID, _ := strconv.Atoi(statusID)
	newStatusID--

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectItemsWhere(&orders, 0, 50, "status_id = ? ", newStatusID); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		tempStorage.CreateLog(&tempOrder, &order, "update", "Guest") // запись успешного удаления в лог
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orders)
	}
}

// 	добавляем объект
func createOrder(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createOrder")

	//	создаем переменные
	var order db.Order //	заказ
	//var orders []db.Order

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", order)
	}

	var err error

	//	получаем цену корзины:
	log.Warnf("\ncartItems: %+v", order.CartItems)
	//	получаем итоговую стоимость
	if err, order.ProductsCost = getCostsByProducts(order.CartItems, order.CurrencyID); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при получении цены товара: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при получении цены товара: : %+v\"}", err), http.StatusBadRequest)
	}

	//	получаем цену доставки

	//	получаем сумму
	order.TotalCost = order.ProductsCost + order.DeliveryCost

	//	заполняем известные поля
	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&order); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectItem(&order); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	}
	//else {
	//	//  все нормально - возвращаем 200
	//	w.WriteHeader(201)
	//	//	возвращаем полученные объекты
	//	_ = json.NewEncoder(w).Encode(order)
	//}

	//	получаем url
	switch payment := order.PaymentID; payment {
	//	tinkoff
	case 0:

	//	paypal
	case 1:
		log.Info("Оплата через PayPal")
		const userName = "AXc_EI6-eB-7LEHLQ0uBujO6LbIFT7srHv9SNxTiW2AdLMCr7PmXLhgf1UQA9lKDCfNFOHFZeczATsVy"
		const password = "EP8DPl6n2IPipOZhhrEou9YHeAU_Sgt7V36antUdrYFSgnGOvzMamgSLnnJqMqhQ102Fm6PgvZWtZZ4G"
		var amount = fmt.Sprintf("%d", order.TotalCost*100)
		const registerURL = "https://api.sandbox.paypal.com/v2/checkout/orders"
		//const returnURL = "http://localhost:4200/payments/final"
		const returnURL = "https://tarkov.grey-line.com/payments/final"
		var orderNumber = fmt.Sprintf("%d", order.ID)

		var registerRequestPaypal PaypalRegisterRequest
		var registerResponsePaypal PaypalRegisterResponse
		//	формируем юнит на оплату
		var paypalPurchaseUnits []PaypalPurchaseUnit
		var reqBodyBytes []byte

		//	печатаем сумму
		log.Warnf("Выбрана валюта. %+v", order.Currency.Code)
		log.Warnf("Выбрана сумма. %+v", order.TotalCost)
		log.Warnf("Сумма к оплате. %+v", order.TotalCost)

		//	формируем структуру для отправки
		paypalPurchaseUnits = append(paypalPurchaseUnits, PaypalPurchaseUnit{
			InvoiceID: orderNumber,
			Amount: PaypalPurchaseUnitAmount{
				CurrencyCode: "USD",
				Value:        amount,
			},
		})
		//	заполняем запрос на регистрацию платежа
		registerRequestPaypal = PaypalRegisterRequest{
			Intent:        "CAPTURE",
			PurchaseUnits: paypalPurchaseUnits,
			ApplicationContext: PaypalApplicationContext{
				ReturnURL: returnURL,
			},
		}

		//registerRequestPaypal = PaypalRegisterRequest{
		//	Intent: "CAPTURE",
		//	ApplicationContext: ,
		//	PurchaseUnits: paypalPurchaseUnits,
		//}

		if order.ID != 0 && order.TotalCost != 0 {
			//	отрубаем проверку сертификата
			tr := &http.Transport{
				TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
			}
			//	объявляем клиента для запроса
			client := &http.Client{Transport: tr}

			//	запаковываем запрос в тело поста
			reqBodyBytes, _ = json.Marshal(&registerRequestPaypal)
			//log.Infof("До конверта в массив байтов: %+v", registerRequestPaypal)
			//log.Infof("До конверта в поток байтов: %+v", reqBodyBytes)
			buf := new(bytes.Buffer)

			log.Infof("После конверта в поток байтов: %+v", buf)
			err := json.NewEncoder(buf).Encode(reqBodyBytes)
			if err != nil {
				log.Errorf("Ошибка при запаковке реквеста. %+v", err)
			}

			//	дергаем запрос
			req, err := http.NewRequest("POST", registerURL, bytes.NewBuffer(reqBodyBytes))
			req.SetBasicAuth(userName, password)
			req.Header.Set("Content-Type", "application/json")
			//	отправляем запрос

			log.Infof("Отправили реквест: %+v", req)
			resp, err := client.Do(req)
			if err != nil {
				log.Errorf("Ошибка в запросе к сервису проверки оплаты. %+v", err)
			} else {
				log.Warnf("Ответ paypal сервиса resp: %+v", resp)
				if err := json.NewDecoder(resp.Body).Decode(&registerResponsePaypal); err != nil {
					log.Warnf("Данные registerResponse распаковались криво: %+v \n Данные: %+v", err, registerResponsePaypal)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные registerResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
				} else {
					log.Warnf("Ответ paypal сервиса registerResponsePaypal: %+v", registerResponsePaypal)
					//  все нормально - возвращаем 200
					for _, link := range registerResponsePaypal.Links {
						if link.Rel == "approve" {
							order.PaymentURL = link.Href
						}
					}
				}
			}

			//	update order
			//	проверяем, что не вернули ошибку
			if err := tempStorage.UpdateItem(&order); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
			}

			// 	дергаем функцию выборки
			//	проверяем, что не вернули ошибку
			if err := tempStorage.SelectItem(&order); err != nil {
				//	вернули ошибку - возвращаем 404
				log.Errorf("Ошибка при выборе объектов: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
				return
			}

		}

	//	sberbank
	case 2:
		log.Info("Оплата через Сбербанк")
		const userName = "grey-shop-api"
		const password = "grey-shop"
		var amount = fmt.Sprintf("%d", order.TotalCost*100)
		const registerURL = "https://3dsec.sberbank.ru/payment/rest/register.do"
		const returnURL = "https://tarkov.grey-line.com/payments/final"
		var orderNumber = fmt.Sprintf("%d", order.ID)

		//var registerRequest = SberbankRegisterRequest{
		//	UserName: userName,
		//	Password: password,
		//	OrderNumber: string(order.ID),
		//	Amount: order.TotalCost,
		//	ReturnUrl: "https://tarkov.grey-shop.com/payments/final",
		//}
		var registerResponse SberbankRegisterResponse
		//	отрубаем проверку сертификата
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		//	объявляем клиента для запроса
		client := &http.Client{Transport: tr}

		if order.ID != 0 && order.TotalCost != 0 {
			// 	возвращаем нотисы
			//req, err := http.NewRequest("GET", registerURL + "?userName=" + userName + "&password=" + password + "?orderNumber=" + "1231111" + "&amount=" + "123123123" + "&returnUrl=" + "https://tarkov.grey-shop.com/payments/final", nil)
			req, err := http.NewRequest("GET", registerURL+"?password="+password+"&orderNumber="+orderNumber+"&amount="+amount+"&returnUrl="+returnURL+"&userName="+userName, nil)
			//"?userName=" + registerRequest.UserName +
			//	"&password=" + registerRequest.Password +
			//	"?orderNumber=" + registerRequest.OrderNumber +
			//	"&amount=" + "123123123" +
			//	"&returnUrl=" + registerRequest.ReturnUrl, nil)
			//req, err := http.NewRequest("POST", registerURL, reqBodyBytes)
			//req.SetBasicAuth(paypalUsername, paypalPassword)
			//	отправляем запрос
			resp, err := client.Do(req)
			if err != nil {
				log.Warnf("Ошибка в запросе к сервису проверки оплаты. %+v", err)
			} else {
				if err := json.NewDecoder(resp.Body).Decode(&registerResponse); err != nil {
					log.Warnf("Данные registerResponse распаковались криво: %+v", registerResponse)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные registerResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
				} else {
					log.Warnf("Ответ sberbank сервиса: %+v", registerResponse)
					//  все нормально - возвращаем 200
					order.PaymentURL = registerResponse.FormUrl
				}
			}

			//	update order
			//	проверяем, что не вернули ошибку
			if err := tempStorage.UpdateItem(&order); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
			}

			// 	дергаем функцию выборки
			//	проверяем, что не вернули ошибку
			if err := tempStorage.SelectItem(&order); err != nil {
				//	вернули ошибку - возвращаем 404
				log.Errorf("Ошибка при выборе объектов: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
				return
			}

		}
	}

	//  все нормально - возвращаем 200
	w.WriteHeader(201)
	//	возвращаем полученные объекты
	_ = json.NewEncoder(w).Encode(order)
}

// 	Удаляем объект
func deleteOrder(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteOrder")

	// 	создаем переменные
	var order db.Order
	//var tempOrder db.Order
	var orders []db.Order

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	orderID := r.URL.Query().Get("id")

	// 	имя получателя
	if orderID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(orderID)
		order.ID = uint(id)
		//tempOrder.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&order); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&order); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	orderID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orders); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orders)
	}
}

// 	Получаем список всех объектов
func getAllOrders(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOrders")

	// 	создаем переменные
	var orders []db.Order

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orders); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orders)
	}
}

// 	Получаем объект по ИД
func getCurrentOrder(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentOrder")

	// 	создаем переменные
	var order db.Order

	//	подключаем хранилище
	tempStorage := storage

	orderID := r.URL.Query().Get("id")

	// 	имя получателя
	if orderID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(orderID)
		order.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&order); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(order)
		}
	}
}

// 	Получаем список объектов по параметрам
func getOrdersByStatus(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOrders")

	// 	создаем переменные
	var orders []db.Order
	var maxOffset int
	var maxLimit int

	//	подключаем хранилище
	tempStorage := storage

	statusID := r.URL.Query().Get("statusID")
	offset := r.URL.Query().Get("offset")
	limit := r.URL.Query().Get("limit")
	if offset != "" {
		// 	конвертируем в UINT
		maxOffset, _ = strconv.Atoi(offset)
	}
	if limit != "" {
		// 	конвертируем в UINT
		maxLimit, _ = strconv.Atoi(limit)
	}

	log.Warnf("%+v %+v %+v", statusID, maxOffset, uint(maxLimit))

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectItemsWhere(&orders, maxOffset, uint(maxLimit), "status_id = ? ", statusID); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orders)
	}
}

// 	Изменяем объект
func updateOrder(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateOrder")

	// 	создаем переменные
	var order db.Order
	var tempOrder db.Order
	var orders []db.Order

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", order)
		//	сохраняем ИД для поиска
		tempOrder.ID = order.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOrder); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&order); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempOrder, &order, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&orders); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(orders)
	}
}
