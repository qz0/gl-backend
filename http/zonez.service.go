package http

import (
	//"crypto/md5"
	"encoding/json"
	"fmt"

	"../db"

	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var ZoneRoutes = []Route{
	//	сами функции
	Route{
		"createZone",
		"Создаем объект",
		"PUT",
		"/zones/create",
		createZone,
		//validateMiddleware(createZone),
	},
	Route{
		"deleteZone",
		"Удаляем объект",
		"DELETE",
		"/zones/delete",
		deleteZone,
		//validateMiddleware(deleteZone),
	},
	Route{
		"getAllZones",
		"Получаем список всех объектов",
		"GET",
		"/zones/read_all",
		getAllZones,
		//validateMiddleware(getAllZones),
	},
	Route{
		"getCurrentZone",
		"Получаем конкретный объект",
		"GET",
		"/zones/read_current",
		getCurrentZone,
		//validateMiddleware(getCurrentZone),
	},
	Route{
		"updateZone",
		"Изменяем объект",
		"PATCH",
		"/zones/update",
		updateZone,
		//validateMiddleware(updateZone),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/zones/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/zones/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/zones/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/zones/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/zones/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createZone(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createZone")

	//	создаем переменные
	var zone db.Zone
	var zones []db.Zone

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&zone); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", zone)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&zone); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&zones); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 201
		w.WriteHeader(201)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(zones)
	}
}

// 	Удаляем объект
func deleteZone(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteZone")

	// 	создаем переменные
	var zone db.Zone
	var zones []db.Zone

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	zoneID := r.URL.Query().Get("id")

	// 	имя получателя
	if zoneID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(zoneID)
		zone.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&zone); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&zone); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			}
		}
	} else {
		//	zoneID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&zones); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(zones)
	}
}

// 	Получаем список всех объектов
func getAllZones(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllZones")

	// 	создаем переменные
	var zones []db.Zone

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&zones); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(zones)
	}
}

// 	Получаем объект по ИД
func getCurrentZone(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentZone")

	// 	создаем переменные
	var zone db.Zone

	//	подключаем хранилище
	tempStorage := storage

	zoneID := r.URL.Query().Get("id")

	// 	имя получателя
	if zoneID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(zoneID)
		zone.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&zone); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(zone)
		}
	}
}

// 	Изменяем объект
func updateZone(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateZone")

	// 	создаем переменные
	var zone db.Zone
	var tempZone db.Zone
	var zones []db.Zone

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&zone); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", zone)
		//	сохраняем ИД для поиска
		tempZone.ID = zone.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempZone); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&zone); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	}
	tempStorage.CreateLog(&tempZone, &zone, "update", "Guest") // запись успешного удаления в лог

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&zones); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(zones)
	}
}
