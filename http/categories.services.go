package http

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var CategoryRoutes = []Route{
	//	сами функции
	Route{
		"createCategory",
		"Создаем объект",
		"PUT",
		"/categories/create",
		createCategory,
		//validateMiddleware(createCategory),
	},
	Route{
		"deleteCategory",
		"Удаляем объект",
		"DELETE",
		"/categories/delete",
		deleteCategory,
		//validateMiddleware(deleteCategory),
	},
	Route{
		"getAllCategories",
		"Получаем список всех объектов",
		"GET",
		"/categories/read_all",
		getAllCategories,
		//validateMiddleware(getAllCategories),
	},
	Route{
		"getAllCategoriesByShop",
		"Получаем список всех объектов",
		"GET",
		"/categories/read_all_by_shop",
		getAllCategoriesByShop,
		//validateMiddleware(getAllCategoriesBySHop),
	},
	Route{
		"getCurrentCategory",
		"Получаем конкретный объект",
		"GET",
		"/categories/read_current",
		getCurrentCategory,
		//validateMiddleware(getCurrentCategory),
	},
	Route{
		"updateCategory",
		"Изменяем объект",
		"PATCH",
		"/categories/update",
		updateCategory,
		//validateMiddleware(updateCategory),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/categories/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/categories/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/categories/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/categories/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/categories/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createCategory(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createCategory")

	//	создаем переменные
	var category db.Category
	var categories []db.Category
	seoflag := 0
	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", category)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&category); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//обновляем Seo Sitemap, если был изменён сеолинк категории
		seoflag = 1
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&categories); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 201
		w.WriteHeader(201)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(categories)

		// если изменилось сео, обновляем sitemap
		if seoflag == 1 {
			tempStorage.SeoCategoryUpdater()
		}
	}
}

// 	Удаляем объект
func deleteCategory(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteCategory")

	// 	создаем переменные
	var category db.Category
	//var tempCategory db.Category
	var categories []db.Category
	// flag проверки изменения сеолинк
	seoflag := 0
	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	categoryID := r.URL.Query().Get("id")

	// 	имя получателя
	if categoryID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(categoryID)
		category.ID = uint(id)
		//tempCategory.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&category); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&category); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//обновляем seoflag, если был изменён сеолинк категории
				seoflag = 1
			}
		}
	} else {
		//	categoryID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&categories); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(categories)

		// если изменилось сео, обновляем sitemap
		if seoflag == 1 {
			tempStorage.SeoCategoryUpdater()
		}

	}
}

// 	Получаем список всех объектов
func getAllCategoriesByShop(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllCategoriesByShop")

	// 	создаем переменные
	var categories []db.Category

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&categories); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(categories)
	}
}

// 	Получаем список всех объектов
func getAllCategories(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllCategories")

	// 	создаем переменные
	var categories []db.Category

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&categories); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(categories)
	}
}

// 	Получаем объект по ИД
func getCurrentCategory(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentCategory")

	// 	создаем переменные
	var category db.Category

	//	подключаем хранилище
	tempStorage := storage

	categoryID := r.URL.Query().Get("id")

	// 	имя получателя
	if categoryID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(categoryID)
		category.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&category); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(category)
		}
	}
}

// 	Получаем объект по SEO link
func getCategoryBySeoLink(seoLink string, categories *[]db.Category) error {
	//	log
	//log.Warn("getCurrentProduct")

	//	подключаем хранилище
	tempStorage := storage

	// 	имя получателя
	if seoLink != "" {
		// 	возвращаем нотисы
		if err := tempStorage.SelectItemsWhere(categories, 0, 1, "seo_link = ?", seoLink); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			return err
		} else {
			//  все нормально - возвращаем 200
			log.Errorf("Выбран продукт: %+v", *categories)
			return nil
		}
	} else {
		log.Error("Пустой линк")
		return errors.New("пустой линк")
	}
}

// 	Изменяем объект
func updateCategory(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCategory")

	// 	создаем переменные
	var category db.Category
	var tempCategory db.Category
	var categories []db.Category

	// flag проверки изменения сеолинк
	seoflag := 0
	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", category)
		//	сохраняем ИД для поиска
		tempCategory.ID = category.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCategory); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&category); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//обновляем Seo Sitemap, если был изменён сеолинк категории
		tempStorage.CreateLog(&tempCategory, &category, "update", "Guest") // запись успешного удаления в лог
		if tempCategory.SeoLink != category.SeoLink {
			seoflag = 1
		}
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&categories); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(categories)

		// если изменилось сео, обновляем sitemap
		if seoflag == 1 {
			tempStorage.SeoCategoryUpdater()
		}
	}
}
