package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/archi-chester/backend/db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var LanguageRoutes = []Route{
	//	сами функции
	Route{
		"createLanguage",
		"Создаем объект",
		"PUT",
		"/languages/create",
		createLanguage,
		//validateMiddleware(createLanguage),
	},
	Route{
		"deleteLanguage",
		"Удаляем объект",
		"DELETE",
		"/languages/delete",
		deleteLanguage,
		//validateMiddleware(deleteLanguage),
	},
	Route{
		"getAllLanguages",
		"Получаем список всех объектов",
		"GET",
		"/languages/read_all",
		getAllLanguages,
		//validateMiddleware(getAllLanguages),
	},
	Route{
		"getCurrentLanguage",
		"Получаем конкретный объект",
		"GET",
		"/languages/read_current",
		getCurrentLanguage,
		//validateMiddleware(getCurrentLanguage),
	},
	Route{
		"updateLanguage",
		"Изменяем объект",
		"PATCH",
		"/languages/update",
		updateLanguage,
		//validateMiddleware(updateLanguage),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/languages/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/languages/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/languages/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/languages/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/languages/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createLanguage(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createLanguage")

	//	создаем переменные
	var language db.Language
	var languages []db.Language

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&language); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", language)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&language); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&languages); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(languages)
	}
}

// 	Удаляем объект
func deleteLanguage(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteLanguage")

	// 	создаем переменные
	var language db.Language
	//var tempLanguage db.Language
	var languages []db.Language

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	languageID := r.URL.Query().Get("id")

	// 	имя получателя
	if languageID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(languageID)
		language.ID = uint(id)
		//tempLanguage.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&language); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&language); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	languageID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&languages); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(languages)
	}
}

// 	Получаем список всех объектов
func getAllLanguages(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllLanguages")

	// 	создаем переменные
	var languages []db.Language

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&languages); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(languages)
	}
}

// 	Получаем объект по ИД
func getCurrentLanguage(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentLanguage")

	// 	создаем переменные
	var language db.Language

	//	подключаем хранилище
	tempStorage := storage

	languageID := r.URL.Query().Get("id")

	// 	имя получателя
	if languageID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(languageID)
		language.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&language); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(language)
		}
	}
}

// 	Изменяем объект
func updateLanguage(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateLanguage")

	// 	создаем переменные
	var language db.Language
	var tempLanguage db.Language
	var languages []db.Language

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&language); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", language)
		//	сохраняем ИД для поиска
		tempLanguage.ID = language.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempLanguage); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&language); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempLanguage, &language, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&languages); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(languages)
	}
}
