package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var ItemShadowRoutes = []Route{
	//	сами функции
	Route{
		"createItemShadow",
		"Создаем объект",
		"PUT",
		"/item_shadows/create",
		createItemShadow,
		//validateMiddleware(createItemShadow),
	},
	Route{
		"deleteItemShadow",
		"Удаляем объект",
		"DELETE",
		"/item_shadows/delete",
		deleteItemShadow,
		//validateMiddleware(deleteItemShadow),
	},
	Route{
		"getAllItemShadows",
		"Получаем список всех объектов",
		"GET",
		"/item_shadows/read_all",
		getAllItemShadows,
		//validateMiddleware(getAllItemShadows),
	},
	Route{
		"getCurrentItemShadow",
		"Получаем конкретный объект",
		"GET",
		"/item_shadows/read_current",
		getCurrentItemShadow,
		//validateMiddleware(getCurrentItemShadow),
	},
	Route{
		"updateItemShadow",
		"Изменяем объект",
		"PATCH",
		"/item_shadows/update",
		updateItemShadow,
		//validateMiddleware(updateItemShadow),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/item_shadows/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/item_shadows/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/item_shadows/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/item_shadows/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/item_shadows/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createItemShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createItemShadow")

	//	создаем переменные
	var itemShadow db.ItemShadow
	var itemShadows []db.ItemShadow

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&itemShadow); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", itemShadow)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&itemShadow); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&itemShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(itemShadows)
	}
}

// 	Удаляем объект
func deleteItemShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteItemShadow")

	// 	создаем переменные
	var itemShadow db.ItemShadow
	//var tempItemShadow db.ItemShadow
	var itemShadows []db.ItemShadow

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	itemShadowID := r.URL.Query().Get("id")

	// 	имя получателя
	if itemShadowID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(itemShadowID)
		itemShadow.ID = uint(id)
		//tempItemShadow.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&itemShadow); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&itemShadow); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	itemShadowID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&itemShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(itemShadows)
	}
}

// 	Получаем список всех объектов
func getAllItemShadows(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllItemShadows")

	// 	создаем переменные
	var itemShadows []db.ItemShadow

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&itemShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(itemShadows)
	}
}

// 	Получаем объект по ИД
func getCurrentItemShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentItemShadow")

	// 	создаем переменные
	var itemShadow db.ItemShadow

	//	подключаем хранилище
	tempStorage := storage

	itemShadowID := r.URL.Query().Get("id")

	// 	имя получателя
	if itemShadowID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(itemShadowID)
		itemShadow.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&itemShadow); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(itemShadow)
		}
	}
}

// 	Изменяем объект
func updateItemShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateItemShadow")

	// 	создаем переменные
	var itemShadow db.ItemShadow
	var tempItemShadow db.ItemShadow
	var itemShadows []db.ItemShadow

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&itemShadow); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", itemShadow)
		//	сохраняем ИД для поиска
		tempItemShadow.ID = itemShadow.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempItemShadow); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&itemShadow); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempItemShadow, &itemShadow, "update", "Guest") // запись успешного удаления в лог
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&itemShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(itemShadows)
	}
}
