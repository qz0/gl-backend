package http

import (
	//"crypto/md5"
	"encoding/json"
	"fmt"

	"../db"

	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var UserRoutes = []Route{
	//	сами функции
	Route{
		"createUser",
		"Создаем объект",
		"PUT",
		"/users/create",
		createUser,
		//validateMiddleware(createUser),
	},
	Route{
		"deleteUser",
		"Удаляем объект",
		"DELETE",
		"/users/delete",
		deleteUser,
		//validateMiddleware(deleteUser),
	},
	Route{
		"getAllUsers",
		"Получаем список всех объектов",
		"GET",
		"/users/read_all",
		getAllUsers,
		//validateMiddleware(getAllUsers),
	},
	Route{
		"getAllUsers",
		"Получаем конкретный объект",
		"GET",
		"/users/read_current",
		getCurrentUser,
		//validateMiddleware(getCurrentUser),
	},
	Route{
		"updateUser",
		"Изменяем объект",
		"PATCH",
		"/users/update",
		updateUser,
		//validateMiddleware(updateUser),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/users/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/users/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/users/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/users/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/users/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем пользователя
func createUser(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createUser")

	//	создаем переменные
	var user db.User
	var users []db.User

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", user)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&user); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&users); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(users)
	}

	//log.Info("insertUser")
	//var login Logon
	//_ = json.NewDecoder(r.Body).Decode(&login)
	//log.Info(login)
	//// 	переменные
	//var newUser db.User
	//var users []db.User
	//tempStorage := storage
	//
	//// 	собираем запись для добавления
	//// 	логин
	//if len(login.Username) > 0 {
	//	newUser.UserName = login.Username
	//	// log.Warnf("Таблица users: %v", db.HasTable(&User{}))in.Username
	//} else {
	//	log.Error("Пустой логин")
	//}
	//
	//// 	пароль
	//if len(login.Password) > 0 {
	//	bytepass := []byte(login.Password)
	//	newUser.Password = fmt.Sprintf("%x", md5.Sum(bytepass))
	//} else {
	//	log.Error("Пустой пароль")
	//}
	//
	//log.Infof("%+v", newUser)
	////	добавление новой записи
	//// 	добавляем запись
	//tempStorage.HandleDB.Create(&newUser)
	//
	//// 	возвращаем нотисы
	//tempStorage.HandleDB.Find(&users)
	//
	//// 	помещаем ответ в массив символов для возврата
	//data, err := json.Marshal(users)
	//if err != nil {
	//	log.Errorf("Ошибка при получении списка пользователей: %v", err)
	//} else {
	//	log.Infof("База пользователей: %s", data)
	//}
	//
	//token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
	//	"ID":       newUser.ID,
	//	"UserName": newUser.UserName,
	//})
	//tokenString, error := token.SignedString([]byte(secretKey))
	//if error != nil {
	//	fmt.Println(error)
	//}
	//w.WriteHeader(200)
	//_ = json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
}

// 	Удаляем Пользователя
func deleteUser(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteUser")

	// 	создаем переменные
	var user db.User
	//var tempUser db.User
	var users []db.User

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	userID := r.URL.Query().Get("id")

	// 	имя получателя
	if userID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(userID)
		user.ID = uint(id)
		//tempUser.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&user); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&user); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	userID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&users); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(users)
	}
}

// 	Получаем список всех пользователей
func getAllUsers(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllUsers")

	// 	создаем переменные
	var users []db.User

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&users); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(users)
	}
}

// 	Получаем пользователя по ИД
func getCurrentUser(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentUser")

	// 	создаем переменные
	var user db.User

	//	подключаем хранилище
	tempStorage := storage

	userID := r.URL.Query().Get("id")

	// 	имя получателя
	if userID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(userID)
		user.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&user); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(user)
		}
	}
}

// 	Изменяем Пользователя
func updateUser(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateUser")

	// 	создаем переменные
	var user db.User
	var tempUser db.User
	var users []db.User

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", user)
		//	сохраняем ИД для поиска
		tempUser.ID = user.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempUser); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&user); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempUser, &user, "update", "Guest") // запись успешного удаления в лог
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&users); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(users)
	}
}

//	UPDATE

//// 	читаем тело запроса в буфер
//bs, err := ioutil.ReadAll(r.Body)
////	проверяем, что не вернули ошибку
//if err != nil {
//	//	вернули ошибку - возвращаем 422
//	log.Errorf("Невозможно прочитать тело запроса: %+v", err)
//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - невозможно прочитать тело запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//	return
//}
////	анмаршалим буфер в структурку
//err = json.Unmarshal(bs, &user)
////	проверяем, что не вернули ошибку
//if err != nil {
//	//	вернули ошибку - возвращаем 422
//	log.Errorf("Неверный формат запроса: %+v", err)
//	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//	return
//} else {
//	log.Infof("Имеем структурку: %+v", user)
//}
