package http

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var ItemRoutes = []Route{
	//	сами функции
	Route{
		"createItemWWW",
		"Создаем объект",
		"PUT",
		"/items/create",
		createItemWWW,
		//validateMiddleware(createItem),
	},
	Route{
		"deleteItem",
		"Удаляем объект",
		"DELETE",
		"/items/delete",
		deleteItem,
		//validateMiddleware(deleteItem),
	},
	Route{
		"getAllItems",
		"Получаем список всех объектов",
		"GET",
		"/items/read_all",
		getAllItems,
		//validateMiddleware(getAllItems),
	},
	Route{
		"getAllItemsByOrder",
		"Получаем список всех объектов",
		"GET",
		"/items/read_all_by_order",
		getAllItemsByOrder,
		//validateMiddleware(getAllItems),
	},
	Route{
		"getAllItemsByProduct",
		"Получаем список всех объектов",
		"GET",
		"/items/read_all_by_product",
		getAllItemsByProduct,
		//validateMiddleware(getAllItemsByProduct),
	},
	Route{
		"getCurrentItem",
		"Получаем конкретный объект",
		"GET",
		"/items/read_current",
		getCurrentItem,
		//validateMiddleware(getCurrentItem),
	},
	Route{
		"updateItem",
		"Изменяем объект",
		"PATCH",
		"/items/update",
		updateItem,
		//validateMiddleware(updateItem),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/items/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/items/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/items/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/items/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/items/update",
		statusOK,
	},
}

//	SERVICES

//	создание айтема
func createItem(item db.Item) (error, db.Item) {
	// 	переменные
	tempStorage := storage

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&item); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		return err, item
	} else {
		//  все нормально - возвращаем 200
		return nil, item
	}
}

// 	добавляем объект
func createItemWWW(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createItem")

	//	создаем переменные
	var item db.Item
	var items []db.Item

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&item); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", item)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err, _ := createItem(item); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&items); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(items)
	}
}

// 	Удаляем объект
func deleteItem(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteItem")

	// 	создаем переменные
	var item db.Item
	//var tempItem db.Item
	var items []db.Item

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	itemID := r.URL.Query().Get("id")

	// 	имя получателя
	if itemID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(itemID)
		item.ID = uint(id)
		//tempItem.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&item); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&item); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	itemID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&items); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(items)
	}
}

//	поиск айтема по параметрам
func findItem(productID uint, statusID uint, optionValues []db.OptionValue) (error, db.Item) {

	log.Warnf("productID: %+v statusID: %+v", productID, statusID)
	//	переменные
	var items []db.Item

	//	подключаем хранилище
	tempStorage := storage

	//	получаем список айтемов нужного продукта и статуса
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectItemsWhere(&items, 0, 100, "product_id = ? AND status_id = ?", fmt.Sprint(productID), fmt.Sprint(statusID)); err != nil {
		var errorItem db.Item
		// детализуем вывод ошибки в лог
		if len(items) == 0 && err == nil {
			err = errors.New("ошибка при выборе items по параметрам - объекты не найдены")
			log.Error("Ошибка при выборе items по параметрам - объекты не найдены")
		} else {
			log.Errorf("Ошибка при выборе items по параметрам - объекты не найдены: %+v", err)
		}
		return err, errorItem
	} else {
		//	что-то найдено

		//	отшибаем по опциям
		if len(optionValues) > 0 {
			var errorItem db.Item
			//	если опция одна просто перебором
			if len(optionValues) == 1 {
				for _, item := range items {
					if item.OptionValues[0].ID == optionValues[0].ID {
						return nil, item
					}
				}
				return errors.New("совпадений по опциям не найдено"), errorItem
			} else {
				// опций больше одной
				return errors.New("передали больше одной опции"), errorItem
			}
		} else {
			//	опций нет
			return nil, items[0]
		}
	}

}

// 	Получаем список всех объектов из продукта
func getAllItemsByOrder(w http.ResponseWriter, r *http.Request) {
	//	log
	// 	создаем переменные
	var order db.Order

	//	подключаем хранилище
	tempStorage := storage

	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		if err := tempStorage.SelectItem(&order); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта order: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(order.Items)
		}
	}
}

func getAllItemsByProduct(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllItemsByProduct")

	// 	создаем переменные
	var product db.Product
	var items []db.Item

	//	подключаем хранилище
	tempStorage := storage

	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	}
	// 	дергаем функцию выборки
	log.Warn(product.ID)
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectItemsWhere(&items, 0, 100, "product_id = ?", fmt.Sprint(product.ID)); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
		return

	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(items)
	}
}

// 	Получаем список всех объектов
func getAllItems(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllItems")

	// 	создаем переменные
	var items []db.Item

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&items); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(items)
	}
}

// 	Получаем объект по ИД
func getCurrentItem(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentItem")

	// 	создаем переменные
	var item db.Item

	//	подключаем хранилище
	tempStorage := storage

	itemID := r.URL.Query().Get("id")
	log.Errorf(itemID)
	// 	имя получателя
	if itemID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(itemID)
		item.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&item); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(item)
		}
	}
}

// 	Изменяем объект
func updateItem(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateItem")

	// 	создаем переменные
	var item db.Item
	var tempItem db.Item
	var items []db.Item

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&item); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", item)
		//	сохраняем ИД для поиска
		tempItem.ID = item.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempItem); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&item); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempItem, &item, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&items); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(items)
	}
}
