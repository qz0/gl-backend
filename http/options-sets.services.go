package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var OptionsSetRoutes = []Route{
	//	сами функции
	//Route{
	//	"changePriorityOfOptionsSet",
	//	"Изменяем приоритет объекта",
	//	"PATCH",
	//	"/options_sets/change_priority",
	//	changePriorityOfOptionsSet,
	//	//validateMiddleware(changePriorityOfOptionsSet),
	//},
	Route{
		"createOptionsSet",
		"Создаем объект",
		"PUT",
		"/options_sets/create",
		createOptionsSet,
		//validateMiddleware(createOptionsSet),
	},
	Route{
		"deleteOptionsSet",
		"Удаляем объект",
		"DELETE",
		"/options_sets/delete",
		deleteOptionsSet,
		//validateMiddleware(deleteOptionsSet),
	},
	Route{
		"getAllOptionsSets",
		"Получаем список всех объектов",
		"GET",
		"/options_sets/read_all",
		getAllOptionsSets,
		//validateMiddleware(getAllOptionsSets),
	},
	Route{
		"getAllOptionsSets",
		"Получаем конкретный объект",
		"GET",
		"/options_sets/read_current",
		getCurrentOptionsSet,
		//validateMiddleware(getCurrentOptionsSet),
	},
	Route{
		"updateOptionsSet",
		"Изменяем объект",
		"PATCH",
		"/options_sets/update",
		updateOptionsSet,
		//validateMiddleware(updateOptionsSet),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/options_sets/change_priority",
		statusOK,
	},
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/options_sets/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/options_sets/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/options_sets/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/options_sets/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/options_sets/update",
		statusOK,
	},
}

//	SERVICES

//func changePriorityOfOptionsSet(w http.ResponseWriter, r *http.Request) {
//	// моя копипаста, но вроде верно понял суть.
//	// 	создаем переменные
//	var optionsSet db.OptionsSet
//	var tempOptionsSet db.OptionsSet
//	var optionsSets []db.OptionsSet
//
//	//	подключаем хранилище
//	tempStorage := storage
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&optionsSet); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", optionsSet)
//		//	сохраняем ИД для поиска
//		tempOptionsSet.ID = optionsSet.ID
//	}
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempOptionsSet); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	} else {
//		log.Warn("Запись есть")
//	}
//	//	выясняем в какую сторону перемещение
//	if optionsSet.Priority < tempOptionsSet.Priority {
//		//	число приоритета уменьшили
//		//	получаем список доставок где приоритет выше
//		if err := tempStorage.SelectItemsWhere(&optionsSets, 0, 10, "priority >= ? and priority < ?", fmt.Sprint(optionsSet.Priority), fmt.Sprint(tempOptionsSet.Priority)); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
//			return
//		} else {
//			//	изменяем там приоритеты
//			for index, currentOptionsSet := range optionsSets {
//				// 	прибавляем к приоритету единицу
//				currentOptionsSet.Priority = currentOptionsSet.Priority + 1
//				// 	дергаем функцию изменения
//				//	проверяем, что не вернули ошибку
//				if err := tempStorage.UpdateItem(&currentOptionsSet); err != nil {
//					//	вернули ошибку - возвращаем 400
//					log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня (%d): %+v", index, err)
//					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err),
//						http.StatusBadRequest)
//				} else {
//					log.Warnf("Updated Option: %v", currentOptionsSet)
//				}
//			}
//		}
//	} else {
//		//	число приоритета такое же или увеличено
//		//	получаем список доставок где приоритет 50
//		if err := tempStorage.SelectItemsWhere(&optionsSets, 0, 10, "priority <= ? and priority > ?",
//			fmt.Sprint(optionsSet.Priority), fmt.Sprint(tempOptionsSet.Priority)); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
//			return
//		} else {
//			//	изменяем там приоритеты
//			for index, currentOptionsSet := range optionsSets {
//				// 	прибавляем к приоритету единицу
//				currentOptionsSet.Priority = currentOptionsSet.Priority - 1
//				// 	дергаем функцию изменения
//				//	проверяем, что не вернули ошибку
//				if err := tempStorage.UpdateItem(&currentOptionsSet); err != nil {
//					//	вернули ошибку - возвращаем 400
//					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня (%d): %+v", index, err)
//					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//				} else {
//					log.Infof("Updated optionsSets: %v", currentOptionsSet)
//				}
//			}
//		}
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&optionsSet); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден_: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&optionsSets); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(optionsSets)
//	}
//}

// 	добавляем объект
func createOptionsSet(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createOptionsSet")

	//	создаем переменные
	var optionsSet db.OptionsSet
	var optionsSets []db.OptionsSet

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&optionsSet); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", optionsSet)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&optionsSet); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionsSets); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionsSets)
	}
}

// 	Удаляем объект
func deleteOptionsSet(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteOptionsSet")

	// 	создаем переменные
	var optionsSet db.OptionsSet
	//var tempOptionsSet db.OptionsSet
	var optionsSets []db.OptionsSet

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	optionsSetID := r.URL.Query().Get("id")

	// 	имя получателя
	if optionsSetID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(optionsSetID)
		optionsSet.ID = uint(id)
		//tempOptionsSet.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&optionsSet); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&optionsSet); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	optionsSetID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionsSets); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionsSets)
	}
}

// 	Получаем список всех объектов
func getAllOptionsSets(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllOptionsSets")

	// 	создаем переменные
	var optionsSets []db.OptionsSet

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionsSets); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionsSets)
	}
}

// 	Получаем объект по ИД
func getCurrentOptionsSet(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentOptionsSet")

	// 	создаем переменные
	var optionsSet db.OptionsSet

	//	подключаем хранилище
	tempStorage := storage

	optionsSetID := r.URL.Query().Get("id")

	// 	имя получателя
	if optionsSetID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(optionsSetID)
		optionsSet.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&optionsSet); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(optionsSet)
		}
	}
}

// 	Изменяем объект
func updateOptionsSet(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateOptionsSet")

	// 	создаем переменные
	var optionsSet db.OptionsSet
	var tempOptionsSet db.OptionsSet
	var optionsSets []db.OptionsSet

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&optionsSet); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", optionsSet)
		//	сохраняем ИД для поиска
		tempOptionsSet.ID = optionsSet.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempOptionsSet); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&optionsSet); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempOptionsSet, &optionsSet, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&optionsSets); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(optionsSets)
	}
}
