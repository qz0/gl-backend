package http

//
//import (
//	"encoding/json"
//	"fmt"
//	"io"
//	"net/http"
//	"os"
//	"strconv"
//
//	"github.com/archi-chester/backend/db"
//
//	log "github.com/sirupsen/logrus"
//)
//
//// 	массив роутинга
//var ImageBrandRoutes = []Route{
//	//	сами функции
//	Route{
//		"createImageBrand",
//		"Создаем объект",
//		"PUT",
//		"/images/brands/create",
//		createImageBrand,
//		//validateMiddleware(createImageBrand),
//	},
//	Route{
//		"deleteImageBrand",
//		"Удаляем объект",
//		"DELETE",
//		"/images/brands/delete",
//		deleteImageBrand,
//		//validateMiddleware(deleteImageBrand),
//	},
//	Route{
//		"getAllImageBrands",
//		"Получаем список всех объектов",
//		"GET",
//		"/images/brands/read_all",
//		getAllImageBrands,
//		//validateMiddleware(getAllImageBrands),
//	},
//	Route{
//		"getCurrentImageBrand",
//		"Получаем конкретный объект",
//		"GET",
//		"/images/brands/read_current",
//		getCurrentImageBrand,
//		//validateMiddleware(getCurrentImageBrand),
//	},
//	Route{
//		"updateImageBrand",
//		"Изменяем объект",
//		"PATCH",
//		"/images/brands/update",
//		updateImageBrand,
//		//validateMiddleware(updateImageBrand),
//	},
//	Route{
//		"uploadImageBrand",
//		"Добавляем файл в репозиторий",
//		"POST",
//		"/images/brands/upload",
//		uploadImageBrand,
//		//validateMiddleware(getAllCurrencies),
//	},
//	//	STATUS OK
//	Route{
//		"statusOK",
//		"Создаем объект",
//		"OPTIONS",
//		"/images/brands/create",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Удаляем объект",
//		"OPTIONS",
//		"/images/brands/delete",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем все объекты",
//		"OPTIONS",
//		"/images/brands/read_all",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Получаем конкретный объект",
//		"OPTIONS",
//		"/images/brands/read_current",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Изменяем объект",
//		"OPTIONS",
//		"/images/brands/update",
//		statusOK,
//	},
//	Route{
//		"statusOK",
//		"Добавляем файл",
//		"OPTIONS",
//		"/images/brands/upload",
//		statusOK,
//	},
//}
//
////	SERVICES
//
//// 	добавляем объект
//func createImageBrand(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("createImageBrand")
//
//	//	создаем переменные
//	var imageBrand db.ImageBrand
//	var imageBrands []db.ImageBrand
//
//	// 	переменные
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&imageBrand); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", imageBrand)
//	}
//
//	// 	дергаем функцию создания
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.CreateItem(&imageBrand); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageBrands); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageBrands)
//	}
//}
//
//// 	Удаляем объект
//func deleteImageBrand(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("deleteImageBrand")
//
//	// 	создаем переменные
//	var imageBrand db.ImageBrand
//	//var tempImageBrand db.ImageBrand
//	var imageBrands []db.ImageBrand
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	получаем параметр
//	imageBrandID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageBrandID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageBrandID)
//		imageBrand.ID = uint(id)
//		//tempImageBrand.ID = uint(id)
//
//		//	проверяем, что такая запись есть
//		if err := tempStorage.SelectItem(&imageBrand); err != nil {
//			//	вернули ошибку - возвращаем 400
//			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		} else {
//			// 	дергаем функцию удаления
//			if err = tempStorage.DeleteItem(&imageBrand); err != nil {
//				//	вернули ошибку - возвращаем 400
//				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
//				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
//			} else {
//				//  все нормально - возвращаем 200
//				w.WriteHeader(200)
//			}
//		}
//	} else {
//		//	imageBrandID is empty
//		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageBrands); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageBrands)
//	}
//}
//
//// 	Получаем список всех объектов
//func getAllImageBrands(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getAllImageBrands")
//
//	// 	создаем переменные
//	var imageBrands []db.ImageBrand
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&imageBrands); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(imageBrands)
//	}
//}
//
//// 	Получаем объект по ИД
//func getCurrentImageBrand(w http.ResponseWriter, r *http.Request) {
//	//	log
//	//log.Warn("getCurrentImageBrand")
//
//	// 	создаем переменные
//	var image db.Image
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	imageID := r.URL.Query().Get("id")
//
//	// 	имя получателя
//	if imageID != "" {
//		// 	конвертируем в UINT
//		id, _ := strconv.Atoi(imageID)
//		image.ID = uint(id)
//		// 	возвращаем нотисы
//		if err := tempStorage.SelectItem(&image); err != nil {
//			//	вернули ошибку - возвращаем 404
//			log.Errorf("Ошибка при выборе объекта: %+v", err)
//			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
//			return
//		} else {
//			//  все нормально - возвращаем 200
//			w.WriteHeader(200)
//			//	возвращаем полученные объекты
//			_ = json.NewEncoder(w).Encode(image)
//		}
//	}
//}
//
//// 	Изменяем объект
//func updateImageBrand(w http.ResponseWriter, r *http.Request) {
//	//	log
//	log.Warn("updateImageBrand")
//
//	// 	создаем переменные
//	var image db.Image
//	var tempImage db.Image
//	var images []db.Image
//
//	//	подключаем хранилище
//	tempStorage := storage
//
//	//	декодирование переданной структуры из запроса
//	//	проверяем, что не вернули ошибку
//	if err := json.NewDecoder(r.Body).Decode(&image); err != nil {
//		//	вернули ошибку - возвращаем 422
//		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
//		return
//	} else {
//		log.Infof("Имеем структурку: %+v", image)
//		//	сохраняем ИД для поиска
//		tempImage.ID = image.ID
//	}
//
//	//	проверяем, что такая запись есть
//	if err := tempStorage.SelectItem(&tempImage); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//		return
//	}
//
//	// 	дергаем функцию изменения
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.UpdateItem(&image); err != nil {
//		//	вернули ошибку - возвращаем 400
//		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//	}
//
//	// 	дергаем функцию выборки
//	//	проверяем, что не вернули ошибку
//	if err := tempStorage.SelectAllItems(&images); err != nil {
//		//	вернули ошибку - возвращаем 404
//		log.Errorf("Ошибка при выборе объектов: %+v", err)
//		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
//		return
//	} else {
//		//  все нормально - возвращаем 200
//		w.WriteHeader(200)
//		//	возвращаем полученные объекты
//		_ = json.NewEncoder(w).Encode(images)
//	}
//}
//
//// 	uploadImageBrand - изменяем категорию
//func uploadImageBrand(w http.ResponseWriter, r *http.Request) {
//
//	log.Warn("uploadImageBrand")
//	// 	создаем переменные
//
//	log.Infof("%v", r)
//	fmt.Println("Метод ПОСТ")
//	err := r.ParseMultipartForm(200000)
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//	log.Infof("%v", r)
//
//	// они все тут
//	formdata := r.MultipartForm // ok, no problem so far, read the Form data
//
//	//get the *fileheaders
//	files := formdata.File["myFile"] // grab the filenames
//	//var fileHeader multipart.FileHeader
//
//	log.Warnf("files: %v", files)
//	for key, _ := range files {
//		log.Warnf("file: %v", key)
//		//file, _ := f[key].Open()
//		log.Warn("Success...")
//		//defer file.Close()
//
//	}
//	fmt.Println(len(files))
//
//	file, handler, err := r.FormFile("myFile")
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer file.Close()
//	log.Warnf("%v", handler.Header)
//
//	filename := getNewUUID()
//	//f, err := os.OpenFile("./"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
//	f, err := os.OpenFile("/usr/local/share/images/brands/"+filename+".jpg", os.O_WRONLY|os.O_CREATE, 0666)
//	if err != nil {
//		log.Errorf("%v", err)
//		return
//	}
//	defer f.Close()
//	io.Copy(f, file)
//
//	//file, err := files[0].Open()
//
//	log.Warn("1")
//	log.Errorf("%v", err)
//	//defer file.Close()
//	if err != nil {
//		fmt.Fprintln(w, err)
//		return
//	}
//
//	w.WriteHeader(201)
//	_ = json.NewEncoder(w).Encode(filename)
//}
