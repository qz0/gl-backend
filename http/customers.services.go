package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var CustomerRoutes = []Route{
	//	сами функции
	Route{
		"createCustomer",
		"Создаем объект",
		"PUT",
		"/customers/create",
		createCustomer,
		//validateMiddleware(createCustomer),
	},
	Route{
		"deleteCustomer",
		"Удаляем объект",
		"DELETE",
		"/customers/delete",
		deleteCustomer,
		//validateMiddleware(deleteCustomer),
	},
	Route{
		"getAllCustomers",
		"Получаем список всех объектов",
		"GET",
		"/customers/read_all",
		getAllCustomers,
		//validateMiddleware(getAllCustomers),
	},
	Route{
		"getCurrentCustomer",
		"Получаем конкретный объект",
		"GET",
		"/customers/read_current",
		getCurrentCustomer,
		//validateMiddleware(getCurrentCustomer),
	},
	Route{
		"updateCustomer",
		"Изменяем объект",
		"PATCH",
		"/customers/update",
		updateCustomer,
		//validateMiddleware(updateCustomer),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/customers/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/customers/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/customers/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/customers/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/customers/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createCustomer(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createCustomer")

	//	создаем переменные
	var customer db.Customer
	var customers []db.Customer

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&customer); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", customer)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&customer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customers)
	}
}

// 	Удаляем объект
func deleteCustomer(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteCustomer")

	// 	создаем переменные
	var customer db.Customer
	//var tempCustomer db.Customer
	var customers []db.Customer

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	customerID := r.URL.Query().Get("id")

	// 	имя получателя
	if customerID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(customerID)
		customer.ID = uint(id)
		//tempCustomer.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&customer); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&customer); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	customerID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customers)
	}
}

// 	Получаем список всех объектов
func getAllCustomers(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllCustomers")

	// 	создаем переменные
	var customers []db.Customer

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customers)
	}
}

// 	Получаем объект по ИД
func getCurrentCustomer(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentCustomer")

	// 	создаем переменные
	var customer db.Customer

	//	подключаем хранилище
	tempStorage := storage

	customerID := r.URL.Query().Get("id")

	// 	имя получателя
	if customerID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(customerID)
		customer.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&customer); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(customer)
		}
	}
}

// 	Изменяем объект
func updateCustomer(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCustomer")

	// 	создаем переменные
	var customer db.Customer
	var tempCustomer db.Customer
	var customers []db.Customer

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&customer); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", customer)
		//	сохраняем ИД для поиска
		tempCustomer.ID = customer.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCustomer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&customer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempCustomer, &customer, "update", "Guest") // запись успешного удаления в лог
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&customers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(customers)
	}
}
