package http

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"../db"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

// 	массив роутинга
var PaypalRoutes = []Route{
	//	сами функции
	Route{
		"checkPaypalPayment",
		"Получаем конкретный объект",
		"GET",
		"/paypal/check_status",
		checkPaypalPayment,
		//validateMiddleware(getCurrentPayment),
	},
	////	STATUS OK
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/paypal/check_status",
		statusOK,
	},
}

// 	Получаем объект по ИД
func checkPaypalPayment(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("checkSberbankPayment")
	//var paymentResponse PaymentResponse

	const userName = "AXc_EI6-eB-7LEHLQ0uBujO6LbIFT7srHv9SNxTiW2AdLMCr7PmXLhgf1UQA9lKDCfNFOHFZeczATsVy"
	const password = "*"
	const checkStatusURL = "https://api.sandbox.paypal.com/v2/checkout/orders/"
	var checkStatusResponse PaypalCheckStatusResponse
	var order db.Order //	заказ
	var tempOrder db.Order
	//const language = "EN"

	//	подключаем хранилище
	tempStorage := storage
	//
	////	отрубаем проверку сертификата
	//tr := &http.Transport{
	//	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	//}
	////	объявляем клиента для запроса
	//client := &http.Client{Transport: tr}

	paypalToken := r.URL.Query().Get("token")

	// 	имя получателя
	if paypalToken != "" {
		//	отрубаем проверку сертификата
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		//	объявляем клиента для запроса
		client := &http.Client{Transport: tr}

		log.Warnf("URL: %+v", checkStatusURL+paypalToken+"/capture")
		//	дергаем сервис
		req, err := http.NewRequest("POST", checkStatusURL+paypalToken+"/capture", nil)
		req.SetBasicAuth(userName, password)
		req.Header.Set("Content-Type", "application/json")
		//	отправляем запрос
		resp, err := client.Do(req)
		if err != nil {
			log.Warnf("Ошибка в запросе к сервису проверки оплаты. %+v", err)
		} else {
			if err := json.NewDecoder(resp.Body).Decode(&checkStatusResponse); err != nil {
				log.Warnf("Исходные данные: %+v", resp)
				log.Warnf("Данные checkStatusResponse распаковались криво: %+v", checkStatusResponse)
				log.Warnf("Исходные данные: %+v", resp.Body)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Данные checkStatusResponse распаковались криво : %+v\"}", err), http.StatusBadRequest)
			} else {
				log.Warnf("Ответ sberbank сервиса: %+v", checkStatusResponse)
				//  все нормально - возвращаем 200

				//	получаем ордер
				if checkStatusResponse.Status == "COMPLETED" && checkStatusResponse.PurchaseUnits[0].Payments.Captures[0].InvoiceID != "" {
					// 				if checkStatusResponse.OrderStatus == 2 && checkStatusResponse.OrderNumber != "" {
					//	меняем у ордера статус
					orderID := checkStatusResponse.PurchaseUnits[0].Payments.Captures[0].InvoiceID
					// 	имя получателя
					if orderID != "" {
						// 	конвертируем в UINT
						id, _ := strconv.Atoi(orderID)
						order.ID = uint(id)
						// 	возвращаем нотисы
						if err := tempStorage.SelectItem(&order); err != nil {
							//	вернули ошибку - возвращаем 404
							log.Errorf("Ошибка при выборе объекта: %+v", err)
							http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
							return
						} else {

							log.Warn("Смена статуса заказа")
							if order.Comments != "" {
								order.StatusID = 3 //	статус на comment check
							} else {
								order.StatusID = 4 //	статус на waiting
								//	todo - проверка наличия и статус - - сборка
							}

							tempOrder.ID = order.ID
							//	еще один ордер для логов
							if err := tempStorage.SelectItem(&tempOrder); err != nil {
								//	вернули ошибку - возвращаем 404
								log.Errorf("Ошибка при выборе объекта: %+v", err)
							}

							log.Warnf("Попытка вызова апдейт заказа: %+v", order.StatusID)
							order.Comments = "has paid"
							// 	дергаем функцию изменения
							//	проверяем, что не вернули ошибку
							if err := tempStorage.UpdateItemSelect(&order, "status_id", nil); err != nil {
								//	вернули ошибку - возвращаем 400
								log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
								http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
								return
							} else {
								//  все нормально - возвращаем 200
								tempStorage.CreateLog(&tempOrder, &order, "update", "Guest") // запись успешного удаления в лог

								//	пробуем создать айтемы
								// CREATE ITEMS

								log.Warnf("Order ID=%v выбран успешно  элементов в корзине %v", order.ID, len(order.CartItems))
								//	пробуем создать items из incoming items
								for _, cartItem := range order.CartItems {
									log.Warnf("Создание продукта id= %v в количестве %v", cartItem.ProductID, cartItem.OrderCount)
									//	по количеству записей в amount
									for i := 0; uint(i) < cartItem.OrderCount; i++ {
										//	создали новый айтем
										var item db.Item
										//	дублируем продукт
										item.ProductID = cartItem.ProductID
										//	дублируем список опций
										item.OptionValues = cartItem.OptionValues
										//	проставляем статус на складе
										item.StatusID = 1 //	needed
										//	проставляем ордер
										item.OrderID = order.ID //	id

										//	пробуем найти
										var selectedItem db.Item
										var tempSelectedItem db.Item
										var err error
										//	проставляем искомое
										selectedItem.ProductID = cartItem.ProductID
										selectedItem.StatusID = 2
										if err, selectedItem = findItem(cartItem.ProductID, 2, cartItem.OptionValues); err != nil {
											//	вернули ошибку - возвращаем 404
											log.Errorf("Ошибка при выборе объектов: %+v", err)

											//	пробуем создать
											if err := tempStorage.CreateItem(&item); err != nil {
												//	вернули ошибку - возвращаем 404
												log.Errorf("Ошибка при создании объекта: %+v", err)
												http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при обновлении создании: %+v\"}", err), http.StatusNotFound)
												return
											} else {
												log.Errorf("Создали айтем: %+v", item.ID)
											}

										} else {

											log.Warnf("Выбрали существующий айтем selectedItem: %+v", selectedItem.ID)
											//	еще один вызов для лога
											if err := tempStorage.SelectItem(&tempSelectedItem); err != nil {
												//	вернули ошибку - возвращаем 404
												log.Errorf("Ошибка при выборе объектов: %+v", err)
											}
											log.Warnf("Выбрали существующий айтем айтем: %+v", selectedItem.ID)
											//	меняем статус проставляем ордер
											selectedItem.StatusID = 4
											selectedItem.OrderID = order.ID

											if err := tempStorage.UpdateItemSelect(&selectedItem, "status_id", "order_id"); err != nil {
												//	вернули ошибку - возвращаем 400
												log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
												http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
												return
											} else {
												//  все нормально - возвращаем 200
												tempStorage.CreateLog(&tempSelectedItem, &selectedItem, "update", "Guest") // запись успешного удаления в лог

											}
										}
									}
								}
							}
						}
					}

					// 	дергаем функцию выборки
					//	проверяем, что не вернули ошибку
					if err := tempStorage.SelectItem(&order); err != nil {
						//	вернули ошибку - возвращаем 404
						log.Errorf("Ошибка при выборе объектов: %+v", err)
						http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
						return
					} else {
						//  все нормально - возвращаем 200
						w.WriteHeader(200)
						//	возвращаем полученные объекты
						_ = json.NewEncoder(w).Encode(order)
						return
					}

				}
			}
		}

	} else {
		//	вернули ошибку - возвращаем 404
		log.Error("Ошибка при изменении статуса заказа - sberbankOrderID пустой")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при изменении статуса заказа - sberbankOrderID пустой\"}"), http.StatusNotFound)
		return
	}

	//	вернули ошибку - возвращаем 404
	log.Error("Ошибка при изменении статуса заказа")
	http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при изменении статуса заказа\"}"), http.StatusNotFound)
	return

	//var registerRequest = SberbankRegisterRequest{
	//	UserName: userName,
	//	Password: password,
	//	OrderNumber: string(order.ID),
	//	Amount: order.TotalCost,
	//	ReturnUrl: "https://tarkov.grey-shop.com/payments/final",
	//}
}
