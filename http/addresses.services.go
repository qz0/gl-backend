package http

import (
	//"crypto/md5"
	"encoding/json"
	"fmt"

	"../db"

	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var AddressRoutes = []Route{
	//	сами функции
	Route{
		"createAddress",
		"Создаем объект",
		"PUT",
		"/addresses/create",
		createAddress,
		//validateMiddleware(createAddress),
	},
	Route{
		"deleteAddress",
		"Удаляем объект",
		"DELETE",
		"/addresses/delete",
		deleteAddress,
		//validateMiddleware(deleteAddress),
	},
	Route{
		"getAllAddresses",
		"Получаем список всех объектов",
		"GET",
		"/addresses/read_all",
		getAllAddresses,
		//validateMiddleware(getAllAddresses),
	},
	Route{
		"getCurrentAddress",
		"Получаем конкретный объект",
		"GET",
		"/addresses/read_current",
		getCurrentAddress,
		//validateMiddleware(getCurrentAddress),
	},
	Route{
		"updateAddress",
		"Изменяем объект",
		"PATCH",
		"/addresses/update",
		updateAddress,
		//validateMiddleware(updateAddress),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/addresses/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/addresses/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/addresses/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/addresses/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/addresses/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем пользователя
func createAddress(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createAddress")

	//	создаем переменные
	var address db.Address
	var addresses []db.Address

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&address); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", address)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&address); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&addresses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(addresses)
	}
}

// 	Удаляем Пользователя
func deleteAddress(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteAddress")

	// 	создаем переменные
	var address db.Address
	//var tempAddress db.Address
	var addresses []db.Address

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	addressID := r.URL.Query().Get("id")

	// 	имя получателя
	if addressID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(addressID)
		address.ID = uint(id)
		//tempAddress.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&address); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&address); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	addressID is empty
		log.Errorf("Ошибка при удалении объекта - передан пjson.NewEncoder(w).Encode(устой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&addresses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(addresses)
	}
}

// 	Получаем список всех пользователей
func getAllAddresses(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllAddresses")

	// 	создаем переменные
	var addresses []db.Address

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&addresses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(addresses)
	}
}

// 	Получаем пользователя по ИД
func getCurrentAddress(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentAddress")

	// 	создаем переменные
	var address db.Address

	//	подключаем хранилище
	tempStorage := storage

	addressID := r.URL.Query().Get("id")

	// 	имя получателя
	if addressID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(addressID)
		address.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&address); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(address)
		}
	}
}

// 	Изменяем Пользователя
func updateAddress(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateAddress")

	// 	создаем переменные
	var address db.Address
	var tempAddress db.Address
	var addresses []db.Address

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&address); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", address)
		//	сохраняем ИД для поиска
		tempAddress.ID = address.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempAddress); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&address); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempAddress, &address, "update", "Guest") // запись успешного удаления в лог
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&addresses); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(addresses)
	}
}
