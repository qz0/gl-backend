package http

import (
	"crypto/tls"
	"fmt"

	"gopkg.in/gomail.v2"
	"net"
	"net/mail"
	"net/smtp"

	log "github.com/sirupsen/logrus"
)

//	SERVICES

func sendMail(email string, subject string, body string) {

	from := mail.Address{"Greyline Shop", "shop@grey-line.com"}
	to := mail.Address{"Alexey", email}

	// Setup headers
	headers := make(map[string]string)
	log.Warn(from.String(), to.String())
	headers["From"] = "shop@grey-line.com"
	headers["To"] = to.String()
	headers["Subject"] = subject

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body

	// Connect to the SMTP Server
	servername := "smtp.yandex.ru:465"

	host, _, _ := net.SplitHostPort(servername)

	auth := smtp.PlainAuth("", "shop@grey-line.com", "*", host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	// Here is the key, you need to call tls.Dial instead of smtp.Dial
	// for smtp servers running on 465 that require an ssl connection
	// from the very beginning (no starttls)
	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		log.Panic(err)
	}

	c, err := smtp.NewClient(conn, host)
	if err != nil {
		log.Panic(err)
	}

	// Auth
	if err = c.Auth(auth); err != nil {
		log.Panic(err)
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		log.Panic(err)
	}

	if err = c.Rcpt(to.Address); err != nil {
		log.Panic(err)
	}

	// c.Data
	// Data
	ww, err2 := c.Data()
	if err2 != nil {
		log.Panic(err)
	}

	_, err = ww.Write([]byte(message))
	if err != nil {
		log.Panic(err)
	}

	err = ww.Close()
	if err != nil {
		log.Panic(err)
	}

	c.Quit()
}

func sendMailV2(email string, subject string, body string) {

	m := gomail.NewMessage()
	m.SetHeader("From", "shop@grey-line.com")
	m.SetHeader("To", email)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	// Send the email to Bob
	d := gomail.NewDialer("smtp.yandex.ru", 465, "shop@grey-line.com", "*")
	if err := d.DialAndSend(m); err != nil {
		log.Errorf("Ошибка в sendMailV2: ", err)
		panic(err)
	} else {
		log.Warn("sendMailV2 успешно отработал")
	}
}
