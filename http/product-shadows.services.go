package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var ProductShadowRoutes = []Route{
	//	сами функции
	Route{
		"createProductShadow",
		"Создаем объект",
		"PUT",
		"/product_shadows/create",
		createProductShadow,
		//validateMiddleware(createProductShadow),
	},
	Route{
		"deleteProductShadow",
		"Удаляем объект",
		"DELETE",
		"/product_shadows/delete",
		deleteProductShadow,
		//validateMiddleware(deleteProductShadow),
	},
	Route{
		"getAllProductShadows",
		"Получаем список всех объектов",
		"GET",
		"/product_shadows/read_all",
		getAllProductShadows,
		//validateMiddleware(getAllProductShadows),
	},
	Route{
		"getAllProductShadows",
		"Получаем конкретный объект",
		"GET",
		"/product_shadows/read_current",
		getCurrentProductShadow,
		//validateMiddleware(getCurrentProductShadow),
	},
	Route{
		"updateProductShadow",
		"Изменяем объект",
		"PATCH",
		"/product_shadows/update",
		updateProductShadow,
		//validateMiddleware(updateProductShadow),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/product_shadows/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/product_shadows/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/product_shadows/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/product_shadows/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/product_shadows/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createProductShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createProductShadow")

	//	создаем переменные
	var productShadow db.ProductShadow
	var productShadows []db.ProductShadow

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&productShadow); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", productShadow)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&productShadow); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&productShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(productShadows)
	}
}

// 	Удаляем объект
func deleteProductShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteProductShadow")

	// 	создаем переменные
	var productShadow db.ProductShadow
	//var tempProductShadow db.ProductShadow
	var productShadows []db.ProductShadow

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	productShadowID := r.URL.Query().Get("id")

	// 	имя получателя
	if productShadowID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(productShadowID)
		productShadow.ID = uint(id)
		//tempProductShadow.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&productShadow); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&productShadow); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	productShadowID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&productShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(productShadows)
	}
}

// 	Получаем список всех объектов
func getAllProductShadows(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllProductShadows")

	// 	создаем переменные
	var productShadows []db.ProductShadow

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&productShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(productShadows)
	}
}

// 	Получаем объект по ИД
func getCurrentProductShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentProductShadow")

	// 	создаем переменные
	var productShadow db.ProductShadow

	//	подключаем хранилище
	tempStorage := storage

	productShadowID := r.URL.Query().Get("id")

	// 	имя получателя
	if productShadowID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(productShadowID)
		productShadow.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&productShadow); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(productShadow)
		}
	}
}

// 	Изменяем объект
func updateProductShadow(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateProductShadow")

	// 	создаем переменные
	var productShadow db.ProductShadow
	var tempProductShadow db.ProductShadow
	var productShadows []db.ProductShadow

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&productShadow); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", productShadow)
		//	сохраняем ИД для поиска
		tempProductShadow.ID = productShadow.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempProductShadow); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&productShadow); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempProductShadow, &productShadow, "update", "Guest") // запись успешного удаления в лог

		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&productShadows); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(productShadows)
	}
}
