package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var ManufacturerRoutes = []Route{
	//	сами функции
	/*	Route{
		"changePriorityOfManufactures",
		"Изменяем приоритет объекта",
		"PATCH",
		"/manufacturers/change_priority",
		changePriorityOfManufacturer,
		//validateMiddleware(changePriorityOfManufactures),
	},*/
	Route{
		"createManufacturer",
		"Создаем объект",
		"PUT",
		"/manufacturers/create",
		createManufacturer,
		//validateMiddleware(createManufacturer),
	},
	Route{
		"deleteManufacturer",
		"Удаляем объект",
		"DELETE",
		"/manufacturers/delete",
		deleteManufacturer,
		//validateMiddleware(deleteManufacturer),
	},
	Route{
		"getAllManufacturers",
		"Получаем список всех объектов",
		"GET",
		"/manufacturers/read_all",
		getAllManufacturers,
		//validateMiddleware(getAllManufacturers),
	},
	Route{
		"getCurrentManufacturer",
		"Получаем конкретный объект",
		"GET",
		"/manufacturers/read_current",
		getCurrentManufacturer,
		//validateMiddleware(getCurrentManufacturer),
	},
	Route{
		"updateManufacturer",
		"Изменяем объект",
		"PATCH",
		"/manufacturers/update",
		updateManufacturer,
		//validateMiddleware(updateManufacturer),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/manufacturers/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/manufacturers/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/manufacturers/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/manufacturers/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/manufacturers/update",
		statusOK,
	},
}

//	SERVICES
/* Ждём пока главный добавит у себя или одобрит
func changePriorityOfManufacturer(w http.ResponseWriter, r *http.Request) {
	// моя копипаста, но вроде верно понял суть.
	// 	создаем переменные
	var manufacturer db.Manufacturer
	var tempManufacturer db.Manufacturer
	var manufacturers []db.Manufacturer

	//	подключаем хранилище
	tempStorage := storage
	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&manufacturer); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", manufacturer)
		//	сохраняем ИД для поиска
		tempManufacturer.ID = manufacturer.ID
	}
	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempManufacturer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	} else {
		log.Warn("Запись есть")
	}
	//	выясняем в какую сторону перемещение
	if manufacturer.Priority < tempManufacturer.Priority {
		//	число приоритета уменьшили
		//	получаем список доставок где приоритет выше
		if err := tempStorage.SelectItemsWhere(&manufacturer, 0, 50, "priority >= ? and priority < ?", fmt.Sprint(manufacturer.Priority), fmt.Sprint(tempManufacturer.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах верхнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentManufacturer := range manufacturers {
				// 	прибавляем к приоритету единицу
				currentManufacturer.Priority = currentManufacturer.Priority + 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentManufacturer); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах верхнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err),
						http.StatusBadRequest)
				} else {
					log.Warnf("Updated Manufacturer: %v", currentManufacturer)
				}
			}
		}
	} else {
		//	число приоритета такое же или увеличено
		//	получаем список доставок где приоритет ниже
		if err := tempStorage.SelectItemsWhere(&manufacturers, 0, 50, "priority <= ? and priority > ?",
			fmt.Sprint(manufacturer.Priority), fmt.Sprint(tempManufacturer.Priority)); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при попытке изменения приоритета в элементах нижнего уровня - изменяемые объекты не найдены: : %+v\"}", err), http.StatusBadRequest)
			return
		} else {
			//	изменяем там приоритеты
			for index, currentManufacturer := range manufacturers {
				// 	прибавляем к приоритету единицу
				currentManufacturer.Priority = currentManufacturer.Priority - 1
				// 	дергаем функцию изменения
				//	проверяем, что не вернули ошибку
				if err := tempStorage.UpdateItem(&currentManufacturer); err != nil {
					//	вернули ошибку - возвращаем 400
					log.Errorf("Ошибка при попытке изменения приоритета в элементах нижнего уровня (%d): %+v", index, err)
					http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
				} else {
					log.Infof("Updated Manufacturer: %v", currentManufacturer)
				}
			}
		}
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&manufacturer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден_: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&manufacturers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(manufacturers)
	}
}
*/

// 	добавляем объект
func createManufacturer(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createManufacturer")

	//	создаем переменные
	var manufacturer db.Manufacturer
	var manufacturers []db.Manufacturer

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&manufacturer); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", manufacturer)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&manufacturer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&manufacturers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(manufacturers)
	}
}

// 	Удаляем объект
func deleteManufacturer(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteManufacturer")

	// 	создаем переменные
	var manufacturer db.Manufacturer
	//var tempManufacturer db.Manufacturer
	var manufacturers []db.Manufacturer

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	manufacturerID := r.URL.Query().Get("id")

	// 	имя получателя
	if manufacturerID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(manufacturerID)
		manufacturer.ID = uint(id)
		//tempManufacturer.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&manufacturer); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&manufacturer); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	manufacturerID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&manufacturers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(manufacturers)
	}
}

// 	Получаем список всех объектов
func getAllManufacturers(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllManufacturers")

	// 	создаем переменные
	var manufacturers []db.Manufacturer

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&manufacturers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(manufacturers)
	}
}

// 	Получаем объект по ИД
func getCurrentManufacturer(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentManufacturer")

	// 	создаем переменные
	var manufacturer db.Manufacturer

	//	подключаем хранилище
	tempStorage := storage

	manufacturerID := r.URL.Query().Get("id")

	// 	имя получателя
	if manufacturerID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(manufacturerID)
		manufacturer.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&manufacturer); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(manufacturer)
		}
	}
}

// 	Изменяем объект
func updateManufacturer(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateManufacturer")

	// 	создаем переменные
	var manufacturer db.Manufacturer
	var tempManufacturer db.Manufacturer
	var manufacturers []db.Manufacturer

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&manufacturer); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", manufacturer)
		//	сохраняем ИД для поиска
		tempManufacturer.ID = manufacturer.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempManufacturer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&manufacturer); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempManufacturer, &manufacturer, "update", "Guest") // запись успешного удаления в лог

		log.Warnf("Manufacturer: %+v", manufacturer)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&manufacturers); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)

		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(manufacturers)
	}
}
