package http

import (
	//"crypto/md5"
	"encoding/json"
	"fmt"

	"../db"

	//"github.com/dgrijalva/jwt-go"
	//"io/ioutil"
	"net/http"
	"strconv"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var PurchaseRoutes = []Route{
	//	сами функции
	Route{
		"createPurchase",
		"Создаем объект",
		"PUT",
		"/purchases/create",
		createPurchase,
		//validateMiddleware(createPurchase),
	},
	Route{
		"deletePurchase",
		"Удаляем объект",
		"DELETE",
		"/purchases/delete",
		deletePurchase,
		//validateMiddleware(deletePurchase),
	},
	Route{
		"getAllPurchases",
		"Получаем список всех объектов",
		"GET",
		"/purchases/read_all",
		getAllPurchases,
		//validateMiddleware(getAllPurchases),
	},
	Route{
		"getCurrentPurchase",
		"Получаем конкретный объект",
		"GET",
		"/purchases/read_current",
		getCurrentPurchase,
		//validateMiddleware(getCurrentPurchase),
	},
	Route{
		"updatePurchase",
		"Изменяем объект",
		"PATCH",
		"/purchases/update",
		updatePurchase,
		//validateMiddleware(updatePurchase),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/purchases/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/purchases/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/purchases/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/purchases/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/purchases/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createPurchase(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createPurchase")

	//	создаем переменные
	var purchase db.Purchase
	var purchases []db.Purchase

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&purchase); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", purchase)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&purchase); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&purchases); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(purchases)
	}
}

// 	Удаляем объект
func deletePurchase(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deletePurchase")

	// 	создаем переменные
	var purchase db.Purchase
	//var tempPurchase db.Purchase
	var purchases []db.Purchase

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	purchaseID := r.URL.Query().Get("id")

	// 	имя получателя
	if purchaseID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(purchaseID)
		purchase.ID = uint(id)
		//tempPurchase.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&purchase); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&purchase); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	purchaseID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&purchases); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(purchases)
	}
}

// 	Получаем список всех объектов
func getAllPurchases(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllPurchases")

	// 	создаем переменные
	var purchases []db.Purchase

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&purchases); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(purchases)
	}
}

// 	Получаем объект по ИД
func getCurrentPurchase(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentPurchase")

	// 	создаем переменные
	var purchase db.Purchase

	//	подключаем хранилище
	tempStorage := storage

	purchaseID := r.URL.Query().Get("id")

	// 	имя получателя
	if purchaseID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(purchaseID)
		purchase.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&purchase); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(purchase)
		}
	}
}

// 	Изменяем объект
func updatePurchase(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updatePurchase")

	// 	создаем переменные
	var purchase db.Purchase
	var tempPurchase db.Purchase
	var purchases []db.Purchase

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&purchase); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", purchase)
		//	сохраняем ИД для поиска
		tempPurchase.ID = purchase.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempPurchase); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&purchase); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempPurchase, &purchase, "update", "Guest") // запись успешного удаления в лог

		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&purchases); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(purchases)
	}
}
