package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var AssemblyRoutes = []Route{
	//	сами функции
	Route{
		"createAssembly",
		"Создаем объект",
		"PUT",
		"/assemblies/create",
		createAssembly,
		//validateMiddleware(createAssembly),
	},
	Route{
		"deleteAssembly",
		"Удаляем объект",
		"DELETE",
		"/assemblies/delete",
		deleteAssembly,
		//validateMiddleware(deleteAssembly),
	},
	Route{
		"getAllAssemblies",
		"Получаем список всех объектов",
		"GET",
		"/assemblies/read_all",
		getAllAssemblies,
		//validateMiddleware(getAllAssemblies),
	},
	Route{
		"getCurrentAssembly",
		"Получаем конкретный объект",
		"GET",
		"/assemblies/read_current",
		getCurrentAssembly,
		//validateMiddleware(getCurrentAssembly),
	},
	Route{
		"updateAssembly",
		"Изменяем объект",
		"PATCH",
		"/assemblies/update",
		updateAssembly,
		//validateMiddleware(updateAssembly),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/assemblies/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/assemblies/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/assemblies/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/assemblies/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/assemblies/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем пользователя
func createAssembly(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createAssembly")

	//	создаем переменные
	var assembly db.Assembly
	var assemblies []db.Assembly

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&assembly); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", assembly)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&assembly); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&assemblies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(assemblies)
	}

	//log.Info("insertAssembly")
	//var login Logon
	//_ = json.NewDecoder(r.Body).Decode(&login)
	//log.Info(login)
	//// 	переменные
	//var newAssembly db.Assembly
	//var assemblies []db.Assembly
	//tempStorage := storage
	//
	//// 	собираем запись для добавления
	//// 	логин
	//if len(login.Assemblyname) > 0 {
	//	newAssembly.AssemblyName = login.Assemblyname
	//	// log.Warnf("Таблица assemblies: %v", db.HasTable(&Assembly{}))in.Assemblyname
	//} else {
	//	log.Error("Пустой логин")
	//}
	//
	//// 	пароль
	//if len(login.Password) > 0 {
	//	bytepass := []byte(login.Password)
	//	newAssembly.Password = fmt.Sprintf("%x", md5.Sum(bytepass))
	//} else {
	//	log.Error("Пустой пароль")
	//}
	//
	//log.Infof("%+v", newAssembly)
	////	добавление новой записи
	//// 	добавляем запись
	//tempStorage.HandleDB.Create(&newAssembly)
	//
	//// 	возвращаем нотисы
	//tempStorage.HandleDB.Find(&assemblies)
	//
	//// 	помещаем ответ в массив символов для возврата
	//data, err := json.Marshal(assemblies)
	//if err != nil {
	//	log.Errorf("Ошибка при получении списка пользователей: %v", err)
	//} else {
	//	log.Infof("База пользователей: %s", data)
	//}
	//
	//token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
	//	"ID":       newAssembly.ID,
	//	"AssemblyName": newAssembly.AssemblyName,
	//})
	//tokenString, error := token.SignedString([]byte(secretKey))
	//if error != nil {
	//	fmt.Println(error)
	//}
	//w.WriteHeader(200)
	//_ = json.NewEncoder(w).Encode(JwtToken{Token: tokenString})
}

// 	Удаляем Пользователя
func deleteAssembly(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteAssembly")

	// 	создаем переменные
	var assembly db.Assembly
	//var tempAssembly db.Assembly
	var assemblies []db.Assembly

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	assemblyID := r.URL.Query().Get("id")

	// 	имя получателя
	if assemblyID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(assemblyID)
		assembly.ID = uint(id)
		//tempAssembly.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&assembly); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&assembly); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	assemblyID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&assemblies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(assemblies)
	}
}

// 	Получаем список всех пользователей
func getAllAssemblies(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllAssemblies")

	// 	создаем переменные
	var assemblies []db.Assembly

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&assemblies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(assemblies)
	}
}

// 	Получаем пользователя по ИД
func getCurrentAssembly(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentAssembly")

	// 	создаем переменные
	var assembly db.Assembly

	//	подключаем хранилище
	tempStorage := storage

	assemblyID := r.URL.Query().Get("id")

	// 	имя получателя
	if assemblyID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(assemblyID)
		assembly.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&assembly); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(assembly)
		}
	}
}

// 	Изменяем Пользователя
func updateAssembly(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateAssembly")

	// 	создаем переменные
	var assembly db.Assembly
	var tempAssembly db.Assembly
	var assemblies []db.Assembly

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&assembly); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", assembly)
		//	сохраняем ИД для поиска
		tempAssembly.ID = assembly.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempAssembly); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&assembly); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempAssembly, &assembly, "update", "Guest") // запись успешного удаления в лог
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&assemblies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(assemblies)
	}
}
