package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var CountryRoutes = []Route{
	//	сами функции
	Route{
		"getAllShortCountries",
		"Получаем список всех объектов в сокращенной форме",
		"GET",
		"/countries/read_all_short",
		getAllShortCountries,
		//validateMiddleware(getAllShortCountries),
	},
	Route{
		"createCountry",
		"Создаем объект",
		"PUT",
		"/countries/create",
		createCountry,
		//validateMiddleware(createCountry),
	},
	Route{
		"deleteCountry",
		"Удаляем объект",
		"DELETE",
		"/countries/delete",
		deleteCountry,
		//validateMiddleware(deleteCountry),
	},
	Route{
		"getAllCountries",
		"Получаем список всех объектов",
		"GET",
		"/countries/read_all",
		getAllCountries,
		//validateMiddleware(getAllCountries),
	},
	Route{
		"getCurrentCountry",
		"Получаем конкретный объект",
		"GET",
		"/countries/read_current",
		getCurrentCountry,
		//validateMiddleware(getCurrentCountry),
	},
	Route{
		"updateCountry",
		"Изменяем объект",
		"PATCH",
		"/countries/update",
		updateCountry,
		//validateMiddleware(updateCountry),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/countries/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/countries/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/countries/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/countries/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/countries/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createCountry(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createCountry")

	//	создаем переменные
	var country db.Country
	var countries []db.Country

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&country); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", country)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&country); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&countries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(countries)
	}
}

// 	Удаляем объект
func deleteCountry(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteCountry")

	// 	создаем переменные
	var country db.Country
	//var tempCountry db.Country
	var countries []db.Country

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	countryID := r.URL.Query().Get("id")

	// 	имя получателя
	if countryID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(countryID)
		country.ID = uint(id)
		//tempCountry.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&country); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&country); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	countryID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&countries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(countries)
	}
}

// 	Получаем список всех объектов
func getAllCountries(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllCountries")

	// 	создаем переменные
	var countries []db.Country

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&countries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(countries)
	}
}

// 	Получаем шорт список всех объектов
func getAllShortCountries(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllShortCountries")

	// 	создаем переменные
	var countries []db.Country
	var countriesShort []db.CountryShort

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItemsShort(&countries, "id, name, name_ru"); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//	перекодируем в сжатую версию
		for _, country := range countries {
			countriesShort = append(countriesShort, db.ConvertCountryLongToShort(country))
		}
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(countriesShort)
	}
}

// 	Получаем объект по ИД
func getCurrentCountry(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentCountry")

	// 	создаем переменные
	var country db.Country

	//	подключаем хранилище
	tempStorage := storage

	countryID := r.URL.Query().Get("id")

	// 	имя получателя
	if countryID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(countryID)
		country.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&country); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(country)
		}
	}
}

// 	Изменяем объект
func updateCountry(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCountry")

	// 	создаем переменные
	var country db.Country
	var tempCountry db.Country
	var countries []db.Country

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&country); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", country)
		//	сохраняем ИД для поиска
		tempCountry.ID = country.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCountry); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&country); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		tempStorage.CreateLog(&tempCountry, &country, "update", "Guest") // запись успешного удаления в лог
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&countries); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(countries)
	}
}
