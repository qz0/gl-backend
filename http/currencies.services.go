package http

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"../db"

	log "github.com/sirupsen/logrus"
)

// 	массив роутинга
var CurrencyRoutes = []Route{
	//	сами функции
	Route{
		"createCurrency",
		"Создаем объект",
		"PUT",
		"/currencies/create",
		createCurrency,
		//validateMiddleware(createCurrency),
	},
	Route{
		"deleteCurrency",
		"Удаляем объект",
		"DELETE",
		"/currencies/delete",
		deleteCurrency,
		//validateMiddleware(deleteCurrency),
	},
	Route{
		"getAllCurrencies",
		"Получаем список всех объектов",
		"GET",
		"/currencies/read_all",
		getAllCurrencies,
		//validateMiddleware(getAllCurrencies),
	},
	Route{
		"getCurrentCurrency",
		"Получаем конкретный объект",
		"GET",
		"/currencies/read_current",
		getCurrentCurrency,
		//validateMiddleware(getCurrentCurrency),
	},
	Route{
		"updateCurrency",
		"Изменяем объект",
		"PATCH",
		"/currencies/update",
		updateCurrency,
		//validateMiddleware(updateCurrency),
	},
	//	STATUS OK
	Route{
		"statusOK",
		"Создаем объект",
		"OPTIONS",
		"/currencies/create",
		statusOK,
	},
	Route{
		"statusOK",
		"Удаляем объект",
		"OPTIONS",
		"/currencies/delete",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем все объекты",
		"OPTIONS",
		"/currencies/read_all",
		statusOK,
	},
	Route{
		"statusOK",
		"Получаем конкретный объект",
		"OPTIONS",
		"/currencies/read_current",
		statusOK,
	},
	Route{
		"statusOK",
		"Изменяем объект",
		"OPTIONS",
		"/currencies/update",
		statusOK,
	},
}

//	SERVICES

// 	добавляем объект
func createCurrency(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("createCurrency")

	//	создаем переменные
	var currency db.Currency
	var currencies []db.Currency

	// 	переменные
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&currency); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", currency)
	}

	// 	дергаем функцию создания
	//	проверяем, что не вернули ошибку
	if err := tempStorage.CreateItem(&currency); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при создании объекта - не удалось создать объект в базе данных: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&currencies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(currencies)
	}
}

// 	Удаляем объект
func deleteCurrency(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("deleteCurrency")

	// 	создаем переменные
	var currency db.Currency
	//var tempCurrency db.Currency
	var currencies []db.Currency

	//	подключаем хранилище
	tempStorage := storage

	// 	получаем параметр
	currencyID := r.URL.Query().Get("id")

	// 	имя получателя
	if currencyID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(currencyID)
		currency.ID = uint(id)
		//tempCurrency.ID = uint(id)

		//	проверяем, что такая запись есть
		if err := tempStorage.SelectItem(&currency); err != nil {
			//	вернули ошибку - возвращаем 400
			log.Errorf("Ошибка при удалении объекта - удаляемый объект не найден: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - удаляемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		} else {
			// 	дергаем функцию удаления
			if err = tempStorage.DeleteItem(&currency); err != nil {
				//	вернули ошибку - возвращаем 400
				log.Errorf("Ошибка при удалении объекта - не удалось удалить объект из базы: %+v", err)
				http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - не удалось удалить объект из базы: : %+v\"}", err), http.StatusBadRequest)
			} else {
				//  все нормально - возвращаем 200
				w.WriteHeader(200)
			}
		}
	} else {
		//	currencyID is empty
		log.Errorf("Ошибка при удалении объекта - передан пустой идентификатор")
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при удалении объекта - передан пустой идентификатор\"}"), http.StatusBadRequest)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&currencies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(currencies)
	}
}

// 	Получаем список всех объектов
func getAllCurrencies(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getAllCurrencies")

	// 	создаем переменные
	var currencies []db.Currency

	//	подключаем хранилище
	tempStorage := storage

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&currencies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(currencies)
	}
}

// 	Получаем объект по ИД
func getCurrentCurrency(w http.ResponseWriter, r *http.Request) {
	//	log
	//log.Warn("getCurrentCurrency")

	// 	создаем переменные
	var currency db.Currency

	//	подключаем хранилище
	tempStorage := storage

	currencyID := r.URL.Query().Get("id")

	// 	имя получателя
	if currencyID != "" {
		// 	конвертируем в UINT
		id, _ := strconv.Atoi(currencyID)
		currency.ID = uint(id)
		// 	возвращаем нотисы
		if err := tempStorage.SelectItem(&currency); err != nil {
			//	вернули ошибку - возвращаем 404
			log.Errorf("Ошибка при выборе объекта: %+v", err)
			http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - объект не найден: : %+v\"}", err), http.StatusNotFound)
			return
		} else {
			//  все нормально - возвращаем 200
			w.WriteHeader(200)
			//	возвращаем полученные объекты
			_ = json.NewEncoder(w).Encode(currency)
		}
	}
}

// 	Изменяем объект
func updateCurrency(w http.ResponseWriter, r *http.Request) {
	//	log
	log.Warn("updateCurrency")

	// 	создаем переменные
	var currency db.Currency
	var tempCurrency db.Currency
	var currencies []db.Currency

	//	подключаем хранилище
	tempStorage := storage

	//	декодирование переданной структуры из запроса
	//	проверяем, что не вернули ошибку
	if err := json.NewDecoder(r.Body).Decode(&currency); err != nil {
		//	вернули ошибку - возвращаем 422
		log.Errorf("Ошибка при создании объекта - неверный формат запроса: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при создании объекта - неверный формат запроса: : %+v\"}", err), http.StatusUnprocessableEntity)
		return
	} else {
		log.Infof("Имеем структурку: %+v", currency)
		//	сохраняем ИД для поиска
		tempCurrency.ID = currency.ID
	}

	//	проверяем, что такая запись есть
	if err := tempStorage.SelectItem(&tempCurrency); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при выборе объекта - изменяемый объект не найден: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при сохранении объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
		return
	}

	// 	дергаем функцию изменения
	//	проверяем, что не вернули ошибку
	if err := tempStorage.UpdateItem(&currency); err != nil {
		//	вернули ошибку - возвращаем 400
		log.Errorf("Ошибка при попытке сохранения изменений: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объекта - изменяемый объект не найден: : %+v\"}", err), http.StatusBadRequest)
	} else {
		tempStorage.CreateLog(&tempCurrency, &currency, "update", "Guest") // запись успешного удаления в лог
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
	}

	// 	дергаем функцию выборки
	//	проверяем, что не вернули ошибку
	if err := tempStorage.SelectAllItems(&currencies); err != nil {
		//	вернули ошибку - возвращаем 404
		log.Errorf("Ошибка при выборе объектов: %+v", err)
		http.Error(w, fmt.Sprintf("{\"status\":\"error\",\"error\":\"Ошибка при выборе объектов - массив объектов не найден: : %+v\"}", err), http.StatusNotFound)
		return
	} else {
		//  все нормально - возвращаем 200
		w.WriteHeader(200)
		//	возвращаем полученные объекты
		_ = json.NewEncoder(w).Encode(currencies)
	}
}
