package main

import (
	"./db"
	"./http"
	"./settings"
	log "github.com/sirupsen/logrus"
)

//  backend for grey-line.com
// 	точка входа
func main() {
	// выставляю уровень логов. Раскоментить желаемое что бы выбрать уровень с которого отображаются логи..
	log.SetLevel(log.DebugLevel)
	// log.SetLevel(log.InfoLevel)

	//	объявили настройки
	mySettings := &settings.SettingsStruct{ListeningPort: settings.LISTENING_PORT}

	//	проинициализировали
	mySettings.LoadSettings(settings.SETTINGS_FILE_NAME)

	log.Infof("Загружены настройки %v", *mySettings)

	// 	создаем БД
	var dataBase db.DataBase
	err := dataBase.InitDB(
		mySettings.ServerDB, mySettings.PortDB, mySettings.UserNameDB, mySettings.NameDB,
		mySettings.PasswordDB, mySettings.SslModeDB, mySettings.SchemaNameDB)
	if err != nil {
		log.Error("Er", err)
		return
	} else {
		log.Info("Подключено к БД.")
	}
	defer dataBase.CloseDB()

	// Включаем роутер
	http.CreateWebServer(mySettings.ListeningPort, dataBase, settings.HttpMode)

}
