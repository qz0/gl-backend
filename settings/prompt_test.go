package settings

import (
	"os"
	"testing"
)

func TestInitialPromptArgs(t *testing.T) {
	//	эталон
	const truePort = "10000"

	//	с одним значением
	t.Run("os.Args = 2", func(t *testing.T) {
		//	порт равно 10000
		os.Args = []string{"", truePort}

		InitialPromptArgs()

		if ArgsPort != truePort {
			t.Errorf("Wrong port: got %s want %s", ArgsPort, truePort)
		}
	})

	//	с несколькими
	t.Run("os.Args > 2", func(t *testing.T) {
		//	порт равно 10000
		os.Args = []string{"", truePort, "httpMode"}

		InitialPromptArgs()

		if ArgsPort != truePort && len(os.Args) > 2 {
			t.Errorf("Wrong port: got %s want %s", ArgsPort, truePort)
		}
	})
}
