package settings

import (
	"encoding/json"
	"os"
	"testing"
)

func TestLoadSettings(t *testing.T) {
	//	эталон
	var trueSettings = SettingsStruct{
		LISTENING_PORT,
		DB_HOST,
		DB_PORT,
		DB_USERNAME,
		DB_NAME,
		DB_PASSWORD,
		DB_SSL_MODE,
		DB_SCHEMA,
	}
	var TEST_SETTINGS_FILE_NAME = "test.conf"

	//	грузим настройки
	//	файл настроек существует
	t.Run("loadSettings: file exist", func(t *testing.T) {
		//	переменные для тестирования
		var testSettings SettingsStruct

		//	предварительно создаем временный файл настроек
		testFile, err := os.OpenFile("./"+TEST_SETTINGS_FILE_NAME, os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			t.Errorf("Can't create temporary settings file: %v", err)
		} else {
			//	Маршализируем
			buf, _ := json.Marshal(trueSettings)

			//	Копируем структурку в файлик
			testFile.Write(buf)
		}
		testFile.Close()

		//	тест
		testSettings.LoadSettings(TEST_SETTINGS_FILE_NAME)

		//	проверка
		if trueSettings != testSettings {
			t.Errorf("Wrong settings: got %v want %v", testSettings, trueSettings)
		}

		//	удаляем временный файл настроек
		err = os.Remove("./" + TEST_SETTINGS_FILE_NAME)
		if err != nil {
			t.Errorf("Can't remove temporary settings file: %v", err)
		}

	})

	//	файл настроек не существует
	t.Run("loadSettings: file doesn't exist", func(t *testing.T) {
		//	переменные для тестирования
		var testSettings SettingsStruct

		//	проверяем существует ли временный файл настроек, если нет - создаем
		if _, err := os.Stat(TEST_SETTINGS_FILE_NAME); err != nil {
			//	тест
			testSettings.LoadSettings(TEST_SETTINGS_FILE_NAME)

			//	проверка
			if trueSettings != testSettings {
				t.Errorf("Wrong settings: got %v want %v", testSettings, trueSettings)
			}

			//	удаляем временный файл настроек
			err := os.Remove("./" + TEST_SETTINGS_FILE_NAME)
			if err != nil {
				t.Errorf("Can't remove temporary settings file: %v", err)
			}
		}
	})
}
