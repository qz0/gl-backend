package settings

import (
	"encoding/json"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

//	функции инициализации

//	константы инициализации
const (
	//	значение порта по умолчанию, в случае, если файл конфигурации отсутствует
	//	(только для дева, на проде закомментировать)
	LISTENING_PORT = 700
	DB_HOST        = "gitlab.grey-line.com"
	DB_PORT        = 10000
	DB_USERNAME    = "greyline"
	DB_PASSWORD    = "*"
	DB_NAME        = "greyline"
	DB_SCHEMA      = "public"
	DB_SSL_MODE    = "disable"
	//	файл настроек по умолчанию
	SETTINGS_FILE_NAME = "backend.conf"
)

var mySettings SettingsStruct

//	Объявляем структурку настроек

//	программа загрузки настроек
func (mySettings *SettingsStruct) LoadSettings(settingsFileName string) {

	//	Читаем содержимое файла настроек
	myFile, err := os.Open(settingsFileName)
	if err != nil {
		//	не вышло - пробуем в корень
		log.Error("Не нашел конфиг по относительнмоу пути - пробую абсолютный.")
		myFile, err = os.Open("/opt/greyline/" + settingsFileName)
		if err != nil {

			//	Файла нет - создаем
			log.Warn("Файла нет - создаем.")
			myFile, err := os.OpenFile("./"+settingsFileName, os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer myFile.Close()

			//	Объявляем для примера один параметр
			mySettings.ListeningPort = LISTENING_PORT
			mySettings.ServerDB = DB_HOST
			mySettings.PortDB = DB_PORT
			mySettings.UserNameDB = DB_USERNAME
			mySettings.PasswordDB = DB_PASSWORD
			mySettings.NameDB = DB_NAME
			mySettings.SslModeDB = DB_SSL_MODE
			mySettings.SchemaNameDB = DB_SCHEMA
			//	Маршализируем
			buf, _ := json.Marshal(*mySettings)

			log.Infof("%v", *mySettings)
			//	Копируем структурку в файлик
			myFile.Write(buf)

			return

		}
		defer myFile.Close()
	}
	//	Отложенно закрываем
	defer myFile.Close()

	// Получить размер файла
	stat, err := myFile.Stat()
	if err != nil {
		return
	}

	// Чтение файла
	buf := make([]byte, stat.Size())
	_, err = myFile.Read(buf)
	if err != nil {
		return
	}

	//	инитим командную строку
	InitialPromptArgs()

	//	Маршалим прочитанное в структуру
	json.Unmarshal(buf, &mySettings)

	//	Выводим сообщение
	log.Info("Процесс инициализации завершен")
	log.Infof("Текущая конфигурация: %v", *mySettings)
}
