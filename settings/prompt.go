package settings

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

var ArgsPort string
var HttpMode = false

//	Инициализация данных из командной строки
func InitialPromptArgs() {
	//	Представляемся
	fmt.Println("Вошли в initPromptArgs")
	//	Проверяем сколько аргументов в командной строке
	if len(os.Args) > 1 {
		//	Аргумента два - первый : порт подкючения
		ArgsPort = os.Args[1]
		log.Infof("Через командную строку передали порт %v", ArgsPort)

		//	Проверяем сколько аргументов в командной строке
		if len(os.Args) > 2 {
			//	Аргумента три - второй : http mode
			HttpMode = true
			log.Infof("Через командную строку передали порт %v", ArgsPort)
		}
	}
}
