package settings

//	структура одного поста
type SettingsStruct struct {
	ListeningPort int
	ServerDB      string
	PortDB        int
	UserNameDB    string
	NameDB        string
	PasswordDB    string
	SslModeDB     string
	SchemaNameDB  string
}
