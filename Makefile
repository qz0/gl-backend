OUT := backend
PKG := github.com/archi-chester/backend
DEB_TEMPLATE := backend*.deb

PL_VERSION := ${CI_PIPELINE_ID}.${SHA_VER}
ifeq ("${PL_VERSION}", ".")
	PL_VERSION:=manual
endif
VERSION := $(shell cat VERSION).${PL_VERSION}_($(shell date +%d.%m.%Y))
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/)

all: build

build: clean
	go build -i -v -o ${OUT} -ldflags="-X main.version=${VERSION}"

deb: build
	sh packaging/packaging.sh

deploy: build deb deployYandex

deployYandex:
	sh -c "scp ${DEB_TEMPLATE} greyline:Qz123456@ssh.grey-line.com:/tmp/"
	ssh greyline:Qz123456@ssh.grey-line.com "sudo dpkg -i /tmp/${DEB_TEMPLATE} && rm -f /tmp/${DEB_TEMPLATE}; sudo apt-get install -f -y"

clean:
	-@rm ${OUT} ${DEB_TEMPLATE}

