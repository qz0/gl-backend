#!/usr/bin/env bash

API_USER=backend

chown -R ${API_USER}:${API_USER} /opt/greyline
chkconfig backend on