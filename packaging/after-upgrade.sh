#!/usr/bin/env bash

API_USER=backend

chown -R ${API_USER}:${API_USER} /opt/greyline

if [ -x /etc/init.d/backend ]; then
    service backend start
fi